//package com.amindset.ve.beyond.Adapter
//
//import android.view.LayoutInflater
//import android.view.View
//import android.view.ViewGroup
//import android.widget.LinearLayout
//import android.widget.TextView
//import androidx.recyclerview.widget.LinearLayoutManager
//import androidx.recyclerview.widget.RecyclerView
//import com.amindset.ve.beyond.Model.FetchAllCalendar.Payload
//import com.amindset.ve.beyond.R
//import kotlinx.android.synthetic.main.view_header.view.*
//
//class ParentAdapter(private val parents : List<Payload>) : RecyclerView.Adapter<ParentAdapter.ViewHolder>(){
//
//    private val viewPool = RecyclerView.RecycledViewPool()
//
//    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
//        val v = LayoutInflater.from(parent.context).inflate(R.layout.view_header,parent,false)
//        return ViewHolder(v)
//    }
//
//    override fun getItemCount(): Int {
//       return parents.size
//    }
//
//    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
//        val parent = parents[position]
////        holder.textView.text = parent.date
//        holder.recyclerView.apply {
//            adapter = ChildAdapter(parent.data)
//            recycledViewPool = viewPool
//        }
//    }
//
//
//    inner class ViewHolder(itemView : View) : RecyclerView.ViewHolder(itemView){
//        val recyclerView : RecyclerView = itemView.rv_child
////        val textView:TextView = itemView.textView
//    }
//}