package com.beyond.ve.beyondapp.ui

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.os.Bundle
import android.os.Looper
import android.text.InputType
import android.text.TextUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import androidx.annotation.NonNull
import androidx.core.content.ContextCompat
import com.beyond.ve.beyondapp.BaseActivity.BaseFragment
import com.beyond.ve.beyondapp.Constant
import com.beyond.ve.beyondapp.Model.ModelSingup.ModelSignUp
import com.beyond.ve.beyondapp.Model.UpdateProfile.ModelUpdateProfile
import com.beyond.ve.beyondapp.R
import com.beyond.ve.beyondapp.Utils.CommonUtils
import com.beyond.ve.beyondapp.Utils.NetworkUtils
import com.beyond.ve.beyondapp.Web.ApiClient
import com.beyond.ve.beyondapp.Web.WebService
import com.beyond.ve.beyondapp.data.AppPreferencesHelper
import com.firebase.client.Firebase
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.*
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.FirebaseDatabase
import kotlinx.android.synthetic.main.fragment_sign_up.*
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Response
import java.util.*


private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"
private var param1: String? = null
private var param2: String? = null

lateinit var   mGoogleApiClient: GoogleApiClient
lateinit var  mLocationRequest : LocationRequest
var  isEyeOnResetPassword : Boolean = false
internal var mFusedLocationClient: FusedLocationProviderClient? = null
internal lateinit var mLocationCallback: LocationCallback
internal  var mLastLocation: Location? = null

var lat : Double = 0.0
var lng : Double = 0.0
var REQUEST_CODE_VERIFY_OTP=101
var RESULT_CODE_VERIFY_OTP=201


class Fragment_SignUp : BaseFragment() , View.OnClickListener , GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener {

    override fun onConnected(p0: Bundle?) {


        if (ContextCompat.checkSelfPermission(
                        this.activity!!,
                        Manifest.permission.ACCESS_COARSE_LOCATION
                ) == PackageManager.PERMISSION_GRANTED
        ) {
            mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this.activity!!)

            mLocationCallback = LocationCallback()

            mFusedLocationClient?.requestLocationUpdates(mLocationRequest, mLocationCallback, Looper.myLooper())


            mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                    mGoogleApiClient
            );


        }

        if (mLastLocation != null) {
            lat = mLastLocation!!.getLatitude();
            lng = mLastLocation!!.getLongitude();
        }


    }

    override fun onConnectionSuspended(p0: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onConnectionFailed(p0: ConnectionResult) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onLocationChanged(p0: Location?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }




    override fun onPause() {
        super.onPause()
        if (mGoogleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
            mGoogleApiClient.disconnect();
        }
    }

    override fun onResume() {
        super.onResume()
        mGoogleApiClient.connect()
        hideLoading()
    }

    override fun onClick(p0: View?) {
        when (p0!!.id) {
            R.id.btn_singup -> {
                validation();
            }
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }


        mGoogleApiClient = GoogleApiClient.Builder(this.activity!!)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API).build()

        mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval((10 * 1000).toLong())        // 10 seconds, in milliseconds
                .setFastestInterval((1 * 1000).toLong())
    }





    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_sign_up, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        btn_singup.setOnClickListener(this)


        tieSingup_Passsword.setOnTouchListener(View.OnTouchListener { v, event ->
            val DRAWABLE_RIGHT = 2
            if (event.action == MotionEvent.ACTION_UP) {
                if (event.rawX >= tieSingup_Passsword.getRight() - tieSingup_Passsword.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width()) {
                    if (isEyeOnResetPassword) {
                        isEyeOnResetPassword = false
                        tieSingup_Passsword.setInputType(InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD)
                        tieSingup_Passsword.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_eye_off, 0)
                    } else {
                        isEyeOnResetPassword = true
                        tieSingup_Passsword.setInputType(InputType.TYPE_CLASS_TEXT)
                        tieSingup_Passsword.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_eye_on, 0)
                    }
                    return@OnTouchListener false
                }
            }
            false
        })
    }
    override fun onStart() {
        super.onStart()
        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }
    }

    private fun validation() {

        if (!TextUtils.isEmpty(tie_firstName.text.toString())) {
            if (!TextUtils.isEmpty(tie_lastName.text.toString())) {
                if (!TextUtils.isEmpty(tie_mail.text.toString())) {
                    if (!TextUtils.isEmpty(tieSingup_Passsword.text.toString())) {
                        if (CommonUtils.isEmailValid(tie_mail.text.toString())) {

                            if(NetworkUtils.isNetworkConnected(activity)) {
                                PostSingInDataToServer(tie_firstName.text.toString(), tie_lastName.text.toString(),
                                        tie_mail.text.toString(), tieSingup_Passsword.text.toString())
                            }
                            else
                            {
                                showMessage(R.string.msg_nointerconncetion)
                            }

                        } else {
                            showSnackBar(getString(R.string.msg_enter_valid_email))
                        }
                    } else {
                        showSnackBar(getString(R.string.msg_enter_password))
                    }

                } else {
                    showSnackBar(getString(R.string.msg_enter_valid_email))
                }

            } else {
                showSnackBar(getString(R.string.msg_enter_last_name))
            }

        } else {
            showSnackBar(getString(R.string.msg_enter_first_name))
        }
    }

    private fun PostSingInDataToServer(firstName: String, lastName: String, email: String, pass: String) {
        val apiService = ApiClient.getClient().create(WebService::class.java)
        val paramObject = HashMap<String, String>()
        paramObject["first_name"] = firstName
        paramObject["last_name"] = lastName
        paramObject["email"] = email
        paramObject["password"] = pass
        paramObject["device_token"] = AppPreferencesHelper(activity).deviceToken
        paramObject["lat"] = ""+lat
        paramObject["lng"] = ""+lng
        val call = apiService.postSignUp(paramObject)
        showLoading()
        call.enqueue(object : retrofit2.Callback<ModelSignUp> {
            override fun onResponse(call: Call<ModelSignUp>, response: Response<ModelSignUp>?) {

                if (response != null) {
                    if (response.isSuccessful && (response.body().statusCode == Constant.RESPONSE_SUCCESSFULLY || response.body().statusCode == Constant.RESPONSE_SUCCESSFULLLY) ) {
                        hideLoading()
                        showSnackBar(response.body().message)
                        var appPreferencesHelper = AppPreferencesHelper(activity)
                        appPreferencesHelper.user_Name =  response.body().payload.firstName
                        appPreferencesHelper.user_LastName =  response.body().payload.lastName
                        appPreferencesHelper.user_Id =  response.body().payload.id
                        appPreferencesHelper.user_Email =  response.body().payload.email
                        appPreferencesHelper.user_XAUTH = response.headers().get("X-Auth")
                        addfirebaseUser(appPreferencesHelper.user_Id)
                    } else if (response.code() == Constant.RESPONSE_UNAUTHORIZED || response.code() == Constant.RESPONSE_FAILURE) {
                        hideLoading()

                        try {
                            val jObjError = JSONObject(response.errorBody().string())
                            showSnackBar(jObjError.getString("message"))
                        } catch (e: Exception) {
                            showSnackBar("" + e.message)

                        }


                    }
                }
            }

            override fun onFailure(call: Call<ModelSignUp>, t: Throwable) {
                // Log error here since request failed
                hideLoading()
                showSnackBar(getString(R.string.failure_please_try_again))


            }


        })
    }

    private fun addfirebaseUser(userid:Int) {
        var email =  ""+userid+"@beyond.com"
        Firebase.setAndroidContext(activity)
        val password = "12345678"
        val mAuth = FirebaseAuth.getInstance()
        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this!!.activity!!, object:OnCompleteListener<AuthResult> {
                    override fun onComplete(@NonNull task:Task<AuthResult>) {
                        if (!task.isSuccessful()){
                            Log.e("Signup Error", "onCancelled", task.getException())
                        }else{
                            val user = mAuth.getCurrentUser()
                            val uid = user!!.getUid()
                            Log.e("Fire base User id", uid)
                            var pref=AppPreferencesHelper(activity)
                            pref.userIdFirebase=uid
                            registerUser(uid,pref.user_Name+" "+pref.user_LastName,email,pref.user_avatar, userid )

                            // update chat id on server
                            postUserProfileData()
                        }
                    }
                })
    }

    private fun registerUser(fireBaseId: String, name: String, email: String, urlProfileImg: String , userId : Int) {

        var baseUrl = FirebaseDatabase.getInstance().reference.toString()
        val refUsers = Firebase(baseUrl+"/users/" + fireBaseId + "/credentials/")
        val map = HashMap<String, String>()
        map["email"] = email
        map["isOnline"] = "false"
        map["isTyping"] = "false"
        map["lastSeen"] = "0"
        map["name"] = name
        map["profilePicLink"] = urlProfileImg
        refUsers.setValue(map)

        val intent = Intent(activity, ActivityVerificationCode::class.java)
        intent.putExtra("activity","signup")
        intent.putExtra("email",tie_mail.text.toString())
        intent.putExtra("userId",""+userId)
        startActivityForResult(intent, REQUEST_CODE_VERIFY_OTP)
        activity!!.overridePendingTransition(R.anim.entry_from_right, R.anim.exit_to_left)
//        activity!!.finish()
    }

    private fun postUserProfileData() {
        val apiService = ApiClient.getClient().create(WebService::class.java)
        val paramObject = HashMap<String, String>()
        var pref = AppPreferencesHelper(activity)
        paramObject["chat_id"] = pref.userIdFirebase
        val call = apiService.postUpdateProfile(paramObject,"Bearer " + pref.user_XAUTH)
        showLoading()
        call.enqueue(object : retrofit2.Callback<ModelUpdateProfile> {
            override fun onResponse(call: Call<ModelUpdateProfile>, response: Response<ModelUpdateProfile>?) {
            }
            override fun onFailure(call: Call<ModelUpdateProfile>, t: Throwable) {
                // Log error here since request failed
                hideLoading()
                showSnackBar(getString(R.string.failure_please_try_again))
            }
        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if(requestCode==REQUEST_CODE_VERIFY_OTP){
            val isVerifyOTP= data!!.getBooleanExtra("isVerifyOTP",false)
            if(isVerifyOTP)
                activity!!.finish()
            else
                AppPreferencesHelper(activity).pref.edit().clear().commit()
        }
    }
}
