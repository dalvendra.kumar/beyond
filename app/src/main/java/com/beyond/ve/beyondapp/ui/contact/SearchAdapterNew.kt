package com.beyond.ve.beyondapp.ui.contact

import android.content.Context
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.beyond.ve.beyondapp.Model.contact.Payload
import com.beyond.ve.beyondapp.R
import com.beyond.ve.beyondapp.Utils.ImageUtils
import kotlinx.android.synthetic.main.dashboard_search_item.view.*
import java.util.*

class SearchAdapterNew(var context: Context, var restaurantList: ArrayList<Payload>, val clickListener: (Payload) -> Unit) : RecyclerView.Adapter<SearchAdapterNew.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        var view = LayoutInflater.from(parent.context).inflate(R.layout.dashboard_search_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(restaurantList.get(position), clickListener)
    }

    override fun getItemCount(): Int {
        return restaurantList.size
    }


    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(restaurant: Payload, clickListener: (Payload) -> Unit) {

            if (!TextUtils.isEmpty(restaurant.getFirst_name()))
                itemView.name.setText(restaurant.getFirst_name())

            if (!TextUtils.isEmpty(restaurant.company_name))
                itemView.msg.setText(restaurant.company_name)

            if (!TextUtils.isEmpty(restaurant.getAvatar())) {
                ImageUtils.displayImageFromUrl(context, itemView.image, restaurant.getAvatar())
            }


            if (!TextUtils.isEmpty(restaurant.isConnected)) {
                if (restaurant.isConnected.equals("normal")) {
                    itemView.setOnClickListener {
                        clickListener(restaurant)
                    }
                } else {
                    itemView.setOnClickListener(null)
                }
                if (restaurant.isConnected.equals("requested")) {
                    itemView.sttus.setText("Requested")
                } else if (restaurant.isConnected.equals("connected")) {
                    itemView.sttus.setText("Connected")
                } else if (restaurant.isConnected.equals("normal")) {
                    itemView.sttus.setText("Connect")
                }


            }

        }
    }
}