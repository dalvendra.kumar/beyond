package com.beyond.ve.beyondapp.ui
import android.Manifest
import android.app.Activity
import android.app.AlertDialog
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.view.Gravity
import android.view.View
import android.view.animation.AnimationUtils
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import com.beyond.ve.beyondapp.BaseActivity.BaseActivity
import com.beyond.ve.beyondapp.Adapter.UserMediaAdapter
import com.beyond.ve.beyondapp.Model.ModelAvtar.ModelUpdateAvtar
import com.beyond.ve.beyondapp.Model.UserProfile
import com.beyond.ve.beyondapp.R
import com.beyond.ve.beyondapp.Utils.CommonDialog
import com.beyond.ve.beyondapp.Utils.CommonUtils
import com.beyond.ve.beyondapp.Utils.ImageUtils
import com.beyond.ve.beyondapp.Web.ApiClient
import com.beyond.ve.beyondapp.Web.WebService
import com.beyond.ve.beyondapp.data.AppPreferencesHelper
import com.firebase.client.Firebase
import com.google.firebase.database.FirebaseDatabase
import com.google.gson.JsonElement
import io.reactivex.SingleObserver
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_show_profile.*
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class ShowProfileActivity : BaseActivity()  , View.OnClickListener, UserMediaAdapter.MediaListener {

    override fun onAddNewMedia() {
        imageFileList.clear()
        imageId=IMAGE_MEDIA
        selectImage()
    }

    override fun onClick(p0: View?) {
        when(p0!!.id)
        {
            R.id.iv_upload->{

                imageFileList.clear()
                imageId=IMAGE_LOGO
                selectImage()
            }

            R.id.iv_cancel->
            {
                InvisibleUploadMainScreen(cl_upload_view)
            }

            R.id.iv_setting->
            {
                val intent = Intent(this, UserSettingActivity::class.java)
                startActivity(intent)
            }

            R.id.tv_settings->
            {
                val intent = Intent(this, UserSettingActivity::class.java)
                startActivity(intent)
            }
            R.id.back_showPrfileActivity->{
                finish()
            }

            R.id.profile_image_show_profile->{
                imageFileList.clear()
                imageId=IMAGE_PROFILE
                selectImage()
            }
            R.id.iv_insertLinkcancel->{
                InvisibleUploadMainScreen(cl_insert_view)
            }
            R.id.btn_savefield->{
                if(isValidInput())
                    updateProfileDetails()
            }

            R.id.tv_lebel_bio->{
                if(edtBio.visibility==View.GONE) {
                    edtBio.visibility = View.VISIBLE
                    edtBio.requestFocus()
                }else
                    edtBio.visibility=View.GONE
            }
            R.id.tv_lebel_field->{
                if(edtField.visibility==View.GONE) {
                    edtField.visibility = View.VISIBLE
                    edtField.requestFocus()
                }else
                    edtField.visibility=View.GONE
            }
            R.id.tv_industry_field->{
                if(edtIndustry.visibility==View.GONE) {
                    edtIndustry.visibility = View.VISIBLE
                    edtIndustry.requestFocus()
                } else
                    edtIndustry.visibility=View.GONE
            }
            R.id.tv_country_field-> {
                if (edtCountry.visibility == View.GONE){
                    edtCountry.visibility = View.VISIBLE
                    edtCountry.requestFocus()
                }else
                    edtCountry.visibility=View.GONE
            }
            R.id.tv_citystate_field->{
                if(edtState.visibility==View.GONE){
                    edtState.visibility=View.VISIBLE
                    edtWebsite.requestFocus()
                }else
                    edtState.visibility=View.GONE
            }

            R.id.tv_website_field->{
                if(edtWebsite.visibility==View.GONE) {
                    edtWebsite.visibility = View.VISIBLE
                    edtWebsite.requestFocus()
                }else
                    edtWebsite.visibility=View.GONE
            }

            R.id.iv_ChatorEdit->{
                if(bolUpdats) {
                    toggleEdit(true)
                    iv_ChatorEdit.setImageDrawable(getResources().getDrawable(R.drawable.ic_close_white, application.theme));
                    bolUpdats=false
                    iv_setting.visibility = View.GONE
                    tv_settings.visibility = View.GONE
                    btn_savefield.visibility = View.VISIBLE
                }else{
                    toggleEdit(false)
                    iv_setting.visibility = View.VISIBLE
                    tv_settings.visibility = View.VISIBLE
                    btn_savefield.visibility = View.GONE
                    bolUpdats=true
                    iv_ChatorEdit.setImageDrawable(getResources().getDrawable(R.drawable.ic_edit, application.theme))
                }
            }

            R.id.tv_insertImageLink->{
                InvisibleUploadMainScreen(cl_upload_view)
                val slideUp = AnimationUtils.loadAnimation(applicationContext, R.anim.slide_up)
                if (cl_insert_view.getVisibility() === View.INVISIBLE) {
                    cl_insert_view.startAnimation(slideUp)
                    cl_insert_view.setVisibility(View.VISIBLE)
                    cl_insert_view.alpha = 1.0F
                }
            }
            R.id.txtAvailiable->{
                CommonDialog.showAvailiabilityOpt(this,profile!!.payload, Runnable { updateStatus(profile!!.payload.isActive) })
            }
        }
    }

    var bolUpdats = true
    var isAddMedia= false
    var profile: UserProfile? = null
    var adapter:UserMediaAdapter?=null
    var medias =ArrayList<UserProfile.Media>()
    var REQUEST_CAMERA=201
    var REQUEST_BROWSE=202
    var IMAGE_PROFILE=101
    var IMAGE_LOGO=102
    var IMAGE_MEDIA=103
    var imageId=0;
    internal  var photoFile: File?=null
    internal  var photoURI: Uri?=null
    private val imageFileList = ArrayList<File>()

    private fun InvisibleUploadMainScreen(cl_insert_view: ConstraintLayout) {

        val slideDown = AnimationUtils.loadAnimation(applicationContext, R.anim.slide_down)

        if (cl_insert_view.getVisibility() === View.VISIBLE) {
            cl_insert_view.startAnimation(slideDown)
            cl_insert_view.setVisibility(View.INVISIBLE)
            cl_insert_view.alpha = 1.0F
        }
    }


    private lateinit var refUserCred: Firebase

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        getSupportActionBar()!!.hide();
        setContentView(R.layout.activity_show_profile)

        iv_upload.setOnClickListener(this)

        iv_cancel.setOnClickListener(this)

        tv_insertImageLink.setOnClickListener(this)

        iv_insertLinkcancel.setOnClickListener(this)

        back_showPrfileActivity.setOnClickListener(this)
        tv_website_field.setOnClickListener(this)
        tv_lebel_bio.setOnClickListener(this)
        tv_lebel_field.setOnClickListener(this)
        tv_industry_field.setOnClickListener(this)
        tv_country_field.setOnClickListener(this)
        tv_citystate_field.setOnClickListener(this)

        tv_settings.setOnClickListener(this)
        iv_setting.setOnClickListener(this)
        iv_ChatorEdit.setOnClickListener(this)
        btn_savefield.setOnClickListener(this)
        txtAvailiable.setOnClickListener(this)
        profile_image_show_profile.setOnClickListener(this)

        iv_setting.visibility = View.VISIBLE
        tv_settings.visibility = View.VISIBLE

        toggleEdit(false)
        lytBar.visibility = View.VISIBLE
        getUserDetails()

        val baseUrl = FirebaseDatabase.getInstance().reference.toString()
        refUserCred= Firebase(baseUrl + "/users/" + AppPreferencesHelper(this).userIdFirebase + "/credentials")
    }

    private fun getUserDetails() {
        val service = ApiClient.getClient().create(WebService::class.java)
        val call = service.getUser("","Bearer " + AppPreferencesHelper(this).user_XAUTH)
        call.enqueue(object : Callback<UserProfile> {
            override fun onResponse(call: Call<UserProfile>, response: Response<UserProfile>) {
                lytBar.visibility = View.GONE
                if (response.isSuccessful) {
                    if(isAddMedia) {
                        medias.clear()
                        medias.addAll(response.body()!!.payload.media)
                        isAddMedia=false
//                        var manager= LinearLayoutManager(applicationContext,LinearLayoutManager.HORIZONTAL,false)
//                        adapter=UserMediaAdapter(medias,this@ShowProfileActivity,false,CommonUtils.returmWindowWidth(this@ShowProfileActivity))
//                        rv_media_list.layoutManager=manager
//                        rv_media_list.itemAnimator=DefaultItemAnimator()
//                        rv_media_list.adapter=adapter
                        adapter!!.notifyDataSetChanged()
                    }else {
                        profile = response.body()
                        setUserData(profile!!.payload)
                    }
                } else {
                    try {
                        val jObjError = JSONObject(response.errorBody().string())
                        showSnackBar(jObjError.getString("message"))
                    } catch (e: Exception) {
                        showSnackBar("" + e.message)
                    }

                }
            }

            override fun onFailure(call: Call<UserProfile>, t: Throwable) {
                t.printStackTrace()
                lytBar.visibility = View.GONE
            }
        })
    }

    private fun isValidInput(): Boolean {
        var fullName=tv_fullName.text.toString().trim()
        if(fullName.length<4 || fullName.indexOf(" ")<0 ){
            tv_fullName.setError("")
            return false
        } else if(tv_changedPosition.text.toString().length<4){
            tv_changedPosition.setError("")
            return false
        }  else if(tv_compName.text.toString().length<4){
            tv_compName.setError("")
            return false
        }  else if(tv_contactNumber.text.toString().length<4){
            tv_contactNumber.setError("")
            return false
        } else if(!CommonUtils.isEmailValid(tv_useremail.text.toString())){
            tv_useremail.setError("")
            return false
        } else if(edtBio.visibility==View.VISIBLE && edtBio.text.toString().length<4){
            edtBio.setError("")
            return false
        } else if(edtField.visibility==View.VISIBLE && edtField.text.toString().length<4){
            edtField.setError("")
            return false
        } else if(edtIndustry.visibility==View.VISIBLE && edtIndustry.text.toString().length<4){
            edtIndustry.setError("")
            return false
        } else if(edtWebsite.visibility==View.VISIBLE && edtWebsite.text.toString().length<4){
            edtWebsite.setError("")
            return false
        } else if(edtCountry.visibility==View.VISIBLE && edtCountry.text.toString().length<4){
            edtCountry.setError("")
            return false
        } else if(edtState.visibility==View.VISIBLE && edtState.text.toString().length<4){
            edtState.setError("")
            return false
        }
        return true
    }

    private fun updateStatus(status :Int) {

        if(status==1){
            txtAvailiable.setText("Available")
            txtAvailiable.setCompoundDrawablesWithIntrinsicBounds(R.drawable.indi_available,0,0,0);
        }else if(status==2){
            txtAvailiable.setText("Busy")
            txtAvailiable.setCompoundDrawablesWithIntrinsicBounds(R.drawable.indi_busy,0,0,0);
        }else if(status==3){
            txtAvailiable.setText("Invisible")
            txtAvailiable.setCompoundDrawablesWithIntrinsicBounds(R.drawable.indi_invisible,0,0,0);
        }

        var body=HashMap<String,String>()
        body.put("isActive",status.toString())
        body.put("device_token",AppPreferencesHelper(this@ShowProfileActivity).deviceToken)
        val service = ApiClient.getClient().create(WebService::class.java)
        val call = service.updateProfile("Bearer " + AppPreferencesHelper(this).user_XAUTH,body)
        call.enqueue(object : Callback<JsonElement> {
            override fun onResponse(call: Call<JsonElement>, response: Response<JsonElement>) {
                if (!response.isSuccessful){
                    try {
                        val jObjError = JSONObject(response.errorBody().string())
                        showSnackBar(jObjError.getString("message"))
                    } catch (e: Exception) {
                        showSnackBar("" + e.message)
                    }
                }
            }
            override fun onFailure(call: Call<JsonElement>, t: Throwable) {
                t.printStackTrace()
            }
        })
    }


    private fun updateProfileDetails() {
        var body=HashMap<String,String>()
        var fullName=tv_fullName.text.toString().trim()
        var index =fullName.indexOf(" ")
        body.put("first_name",fullName.substring(0,index))
        body.put("last_name",fullName.substring(index+1,fullName.length))
        body.put("position",tv_changedPosition.text.toString())
        body.put("company_name",tv_compName.text.toString())
        body.put("mobile",tv_contactNumber.text.toString())
        body.put("email",tv_useremail.text.toString())
        body.put("bio",edtBio.text.toString())
        body.put("field",edtField.text.toString())
        body.put("industry",edtIndustry.text.toString())
        body.put("website",edtWebsite.text.toString())
        body.put("country",edtCountry.text.toString())
        body.put("state",edtState.text.toString())
        body.put("chat_id",AppPreferencesHelper(this).userIdFirebase)
        body.put("device_token",AppPreferencesHelper(this@ShowProfileActivity).deviceToken)
        val service = ApiClient.getClient().create(WebService::class.java)

        val call = service.updateProfile("Bearer " + AppPreferencesHelper(this).user_XAUTH,body)
        call.enqueue(object : Callback<JsonElement> {
            override fun onResponse(call: Call<JsonElement>, response: Response<JsonElement>) {
                if (response.isSuccessful) {
                    toggleEdit(false)

                    tv_lebel_bio.setText(edtBio.text.toString())
                    tv_lebel_field.setText(edtField.text.toString())
                    tv_industry_field.setText(edtIndustry.text.toString())
                    tv_website_field.setText(edtWebsite.text.toString())
                    tv_country_field.setText(edtCountry.text.toString())
                    tv_citystate_field.setText(edtState.text.toString())

                    iv_setting.visibility = View.VISIBLE
                    tv_settings.visibility = View.VISIBLE
                    btn_savefield.visibility = View.GONE
                    bolUpdats=true
                    iv_ChatorEdit.setImageDrawable(getResources().getDrawable(R.drawable.ic_edit, application.theme))
                    refUserCred.child("name").setValue(fullName)
                    showSnackBar("Successfully updated user profile.")
                } else {
                    try {
                        val jObjError = JSONObject(response.errorBody().string())
                        showSnackBar(jObjError.getString("message"))
                    } catch (e: Exception) {
                        showSnackBar("" + e.message)
                    }

                }
            }

            override fun onFailure(call: Call<JsonElement>, t: Throwable) {
                t.printStackTrace()
            }
        })
    }

    private fun setUserData(payload: UserProfile.Payload?) {
        if (payload != null) {
            tv_compName.setText(payload.company_name)
            tv_fullName.setText(payload.first_name+" "+payload.last_name)
            tv_changedPosition.setText(payload.position)
            tv_contactNumber.setText(payload.mobile)
            tv_useremail.setText(payload.email)
            tv_lebel_bio.setText(payload.bio)
            tv_lebel_field.setText(payload.field)
            tv_industry_field.setText(payload.industry)

            edtBio.setText(payload.bio)
            edtField.setText(payload.field)
            edtIndustry.setText(payload.industry)
            edtWebsite.setText(payload.website)
            edtCountry.setText(payload.country)
            edtState.setText(payload.state)
            tv_country_field.setText(payload.company_name)
            tv_citystate_field.setText(payload.state)
            medias.addAll(payload.media)
            var manager= LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false)
            adapter=UserMediaAdapter(medias,this,false,CommonUtils.returmWindowWidth(this))
            rv_media_list.layoutManager=manager
            rv_media_list.itemAnimator=DefaultItemAnimator()
            rv_media_list.adapter=adapter

            ImageUtils.displayImageFromUrl(this,iv_upload,payload.company_logo,R.mipmap.ic_logo_placeholder_profile)
            ImageUtils.displayImageFromUrl(this,findViewById(R.id.profile_image_show_profile),payload.avatar,R.mipmap.ic_user_placeholdr)
            refUserCred.child("profilePicLink").setValue(payload.avatar)

            if(payload.linkedIn_id!=null)
                txtHintLinkedIn.setText("Linked")
            else
                txtHintLinkedIn.setText("Link")


            if(payload.website.length>1)
                txtLinkWebsite.setText("Linked")
            else
                txtLinkWebsite.setText("Link")

            if(payload.isActive==1){
                txtAvailiable.setText("Available")
                txtAvailiable.setCompoundDrawablesWithIntrinsicBounds(R.drawable.indi_available,0,0,0);
            }else if(payload.isActive==2){
                txtAvailiable.setText("Busy")
                txtAvailiable.setCompoundDrawablesWithIntrinsicBounds(R.drawable.indi_busy,0,0,0);
            }else if(payload.isActive==3){
                txtAvailiable.setText("Invisible")
                txtAvailiable.setCompoundDrawablesWithIntrinsicBounds(R.drawable.indi_invisible,0,0,0);
            }

        }
    }

    private fun toggleEdit(isEditableViews: Boolean){
        tv_compName.isEnabled=isEditableViews
        tv_fullName.isEnabled=isEditableViews
        tv_changedPosition.isEnabled=isEditableViews
        tv_contactNumber.isEnabled=isEditableViews
        tv_useremail.isEnabled=isEditableViews
        adapter?.setEditMode(isEditableViews)
        adapter?.notifyDataSetChanged()
        if(isEditableViews){
            tv_website_field.setOnClickListener(this)
            tv_lebel_bio.setOnClickListener(this)
            tv_lebel_field.setOnClickListener(this)
            tv_industry_field.setOnClickListener(this)
            tv_country_field.setOnClickListener(this)
            tv_citystate_field.setOnClickListener(this)
            tv_lebel_bio.requestFocus()
        }else {
            tv_website_field.setOnClickListener(null)
            tv_lebel_bio.setOnClickListener(null)
            tv_lebel_field.setOnClickListener(null)
            tv_industry_field.setOnClickListener(null)
            tv_country_field.setOnClickListener(null)
            tv_citystate_field.setOnClickListener(null)
            edtBio.visibility = View.GONE
            edtField.visibility = View.GONE
            edtIndustry.visibility = View.GONE
            edtCountry.visibility = View.GONE
            edtState.visibility = View.GONE
            edtWebsite.visibility = View.GONE
        }
    }


    fun selectImage() {
        val items = arrayOf<CharSequence>("Take Photo", "Choose from Library", "Cancel")
        val title = TextView(this)
        title.text = "Add Photo!"
        title.setBackgroundColor(Color.BLACK)
        title.setPadding(10, 15, 15, 10)
        title.gravity = Gravity.CENTER
        title.setTextColor(Color.WHITE)
        title.textSize = 22f


        val builder = AlertDialog.Builder(this)
        builder.setCustomTitle(title)

        // builder.setTitle("Add Photo!");
        builder.setItems(items) { dialog, item ->
            if (items[item] == "Take Photo") {
                if (ContextCompat.checkSelfPermission(this!!, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(this!!,arrayOf(Manifest.permission.CAMERA),0)
                } else {
                    captureProfilePhoto()
                }
            } else if (items[item] == "Choose from Library") {
                if (this?.let {ContextCompat.checkSelfPermission(it,Manifest.permission.READ_EXTERNAL_STORAGE)} != PackageManager.PERMISSION_GRANTED) {
                    if (ActivityCompat.shouldShowRequestPermissionRationale(this!!,Manifest.permission.READ_EXTERNAL_STORAGE)) {

                    } else {
                        ActivityCompat.requestPermissions(this!!,arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE),0)
                    }
                }
                if (ContextCompat.checkSelfPermission(this!!,android.Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    if (ActivityCompat.shouldShowRequestPermissionRationale(this!!,android.Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

                    } else {
                        ActivityCompat.requestPermissions(this!!,arrayOf(android.Manifest.permission.WRITE_EXTERNAL_STORAGE),0)
                    }
                } else {
                    browseProfilePhoto()
                }

            } else if (items[item] == "Cancel") {
                dialog.dismiss()
            }
        }
        builder.show()
    }


    private fun browseProfilePhoto() {
        val intent = Intent()
        intent.type = "image/*"
        intent.action = Intent.ACTION_GET_CONTENT
        startActivityForResult(Intent.createChooser(intent, "Select picture"), REQUEST_BROWSE)
    }



    private fun captureProfilePhoto() {
        val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        photoFile
        try {
            photoFile = createImageFile()
        } catch (ex: IOException) {
            ex.printStackTrace()
        }
        // Continue only if the File was successfully created
        if (photoFile != null) {
            photoURI = FileProvider.getUriForFile(this!!,getApplicationContext().getPackageName() + ".provider",photoFile!!)
            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
            takePictureIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION or Intent.FLAG_GRANT_WRITE_URI_PERMISSION)
            takePictureIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            startActivityForResult(takePictureIntent, REQUEST_CAMERA )

        }
    }
    lateinit var  imageFilePath : String ;

    @Throws(IOException::class)
    private fun createImageFile(): File? {
        val timeStamp =SimpleDateFormat("yyyyMMdd_HHmmss",Locale.getDefault()).format( Date());
        val imageFileName = "IMG_" + timeStamp + "_";
        val storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        val image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        imageFilePath = image.getAbsolutePath();
        return image;

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when(requestCode){
            REQUEST_BROWSE->{
                if (resultCode == Activity.RESULT_OK) {
                    if (data != null) {
                        handleGalleryIntent(data)
                    }
                    else {
                        showMessage("Fail to upload image ")
                    }
                }
            }
            REQUEST_CAMERA->
            {
                if (resultCode == Activity.RESULT_OK) {
                    grabImage(data)
                }
            }
        }

    }

    fun grabImage( data: Intent?) {

        contentResolver.notifyChange(photoURI, null)
        val cr = contentResolver
        val bitmap: Bitmap
        try {

            bitmap =   MediaStore.Images.Media.getBitmap(getContentResolver(),
                    FileProvider.getUriForFile(this,getApplicationContext().getPackageName() + ".provider",  File(imageFilePath)))

            val uri = ActivityProfile.getImageUri(applicationContext, bitmap)
            val file = File(ActivityProfile.getRealPathFromURI(applicationContext, uri))
            imageFileList.add(file)


            if(imageId==IMAGE_PROFILE)
                profile_image_show_profile.setImageBitmap(bitmap)
            else if(imageId==IMAGE_MEDIA){

            }else if(imageId==IMAGE_LOGO)
                iv_upload.setImageBitmap(bitmap)

            HitServerToUpdateAvtar()

        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    private fun handleGalleryIntent(data: Intent) {
        try {
            val selectedImageUri = data.data
            val file = File(ActivityProfile.getRealPathForGalley(this, selectedImageUri))
            imageFileList.add(file)
            val imageBitmap = MediaStore.Images.Media.getBitmap(this!!.contentResolver, selectedImageUri)

            if(imageId==IMAGE_PROFILE)
                profile_image_show_profile.setImageBitmap(imageBitmap)
            else if(imageId==IMAGE_MEDIA){

            }else if(imageId==IMAGE_LOGO)
                iv_upload.setImageBitmap(imageBitmap)

            HitServerToUpdateAvtar()

        } catch (e: IOException) {
            e.printStackTrace()
        }

    }

    private fun HitServerToUpdateAvtar() {
        val a = ArrayList<MultipartBody.Part?>()
        var app = AppPreferencesHelper(this)
        showLoading()
        val apiService = ApiClient.getClient().create(WebService::class.java)
        if (imageFileList.size > 0) {
            for (i in 0..imageFileList.size-1) {
                val requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), imageFileList.get(i))
                var body : MultipartBody.Part?=null

                if(imageId==IMAGE_PROFILE)
                    body = MultipartBody.Part.createFormData("avatar",imageFileList.get(i)!!.getName(), requestFile)
                else if(imageId==IMAGE_MEDIA)
                    body = MultipartBody.Part.createFormData("media",imageFileList.get(i)!!.getName(), requestFile)
                else if(imageId==IMAGE_LOGO)
                    body = MultipartBody.Part.createFormData("company_logo",imageFileList.get(i)!!.getName(), requestFile)

                a.add(body)

            }
        }
        if(imageId==IMAGE_PROFILE)
            apiService.updateCounselorAvtar(a , "Bearer "+app.user_XAUTH).subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(object : SingleObserver<ModelUpdateAvtar> {
                        override fun onSubscribe(d: Disposable) {

                        }
                        override fun onSuccess(updateAvtar: ModelUpdateAvtar) {
                            a.clear()
                            hideLoading()
                            showMessage(updateAvtar.message)
//                            refUserCred.child("profilePicLink").setValue(url)
                            hideLoading()
                        }

                        override fun onError(e: Throwable) {
                            a.clear()
                            hideLoading()
                        }
                    })
        else if(imageId==IMAGE_MEDIA)
            CommonDialog.showAreYouSureAlert(this,"Are You sure you want to add Media ?", Runnable {
                apiService.addMedia(a , "Bearer "+app.user_XAUTH).subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(object : SingleObserver<JsonElement> {
                            override fun onSubscribe(d: Disposable) {

                            }
                            override fun onSuccess(json: JsonElement) {
                                a.clear()
                                hideLoading()
                                showMessage(json.asJsonObject.get("message").asString)
                                hideLoading()
                                isAddMedia=true
                                getUserDetails()
                            }

                            override fun onError(e: Throwable) {
                                a.clear()
                                hideLoading()
//                        handleError(e)
                            }
                        })
            })
        else if(imageId==IMAGE_LOGO)
            apiService.updateCompLogo(a , "Bearer "+app.user_XAUTH).subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(object : SingleObserver<JsonElement> {
                        override fun onSubscribe(d: Disposable) {

                        }
                        override fun onSuccess(json: JsonElement) {
                            a.clear()
                            hideLoading()
                            showMessage(json.asJsonObject.get("message").asString)
                            hideLoading()
                        }

                        override fun onError(e: Throwable) {
                            a.clear()
                            hideLoading()
//                        handleError(e)
                        }
                    })

    }
}
