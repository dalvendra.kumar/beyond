package com.beyond.ve.beyondapp.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.beyond.ve.beyondapp.R

class ActivitySignIn : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        getSupportActionBar()!!.hide();
        setContentView(R.layout.fragment_sign_in)
    }
}
