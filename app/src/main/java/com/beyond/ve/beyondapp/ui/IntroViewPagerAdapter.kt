package com.beyond.ve.beyondapp.ui

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter

class IntroViewPagerAdapter(supportFragmentManager: FragmentManager) : FragmentStatePagerAdapter(supportFragmentManager)
{
 override fun getItem(position: Int): Fragment {
  return if (position == 0) {
   DashboardFragment()
  } else

   UpdatesContactFragment()
 }
 override fun getCount(): Int {
 return 2
 }
}