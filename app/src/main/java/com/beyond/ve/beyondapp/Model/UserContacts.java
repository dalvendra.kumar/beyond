package com.beyond.ve.beyondapp.Model;

import java.util.ArrayList;

public class UserContacts {

    private int status,statusCode;
    private String message,description;
    private Payload payload;

    public Payload getPayload() {
        return payload;
    }

    public void setPayload(Payload payload) {
        this.payload = payload;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    public class Payload {
        private ArrayList<Contact> requests;
        private ArrayList<Contact> contacts;
        private ArrayList<Contact> local;

        public ArrayList<Contact> getRequests() {
            return requests;
        }

        public void setRequests(ArrayList<Contact> requests) {
            this.requests = requests;
        }

        public ArrayList<Contact> getContacts() {
            return contacts;
        }

        public void setContacts(ArrayList<Contact> contacts) {
            this.contacts = contacts;
        }

        public ArrayList<Contact> getLocal() {
            return local;
        }

        public void setLocal(ArrayList<Contact> local) {
            this.local = local;
        }
    }
}
