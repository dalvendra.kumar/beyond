package com.beyond.ve.beyondapp.Adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.beyond.ve.beyondapp.Model.Movie;
import com.beyond.ve.beyondapp.R;
import com.wajahatkarim3.easyflipview.EasyFlipView;

import java.util.List;

import androidx.recyclerview.widget.RecyclerView;

public class ProfileMediaAdapter extends RecyclerView.Adapter<ProfileMediaAdapter.MyViewHolder> {

    private List<Movie> moviesList;
    private ItemClickListener mClickListener;

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView tv_meetingwith, year, genre;
        EasyFlipView myEasyFlipView;
        public MyViewHolder(View view) {
            super(view);

//            myEasyFlipView = itemView.findViewById(R.id.flipView);
            view.setOnClickListener(this);
//            tv_meetingwith = (TextView) view.findViewById(R.id.tv_meetingwith);
//            genre = (TextView) view.findViewById(R.id.genre);
//            year = (TextView) view.findViewById(R.id.year);
        }
        @Override
        public void onClick(View view) {
//            myEasyFlipView.flipTheView();
//            myEasyFlipView.setFlipDuration(1000);
            if (mClickListener != null) {
                mClickListener.onItemClick(view, getAdapterPosition());
            }
        }
    }


    public ProfileMediaAdapter(List<Movie> moviesList) {
        this.moviesList = moviesList;
    }
 
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_recent_meeting, parent, false);
 
        return new MyViewHolder(itemView);
    }
 
    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

//        holder.tv_meetingwith.setText(""+holder.getAdapterPosition());
//        Movie movie = moviesList.get(position);
//        holder.title.setText(movie.getTitle());
//        holder.genre.setText(movie.getGenre());
//        holder.year.setText(movie.getYear());
    }
 
    @Override
    public int getItemCount() {
        return moviesList.size();
    }

    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }

    void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }
}