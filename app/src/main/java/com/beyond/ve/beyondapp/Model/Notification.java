package com.beyond.ve.beyondapp.Model;

public class Notification {
    private int id,user_id,assigned_to,is_seen,target_id;
    private String description,title,target_type,created_at,updated_at,first_name,last_name,avatar;

    public int getId() {
        return id;
    }

    public int getUser_id() {
        return user_id;
    }

    public int getAssigned_to() {
        return assigned_to;
    }

    public int getIs_seen() {
        return is_seen;
    }

    public int getTarget_id() {
        return target_id;
    }

    public String getDescription() {
        return description;
    }

    public String getTitle() {
        return title;
    }

    public String getTarget_type() {
        return target_type;
    }

    public String getCreated_at() {
        return created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public String getFirst_name() {
        return first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public String getAvatar() {
        return avatar;
    }

    @Override
    public String toString() {
        return "Notification{" +
                "id=" + id +
                ", user_id=" + user_id +
                ", assigned_to=" + assigned_to +
                ", is_seen=" + is_seen +
                ", target_id=" + target_id +
                ", description='" + description + '\'' +
                ", title='" + title + '\'' +
                ", target_type='" + target_type + '\'' +
                ", created_at='" + created_at + '\'' +
                ", updated_at='" + updated_at + '\'' +
                ", first_name='" + first_name + '\'' +
                ", last_name='" + last_name + '\'' +
                ", avatar='" + avatar + '\'' +
                '}';
    }
}
