package com.beyond.ve.beyondapp.ui

import android.location.Location
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.beyond.ve.beyondapp.Constant
import com.beyond.ve.beyondapp.Model.LinkedinResponse
import com.beyond.ve.beyondapp.Model.ModelCreateCalendar
import com.beyond.ve.beyondapp.Model.ModelSingup.LinkedinSignInRegRequest
import com.beyond.ve.beyondapp.R
import com.beyond.ve.beyondapp.Web.ApiClient
import com.beyond.ve.beyondapp.Web.WebService
import com.beyond.ve.beyondapp.data.AppPreferencesHelper
import com.beyond.ve.beyondapp.location.ConstantsValues
import com.beyond.ve.beyondapp.location.LocationDetector
import com.beyond.ve.beyondapp.location.LocationListenerActivity
import com.beyond.ve.beyondapp.location.LocationUtils
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.iid.FirebaseInstanceId
import com.omralcorut.linkedinsignin.Linkedin
import com.omralcorut.linkedinsignin.LinkedinLoginViewResponseListener
import com.omralcorut.linkedinsignin.model.LinkedinToken
import kotlinx.android.synthetic.main.activity_signin_signup.*
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class ActivitySignupSignin : LocationListenerActivity() , View.OnClickListener{

    var locationUtils: LocationUtils? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        getSupportActionBar()!!.hide();

        setContentView(R.layout.activity_signin_signup)

        btn_signup_singin.setOnClickListener(this)
        cv_linkedin.setOnClickListener(this)
        txt_haveAccount.setOnClickListener(this)
        listenLocation(object:LocationDetector() {

            override fun OnLocationChange(location: Location) {
                /* if (selectedLocation != null) {
            setMap(new LatLng(selectedLocation.getLatitude(), selectedLocation.getLongitude()));
            }*/

            }
            override fun OnLastLocationFound(location:Location) {
//                currentLocation = location
                /* LatLng curentLatLong = new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude());
            setMap(curentLatLong);*/
            }
            override fun onErrors(code:Int) {
                when (code) {
                    ConstantsValues.GPS_DISABLED -> Log.e("ERROR->", "GPS DISABLED")
                    ConstantsValues.NO_NETWORK -> Log.e("ERROR->", "NO_NETWORK")
                    ConstantsValues.PERMISSION_REJECTED -> Log.e("ERROR->", "PERMISSION_REJECTED")
                    ConstantsValues.PLAYSERICE_ERROR -> Log.e("ERROR->", "PLAYSERICE_ERROR")
                    ConstantsValues.GOOGLE_PLAY_CONNECTION_ERROR -> Log.e("ERROR->", "GOOGLE_PLAY_CONNECTION_ERROR")
                    ConstantsValues.GOOGLE_PLAY_CONNECTION_SUSPEND -> Log.e("ERROR->", "GOOGLE_PLAY_CONNECTION_SUSPEND")
                    ConstantsValues.NO_LAST_LOCATION_FOUND -> Log.e("ERROR->", "NO_LAST_LOCATION_FOUND")
                    ConstantsValues.PERMISSION_RATIONALE -> Log.e("ERROR->", "PERMISSION_RATIONALE")
                }
            }
        })
        var fragment:Fragment?
        var isPrevSignIn =intent.getBooleanExtra("isPrevSignIn",false)

        if(isPrevSignIn) {
            fragment = FragmentPrevSingIn()
            txt_haveAccount.setText("Some one else?")
        }else
            fragment = Fragment_Signin()


        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.flContent, fragment, "")
                .disallowAddToBackStack()
                .commit()
    }
    override fun onClick(id: View?) {
        when(id!!.id)
        {
            R.id.btn_signup_singin->{
                if(btn_signup_singin.text.equals("SIGN UP")){
                    AppPreferencesHelper(applicationContext).clearPrefe()
                    btn_signup_singin.text ="SIGN IN"
                    txt_haveAccount.setText("Have an account?")
                    var fragmentSignUp : Fragment_SignUp = Fragment_SignUp()
                    getSupportFragmentManager()
                            .beginTransaction().setCustomAnimations(R.anim.entry_from_right, R.anim.exit_to_left)
                            .replace(R.id.flContent, fragmentSignUp, "")
                            .disallowAddToBackStack()
                            .commit()
                }else
                {
                    btn_signup_singin.text ="SIGN UP"
                    txt_haveAccount.setText("Don't have an account?")
                    var fragmentSignIn = Fragment_Signin()
                    getSupportFragmentManager()
                            .beginTransaction().setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right)
                            .replace(R.id.flContent, fragmentSignIn, "")
                            .disallowAddToBackStack()
                            .commit()
                }

            }

            R.id.cv_linkedin -> {
                loginLinkedin()
            }

            R.id.txt_haveAccount -> {
                if(txt_haveAccount.text.equals("Some one else?")){
                    btn_signup_singin.text ="SIGN UP"
                    txt_haveAccount.setText("Don't have an account?")
                    AppPreferencesHelper(applicationContext).clearPrefe()
                    var fragmentSignIn = Fragment_Signin()
                    getSupportFragmentManager()
                            .beginTransaction().setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right)
                            .replace(R.id.flContent, fragmentSignIn, "")
                            .disallowAddToBackStack()
                            .commit()
                }
            }
        }
    }


    fun loginLinkedin() {
        Linkedin.login(this, object : LinkedinLoginViewResponseListener {
            override fun linkedinDidLoggedIn(linkedinToken: LinkedinToken) {
                Log.e("linkedinDidSuccess", linkedinToken.toString())
                // Success
                getUserInfo(linkedinToken?.accessToken!!)

            }

            override fun linkedinLoginDidFail(error: String) {
                // Fail
                Log.e("linkedinDidFail", error)
            }
        })
    }
    override fun onResume() {
        super.onResume()
        hideLoading()
        if (AppPreferencesHelper(applicationContext).deviceToken .equals(""))
            FirebaseInstanceId.getInstance().instanceId
                    .addOnCompleteListener(OnCompleteListener { task ->
                        if (!task.isSuccessful) {
                            Log.w("FirebaseInstanceId", "getInstanceId failed", task.exception)
                            return@OnCompleteListener
                        }
                        // Get new Instance ID token
                        var token=task.result?.token
                            AppPreferencesHelper(applicationContext).deviceToken=token
                    })
    }
    private fun getUserInfo(token: String) {
        val apiService = ApiClient.getClientLinkedin().create(WebService::class.java)
        apiService.getLinkedinUserProfile(token).enqueue(object : Callback<LinkedinResponse> {
            override fun onResponse(call: Call<LinkedinResponse>, response: Response<LinkedinResponse>) {
                Log.e("onDataResponse==", response.body().toString())
                if (response != null) {
//                    if (response.isSuccessful && (response.body().status == Constant.RESPONSE_SUCCESSFULLY || response.body().statusCode == Constant.RESPONSE_SUCCESSFULLLY)) {
                    hideLoading()
                    linkedinLogin(response)

//                    } else if (response.code() == Constant.RESPONSE_UNAUTHORIZED || response.code() == Constant.RESPONSE_FAILURE) {
//                        hideLoading()
//                        try {
//                            val jObjError = JSONObject(response.errorBody().string())
//                            showSnackBar(jObjError.getString("message"))
//                        } catch (e: Exception) {
//                            showSnackBar("" + e.message)
//                        }
//                    }
                }
            }

            override fun onFailure(call: Call<LinkedinResponse>, t: Throwable) {
                Log.e("onDataFailure==", t.toString())
            }
        })


    }

    private fun linkedinLogin(response: Response<LinkedinResponse>) {
        val apiService = ApiClient.getClient().create(WebService::class.java)
        var abc = LinkedinSignInRegRequest()
        abc.first_name = response.body().localizedFirstName
        abc.last_name = response.body().localizedLastName
        abc.lat = 28.5355
        abc.lng = 77.3910
        abc.linkedIn_id = response.body().id
        val call = apiService.linkedinSignInRegister(abc, "Bearer " + AppPreferencesHelper(this).user_XAUTH)
        showLoading()
        call.enqueue(object : retrofit2.Callback<ModelCreateCalendar> {
            override fun onResponse(call: Call<ModelCreateCalendar>, response: Response<ModelCreateCalendar>?) {
                Log.e("onResponse",response.toString())
                if (response != null) {
                    if (response.isSuccessful && (response.body().statusCode == Constant.RESPONSE_SUCCESSFULLY ||
                                    response.body().statusCode == Constant.RESPONSE_SUCCESSFULLLY)) {
                        hideLoading()
                        showSnackBar(response.body().message)
                    } else if (response.code() == Constant.RESPONSE_UNAUTHORIZED ||
                            response.code() == Constant.RESPONSE_FAILURE) {
                        hideLoading()
                        try {
                            val jObjError = JSONObject(response.errorBody().string())
                            showSnackBar(jObjError.getString("message"))
                        } catch (e: Exception) {
                            showSnackBar(""+ e.message)
                        }
                    }
                }
            }

            override fun onFailure(call: Call<ModelCreateCalendar>, t: Throwable) {
                // Log error here since request failed
                hideLoading()
                showSnackBar(getString(R.string.failure_please_try_again))
            }
        })
    }





}
