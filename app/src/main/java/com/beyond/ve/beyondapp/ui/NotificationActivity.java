package com.beyond.ve.beyondapp.ui;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.graphics.Canvas;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.beyond.ve.beyondapp.BaseActivity.BaseActivity;
import com.beyond.ve.beyondapp.Adapter.NotificationAdapter;
import com.beyond.ve.beyondapp.Model.DeleteNotiModel;
import com.beyond.ve.beyondapp.Model.Notification;
import com.beyond.ve.beyondapp.Model.NotificationResponse;
import com.beyond.ve.beyondapp.R;
import com.beyond.ve.beyondapp.Utils.CommonUtils;
import com.beyond.ve.beyondapp.Web.ApiClient;
import com.beyond.ve.beyondapp.Web.WebService;
import com.beyond.ve.beyondapp.data.AppPreferencesHelper;
import com.beyond.ve.beyondapp.databinding.ActivityNotificationBinding;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import org.json.JSONObject;

import java.util.ArrayList;

public class NotificationActivity extends BaseActivity implements View.OnClickListener {
    private ActivityNotificationBinding binding;
    private ArrayList<Notification> notifications=new ArrayList<>();
    private NotificationAdapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding= DataBindingUtil.setContentView(this,R.layout.activity_notification);

        getNotification();

        init();
    }

    private void init() {
        binding.txtClearAll.setOnClickListener(this);
        binding.viewExit.setOnClickListener(this);
        adapter=new NotificationAdapter(notifications, CommonUtils.returmWindowWidth(this));
        binding.recyclerView.setLayoutManager(new LinearLayoutManager(this));
        binding.recyclerView.setItemAnimator(new DefaultItemAnimator());
        binding.recyclerView.setAdapter(adapter);

        final ItemTouchHelper.SimpleCallback itemTouchHelperCallback = new ItemTouchHelper.SimpleCallback(ItemTouchHelper.ANIMATION_TYPE_DRAG, ItemTouchHelper.LEFT|ItemTouchHelper.RIGHT) {
            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return true;
            }

            @Override
            public boolean isItemViewSwipeEnabled() {
                return true;
            }
            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                deleteNotification(notifications.get(viewHolder.getPosition()).getId());
                notifications.remove(viewHolder.getPosition());
                adapter.notifyDataSetChanged();
            }
            @Override
            public void onChildDraw(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {
                // view the background view
            }
        };
        // attaching the touch helper to recycler view
        ItemTouchHelper itemTouchHelper= new ItemTouchHelper(itemTouchHelperCallback);
        itemTouchHelper.attachToRecyclerView(binding.recyclerView);
        int pointerYLoc=getIntent().getExtras().getInt("pointerYLoc",0);
        ConstraintLayout.LayoutParams params= (ConstraintLayout.LayoutParams) binding.imgTrangle.getLayoutParams();
        params.setMargins(0,pointerYLoc, (int) getResources().getDimension(R.dimen.marginSmall),0);
        binding.imgTrangle.setLayoutParams(params);
    }


    private void getNotification() {
        WebService service= ApiClient.getClient().create(WebService.class);
        Call<NotificationResponse> call= service.getNotification("Bearer " + new AppPreferencesHelper(this).getUser_XAUTH());
        call.enqueue(new Callback<NotificationResponse>() {
            @Override
            public void onResponse(Call<NotificationResponse> call, Response<NotificationResponse> response) {
                if(response.isSuccessful()){
                    notifications.addAll(response.body().getPayload());
                    adapter.notifyDataSetChanged();
                }else{
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        showSnackBar(jObjError.getString("message"));
                    } catch (Exception e) {
                        showSnackBar(""+e.getMessage());
                    }
                }
            }
            @Override
            public void onFailure(Call<NotificationResponse> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    private void deleteNotification(int id){
        DeleteNotiModel deleteNotiModel=new DeleteNotiModel();
        deleteNotiModel.putId(id);
        WebService service= ApiClient.getClient().create(WebService.class);
        Call<JsonElement> call= service.deleteNoti("Bearer " + new AppPreferencesHelper(this).getUser_XAUTH(),deleteNotiModel);
        call.enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                if(response.isSuccessful()){
                    Toast.makeText(getBaseContext(),((JsonObject) response.body()).get("message").getAsString(),Toast.LENGTH_SHORT).show();
                    adapter.notifyDataSetChanged();
                }else{
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        showSnackBar(jObjError.getString("message"));
                    } catch (Exception e) {
                        showSnackBar(""+e.getMessage());
                    }
                }
            }
            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    private void delAllNotification(){
        WebService service= ApiClient.getClient().create(WebService.class);
        Call<JsonElement> call= service.delAllNoti("Bearer " + new AppPreferencesHelper(this).getUser_XAUTH());
        call.enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                if(response.isSuccessful()){
                    notifications.clear();
                    adapter.notifyDataSetChanged();
                    Toast.makeText(getBaseContext(),((JsonObject) response.body()).get("message").getAsString(),Toast.LENGTH_SHORT).show();
                    NotificationActivity.this.finish();
                }else{
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        showSnackBar(jObjError.getString("message"));
                    } catch (Exception e) {
                        showSnackBar(""+e.getMessage());
                    }
                }
            }
            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.txtClearAll:
                delAllNotification();
                break;
            case R.id.viewExit:
                onBackPressed();
                break;
        }
    }
}
