package com.beyond.ve.beyondapp.ui;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.ActivityCompat;
import androidx.core.app.NavUtils;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.beyond.ve.beyondapp.ChatAdapter;
import com.beyond.ve.beyondapp.Model.chat.ChatMsg;
import com.beyond.ve.beyondapp.R;
import com.beyond.ve.beyondapp.Utils.CommonUtils;
import com.beyond.ve.beyondapp.Utils.DateUtils;
import com.beyond.ve.beyondapp.Utils.ImageUtils;
import com.beyond.ve.beyondapp.Web.ApiClient;
import com.beyond.ve.beyondapp.Web.WebService;
import com.beyond.ve.beyondapp.data.AppPreferencesHelper;
import com.beyond.ve.beyondapp.service.MyFirebaseMessagingService;
import com.firebase.client.ChildEventListener;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.google.gson.JsonElement;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.InetAddress;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.UUID;

public class ChatActivity extends AppCompatActivity implements View.OnClickListener {

    private static final int REQUEST_CODE_CAMERA = 201,REQUEST_CODE_PERMISION_CAMERA=202;
    private Firebase reference, refMyUnread,refFrndUnread,refTyping,refFrndCre;
    private StorageReference storageReference;
    private String friendId,friendAppId,friendName,friendLocation,friendProfilePic,friendLastSeen,saveLocation,userId,groupUserIds;
    private int frndUnReadCount=0,deviceWidth;
    private boolean isAddChild=false,isSend=false,isOnline;
    private TextView txtTitle,txtSubtitle;
    private CircleImageView imgFrnd;
    private EditText edtMsg;
    private RecyclerView recyclerView;
    private ChatAdapter adapter;
    private ArrayList<ChatMsg> msgs=new ArrayList<>();
    private File photoFile;
    private Uri photoURI;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        Firebase.setAndroidContext(this);

        deviceWidth= CommonUtils.returmWindowWidth(this);

        init();

        initFirebase();


        if(!friendId.startsWith("-")) {
            initFrndUnreadListener();
            txtSubtitle.setText(DateUtils.getTimeDate(friendLastSeen));
            ImageUtils.displayImageFromUrl(this,imgFrnd,friendProfilePic,R.drawable.ic_group);

        }else
            ImageUtils.displayImageFromUrl(this,imgFrnd,friendProfilePic);
    }

    private void init() {
        setSupportActionBar((Toolbar) findViewById(R.id.action_bar));
        userId= new AppPreferencesHelper(this).getUserIdFirebase();

        if (Build.VERSION.SDK_INT >= 21) {
            getWindow().setStatusBarColor(ContextCompat.getColor(this,R.color.white)); //status bar or the time bar at the top
        }

        friendId= getIntent().getExtras().getString("friendId");
        friendAppId= getIntent().getExtras().getString("friendAppId");
        friendName= getIntent().getExtras().getString("friendName");
        friendLocation= getIntent().getExtras().getString("friendLocation");
        friendProfilePic= getIntent().getExtras().getString("friendProfilePic");
        friendLastSeen= getIntent().getExtras().getString("friendLastSeen");
        groupUserIds= getIntent().getExtras().getString("groupUserIds");

        adapter=new ChatAdapter(msgs,userId,deviceWidth);
        LinearLayoutManager manager=new LinearLayoutManager(this);

        imgFrnd=findViewById(R.id.imgUserProfile);
        txtTitle=findViewById(R.id.txtTitle);
        txtSubtitle=findViewById(R.id.txtSubtitle);
        edtMsg=findViewById(R.id.edtMsg);
        recyclerView=findViewById(R.id.recyclerChat);
        txtTitle.setText(friendName);

        ConstraintLayout.LayoutParams paramsProfile= (ConstraintLayout.LayoutParams) imgFrnd.getLayoutParams();
        paramsProfile.width=deviceWidth/9;
        paramsProfile.height=deviceWidth/9;
        imgFrnd.setLayoutParams(paramsProfile);

        recyclerView.setLayoutManager(manager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);


        findViewById(R.id.imgSend).setOnClickListener(this);
        findViewById(R.id.imgGallery).setOnClickListener(this);
        findViewById(R.id.imgCam).setOnClickListener(this);
        findViewById(R.id.imgBack).setOnClickListener(this);

        edtMsg.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(!isSend) {
                    refTyping.child("isTyping").setValue(true);
                    isSend=true;
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            isSend=false;
                        }
                    },5*1000);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        edtMsg.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                v.getId();
                adapter.setShowMsgAnim(false);
                if(hasFocus && adapter!=null)
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            recyclerView.smoothScrollToPosition(adapter.getItemCount());
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    adapter.setShowMsgAnim(true);
                                }
                            },2000);
                        }
                    },300);
            }
        });
        edtMsg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                adapter.setShowMsgAnim(false);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        recyclerView.smoothScrollToPosition(adapter.getItemCount());
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                adapter.setShowMsgAnim(true);
                            }
                        },2000);
                    }
                },300);
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()==android.R.id.home)
            onBackPressed();
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = NavUtils.getParentActivityIntent(ChatActivity.this);
        startActivity(intent);
    }

    // That method not in use as orderByChild is not working properly.
    private void getLastMsg(){
        String baseUrl=FirebaseDatabase.getInstance().getReference().toString();
        refMyUnread = new Firebase(baseUrl+"/users/"+userId+"/conversations/"+friendId);
        DatabaseReference databaseReference= FirebaseDatabase.getInstance().getReference();
        Query query=databaseReference.child("conversations").child(friendLocation).orderByChild("timestamp").limitToLast(30);
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(com.google.firebase.database.DataSnapshot dataSnapshot) {
                try {
                    isAddChild=true;
                    HashMap<String, Object> mapConversion = (HashMap<String, Object>) dataSnapshot.getValue();
                    for (Object key: mapConversion.keySet()) {
                        final Map<String, Object> map = (Map<String, Object>)mapConversion.get(key);
                        ChatMsg msg=new ChatMsg();
                        msg.setContent(map.get("content").toString());
                        msg.setFromID(map.get("fromID").toString());
                        msg.setToID(map.get("toID").toString());
                        msg.setIsRead(map.get("isRead").toString());
                        msg.setType(map.get("type").toString());
                        Long lastSeenMilli= Long.parseLong(map.get("timestamp").toString());
                        msg.setTimestamp(String.valueOf(lastSeenMilli*1000));
                        msgs.add(msg);
                    }
                    adapter.notifyDataSetChanged();
                    refMyUnread.child("unreadCount").setValue("0");
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
    }

    private void initFirebase() {
        String baseUrl=FirebaseDatabase.getInstance().getReference().toString();
        storageReference =FirebaseStorage.getInstance().getReference();
        refTyping= new Firebase(baseUrl+"/users/"+userId+"/credentials");
        reference = new Firebase(baseUrl+"/conversations/"+friendLocation);
        reference.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, final String s) {
                if(s!=null) {
                    String content="";
                    try {
                        final Map map = dataSnapshot.getValue(Map.class);
                        ChatMsg msg=new ChatMsg();
                        msg.setFromID(map.get("fromID").toString());
                        content=map.get("content").toString();
                        msg.setContent(map.get("content").toString());
                        msg.setToID(map.get("toID").toString());
                        msg.setIsRead(map.get("isRead").toString());
                        msg.setType(map.get("type").toString());
                        Long lastSeenMilli= Long.parseLong(map.get("timestamp").toString());
                        msg.setTimestamp(String.valueOf(lastSeenMilli*1000));
                        if(userId.equalsIgnoreCase(msg.getToID())) {
                            reference.child(s).child("isRead").setValue("true");
                            msg.setIsRead("true");
                        }
                        msgs.add(msg);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if(isAddChild){
                        adapter.setShowMsgAnim(false);
                        sendNotification(content);
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                adapter.setShowMsgAnim(true);
                            }
                        },500);
                    }
                    adapter.notifyDataSetChanged();
                    recyclerView.smoothScrollToPosition(adapter.getItemCount());
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });


        if(!friendId.startsWith("-")) {
            refFrndCre = new Firebase(baseUrl + "/users/" + friendId + "/credentials");
            refFrndCre.addChildEventListener(new ChildEventListener() {
                @Override
                public void onChildAdded(DataSnapshot dataSnapshot, String s) {

                }
                @Override
                public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                    switch (dataSnapshot.getKey()){
                        case "name":
                            friendName=dataSnapshot.getValue().toString();
                            txtTitle.setText(friendName);
                            break;
                        case "lastSeen":
                            friendLastSeen=dataSnapshot.getValue().toString();
                            txtSubtitle.setText(DateUtils.getTimeDate(friendLastSeen));
                            break;
                        case "isTyping":
                            boolean isTyping= Boolean.parseBoolean(dataSnapshot.getValue().toString());
                            if(isTyping)
                                txtSubtitle.setText(" typing...");
                            else if(isOnline)
                                txtSubtitle.setText("online");
                            else
                                txtSubtitle.setText(DateUtils.getTimeDate(friendLastSeen));
                            break;
                        case "profilePicLink":
                            friendProfilePic=dataSnapshot.getValue().toString();
                            ImageUtils.displayImageFromUrl(getBaseContext(),imgFrnd,friendProfilePic);
                            break;
                        case "isOnline":
                            isOnline= Boolean.parseBoolean(dataSnapshot.getValue().toString());
                            if(isOnline)
                                txtSubtitle.setText("online");
                            break;
                    }
                }

                @Override
                public void onChildRemoved(DataSnapshot dataSnapshot) {

                }
                @Override
                public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                }
                @Override
                public void onCancelled(FirebaseError firebaseError) {

                }
            });
        }

    }

    private void sendNotification(String content) {
        HashMap<String,String> body=new HashMap<>();
        if(friendId.startsWith("-"))
            body.put("user_id",groupUserIds);
        else
            body.put("user_id",friendAppId);
        body.put("sender_id",new AppPreferencesHelper(this).getUser_Id()+"");
        body.put("message",content);
        body.put("title",new AppPreferencesHelper(this).getUser_Name());

        WebService service= ApiClient.getClient().create(WebService.class);
        Call<JsonElement> call= service.sendNotification("Bearer " + new AppPreferencesHelper(this).getUser_XAUTH(),body);
        call.enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                response.body();
            }
            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.imgSend:
                sendMsg();
                break;
            case R.id.imgGallery:
                ImageUtils.chooseImage(this);
                break;
            case R.id.imgCam:
                capturePhoto();
                break;
            case R.id.imgBack:
                onBackPressed();
                break;
        }
    }

    private void capturePhoto() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_CODE_PERMISION_CAMERA);
            return;
        }

        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        try {
            photoFile = createImageFile();
        } catch (Exception e) {
            e.printStackTrace();
        }
        // Continue only if the File was successfully created
        if (photoFile != null) {
            photoURI = FileProvider.getUriForFile(this,getApplicationContext().getPackageName() + ".provider",photoFile);
            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
            takePictureIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
            takePictureIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            startActivityForResult(takePictureIntent,REQUEST_CODE_CAMERA );
        }
    }

    private File createImageFile() {
        String timeStamp =new SimpleDateFormat("yyyyMMdd_HHmmss",Locale.getDefault()).format(new Date());
        String imageFileName = "IMG_" + timeStamp + "_";
        String suffix = ".jpg";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = null;
        try {
            image = File.createTempFile(imageFileName,suffix,storageDir);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return image;

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == ImageUtils.PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null ){
            Uri filePath = data.getData();
            try {
                uploadImage(getBaseContext(),storageReference,ImageUtils.Storage.DIR_MSG,filePath);
            }catch (Exception e){
                e.printStackTrace();
            }
        }else if(requestCode== REQUEST_CODE_CAMERA){
            uploadImage(getBaseContext(),storageReference,ImageUtils.Storage.DIR_MSG,photoURI);
        }
    }

    private void uploadImage(final Context context, StorageReference storageReference, String dirName, Uri filePath) {
        if(filePath != null){
            final ProgressDialog progressDialog = new ProgressDialog(this);
            progressDialog.setTitle("Uploading...");
            progressDialog.show();
            saveLocation = dirName + "/" + UUID.randomUUID().toString();
            StorageReference ref = storageReference.child(saveLocation);
            ref.putFile(filePath)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            getDownloadUrl(taskSnapshot.getMetadata().getPath(),progressDialog);
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            progressDialog.dismiss();
                            saveLocation ="";
                            Toast.makeText(context, "Failed "+e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    })
                    .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                            double progress = (100.0*taskSnapshot.getBytesTransferred()/taskSnapshot.getTotalByteCount());
                            progressDialog.setMessage("Uploaded "+(int)progress+"%");
                        }
                    });
        }
    }

    private void getDownloadUrl(String path, final ProgressDialog progressDialog){
        try {
            StorageReference storageRef= FirebaseStorage.getInstance().getReference();
            storageRef.child(path).getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                @Override
                public void onSuccess(Uri uri) {
                    progressDialog.dismiss();
                    sendImage(uri.toString());

                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {
                    exception.printStackTrace();
                    progressDialog.dismiss();
                    // Handle any errors
                }
            });

        }catch (Exception e){
            e.printStackTrace();
            progressDialog.dismiss();
        }
    }

    private void sendMsg(){
        refTyping.child("isTyping").setValue(false);
        String messageText = edtMsg.getText().toString();
        if(!messageText.equals("")){
            Map<String, String> map = new HashMap<>();
            map.put("timestamp", String.valueOf(System.currentTimeMillis()/1000));
            map.put("isRead", String.valueOf(false));
            map.put("content", messageText);
            map.put("toID", friendId);
            map.put("fromID", userId);
            map.put("type", "text");

            edtMsg.setText("");
            reference.push().setValue(map);
            isAddChild=true;
            if(!friendId.startsWith("-")) {
                frndUnReadCount++;
                refFrndUnread.child("unreadCount").setValue(frndUnReadCount);
            }
        }
    }

    private void sendImage(String path){
        refTyping.child("isTyping").setValue(false);
        Map<String, String> map = new HashMap<>();
        map.put("timestamp", String.valueOf(System.currentTimeMillis()/1000));
        map.put("isRead", String.valueOf(false));
        map.put("content", path);
        map.put("toID", friendId);
        map.put("fromID", userId);
        map.put("type", "photo");

        edtMsg.setText("");
        reference.push().setValue(map);

        isAddChild=true;

        if(!friendId.startsWith("-")) {
            frndUnReadCount++;
            refFrndUnread.child("unreadCount").setValue(frndUnReadCount);
        }
    }

//    public ArrayList<ClientScanResult> getClientList(boolean onlyReachables, int reachableTimeout) {
//        BufferedReader br = null;
//        ArrayList<ClientScanResult> result = null;
//        try {
//            result = new ArrayList<ClientScanResult>();
//            br = new BufferedReader(new FileReader("/proc/net/arp"));
//            String line;
//            int i = 0;
//            while ((line = br.readLine()) != null) {
//                String[] splitted = line.split(" +");
//                if ((splitted != null) && (splitted.length >= 4)) {
//                    String mac = splitted[3];
//                    if (mac.matches("..:..:..:..:..:..")) {
//                        boolean isReachable = InetAddress.getByName(splitted[0]).isReachable(reachableTimeout);
//
//                        if (!onlyReachables || isReachable) {
//                            result.add(new ClientScanResult(splitted[0], splitted[3], splitted[5], isReachable));
//                            i++;
//                        }
//                    }
//                }
//            }
//        } catch (Exception e) {
//            Log.e(this.getClass().toString(), e.getMessage());
//        } finally {
//            try {
//                br.close();
//            } catch (IOException e) {
//                Log.e(this.getClass().toString(), e.getMessage());
//            }
//        }
//        return result;
//    }
    private void initFrndUnreadListener(){
        refFrndUnread = new Firebase("https://beyond-6fdea.firebaseio.com/users/"+friendId+"/conversations/"+userId);
        refFrndUnread.addValueEventListener(new com.firebase.client.ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                try{
                    if(dataSnapshot.getValue()!=null)
                        frndUnReadCount= Integer.parseInt(dataSnapshot.getValue(HashMap.class).get("unreadCount").toString());
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        MyFirebaseMessagingService.isChatShowNotification=false;
    }

    @Override
    protected void onStop() {
        super.onStop();
        MyFirebaseMessagingService.isChatShowNotification=true;
    }
}
