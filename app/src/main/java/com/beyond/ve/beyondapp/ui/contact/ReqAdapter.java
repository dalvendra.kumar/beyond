package com.beyond.ve.beyondapp.ui.contact;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.beyond.ve.beyondapp.R;
import com.beyond.ve.beyondapp.Utils.ImageUtils;

import java.util.ArrayList;

import androidx.recyclerview.widget.RecyclerView;

public class ReqAdapter extends RecyclerView.Adapter<ReqAdapter.ViewHoldeer> {
    public ArrayList<Model> requestList;
    public ObservableClick observableClick;
    public  Context context;

    public ReqAdapter(Context context,ArrayList<Model> requestList, ObservableClick observableClick) {
        this.requestList = requestList;
        this.observableClick = observableClick;
        this.context = context;
    }

    @Override
    public ViewHoldeer onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHoldeer(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.request_contact_item, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHoldeer holder, final int position) {


        if (!TextUtils.isEmpty(requestList.get(position).getFirst_name()))
            holder.name.setText(requestList.get(position).getFirst_name());

        if (!TextUtils.isEmpty(requestList.get(position).getCompany_name()))
            holder.companyname.setText(requestList.get(position).getCompany_name());

        if (!TextUtils.isEmpty(requestList.get(position).getAvatar())) {
            ImageUtils.displayImageFromUrl(context, holder.image, requestList.get(position).getAvatar());
        }

        holder.cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                observableClick.observeCancel(requestList.get(position));
            }
        });

        holder.select.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                observableClick.onserveSelect(requestList.get(position));
            }
        });

    }

    @Override
    public int getItemCount() {
        return requestList.size();
    }

    public static class ViewHoldeer extends RecyclerView.ViewHolder {
        public ImageView cancel, select,image;
        public TextView name,companyname;

        public ViewHoldeer(View itemView) {
            super(itemView);
            image = (ImageView) itemView.findViewById(R.id.image);
            cancel = (ImageView) itemView.findViewById(R.id.cancel);
            select = (ImageView) itemView.findViewById(R.id.select);
            name = (TextView) itemView.findViewById(R.id.name);
            companyname = (TextView) itemView.findViewById(R.id.companyname);
        }
    }
}


