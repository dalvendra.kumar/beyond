package com.beyond.ve.beyondapp.ui

import android.content.Intent
import android.os.Bundle
import android.text.InputType
import android.text.TextUtils
import android.view.MotionEvent
import android.view.View
import com.beyond.ve.beyondapp.BaseActivity.BaseActivity
import com.beyond.ve.beyondapp.Constant
import com.beyond.ve.beyondapp.Model.NewPasswordModelToRetrofit
import com.beyond.ve.beyondapp.Model.ResetPassword.ModelResetPassword
import com.beyond.ve.beyondapp.R
import com.beyond.ve.beyondapp.Web.ApiClient
import com.beyond.ve.beyondapp.Web.WebService
import kotlinx.android.synthetic.main.activity_new_password.*
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Response


class ActivityNewPassword : BaseActivity() , View.OnClickListener {

    var  isEyeOnNewPassword : Boolean = false
    var  isEyeOnResetPassword : Boolean = false

    override fun onClick(p0: View?) {
        when(p0!!.id){
            R.id.btn_SavePassword->
            {
                validation();
            }
        }
    }

    private fun validation() {
        if(!TextUtils.isEmpty(tie_new_password.text.toString()) && tie_new_password.text!!.length>5 )
        {
                if (!TextUtils.isEmpty(tie_confirm_password.text.toString())) {

                    if(tie_new_password.text.toString().equals(tie_confirm_password.text.toString()))

                    PostNewPasswordDataToServer(tie_new_password.text.toString(),tie_confirm_password.text.toString())

                    else

                        showSnackBar(getString(R.string.msg_password_does_not_match))

                } else {
                    showSnackBar(getString(R.string.msg_enter_password))

                }

        }

        else
        {
            showMessage(getString(R.string.msg_enter_six_digit_password))
        }

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportActionBar!!.hide()
        setContentView(R.layout.activity_new_password)
        btn_SavePassword.setOnClickListener(this)



        tie_confirm_password.setOnTouchListener(View.OnTouchListener { v, event ->
            val DRAWABLE_RIGHT = 2
            if (event.action == MotionEvent.ACTION_UP) {
                if (event.rawX >= tie_new_password.getRight() - tie_new_password.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width()) {
                    if (isEyeOnResetPassword) {
                        isEyeOnResetPassword = false
                        tie_confirm_password.setInputType(InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD)
                        tie_confirm_password.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_eye_off, 0)
                    } else {
                        isEyeOnResetPassword = true
                        tie_confirm_password.setInputType(InputType.TYPE_CLASS_TEXT)
                        tie_confirm_password.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_eye_on, 0)
                    }
                    return@OnTouchListener false
                }
            }
            false
        })



        tie_new_password.setOnTouchListener(View.OnTouchListener { v, event ->
            val DRAWABLE_RIGHT = 2
            if (event.action == MotionEvent.ACTION_UP) {
                if (event.rawX >= tie_new_password.getRight() - tie_new_password.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width()) {
                    if (isEyeOnNewPassword) {
                        isEyeOnNewPassword = false
                        tie_new_password.setInputType(InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD)
                        tie_new_password.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_eye_off, 0)
                    } else {
                        isEyeOnNewPassword = true
                        tie_new_password.setInputType(InputType.TYPE_CLASS_TEXT)
                        tie_new_password.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_eye_on, 0)
                    }
                    return@OnTouchListener false
                }
            }
            false
        })



    }

    private fun PostNewPasswordDataToServer(newPassword : String , confirmPassword : String ) {

        val apiService = ApiClient.getClient().create(WebService::class.java)
              var abc : NewPasswordModelToRetrofit = NewPasswordModelToRetrofit()
              abc.id = intent.extras.getInt("userId")
              abc.secret = intent.extras.getInt("secret")
              abc.password = newPassword
              abc.password_confirmation = confirmPassword

        val call = apiService.postResetPassword(abc)
        showLoading()
        call.enqueue(object : retrofit2.Callback<ModelResetPassword> {
            override fun onResponse(call: Call<ModelResetPassword>, response: Response<ModelResetPassword>?) {
                if (response != null) {
                    if (response.isSuccessful && (response.body().statusCode == Constant.RESPONSE_SUCCESSFULLY || response.body().statusCode == Constant.RESPONSE_SUCCESSFULLLY))
                    {
                        hideLoading()
                        showSnackBar(response.body().message)
                        val intent = Intent(this@ActivityNewPassword, ActivitySignupSignin::class.java)
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
                        startActivity(intent)
                        overridePendingTransition(R.anim.entry_from_right, R.anim.exit_to_left);
                    }else  if (response.code() == Constant.RESPONSE_UNAUTHORIZED || response.code() == Constant.RESPONSE_FAILURE){
                        hideLoading()
                        try {
                            val jObjError = JSONObject(response.errorBody().string())
                            showSnackBar(jObjError.getString("message"))
                        } catch (e: Exception) {
                            showSnackBar(""+e.message)
                        }
                    }
                }
            }
            override fun onFailure(call: Call<ModelResetPassword>, t: Throwable) {
                // Log error here since request failed
                hideLoading()
                showSnackBar(getString(R.string.failure_please_try_again))


            }


        })
    }

}
