package com.beyond.ve.beyondapp.ui

import android.os.Bundle
import android.view.View
import com.beyond.ve.beyondapp.BaseActivity.BaseActivity
import com.beyond.ve.beyondapp.Model.UserProfile
import com.beyond.ve.beyondapp.R
import com.beyond.ve.beyondapp.Utils.ImageUtils
import com.beyond.ve.beyondapp.Web.ApiClient
import com.beyond.ve.beyondapp.Web.WebService
import com.beyond.ve.beyondapp.data.AppPreferencesHelper
import com.beyond.ve.beyondapp.ui.contact.Model
import kotlinx.android.synthetic.main.activity_show_profile_contacts.*
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ShowProfileContactActivity : BaseActivity(), View.OnClickListener {

    override fun onClick(p0: View?) {
        when (p0!!.id) {
            R.id.back_showPrfileActivity -> finish()
        }
    }

    var isAddMedia = false
    var profile: UserProfile? = null
    var medias = ArrayList<UserProfile.Media>()
    var model = Model()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        getSupportActionBar()!!.hide();
        setContentView(R.layout.activity_show_profile_contacts)

        model = intent.getSerializableExtra("model") as Model
        back_showPrfileActivity.setOnClickListener(this)
        lytBar.visibility = View.VISIBLE
        getUserDetails(model)
    }

    private fun getUserDetails(model:Model) {
        val service = ApiClient.getClient().create(WebService::class.java)
        val call = service.getUser(model.id, "Bearer " + AppPreferencesHelper(this).user_XAUTH)
        call.enqueue(object : Callback<UserProfile> {
            override fun onResponse(call: Call<UserProfile>, response: Response<UserProfile>) {
                lytBar.visibility = View.GONE
                if (response.isSuccessful) {
                    if (isAddMedia) {
                        medias.clear()
                        medias.addAll(profile!!.payload.media)
                        isAddMedia = false
                    } else {
                        profile = response.body()
                        setUserData(profile!!.payload)
                    }
                } else {
                    try {
                        val jObjError = JSONObject(response.errorBody().string())
                        showSnackBar(jObjError.getString("message"))
                    } catch (e: Exception) {
                        showSnackBar("" + e.message)
                    }

                }
            }

            override fun onFailure(call: Call<UserProfile>, t: Throwable) {
                t.printStackTrace()
                lytBar.visibility = View.GONE
            }
        })
    }

    private fun setUserData(payload: UserProfile.Payload?) {
        if (payload != null) {
            tv_compName.setText(payload.company_name)
            tv_fullName.setText(payload.first_name + " " + payload.last_name)
            tv_changedPosition.setText(payload.position)
            tv_contactNumber.setText(payload.mobile)
            tv_useremail.setText(payload.email)
            tv_lebel_bio.setText(payload.bio)
            tv_lebel_field.setText(payload.field)
            tv_industry_field.setText(payload.industry)

            edtBio.setText(payload.bio)
            edtField.setText(payload.field)
            edtIndustry.setText(payload.industry)
            edtWebsite.setText(payload.website)
            edtCountry.setText(payload.country)
            edtState.setText(payload.state)



            tv_country_field.setText(payload.company_name)
            tv_citystate_field.setText(payload.state)
            medias.addAll(payload.media)

            ImageUtils.displayImageFromUrl(this, iv_upload, payload.company_logo, R.mipmap.ic_logo_placeholder_profile)
            ImageUtils.displayImageFromUrl(this, findViewById(R.id.profile_image_show_profile), payload.avatar, R.mipmap.ic_user_placeholdr)

            if (payload.linkedIn_id != null)
                txtHintLinkedIn.setText("Linked")
            else
                txtHintLinkedIn.setText("Link")


            if (payload.website.length > 1)
                txtLinkWebsite.setText("Linked")
            else
                txtLinkWebsite.setText("Link")

            if (payload.isActive == 1) {
                txtAvailiable.setText("Availiable")
                txtAvailiable.setCompoundDrawablesWithIntrinsicBounds(R.drawable.indi_available, 0, 0, 0);
            } else if (payload.isActive == 2) {
                txtAvailiable.setText("Busy")
                txtAvailiable.setCompoundDrawablesWithIntrinsicBounds(R.drawable.indi_busy, 0, 0, 0);
            } else if (payload.isActive == 3) {
                txtAvailiable.setText("Invisible")
                txtAvailiable.setCompoundDrawablesWithIntrinsicBounds(R.drawable.indi_invisible, 0, 0, 0);
            }

        }
    }

}
