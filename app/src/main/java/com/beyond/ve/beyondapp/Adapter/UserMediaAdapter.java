package com.beyond.ve.beyondapp.Adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.beyond.ve.beyondapp.Model.UserProfile;
import com.beyond.ve.beyondapp.R;
import com.beyond.ve.beyondapp.Utils.ImageUtils;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class UserMediaAdapter extends RecyclerView.Adapter<UserMediaAdapter.ViewHolder> {
    private ArrayList<UserProfile.Media> mediaArrayList;
    private MediaListener listener;
    private boolean isEditMode;
    private int deviceWidth;

    public UserMediaAdapter(ArrayList<UserProfile.Media> mediaArrayList, MediaListener listener, boolean isEditMode,int deviceWidth) {
        this.mediaArrayList = mediaArrayList;
        this.listener = listener;
//        this.isEditMode = isEditMode;
        this.isEditMode = true;
        this.deviceWidth = deviceWidth;
    }

    public void setEditMode(boolean editMode) {
//        isEditMode = editMode;
        isEditMode = true;
    }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.item_user_media,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        if(position==mediaArrayList.size()) {
            holder.imgMedia.setBackgroundResource(R.drawable.bg_add_media);
            holder.imgMedia.setImageResource(R.mipmap.ic_plus);
            holder.imgMedia.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onAddNewMedia();
                }
            });
        }else
            ImageUtils.displayImageFromUrl(holder.itemView.getContext(),holder.imgMedia,mediaArrayList.get(position).getMedia());
    }

    @Override
    public int getItemCount() {
        if(isEditMode)
            return mediaArrayList.size()+1;
        else
            return mediaArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private ImageView imgMedia;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            imgMedia=itemView.findViewById(R.id.imgMedia);
            ViewGroup.LayoutParams params=imgMedia.getLayoutParams();
            int size=deviceWidth/5;
            params.height=size;
            params.width=size+size/3;
            imgMedia.setLayoutParams(params);
        }
    }

    public interface MediaListener{
        void onAddNewMedia();
    }
}
