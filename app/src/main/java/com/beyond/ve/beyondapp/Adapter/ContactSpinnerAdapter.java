//package com.amindset.ve.beyond.Adapter;
//
//import android.content.Context;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.BaseAdapter;
//import android.widget.ImageView;
//import android.widget.TextView;
//
//import com.amindset.ve.beyond.R;
//
//public class ContactSpinnerAdapter extends BaseAdapter {
//    Context context;
//    int flags[];
//    String[] countryNames;
//    LayoutInflater inflter;
//
//    public ContactSpinnerAdapter(Context applicationContext, int[] flags, String[] countryNames) {
//        this.context = applicationContext;
//        this.flags = flags;
//        this.countryNames = countryNames;
//        inflter = (LayoutInflater.from(applicationContext));
//    }
//
//    @Override
//    public int getCount() {
//        return flags.length;
//    }
//
//    @Override
//    public Object getItem(int i) {
//        return null;
//    }
//
//    @Override
//    public long getItemId(int i) {
//        return 0;
//    }
//
//    @Override
//    public View getView(int i, View view, ViewGroup viewGroup) {
//        view = inflter.inflate(R.layout.item_spinner_contact_name, null);
//        ImageView icon = (ImageView) view.findViewById(R.id.imageView);
//        TextView names = (TextView) view.findViewById(R.id.textView);
//        icon.setImageResource(flags[i]);
//        names.setText(countryNames[i]);
//        return view;
//    }
//}