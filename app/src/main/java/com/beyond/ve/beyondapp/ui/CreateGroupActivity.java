package com.beyond.ve.beyondapp.ui;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.SearchView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Toast;

import com.beyond.ve.beyondapp.BaseActivity.BaseActivity;
import com.beyond.ve.beyondapp.ChatUsersAdapter;
import com.beyond.ve.beyondapp.Model.Contact;
import com.beyond.ve.beyondapp.Model.UserContacts;
import com.beyond.ve.beyondapp.R;
import com.beyond.ve.beyondapp.SelectedUsersAdapter;
import com.beyond.ve.beyondapp.Utils.CommonUtils;
import com.beyond.ve.beyondapp.Utils.ImageUtils;
import com.beyond.ve.beyondapp.Web.ApiClient;
import com.beyond.ve.beyondapp.Web.WebService;
import com.beyond.ve.beyondapp.data.AppPreferencesHelper;
import com.beyond.ve.beyondapp.data.PreferencesHelper;
import com.beyond.ve.beyondapp.databinding.ActivityCreateGroupBinding;
import com.firebase.client.ChildEventListener;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.FirebaseApp;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class CreateGroupActivity extends BaseActivity implements ChatUsersAdapter.UserObserver, View.OnClickListener, SearchView.OnQueryTextListener, SearchView.OnCloseListener, SelectedUsersAdapter.ContactUnselectListener {
    public static final int RESULT_CODE_GROUP_CREATED=202;
    private ArrayList<Contact> contactArrayList =new ArrayList<>();
    private ArrayList<Contact> selectedContacts =new ArrayList<>();
    private ArrayList<Contact> queryContacts=new ArrayList<>();
    private ActivityCreateGroupBinding binding;
    private boolean isSelectUserMode=true;
    private int deviceWidth;
    private SelectedUsersAdapter selectedContactsAdapter;
    private ChatUsersAdapter contactsAdapter;
    private Firebase refCon,refUser;
    private Uri filePath;
    private String groupImageUrl="";
    private StorageReference storageReference;
    PreferencesHelper appPreferencesHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding= DataBindingUtil.setContentView(this,R.layout.activity_create_group);

        initViews();

        initClickListener();

        appPreferencesHelper=new AppPreferencesHelper(this);


        toggleView(isSelectUserMode);

        getContactList();

        Firebase.setAndroidContext(getApplication());

        FirebaseApp.initializeApp(getApplicationContext());

        refCon = new Firebase("https://beyond-6fdea.firebaseio.com/conversations/");
        refUser = new Firebase("https://beyond-6fdea.firebaseio.com/users/");
        refCon.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                if(s!=null)
                    s.length();
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                if(s!=null)
                    s.length();
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });
        storageReference= FirebaseStorage.getInstance().getReference();

    }

    private void initClickListener() {
        binding.searchView.setOnQueryTextListener(this);
        binding.searchView.setOnCloseListener(this);
        binding.searchView.setOnSearchClickListener(this);
        binding.fabNext.setOnClickListener(this);
        binding.txtCreate.setOnClickListener(this);
        binding.imgGroup.setOnClickListener(this);
    }
    private void getContactList() {
        WebService service=ApiClient.getClient().create(WebService.class);
        Call<UserContacts> call= service.getContacts("Bearer " +appPreferencesHelper.getUser_XAUTH());
        call.enqueue(new Callback<UserContacts>() {
            @Override
            public void onResponse(Call<UserContacts> call, Response<UserContacts> response) {
                if(response.isSuccessful()){
                    contactArrayList.addAll(response.body().getPayload().getContacts());
                    contactsAdapter.notifyDataSetChanged();
                }else{
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        showSnackBar(jObjError.getString("message"));
                    } catch (Exception e) {
                        showSnackBar(""+e.getMessage());

                    }
                }
            }
            @Override
            public void onFailure(Call<UserContacts> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    private void toggleView(boolean isSelectUserMode) {
        if(isSelectUserMode){
            binding.lytGroupDetails.setVisibility(View.GONE);
            binding.txtParticpantsCount.setVisibility(View.GONE);
            binding.txtCreate.setVisibility(View.GONE);
            binding.searchView.setVisibility(View.VISIBLE);
            binding.recyclerViewUsers.setVisibility(View.VISIBLE);
            binding.fabNext.setVisibility(View.VISIBLE);
            selectedContactsAdapter.setShowName(false);

        }else {
            binding.lytGroupDetails.setVisibility(View.VISIBLE);
            binding.txtParticpantsCount.setVisibility(View.VISIBLE);
            binding.txtCreate.setVisibility(View.VISIBLE);
            binding.searchView.setVisibility(View.GONE);
            binding.recyclerViewUsers.setVisibility(View.GONE);
            binding.fabNext.setVisibility(View.GONE);
            selectedContactsAdapter.setShowName(true);
        }
    }

    private void initViews(){
        deviceWidth= CommonUtils.returmWindowWidth(this);
        contactsAdapter =new ChatUsersAdapter(contactArrayList,deviceWidth,this);
        selectedContactsAdapter =new SelectedUsersAdapter(this,selectedContacts,deviceWidth);

        LinearLayoutManager managerHorizontal=new LinearLayoutManager(this,RecyclerView.HORIZONTAL,false);
        LinearLayoutManager managerVertical=new LinearLayoutManager(this,RecyclerView.VERTICAL,false);

        binding.recyclerViewSelected.setItemAnimator(new DefaultItemAnimator());
        binding.recyclerViewSelected.setLayoutManager(managerHorizontal);
        binding.recyclerViewSelected.setAdapter(selectedContactsAdapter);

        binding.recyclerViewUsers.setItemAnimator(new DefaultItemAnimator());
        binding.recyclerViewUsers.setLayoutManager(managerVertical);
        binding.recyclerViewUsers.setAdapter(contactsAdapter);

        ConstraintLayout.LayoutParams params= (ConstraintLayout.LayoutParams) binding.imgGroup.getLayoutParams();
        int size=CommonUtils.returmWindowWidth(this)/6;
        params.height=size;
        params.width=size;
        binding.imgGroup.setLayoutParams(params);

    }

    @Override
    public void onSelectUser(Contact user) {
        if(user.isSelected())
            selectedContacts.add(user);
        else {
            for(Contact chatUser: selectedContacts)
                if(chatUser.getId()==user.getId())
                    selectedContacts.remove(chatUser);
        }
        selectedContactsAdapter.notifyDataSetChanged();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.fabNext:
                if(selectedContacts.size()>0) {
                    isSelectUserMode = !isSelectUserMode;
                    toggleView(isSelectUserMode);
                    selectedContactsAdapter.notifyDataSetChanged();
                }else
                    Snackbar.make(binding.searchView,getString(R.string.msg_select_contact),Snackbar.LENGTH_SHORT).show();

                break;
            case R.id.searchView:
                queryContacts.clear();
                contactsAdapter =new ChatUsersAdapter(queryContacts,deviceWidth,this);
                LinearLayoutManager managerVertical=new LinearLayoutManager(this,RecyclerView.VERTICAL,false);
                binding.recyclerViewUsers.setItemAnimator(new DefaultItemAnimator());
                binding.recyclerViewUsers.setLayoutManager(managerVertical);
                binding.recyclerViewUsers.setAdapter(contactsAdapter);
                break;
            case R.id.txtCreate:
                if(binding.edtGroupName.getText().toString().trim().length()>3) {
                    if (filePath==null) {
                        final ProgressDialog progressDialog = new ProgressDialog(this);
                        progressDialog.setTitle("Please wait...");
                        progressDialog.show();
                        groupImageUrl="";
                        String keyConversation = refCon.push().getKey();
                        String keyGroup = refCon.push().getKey();
                        Map<String, String> map = getWelcomeMsg(keyGroup);
                        refCon.child(keyConversation).push().setValue(map);
                        addGroupToUsers(keyGroup, keyConversation);
                        progressDialog.dismiss();
                    }else
                        uploadImage(getBaseContext(),storageReference,ImageUtils.Storage.DIR_GROUP,filePath);
                }else
                    Snackbar.make(binding.searchView,getString(R.string.msg_invalid_name),Snackbar.LENGTH_SHORT).show();
                break;
            case R.id.imgGroup:
                ImageUtils.chooseImage(this);
                break;
        }
    }

    private void addGroupToUsers(String keyGroup,String keyConversation) {
        HashMap<String, String> map = getGroupDetails(keyConversation);

        for(Contact contact:selectedContacts){
            refUser.child(contact.getChat_id()).child("conversations").child(keyGroup).setValue(map);
        }
        refUser.child(appPreferencesHelper.getUserIdFirebase()).child("conversations").child(keyGroup).setValue(map);
        Intent intentGroup=new Intent();
        map.put("keyGroup",keyGroup);
        intentGroup.putExtra("groupDetails", map);
        setResult(RESULT_CODE_GROUP_CREATED,intentGroup);
        CreateGroupActivity.this.finish();
    }



    private Map<String, String> getWelcomeMsg(String keyGroup) {
        Map<String, String> map=new HashMap<>();
        map.put("timestamp", String.valueOf(System.currentTimeMillis()));
        map.put("isRead", String.valueOf(false));
        map.put("content", "Welcome to "+binding.edtGroupName.getText().toString().trim());
        map.put("toID", keyGroup);
        map.put("fromID", "");
        map.put("type", "text");
        return map;
    }

    private HashMap<String, String> getGroupDetails(String keyConversation) {
        String groupUserIds="";
        for(Contact contact:selectedContacts){
            groupUserIds=groupUserIds+","+contact.getUserId();
        }

        if(groupUserIds.startsWith(","))
            groupUserIds= groupUserIds.replaceFirst(",","");

        HashMap<String, String> map=new HashMap<>();
        map.put("groupImage", groupImageUrl);
        map.put("groupName", binding.edtGroupName.getText().toString().trim());
        map.put("isArchive", String.valueOf(false));
        map.put("location", keyConversation);
        map.put("groupUserIds", groupUserIds);
        map.put("unreadCount", "0");
        return map;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        showSearchResult(query);
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        if(newText.trim().length()>2)
            showSearchResult(newText);
        return false;
    }

    @Override
    public boolean onClose() {

        queryContacts.clear();
        contactsAdapter =new ChatUsersAdapter(contactArrayList,deviceWidth,this);
        LinearLayoutManager managerVertical=new LinearLayoutManager(this,RecyclerView.VERTICAL,false);
        binding.recyclerViewUsers.setItemAnimator(new DefaultItemAnimator());
        binding.recyclerViewUsers.setLayoutManager(managerVertical);
        binding.recyclerViewUsers.setAdapter(contactsAdapter);
        return false;
    }

    private void showSearchResult(String query) {
        queryContacts.clear();
        for (Contact contact:contactArrayList){
            String content=contact.getFirst_name() +" "+contact.getLast_name();
            if(content.contains(query))
                queryContacts.add(contact);
        }
        if(queryContacts.size()>0)
            contactsAdapter.notifyDataSetChanged();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == ImageUtils.PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null ){
            filePath = data.getData();
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);
                binding.imgGroup.setImageBitmap(bitmap);
            }catch (IOException e){
                e.printStackTrace();
            }
        }
    }

    private void uploadImage(final Context context, StorageReference storageReference, String dirName, Uri filePath) {
        if(filePath != null){

            final ProgressDialog progressDialog = new ProgressDialog(this);
            progressDialog.setTitle("Uploading...");
            progressDialog.show();

            groupImageUrl=dirName+"/"+ UUID.randomUUID().toString();
            StorageReference ref = storageReference.child(groupImageUrl);
            ref.putFile(filePath)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            getDownloadUrl(taskSnapshot.getMetadata().getPath(),progressDialog);
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            groupImageUrl="";
                            progressDialog.dismiss();
                            Toast.makeText(context, "Failed "+e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    })
                    .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                            double progress = (100.0*taskSnapshot.getBytesTransferred()/taskSnapshot.getTotalByteCount());
                            progressDialog.setMessage("Uploaded "+(int)progress+"%");
                        }
                    });
        }
    }

    private void getDownloadUrl(String path, final ProgressDialog progressDialog){
        try {
            StorageReference storageRef= FirebaseStorage.getInstance().getReference();
            storageRef.child(path).getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                @Override
                public void onSuccess(Uri uri) {
                    groupImageUrl=uri.toString();
                    String keyConversation = refCon.push().getKey();
                    String keyGroup = refCon.push().getKey();
                    Map<String, String> map = getWelcomeMsg(keyGroup);
                    refCon.child(keyConversation).push().setValue(map);
                    addGroupToUsers(keyGroup, keyConversation);
                    progressDialog.dismiss();

                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {
                    exception.printStackTrace();
                    progressDialog.dismiss();
                    // Handle any errors
                }
            });

        }catch (Exception e){
            e.printStackTrace();
            progressDialog.dismiss();
        }
    }
    @Override
    public void onUnselectContact(Contact contact) {
        selectedContacts.remove(contact);
        selectedContactsAdapter.notifyDataSetChanged();
        contact.setSelected(false);
        contactsAdapter.notifyDataSetChanged();
    }
}
