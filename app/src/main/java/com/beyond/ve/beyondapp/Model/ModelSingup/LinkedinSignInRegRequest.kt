package com.beyond.ve.beyondapp.Model.ModelSingup

class LinkedinSignInRegRequest {

    public var first_name: String? = null
    public var last_name: String? = null
    public var linkedIn_id: String? = null
    public var lat: Double? = null
    public var lng: Double? = null
    override fun toString(): String {
        return "LinkedinSignInRegRequest(first_name=$first_name, last_name=$last_name, linkedIn_id=$linkedIn_id, lat=$lat, lng=$lng)"
    }


}
