package com.beyond.ve.beyondapp.ui

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.Toast
import com.beyond.ve.beyondapp.BaseActivity.BaseActivity
import com.beyond.ve.beyondapp.Constant
import com.beyond.ve.beyondapp.R
import com.beyond.ve.beyondapp.data.AppPreferencesHelper
import com.beyond.ve.beyondapp.imagePicker.CaptureImageUtils
import com.beyond.ve.beyondapp.ui.contact.ContactFragment
import com.firebase.client.Firebase
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.FirebaseApp
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.FirebaseDatabase
import kotlinx.android.synthetic.main.activity_dashboad.*
import kotlinx.android.synthetic.main.lyt_bottom_bar.*
import java.io.File
import java.lang.Exception

class Dashboad : BaseActivity(), View.OnClickListener, CaptureImageUtils.ImageSelectionListener {
    private var chatResponse: Int? = null
    private var isFromChat: Boolean? = false
    private var captureImageUtils: CaptureImageUtils? = null
    var back_pressed: Long = 0

    override fun capturedImage(uri: Uri?, imageFile: File?) {
        Log.e("check", uri.toString() + "----" + imageFile.toString())
        callAddContact(imageFile!!)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            captureImageUtils?.onRequestPermission(requestCode, permissions, grantResults)
        }
    }

    override fun onClick(p0: View?) {
        when (p0!!.id) {

            R.id.iv_cancel -> {
                hideView()
            }

            R.id.imgFab -> {
                val shake: Animation
                shake = AnimationUtils.loadAnimation(applicationContext, R.anim.shake)
                val slideUp = AnimationUtils.loadAnimation(applicationContext, R.anim.slide_up)
                if (cl_onaddbutton.getVisibility() === View.INVISIBLE) {
                    cl_onaddbutton.startAnimation(slideUp)
                    cl_onaddbutton.setVisibility(View.VISIBLE)
                    cl_onaddbutton.alpha = 1.0F
                    iv_camera.startAnimation(shake)
                    iv_upload.startAnimation(shake)
                    imgFab.animate().rotation(0F).start()
                    imgFab.animate()
                            .translationY(0F)
                            .alpha(0.0f).setDuration(500)
                            .setListener(object : AnimatorListenerAdapter() {
                                override fun onAnimationEnd(animation: Animator) {
                                    super.onAnimationEnd(animation)
                                    imgFab.visibility = View.INVISIBLE
                                }
                            })
                }
            }

            R.id.iv_camera -> {captureImageUtils?.handleCamera()}

            R.id.iv_upload -> {captureImageUtils?.handleGallery()}

            R.id.imgCalendar -> {
                hideView()

                imgCalendar.setImageResource(R.drawable.ic_calendar_on_home)
                imgContact.setImageResource(R.drawable.ic_contacts_off_home)
                imgHome.setImageResource(R.drawable.ic_home_off_home)

                var fragment: ScheduleFragmentMonthly = ScheduleFragmentMonthly();
                val ft = supportFragmentManager.beginTransaction()
                ft.replace(R.id.flContent, fragment).addToBackStack(null)
                ft.commit()
            }

            R.id.imgHome -> {
                imgCalendar.setImageResource(R.drawable.ic_calender_off_home)
                imgContact.setImageResource(R.drawable.ic_contacts_off_home)
                imgHome.setImageResource(R.drawable.ic_home_black_24dp)
                var fragment = FragmentViewPager();
                val ft = supportFragmentManager.beginTransaction()
                ft.replace(R.id.flContent, fragment).addToBackStack(null)
                ft.commit()
            }

            R.id.imgChat -> {
                hideView()
                val intent = Intent(this, ChatsActivity::class.java)
                startActivityForResult(intent, Constant.Chat.REQUEST_CODE)
                overridePendingTransition(0, 0)
            }

            R.id.imgContact -> {
                hideView()
                imgContact.setImageResource(R.drawable.ic_contacts_on_home)
                imgCalendar.setImageResource(R.drawable.ic_calender_off_home)
                imgHome.setImageResource(R.drawable.ic_home_off_home)
                supportFragmentManager.beginTransaction()
                        .replace(R.id.flContent, ContactFragment())
                        .addToBackStack(null)
                        .commit()
            }

        }
    }

    override fun onResume() {
        super.onResume()
        if (this.isFromChat!!) {
            when (chatResponse) {
                Constant.Chat.ACTION_CALANDER -> {
                    imgCalendar.performClick()
                }
                Constant.Chat.ACTION_CONTACT -> {
                    imgContact.performClick()
                }
                Constant.Chat.ACTION_HOME -> {
                    imgHome.performClick()
                }
                Constant.Chat.ACTION_CAMERA-> {
                    captureImageUtils?.handleCamera()
                }
                Constant.Chat.ACTION_UPLOAD-> {
                    captureImageUtils?.handleGallery()
                }
            }
            isFromChat = false
        }
    }

    override fun onBackPressed() {
        val fragments = fragmentManager.backStackEntryCount
        if (fragments == 0) {
            if (back_pressed + 2000 > System.currentTimeMillis()) {
                finish()
            } else {
                Toast.makeText(getBaseContext(), "Press once again to exit!", Toast.LENGTH_SHORT).show();
            }
            back_pressed = System.currentTimeMillis();
        }
    }

    private lateinit var baseUrl: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportActionBar!!.hide()
        setContentView(R.layout.activity_dashboad)

        Firebase.setAndroidContext(application)

        FirebaseApp.initializeApp(applicationContext)

        captureImageUtils = CaptureImageUtils(this, this)

        imgFab.setOnClickListener(this)
        iv_cancel.setOnClickListener(this)
        imgCalendar.setOnClickListener(this)
        imgChat.setOnClickListener(this)
        imgHome.setOnClickListener(this)
        imgContact.setOnClickListener(this)

        iv_upload.setOnClickListener(this)
        iv_camera.setOnClickListener(this)

        var fragment: FragmentViewPager = FragmentViewPager()
        val ft = supportFragmentManager.beginTransaction()
        ft.add(R.id.flContent, fragment).addToBackStack(null)
        ft.commit()
        baseUrl = FirebaseDatabase.getInstance().reference.toString()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        captureImageUtils?.onActivityResult(requestCode, resultCode, data)
        if (requestCode == Constant.Chat.REQUEST_CODE && resultCode == Constant.Chat.RESULT_CODE) {
            isFromChat = true
            chatResponse = data!!.extras.get(Constant.Chat.KEY_ACTION) as Int?
        } else
            isFromChat = false
    }

    private fun firebaseAuth() {
        val email: String
        val password: String
        val helper = AppPreferencesHelper(this)
        email = helper.user_Id.toString() + "@beyond.com"
        password = "12345678"
        val mAuth = FirebaseAuth.getInstance()
        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this!!, OnCompleteListener<AuthResult> { task ->
                    if (task.isSuccessful) {
                        val uid = task.result!!.user.uid
                        helper.userIdFirebase = uid
                    }
                })
    }

    private fun callAddContact(bitmap: File) {
        val intent = Intent(this, ActivityAddContact::class.java)
        intent.putExtra("image", bitmap);
        startActivity(intent)
    }

    fun hideView() {
        val slideDown = AnimationUtils.loadAnimation(applicationContext, R.anim.slide_down)
        if (imgFab.getVisibility() === View.INVISIBLE) {
            imgFab.animate()
                    .translationY(10F)
                    .alpha(1.0f).setDuration(200)
                    .setListener(object : AnimatorListenerAdapter() {
                        override fun onAnimationEnd(animation: Animator) {
                            super.onAnimationEnd(animation)
                            imgFab.visibility = View.VISIBLE
                        }
                    })
            cl_onaddbutton.startAnimation(slideDown)
            cl_onaddbutton.animate().rotation(0F).start();
            cl_onaddbutton.animate()
                    .translationY(0F)
                    .alpha(0.0f)
                    .setListener(object : AnimatorListenerAdapter() {
                        override fun onAnimationEnd(animation: Animator) {
                            super.onAnimationEnd(animation)
                            cl_onaddbutton.visibility = View.INVISIBLE
                        }
                    })
        }
    }

    override fun onStop() {
        try {
            var refOnlineStaus = Firebase(baseUrl + "/users/" + AppPreferencesHelper(applicationContext).userIdFirebase + "/credentials")
            refOnlineStaus.child("isOnline").setValue(false)
            refOnlineStaus.child("lastSeen").setValue((System.currentTimeMillis()/1000).toString())
        }catch (e:Exception){
            e.message
        }
        super.onStop()
    }

}
