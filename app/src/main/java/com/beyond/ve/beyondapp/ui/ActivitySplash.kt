package com.beyond.ve.beyondapp.ui

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import com.beyond.ve.beyondapp.BaseActivity.BaseActivity
import com.beyond.ve.beyondapp.R
import com.beyond.ve.beyondapp.Utils.CommonUtils
import com.beyond.ve.beyondapp.data.AppPreferencesHelper
import com.beyond.ve.beyondapp.service.MyFirebaseMessagingService

class ActivitySplash : BaseActivity() {
    private var mDelayHandler: Handler? = null
    private val SPLASH_DELAY: Long = 100
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        getSupportActionBar()!!.hide();
        setContentView(R.layout.activity_splash)

        //Initialize the Handler
        mDelayHandler = Handler()

        MyFirebaseMessagingService.isChatShowNotification = true
        //Navigate with delay
        mDelayHandler!!.postDelayed(mRunnable, SPLASH_DELAY)

        if(!AppPreferencesHelper(applicationContext).otpVerify)
            AppPreferencesHelper(applicationContext).pref.edit().clear().commit()

    }

    public override fun onDestroy() {

        if (mDelayHandler != null) {
            mDelayHandler!!.removeCallbacks(mRunnable)
        }

        super.onDestroy()
    }

    internal val mRunnable: Runnable = Runnable {
        if (!isFinishing) {
            if(AppPreferencesHelper(this).user_XAUTH.length>0){
                val intent = Intent(applicationContext, Dashboad::class.java)
                startActivity(intent)
                finish()
            }else{
                val intent = Intent(applicationContext, ActivitySignupSignin::class.java)
                intent.putExtra("isPrevSignIn",CommonUtils.isEmailValid(AppPreferencesHelper(this).user_Email))
                startActivity(intent)
                finish()
            }
        }
    }


}
