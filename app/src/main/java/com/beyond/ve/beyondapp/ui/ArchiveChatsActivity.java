package com.beyond.ve.beyondapp.ui;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.os.Build;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import com.beyond.ve.beyondapp.ArchiveChatsAdapter;
import com.beyond.ve.beyondapp.Model.chat.ChatUser;
import com.beyond.ve.beyondapp.R;
import com.beyond.ve.beyondapp.Utils.CommonUtils;
import com.beyond.ve.beyondapp.data.AppPreferencesHelper;
import com.firebase.client.Firebase;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.FirebaseApp;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Map;

public class ArchiveChatsActivity extends AppCompatActivity implements ArchiveChatsAdapter.UserObserver, ChatUser.ChangeObserver {
    private ProgressDialog pd;
    private String userId,baseUrl ;
    private ArrayList<ChatUser> users=new ArrayList<>();
    private ArchiveChatsAdapter adapter;
    private RecyclerView recyclerView;
    public static final int RESULT_CODE_UPDATE=201;
    private int deviceWidth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_archive_chats);

        Firebase.setAndroidContext(getApplication());

        FirebaseApp.initializeApp(getApplicationContext());
        baseUrl = FirebaseDatabase.getInstance().getReference().toString();

        deviceWidth= CommonUtils.returmWindowWidth(this);

        getArchiveUsers();

        initActionBar();

        init();

    }

    private void initActionBar() {
        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        if (Build.VERSION.SDK_INT >= 21) {
            getWindow().setStatusBarColor(ContextCompat.getColor(this,R.color.white)); //status bar or the time bar at the top
        }
    }

    private void init() {
        recyclerView = findViewById(R.id.recyclerArchives);
        adapter = new ArchiveChatsAdapter(users,deviceWidth, this);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);

        findViewById(R.id.imgBack).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()==android.R.id.home)
            onBackPressed();
        return super.onOptionsItemSelected(item);
    }
    public void getArchiveUsers(){
        userId= new AppPreferencesHelper(this).getUserIdFirebase();
        DatabaseReference rootRef = FirebaseDatabase.getInstance().getReference();
        DatabaseReference usersdRef = rootRef.child("users").child(userId);
        ValueEventListener eventListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.getValue()==null){
                    return;
                }
                Map mapData=(Map) dataSnapshot.getValue();
                Map mapConversion= (Map) mapData.get("conversations");
                if(mapConversion==null){
                    Snackbar.make(recyclerView,"No Archive till now",Snackbar.LENGTH_SHORT).show();
                    return;
                }
                for (Object id: mapConversion.keySet()) {
                    try {
                        String idStr=id.toString();
                        Map jsonConversion =(Map) mapConversion.get(idStr);
                        try {
                            Boolean isArchive =  Boolean.parseBoolean(String.valueOf(jsonConversion.get("isArchive")));
                            if(!isArchive) {
                                continue;
                            }
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                        String location=jsonConversion.get("location").toString();

                        String unreadCount=jsonConversion.get("unreadCount")==null?"":jsonConversion.get("unreadCount").toString();

                        if(idStr.startsWith("-")){
                            // Add Group Chat
                            String groupImage=jsonConversion.get("groupImage").toString();
                            String groupName=jsonConversion.get("groupName").toString();
                            ChatUser user=new ChatUser(idStr,groupName,"",groupImage,"",unreadCount+"");
                            user.setConversationLocation(location);
                            Firebase firebase= new Firebase(baseUrl+"users/"+userId+"/conversations/"+idStr+"/unreadCount");
                            user.unReadCountListener(firebase,ArchiveChatsActivity.this);
                            users.add(user);
                        }else  {
                            //  Add user to user chat
                            ChatUser user=new ChatUser();
                            user.setId(idStr);
                            user.setNotificationCount(unreadCount);
                            user.setConversationLocation(location);
                            Firebase firebaseCount= new Firebase(baseUrl+"/users/"+userId+"/conversations/"+idStr+"/unreadCount");
                            user.unReadCountListener(firebaseCount,ArchiveChatsActivity.this);
                            Firebase firebase= new Firebase(baseUrl+"/users/"+user.getId()+"/credentials");
                            user.setRefCredentials(firebase);
                            users.add(user);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };
        usersdRef.addListenerForSingleValueEvent(eventListener);
    }

    @Override
    public void onArchive(ChatUser user) {
        disableArchiveUser(user);
    }
    private void disableArchiveUser(ChatUser user){
        Firebase userRef = new Firebase(baseUrl+"/users/"+userId+"/conversations/"+user.getId());
        userRef.child("isArchive").setValue("false");
        users.remove(user);
        adapter.notifyDataSetChanged();
        setResult(RESULT_CODE_UPDATE);
    }

    @Override
    public void onUserChange() {
        adapter.notifyDataSetChanged();
    }
}
