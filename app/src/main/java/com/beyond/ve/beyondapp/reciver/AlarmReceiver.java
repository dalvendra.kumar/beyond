package com.beyond.ve.beyondapp.reciver;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.util.Log;

import com.beyond.ve.beyondapp.service.MyFirebaseMessagingService;

import java.util.Calendar;
import java.util.Date;

import static android.content.Context.ALARM_SERVICE;

public class AlarmReceiver extends BroadcastReceiver {

    private static final int DAILY_REMINDER_REQUEST_CODE = 200;
    private final String TAG = "AlarmReceiver";
    private static final String ACTION_CALENDER_ALERT = "AlarmReceiver.ACTION_CALENDER_ALERT";

    @Override
    public void onReceive(Context context, Intent intent) {
        String action=intent.getAction();
        if (action != null) {
            if (intent.getAction().equalsIgnoreCase(Intent.ACTION_BOOT_COMPLETED)) {
                // Set the alarm here.
                Log.d(TAG, "onReceive: BOOT_COMPLETED");
                return;
            } else if (intent.getAction().equalsIgnoreCase(ACTION_CALENDER_ALERT)) {
                String msg=intent.getExtras().getString("msg");
                MyFirebaseMessagingService.genrateNotification(context,"Alert",msg);
                Log.d(TAG, msg);
            }
        }
    }

    public static void setReminder(Context context, Date date,Long alertTime,String msg){
        // Enable a receiver
        ComponentName receiver = new ComponentName(context, AlarmReceiver.class);
        PackageManager pm = context.getPackageManager();
        pm.setComponentEnabledSetting(receiver,PackageManager.COMPONENT_ENABLED_STATE_ENABLED,PackageManager.DONT_KILL_APP);

        Intent intentData = new Intent(context, AlarmReceiver.class);
        intentData.setAction(ACTION_CALENDER_ALERT);
        intentData.putExtra("msg",msg);

        PendingIntent pendingIntent = PendingIntent.getBroadcast(context,DAILY_REMINDER_REQUEST_CODE, intentData,PendingIntent.FLAG_CANCEL_CURRENT);

        AlarmManager am = (AlarmManager) context.getSystemService(ALARM_SERVICE);
        Calendar calendar=Calendar.getInstance();
        calendar.setTime(date);
        am.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis()-alertTime, pendingIntent);
    }


    public static void cancelReminder(Context context){
        // Disable a receiver
        ComponentName receiver = new ComponentName(context, AlarmReceiver.class);
        PackageManager pm = context.getPackageManager();
        pm.setComponentEnabledSetting(receiver,PackageManager.COMPONENT_ENABLED_STATE_DISABLED,PackageManager.DONT_KILL_APP);

        Intent intent1 = new Intent(context, AlarmReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context,
                DAILY_REMINDER_REQUEST_CODE, intent1, PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager am = (AlarmManager) context.getSystemService(ALARM_SERVICE);
        am.cancel(pendingIntent);
        pendingIntent.cancel();
    }

}