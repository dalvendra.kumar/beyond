package com.beyond.ve.beyondapp.ui

import android.os.Bundle
import android.view.View
import com.beyond.ve.beyondapp.R
import android.webkit.WebView
import android.webkit.WebViewClient
import com.beyond.ve.beyondapp.BaseActivity.BaseActivity
import kotlinx.android.synthetic.main.activity_newsfeed.*


class NewsfeedActivity : BaseActivity() , View.OnClickListener {
    override fun onClick(p0: View?) {
        when (p0!!.id) {
        R.id.back_news ->
        finish()
    }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_newsfeed)
supportActionBar!!.hide()
        back_news.setOnClickListener(this)

        news_webview.getSettings().setJavaScriptEnabled(true);
        news_webview.loadUrl(intent.getStringExtra("newsUrl"));

        news_webview.setWebViewClient(object : WebViewClient() {

            override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
                view.loadUrl(url)
                return true
            }
        })
    }
}
