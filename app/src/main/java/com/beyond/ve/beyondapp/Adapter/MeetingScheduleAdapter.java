package com.beyond.ve.beyondapp.Adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.beyond.ve.beyondapp.Model.FetchAllCalendar.Payload;
import com.beyond.ve.beyondapp.R;

import java.util.ArrayList;
import java.util.List;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class MeetingScheduleAdapter extends RecyclerView.Adapter<MeetingScheduleAdapter.MyViewHolder> {

    private List<Payload> payLoadList = new ArrayList<>();
    private Activity activity;
    private String calendarType;
    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView tv_day, tv_date;
        RecyclerView rv_child;
        private LinearLayout ll_header;


        public MyViewHolder(View view) {
            super(view);

            view.setOnClickListener(this);
            rv_child =  view.findViewById(R.id.rv_child);
            tv_day = (TextView) view.findViewById(R.id.tv_day);
            tv_date = (TextView) view.findViewById(R.id.tv_date);
            ll_header=(LinearLayout)view.findViewById(R.id.ll_header);
        }

        @Override
        public void onClick(View view) {
        }
    }


    public MeetingScheduleAdapter(List<Payload> payLoad, Activity activity,String calendarType) {
        this.payLoadList.clear();
        this.payLoadList = payLoad;
        this.activity=activity;
        this.calendarType=calendarType;
    }
 
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.view_header, parent, false);
        return new MyViewHolder(itemView);
    }
 
    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        if(calendarType.equalsIgnoreCase("week"))
        {
            holder.ll_header.setBackgroundColor(activity.getResources().getColor(R.color.white));
        }

        MeetingScheduleChildAdapter mAdapter = new MeetingScheduleChildAdapter(payLoadList.get(position).getData(),activity,payLoadList.get(position).getData().size());
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(activity);
        holder.rv_child.setLayoutManager(mLayoutManager);
        holder.rv_child.setItemAnimator(new DefaultItemAnimator());
        holder.rv_child.setAdapter(mAdapter);
    }
 
    @Override
    public int getItemCount() {
        return payLoadList.size()>0?payLoadList.size():0;
    }

}