package com.beyond.ve.beyondapp.location;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;

import com.beyond.ve.beyondapp.BaseActivity.BaseActivity;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;

/**
 * Created by Rahul on 19/12/17.
 */

public class LocationListenerActivity extends BaseActivity {
    private boolean isLocationAccessCanceled = false;
    private LocationUtils locationUtils = null;
    private ProgressDialog progressDoalog;
    private LocationDetector dector = null;


    /*
     * Check play store service is available in device
     * */
    public boolean checkGooglePlayServiceAvailability(Context context) {
        int statusCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(context);
        if ((statusCode == ConnectionResult.SUCCESS)) {
            return true;
        } else {
            Dialog dialog = GooglePlayServicesUtil.getErrorDialog(statusCode, this, 10, new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialog) {
                    dector.onErrors(ConstantsValues.PLAYSERICE_ERROR);
                }
            });
            dialog.setCanceledOnTouchOutside(false);
            dialog.show();
            return false;
        }
    }


    void startGettLocation() {
        if (PermissionUtils.checkAllPermissionForLocation(this)) {
            if (chekInternetAndPlayservice()) {
                locationUtils = new LocationUtils(this, dector);
            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (locationUtils != null)
            locationUtils.onStop();
    }


    /*
     * Handle  permission here
     * */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case ConstantsValues.AllPermission:
                if (PermissionUtils.checkResultAllPrmission(this, permissions, grantResults)) {
                    isLocationAccessCanceled = false;
                } else {
                    isLocationAccessCanceled = true;
                    stopProgress();
                    boolean showRationale = ActivityCompat.shouldShowRequestPermissionRationale(this, permissions[0]);
                    if (!showRationale) {
                        dector.onErrors(ConstantsValues.PERMISSION_RATIONALE);
                    } else {
                        dector.onErrors(ConstantsValues.PERMISSION_REJECTED);
                    }


                }
                break;
        }
    }

    /*
     * check play services
     * */
    boolean chekInternetAndPlayservice() {
        if (checkGooglePlayServiceAvailability(this)) {
            return true;
        } else {
            stopProgress();
            dector.onErrors(ConstantsValues.PLAYSERICE_ERROR);
        }
        return false;
    }

    /*
     * Handle Gps Enable event
     * */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        try{
            if (requestCode == LocationUtils.LocationTag) {
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        isLocationAccessCanceled = false;
                        break;
                    case Activity.RESULT_CANCELED:
                        isLocationAccessCanceled = true;
                        stopProgress();
                        dector.onErrors(ConstantsValues.GPS_DISABLED);
                        break;
                }
            }
            super.onActivityResult(requestCode, resultCode, data);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        if (!isLocationAccessCanceled) {
            if (locationUtils != null)
                locationUtils.onStop();
            // to check that {@link listenLocation } called or not in activity that extends this class
            if (dector != null)
                startGettLocation();
        }
        super.onResume();
    }


    @Override
    protected void onStop() {
        if (locationUtils != null)
            locationUtils.onStop();
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        if (locationUtils != null)
            locationUtils.onStop();
        super.onDestroy();
    }

    /*
     *  Progress dialog Will Appear here
     * */
    protected void listenLocation(LocationDetector dector) {
        this.dector = dector;
        if (this.progressDoalog != null && this.progressDoalog.isShowing()) {
            return;
        }
        this.progressDoalog = new ProgressDialog(this);
        this.progressDoalog.setMessage("Please wait....");
        this.progressDoalog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        this.progressDoalog.setCancelable(false);
        // this.progressDoalog.show();
    }

    /*
     * Progress dialog will cancel here
     * */
    protected void stopProgress() {
        if (this.progressDoalog != null && progressDoalog.isShowing()) {
            this.progressDoalog.dismiss();
        }
    }
}
