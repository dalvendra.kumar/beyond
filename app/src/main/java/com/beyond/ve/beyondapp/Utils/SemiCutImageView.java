package com.beyond.ve.beyondapp.Utils;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;

import androidx.annotation.Nullable;

public class SemiCutImageView extends ImageView{
    public SemiCutImageView(Context context) {
        super(context);
    }

    public SemiCutImageView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public SemiCutImageView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public SemiCutImageView(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }
}
