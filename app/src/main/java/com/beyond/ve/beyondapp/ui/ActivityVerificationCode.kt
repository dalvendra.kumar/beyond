package com.beyond.ve.beyondapp.ui

import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.view.View
import com.beyond.ve.beyondapp.BaseActivity.BaseActivity
import com.beyond.ve.beyondapp.Constant
import com.beyond.ve.beyondapp.Model.ForgotPassword.ModelForgotPassword
import com.beyond.ve.beyondapp.Model.OTP.ModelOTP
import com.beyond.ve.beyondapp.R
import com.beyond.ve.beyondapp.Utils.NetworkUtils
import com.beyond.ve.beyondapp.Web.ApiClient
import com.beyond.ve.beyondapp.Web.WebService
import com.beyond.ve.beyondapp.data.AppPreferencesHelper
import kotlinx.android.synthetic.main.activity_verification_code.*
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Response
import java.util.HashMap

class ActivityVerificationCode : BaseActivity(), View.OnClickListener {
    override fun onClick(p0: View?) {
        when (p0!!.id) {
            R.id.btn_verify -> {
                validation()
            }

            R.id.iv_verfication_back -> {
                finish()
            }

            R.id.resendcode -> {
                if (NetworkUtils.isNetworkConnected(this)) {
                    PostForgotPasswordDataToServer(getIntent().getStringExtra("email"))
                } else {
                    showMessage(R.string.msg_nointerconncetion)
                }
            }
        }
    }

    private fun PostForgotPasswordDataToServer(email: String) {
        val apiService = ApiClient.getClient().create(WebService::class.java)
        val paramObject = HashMap<String, String>()
        paramObject["email"] = email
        val call = apiService.postForgotPassword(paramObject)
        showLoading()
        call.enqueue(object : retrofit2.Callback<ModelForgotPassword> {
            override fun onResponse(call: Call<ModelForgotPassword>, response: Response<ModelForgotPassword>?) {

                if (response != null) {
                    if (response.body() != null && response.isSuccessful && (response.body().statusCode == Constant.RESPONSE_SUCCESSFULLY) || response.body().statusCode == Constant.RESPONSE_SUCCESSFULLLY) {
                        hideLoading()
                    } else if (response.code() == Constant.RESPONSE_UNAUTHORIZED) {
                        hideLoading()
                        try {
                            val jObjError = JSONObject(response.errorBody().string())
                            showSnackBar(jObjError.getString("message"))
                        } catch (e: Exception) {
                            showSnackBar("" + e.message)

                        }


                    }
                }
            }

            override fun onFailure(call: Call<ModelForgotPassword>, t: Throwable) {
                hideLoading()
                showSnackBar(getString(R.string.failure_please_try_again))
            }
        })
    }


    private fun validation() {
        if (!TextUtils.isEmpty(et_verficode1.text.toString())) {
            if (!TextUtils.isEmpty(et_verficode2.text.toString())) {
                if (!TextUtils.isEmpty(et_verficode3.text.toString())) {
                    if (!TextUtils.isEmpty(et_verficode4.text.toString())) {

                        PostOTPdigitToServer(et_verficode1.text.toString(), et_verficode2.text.toString(), et_verficode3.text.toString(),
                                et_verficode4.text.toString())
                    } else {
                        showSnackBar("Please enter four number")
                    }
                } else {
                    showSnackBar("Please enter third number")
                }
            } else {
                showSnackBar("Please enter second number")
            }
        } else {
            showSnackBar("Please enter first number")
        }

    }


    var isFromSignUp: String = "no"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        getSupportActionBar()!!.hide();
        setContentView(R.layout.activity_verification_code)

        if (intent.getStringExtra("activity").equals("signup")) {
            isFromSignUp = "yes"
        }
        btn_verify.setOnClickListener(this)

        iv_verfication_back.setOnClickListener(this)

        resendcode.setOnClickListener(this)

        et_verficode1.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                if (et_verficode1.text.toString().length == 1)     //size as per your requirement
                {
                    et_verficode2.requestFocus();
                }
            }
        })
        et_verficode2.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                if (et_verficode2.text.toString().length == 1)     //size as per your requirement
                {
                    et_verficode3.requestFocus();
                }
            }
        })
        et_verficode3.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                if (et_verficode3.text.toString().length == 1)     //size as per your requirement
                {
                    et_verficode4.requestFocus();
                }
            }
        })

    }

    private fun PostOTPdigitToServer(firstDigit: String, secondDigit: String, thirdDigit: String, fourDigit: String) {
        val apiService = ApiClient.getClient().create(WebService::class.java)
        val paramObject = HashMap<String, Int>()
        paramObject["id"] = getIntent().getStringExtra("userId").toInt()
        paramObject["otp"] = (firstDigit + secondDigit + thirdDigit + fourDigit).toInt()
        val call = apiService.postOTP(paramObject)
        showLoading()
        call.enqueue(object : retrofit2.Callback<ModelOTP> {
            override fun onResponse(call: Call<ModelOTP>, response: Response<ModelOTP>?) {

                if (response != null) {
                    if (response.isSuccessful && (response.body().statusCode == Constant.RESPONSE_SUCCESSFULLY || response.body().statusCode == Constant.RESPONSE_SUCCESSFULLLY)) {
                        hideLoading()

                        if (isFromSignUp.equals("yes")){
                            val intent = Intent(this@ActivityVerificationCode, ActivityProfile::class.java)
                            intent.putExtra("email", getIntent().getStringExtra("email"))
                            intent.putExtra("firstName", getIntent().getStringExtra("email"))
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                            startActivity(intent)
                            AppPreferencesHelper(applicationContext).otpVerify=true
                            overridePendingTransition(R.anim.entry_from_right, R.anim.exit_to_left)
                        }else {
                            val intent = Intent(this@ActivityVerificationCode, ActivityNewPassword::class.java)
                            intent.putExtra("secret", response.body().payload.secret)
                            intent.putExtra("userId", response.body().payload.id)
                            startActivity(intent)
                            overridePendingTransition(R.anim.entry_from_right, R.anim.exit_to_left)
                        }
                        var intent=Intent()
                        intent.putExtra("isVerifyOTP",true)
                        setResult(201,intent)
                        this@ActivityVerificationCode.finish()
                    } else if (response.code() == Constant.RESPONSE_UNAUTHORIZED || response.code() == Constant.RESPONSE_FAILURE) {
                        hideLoading()
                        try {
                            val jObjError = JSONObject(response.errorBody().string())
                            showSnackBar(jObjError.getString("message"))
                        } catch (e: Exception) {
                            showSnackBar("" + e.message)

                        }
                    }
                }
            }

            override fun onFailure(call: Call<ModelOTP>, t: Throwable) {
                // Log error here since request failed
                hideLoading()
                showSnackBar(getString(R.string.failure_please_try_again))
            }
        })
    }

    override fun onBackPressed() {
        super.onBackPressed()
        if (isFromSignUp.equals("yes")) {
            var intent = Intent()
            intent.putExtra("isVerifyOTP", false)
            setResult(201, intent)
        }
    }

}
