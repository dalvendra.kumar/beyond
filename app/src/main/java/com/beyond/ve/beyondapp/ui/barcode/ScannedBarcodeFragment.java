package com.beyond.ve.beyondapp.ui.barcode;

import android.Manifest;
import android.app.Dialog;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;

import com.beyond.ve.beyondapp.BaseActivity.BaseFragment;
import com.beyond.ve.beyondapp.Model.UserProfile;
import com.beyond.ve.beyondapp.R;
import com.beyond.ve.beyondapp.Utils.ImageUtils;
import com.beyond.ve.beyondapp.Web.ApiClient;
import com.beyond.ve.beyondapp.Web.WebService;
import com.beyond.ve.beyondapp.data.AppPreferencesHelper;
import com.google.android.gms.vision.CameraSource;
import com.google.android.gms.vision.Detector;
import com.google.android.gms.vision.barcode.Barcode;
import com.google.android.gms.vision.barcode.BarcodeDetector;
import com.google.gson.JsonElement;

import org.json.JSONObject;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ScannedBarcodeFragment extends BaseFragment {


    SurfaceView surfaceView;
    TextView txtBarcodeValue;
    private BarcodeDetector barcodeDetector;
    private CameraSource cameraSource;
    private static final int REQUEST_CAMERA_PERMISSION = 201;
    private static boolean IS_CODE_SCANNED = false;

    Button btnAction;
    String intentData = "";
    boolean isEmail = false;
     Dialog delayDialog ;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_scan_barcode);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_scan_barcode, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews(view);
    }

    private void initViews(View view) {
        txtBarcodeValue = view.findViewById(R.id.txtBarcodeValue);
        surfaceView = view.findViewById(R.id.surfaceView);
        delayDialog = new Dialog(getActivity());
    }

    private void initialiseDetectorsAndSources() {

//        Toast.makeText(getActivity(), "Barcode scanner started", Toast.LENGTH_SHORT).show();

        barcodeDetector = new BarcodeDetector.Builder(getActivity())
                .setBarcodeFormats(Barcode.ALL_FORMATS)
                .build();

        cameraSource = new CameraSource.Builder(getActivity(), barcodeDetector)
                .setRequestedPreviewSize(1920, 1080)
                .setAutoFocusEnabled(true) //you should add this feature
                .build();

        surfaceView.getHolder().addCallback(new SurfaceHolder.Callback() {
            @Override
            public void surfaceCreated(SurfaceHolder holder) {
                try {
                    if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                        cameraSource.start(surfaceView.getHolder());
                    } else {
                        ActivityCompat.requestPermissions(getActivity(), new
                                String[]{Manifest.permission.CAMERA}, REQUEST_CAMERA_PERMISSION);
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                }


            }

            @Override
            public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
            }

            @Override
            public void surfaceDestroyed(SurfaceHolder holder) {
                cameraSource.stop();
            }
        });


        barcodeDetector.setProcessor(new Detector.Processor<Barcode>() {
            @Override
            public void release() {
//                Toast.makeText(getActivity(), "To prevent memory leaks barcode scanner has been stopped", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void receiveDetections(Detector.Detections<Barcode> detections) {
                final SparseArray<Barcode> barcodes = detections.getDetectedItems();
                if (barcodes.size() != 0) {

//
                    Log.e("onResponse11", barcodes.valueAt(0).displayValue);
                    if (!IS_CODE_SCANNED) {
                        getUserDetail(barcodes.valueAt(0).displayValue);
//
                        IS_CODE_SCANNED = true;
                    }
                }
            }
        });
    }

    private void getUserDetail(String barcode) {

        Log.e("onResponse11", barcode);
        WebService service = ApiClient.getClient().create(WebService.class);
        Call<UserProfile> call = service.getUser(barcode, "Bearer " + new AppPreferencesHelper(getActivity()).getUser_XAUTH());
        call.enqueue(new Callback<UserProfile>() {
            @Override
            public void onResponse(Call<UserProfile> call, Response<UserProfile> response) {
                Log.e("onResponse", "onResponse: " + response.toString());
                if (response.isSuccessful()) {
                    showAcceptRidePopUp(response);
                } else {
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        showSnackBar(jObjError.getString("message"));
                    } catch (Exception e) {
                        showSnackBar("" + e.getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<UserProfile> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }


    private void onConnect(int barcode) {
        cameraSource.release();
//        Log.e("onResponse11", barcode.);
        WebService service = ApiClient.getClient().create(WebService.class);
        Call<JsonElement> call = service.sendConnectReq("Bearer " + new AppPreferencesHelper(getActivity()).getUser_XAUTH(), 1);
        call.enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                Log.e("onResponse", "onResponse: " + response.toString());

                if (response.isSuccessful()) {
                    if (delayDialog != null && delayDialog.isShowing())
                        delayDialog.dismiss();
                } else {
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        showSnackBar(jObjError.getString("message"));
                    } catch (Exception e) {
                        showSnackBar("" + e.getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }


    @Override
    public void onPause() {
        super.onPause();
        cameraSource.release();
    }

    @Override
    public void onResume() {
        super.onResume();
        initialiseDetectorsAndSources();


    }

    private void showAcceptRidePopUp(final Response<UserProfile> model) {

        delayDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        delayDialog.setContentView(R.layout.scan_connect_dialog_layout);
        delayDialog.setCancelable(true);
        delayDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);


        TextView tv_full_name = (TextView) delayDialog.findViewById(R.id.tv_full_name);
        TextView tv_comapny_name = (TextView) delayDialog.findViewById(R.id.tv_comapny_name);
        TextView tv_comapny_name2 = (TextView) delayDialog.findViewById(R.id.tv_comapny_name2);
        Button btn_singin = (Button) delayDialog.findViewById(R.id.btn_singin);
        ImageView profile_image = (ImageView) delayDialog.findViewById(R.id.profile_image);


        if (!TextUtils.isEmpty(model.body().getPayload().getFirst_name()) && !TextUtils.isEmpty(model.body().getPayload().getLast_name()))
            tv_full_name.setText(model.body().getPayload().getFirst_name() + " " + model.body().getPayload().getLast_name());

        if (!TextUtils.isEmpty(model.body().getPayload().getCompany_name()))
            tv_comapny_name.setText(model.body().getPayload().getCompany_name());

        if (!TextUtils.isEmpty(model.body().getPayload().getEmail()))
            tv_comapny_name2.setText(model.body().getPayload().getEmail());


//
        if (!TextUtils.isEmpty(model.body().getPayload().getAvatar()))
            ImageUtils.displayImageFromUrl(getActivity(), profile_image, model.body().getPayload().getAvatar());


        btn_singin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onConnect(model.body().getPayload().getId());
//                delayDialog.dismiss();
            }
        });


        delayDialog.show();
    }

}
