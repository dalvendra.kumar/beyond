package com.beyond.ve.beyondapp;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.beyond.ve.beyondapp.Model.Contact;
import com.beyond.ve.beyondapp.Utils.ImageUtils;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import de.hdodenhof.circleimageview.CircleImageView;

public class SelectedUsersAdapter extends RecyclerView.Adapter<SelectedUsersAdapter.ViewHolder>{
    private ArrayList<Contact> users;
    private int deviceWidth;
    private boolean isShowName;
    private ContactUnselectListener listener;

    public SelectedUsersAdapter(ContactUnselectListener listener,ArrayList<Contact> users, int deviceWidth) {
        this.users = users;
        this.deviceWidth=deviceWidth;
        this.listener=listener;
    }

    public interface ContactUnselectListener{
        void onUnselectContact(Contact contact);
    }

    public void setShowName(boolean showName) {
        isShowName = showName;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.item_selected_users,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final Contact contact=users.get(position);
        holder.txtName.setText(contact.getFirst_name());
        ImageUtils.displayImageFromUrl(holder.itemView.getContext(),holder.imgProfile,contact.getAvatar());
        holder.imgRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onUnselectContact(contact);
            }
        });

        if(isShowName)
            holder.txtName.setVisibility(View.VISIBLE);
        else
            holder.txtName.setVisibility(View.GONE);
    }

    @Override
    public int getItemCount() {
        return users.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView txtName;
        private CircleImageView imgProfile;
        private ImageView imgRemove;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            txtName=itemView.findViewById(R.id.txtName);
            imgProfile=itemView.findViewById(R.id.imgProfile);
            imgRemove=itemView.findViewById(R.id.imgRemove);

            ConstraintLayout.LayoutParams params= (ConstraintLayout.LayoutParams) imgProfile.getLayoutParams();
            int size=deviceWidth/6;
            params.height=size;
            params.width=size;
            imgProfile.setLayoutParams(params);

            ConstraintLayout.LayoutParams paramsIndi= (ConstraintLayout.LayoutParams) imgRemove.getLayoutParams();
            paramsIndi.circleRadius=size/2;
            imgRemove.setLayoutParams(paramsIndi);


        }
    }
}
