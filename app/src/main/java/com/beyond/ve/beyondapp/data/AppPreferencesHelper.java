/*
 * Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://mindorks.com/license/apache-v2
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package com.beyond.ve.beyondapp.data;

import android.content.Context;
import android.content.SharedPreferences;


import static android.content.Context.MODE_PRIVATE;

/**
 * Created by Satendra Singh on 27/01/17.
 */

public class AppPreferencesHelper implements PreferencesHelper {


    private static final String PREF_KEY_USER_FIRST_NAME = "PREF_KEY_USER_B_TOKEN";
    private static final String PREF_KEY_USER_LAST_NAME = "PREF_KEY_USER_PAT_ID";
    private static final String PREF_KEY_USER_ID = "PREF_KEY_USER_PFNAME";
    private static final String PREF_KEY_USER_EMAIL = "PREF_KEY_USER_PLNAME";
    private static final String PREF_KEY_USER_AVATAR= "PREF_KEY_USER_USERNAME";
    private static final String PREF_KEY_USER_XAUTH= "PREF_KEY_USER_XAUTH";
    private static final String PREF_KEY_USER_ID_FIREBASE= "PREF_KEY_USER_ID_FIREBASE";
    private static final String PREF_KEY_IS_LOGOUT= "PREF_KEY_IS_LOGOUT";
    private static final String PREF_KEY_OTP_VERIFY= "OTPVerify";
    private static final String PREF_KEY_DEVICE_TOKEN= "DEVICE_TOKEN";
    private static final String PREF_KEY_NOTIFICATION_ENABLE= "PREF_KEY_NOTIFICATION_ENABLE";

    public static final String PREF_NAME = "BeyondPref";


    public SharedPreferences pref;

    Context context;

    public AppPreferencesHelper(Context context)
    {
        pref = context.getSharedPreferences(PREF_NAME, MODE_PRIVATE);
        this.context=context;
    }

    private static SharedPreferences getSharedPreferences(Context context) {
        return context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
    }



    @Override
    public void setUser_Name(String userBearerToken) {
        final SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString(PREF_KEY_USER_FIRST_NAME, userBearerToken);
        editor.commit();
    }

    @Override
    public String getUser_Name() {
        return getSharedPreferences(context).getString(PREF_KEY_USER_FIRST_NAME, "");
    }

    @Override
    public void setUser_LastName(String userBearerToken) {
        final SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString(PREF_KEY_USER_LAST_NAME, userBearerToken);
        editor.commit();
    }

    @Override
    public String getUser_LastName() {
        return getSharedPreferences(context).getString(PREF_KEY_USER_LAST_NAME, "");
    }

    @Override
    public void setUser_Id(Integer userBearerToken) {
        final SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putInt(PREF_KEY_USER_ID, userBearerToken);
        editor.commit();
    }

    @Override
    public Integer getUser_Id() {
        return getSharedPreferences(context).getInt(PREF_KEY_USER_ID, 0);
    }

    @Override
    public void setUser_Email(String userBearerToken) {
        final SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString(PREF_KEY_USER_EMAIL, userBearerToken);
        editor.commit();
    }

    @Override
    public String getUser_Email() {
        return getSharedPreferences(context).getString(PREF_KEY_USER_EMAIL, "");
    }


    @Override
    public void setUser_avatar(String userBearerToken) {
        final SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString(PREF_KEY_USER_AVATAR, userBearerToken);
        editor.commit();
    }

    @Override
    public String getUser_avatar() {
        return getSharedPreferences(context).getString(PREF_KEY_USER_AVATAR, "");
    }

    @Override
    public void setUser_XAUTH(String xAuth) {
        final SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString(PREF_KEY_USER_XAUTH, xAuth);
        editor.commit();
    }

    @Override
    public String getUser_XAUTH() {
        return getSharedPreferences(context).getString(PREF_KEY_USER_XAUTH, "");

    }

    @Override
    public void setUserIdFirebase(String id) {
        final SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString(PREF_KEY_USER_ID_FIREBASE, id);
        editor.commit();
    }

    @Override
    public String getUserIdFirebase() {
        return getSharedPreferences(context).getString(PREF_KEY_USER_ID_FIREBASE, "");
    }

    @Override
    public void setDeviceToken(String deviceToken) {
        if(deviceToken.length()<=0)
            return;
        final SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString(PREF_KEY_DEVICE_TOKEN, deviceToken);
        editor.commit();
    }

    @Override
    public String getDeviceToken() {
        String deviceToken=getSharedPreferences(context).getString(PREF_KEY_DEVICE_TOKEN, "");
        return deviceToken;
    }

    @Override
    public boolean clearPrefe() {
        String token=getDeviceToken();
        getSharedPreferences(context).edit().clear().commit();
        setDeviceToken(token);
        return false;
    }

    @Override
    public void setOTPVerify(Boolean isOTPVerify) {
        final SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putBoolean(PREF_KEY_OTP_VERIFY, isOTPVerify);
        editor.commit();
    }

    @Override
    public Boolean getOTPVerify() {
        return getSharedPreferences(context).getBoolean(PREF_KEY_OTP_VERIFY,false);
    }
    @Override
    public void setNotificationEnable(Boolean isEnable) {
        final SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putBoolean(PREF_KEY_OTP_VERIFY, isEnable);
        editor.commit();
    }

    @Override
    public Boolean getNotificationEnable() {
        return getSharedPreferences(context).getBoolean(PREF_KEY_OTP_VERIFY,true);
    }

}
