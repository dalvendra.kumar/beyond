package com.beyond.ve.beyondapp.Dumnp;

import android.os.Bundle;

import com.beyond.ve.beyondapp.BaseActivity.BaseActivity;
import com.beyond.ve.beyondapp.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class MainActivity extends BaseActivity {

    private RecyclerView mRecyclerView;
    private Adapter adapter;String[] strings;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dumop);

        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerview);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new Adapter(this,getData());
        mRecyclerView.setAdapter(adapter);

        RecyclerSectionItemDecoration sectionItemDecoration =
                new RecyclerSectionItemDecoration(getResources().getDimensionPixelSize(R.dimen.sizeNotificationBg),
                        true,
                        getSectionCallback(getData()),"");
        mRecyclerView.addItemDecoration(sectionItemDecoration);

    }
    public ArrayList<String> getData() {
          strings = getResources().getStringArray(R.array.animals);
          ArrayList<String> list = new ArrayList<String>(Arrays.asList(strings));
         return list;
    }

    private RecyclerSectionItemDecoration.SectionCallback getSectionCallback(final List<String> people) {

        return new RecyclerSectionItemDecoration.SectionCallback() {
            @Override
            public boolean isSection(int position) {

                return true;
            }

            @Override
            public CharSequence getSectionHeader(int position) {
                return strings[position];
            }
        };
    }
}