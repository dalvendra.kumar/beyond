package com.beyond.ve.beyondapp.Web;

import com.beyond.ve.beyondapp.Model.AcceptRequest.AcceptRequest;
import com.beyond.ve.beyondapp.Model.CalendarUpdatesDashboard.CalendarUpdates;
import com.beyond.ve.beyondapp.Model.CreateCalendarModel;
import com.beyond.ve.beyondapp.Model.DeleteNotiModel;
import com.beyond.ve.beyondapp.Model.FetchAllCalendar.ModelAllCalendar;
import com.beyond.ve.beyondapp.Model.ForgotPassword.ModelForgotPassword;
import com.beyond.ve.beyondapp.Model.LinkedinResponse;
import com.beyond.ve.beyondapp.Model.ModelAvtar.ModelUpdateAvtar;
import com.beyond.ve.beyondapp.Model.ModelCreateCalendar;
import com.beyond.ve.beyondapp.Model.ModelSignin;
import com.beyond.ve.beyondapp.Model.ModelSingup.LinkedinSignInRegRequest;
import com.beyond.ve.beyondapp.Model.ModelSingup.ModelSignUp;
import com.beyond.ve.beyondapp.Model.NewPasswordModelToRetrofit;
import com.beyond.ve.beyondapp.Model.NotificationResponse;
import com.beyond.ve.beyondapp.Model.OTP.ModelOTP;
import com.beyond.ve.beyondapp.Model.RejectRequest.RejectRequest;
import com.beyond.ve.beyondapp.Model.RequestListDashboard.RequestList;
import com.beyond.ve.beyondapp.Model.ResetPassword.ModelResetPassword;
import com.beyond.ve.beyondapp.Model.SuggUsers;
import com.beyond.ve.beyondapp.Model.UpdateProfile.ModelUpdateProfile;
import com.beyond.ve.beyondapp.Model.UserContacts;
import com.beyond.ve.beyondapp.Model.UserProfile;
import com.beyond.ve.beyondapp.Model.UserDetail.UserDetailModel;
import com.beyond.ve.beyondapp.Model.UserSearch.ModelUserSearch;
import com.beyond.ve.beyondapp.Model.contact.ModelContact;
import com.beyond.ve.beyondapp.ui.contact.ModelContacts;
import com.google.gson.JsonElement;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.reactivex.Single;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.PATCH;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface WebService {

    @POST("login")
    Call<ModelSignin> postSignIn(@Body HashMap<String, String> body);

    @GET("calender")
    Call<ModelAllCalendar> getCalendar(@Header("Authorization") String authHeader,@Query("userTimezone") String userTimezone);

    @POST("register")
    Call<ModelSignUp> postSignUp(@Body HashMap<String, String> body);

    @PATCH("user")
    Call<ModelUpdateProfile> postUpdateProfile(@Body HashMap<String, String> body, @Header("Authorization") String authHeader);

    @POST("forget-password")
    Call<ModelForgotPassword> postForgotPassword(@Body HashMap<String, String> body);

    @POST("verify-otp")
    Call<ModelOTP> postOTP(@Body HashMap<String, Integer> body);

    @POST("reset-password")
    Call<ModelResetPassword> postResetPassword(@Body NewPasswordModelToRetrofit newPasswordModelToRetrofit);

    @POST("calender")
    Call<ModelCreateCalendar> postCreateCalendar(@Body CreateCalendarModel newPasswordModelToRetrofit, @Header("Authorization") String authHeader);

    @GET("user/search/{fullUrl}")
    Call<ModelUserSearch> getSearchCalendar(@Path("fullUrl") String fullUrl, @Header("Authorization") String authHeader);

    @Multipart
    @POST("user/change-avatar")
    Single<ModelUpdateAvtar> updateCounselorAvtar(@Part List<MultipartBody.Part> l, @Header("Authorization") String authHeader);

    @Multipart
    @POST("user/add-media")
    Single<JsonElement> addMedia(@Part List<MultipartBody.Part> l, @Header("Authorization") String authHeader);

    @Multipart
    @POST("user/company-logo")
    Single<JsonElement> updateCompLogo(@Part List<MultipartBody.Part> l, @Header("Authorization") String authHeader);


    @GET("request/list")
    Call<RequestList> RequestList(@Header("Authorization") String authHeader);


    @GET("calender/updates")
    Call<CalendarUpdates> ContactUpdates(@Header("Authorization") String authHeader,@Query("userTimezone") String userTimezone);

    @GET("contact/list-all")
    Call<UserContacts> getContacts(@Header("Authorization") String authHeader);

    @GET("notification")
    Call<NotificationResponse> getNotification(@Header("Authorization") String authHeader);

    @POST("notification")
    Call<JsonElement> deleteNoti(@Header("Authorization") String authHeader, @Body DeleteNotiModel ids);

    @POST("notification/send-message")
    Call<JsonElement> sendNotification(@Header("Authorization") String authHeader, @Body HashMap<String, String> body);

    @DELETE("notification")
    Call<JsonElement> delAllNoti(@Header("Authorization") String authHeader);

    @GET("contact/list-all")
    Call<ModelContacts> postGetContact(@Header("Authorization") String authHeader);

    @GET("contact/suggestions")
    Call<SuggUsers> getRecentMeetings(@Header("Authorization") String authHeader);

    @GET("user/search-location/near-by")
    Call<SuggUsers> getNearBy(@Header("Authorization") String authHeader, @Query("lat") String lat, @Query("lng") String lng);

    @GET("request/send")
    Call<JsonElement> sendConnectReq(@Header("Authorization") String authHeader, @Query("to") int lat);

    @GET("request/accept")
    Call<AcceptRequest> acceptRequest(@Header("Authorization") String authHeader, @Query("to") int lat);


    @GET("request/reject")
    Call<RejectRequest> rejectRequest(@Header("Authorization") String authHeader, @Query("to") int lat);

    @GET("me")
    Call<LinkedinResponse> getLinkedinUserProfile(@Query("oauth2_access_token") String authHeader);

    @GET("user/{url}")
    Call<UserDetailModel> getUserDetail(@Path("url") String url, @Header("Authorization") String authHeader);


    @POST("register-linkedin")
    Call<ModelCreateCalendar> linkedinSignInRegister(@Body LinkedinSignInRegRequest newPasswordModelToRetrofit, @Header("Authorization") String authHeader);


    @GET("user/{url}")
    Call<UserProfile> getUser(@Path("url") String url, @Header("Authorization") String authHeader);

    @PATCH("user/change-password")
    Call<JsonElement> updatePassword(@Header("Authorization") String authHeader, @Body HashMap<String, String> body);

    @PATCH("user")
    Call<JsonElement> updateProfile(@Header("Authorization") String authHeader, @Body HashMap<String, String> body);

    @PATCH("calender/calender-status-change")
    Call<JsonElement> updateCalStatus(@Header("Authorization") String authHeader, @Body HashMap<String, String> body);

    @GET("content/{value}")
    Call<JsonElement> getContent(@Path("value") String value);

    @GET("user/search/{url}")
    Call<ModelContact> searchContact(@Path("url") String url, @Header("Authorization") String authHeader);

    @GET("/request/send")
    Call<ModelContact> sendRequest(@Query("to") Integer to);

    @Multipart
    @POST("contact/local")
    Call<ModelContact> addContactLocal(@Header("Authorization") String auth, @PartMap Map<String, RequestBody> map, @Part MultipartBody.Part image);


}