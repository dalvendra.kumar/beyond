package com.beyond.ve.beyondapp.Model;

import java.util.ArrayList;

public class NotificationResponse {
    private int status,statusCode;
    private String message,description;
    private ArrayList<Notification> payload;

    public int getStatus() {
        return status;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public String getMessage() {
        return message;
    }

    public String getDescription() {
        return description;
    }

    public ArrayList<Notification> getPayload() {
        return payload;
    }

    @Override
    public String toString() {
        return "NotificationResponse{" +
                "status=" + status +
                ", statusCode=" + statusCode +
                ", message='" + message + '\'' +
                ", description='" + description + '\'' +
                ", payload=" + payload +
                '}';
    }
}
