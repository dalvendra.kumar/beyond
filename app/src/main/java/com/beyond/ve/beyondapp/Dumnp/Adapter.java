package com.beyond.ve.beyondapp.Dumnp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.beyond.ve.beyondapp.R;

import java.util.ArrayList;
import java.util.Arrays;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class Adapter extends RecyclerView.Adapter<Adapter.Holder> {

    Context mContext;
    ArrayList<String> mArrayListString;

    public Adapter(Context mContext, ArrayList<String> mArrayListString) {
        this.mContext = mContext;
        this.mArrayListString = mArrayListString;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.view_item, parent, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
        holder.mTextView.setText(mArrayListString.get(position));

        String[] strings = mContext.getResources().getStringArray(R.array.animalss);
        ArrayList<String> list = new ArrayList<String>(Arrays.asList(strings));

        AdapterChild  mAdapter = new AdapterChild(mContext,list);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(mContext);
        holder.recyclerView.setLayoutManager(mLayoutManager);
        holder.recyclerView.setItemAnimator(new DefaultItemAnimator());
        holder.recyclerView.setAdapter(mAdapter);
    }

    @Override
    public int getItemCount() {
        return mArrayListString.size();
    }

    public class Holder extends RecyclerView.ViewHolder {

        private final TextView mTextView;

        RecyclerView recyclerView;

        public Holder(View itemView) {
            super(itemView);
            mTextView = (TextView) itemView.findViewById(R.id.tv_item);
            recyclerView = (RecyclerView) itemView.findViewById(R.id.recyclervisdfsdew);
        }
    }
}