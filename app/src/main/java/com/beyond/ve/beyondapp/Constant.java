package com.beyond.ve.beyondapp;

public interface Constant {

    Integer RESPONSE_SUCCESSFULLY = 201;
    Integer RESPONSE_SUCCESSFULLLY = 200;
    Integer RESPONSE_UNAUTHORIZED= 401;
    Integer RESPONSE_FAILURE = 400;

    interface Chat{
        String KEY_ACTION="KEY_ACTION";
        int REQUEST_CODE=201;
        int RESULT_CODE=200;
        int ACTION_HOME=101;
        int ACTION_CALANDER=102;
        int ACTION_CONTACT=103;
        int ACTION_CAMERA=104;
        int ACTION_UPLOAD=105;
        int ACTION_CANCEL=106;
    }

}
