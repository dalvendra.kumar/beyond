package com.beyond.ve.beyondapp.Adapter;

import android.app.Activity;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.beyond.ve.beyondapp.Model.FetchAllCalendar.Datum;
import com.beyond.ve.beyondapp.R;

import java.util.ArrayList;
import java.util.List;

import androidx.recyclerview.widget.RecyclerView;

public class MeetingScheduleChildAdapter extends RecyclerView.Adapter<MeetingScheduleChildAdapter.MyViewHolder> {

    private List<Datum> payLoadInsideData = new ArrayList<>();
    private Activity activity;
    private Integer listSize;

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView tv_meeting_desc, tv_meeting_title , tc_time;
        LinearLayout ll_headerTitleandDesc;
        View  view_top , view_bottom;
        public MyViewHolder(View view) {
            super(view);

            view.setOnClickListener(this);
            tv_meeting_desc = (TextView) view.findViewById(R.id.tv_meeting_desc);
            tv_meeting_title = (TextView) view.findViewById(R.id.tv_meeting_title);
            tc_time = (TextView) view.findViewById(R.id.tc_time);
            ll_headerTitleandDesc =  view.findViewById(R.id.ll_headerTitleandDesc);
            view_top =  view.findViewById(R.id.view_top);
            view_bottom =  view.findViewById(R.id.view_bottom);
        }

        @Override
        public void onClick(View view) {
        }
    }


    public MeetingScheduleChildAdapter(List<Datum> payLoadInsideData, Activity activity, Integer listSize) {
        this.listSize=listSize;
        this.payLoadInsideData.clear();
        this.payLoadInsideData = payLoadInsideData;
        this.activity=activity;
    }
 
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_schedulemeeting, parent, false);
 
        return new MyViewHolder(itemView);
    }
 
    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        if(position==0)
        {
            holder.view_top.setVisibility(View.INVISIBLE);
        }

        if(payLoadInsideData.size()  == position + 1 )
        {
            holder.view_bottom.setVisibility(View.INVISIBLE);
        }

        if(listSize>1)
        {

        }
        else
        {
            holder.view_top.setVisibility(View.INVISIBLE);
            holder.view_bottom.setVisibility(View.VISIBLE);

        }
        holder.tv_meeting_title.setText(payLoadInsideData.get(position).getEvent());
        holder.tv_meeting_desc.setText(payLoadInsideData.get(position).getDescription());
        holder.tc_time.setText(payLoadInsideData.get(position).getStart());

        if(payLoadInsideData.get(position).getLabel()!=null
                && payLoadInsideData.get(position).getLabel().equalsIgnoreCase("gray"))
        {
           holder. ll_headerTitleandDesc.setBackground(activity.getResources().getDrawable(R.drawable.bg_schedule_semigray));
            holder.tv_meeting_title.setTextColor(Color.parseColor("#acb4ba"));
            holder.tv_meeting_desc.setTextColor(Color.parseColor("#7e8c96"));

        }

    }
 
    @Override
    public int getItemCount() {
        return payLoadInsideData.size();
    }


}