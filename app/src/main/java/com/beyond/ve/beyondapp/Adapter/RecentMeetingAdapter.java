package com.beyond.ve.beyondapp.Adapter;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.beyond.ve.beyondapp.Model.SuggUsers;
import com.beyond.ve.beyondapp.R;
import com.beyond.ve.beyondapp.Utils.ImageUtils;

import java.util.List;

import androidx.recyclerview.widget.RecyclerView;
import de.hdodenhof.circleimageview.CircleImageView;

public class RecentMeetingAdapter extends RecyclerView.Adapter<RecentMeetingAdapter.MyViewHolder> {

    private List<SuggUsers.Payload> moviesList;
    private ConnectListener listener;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private CircleImageView imgProfile;
        public TextView txtName, txtCompName;
        private Button btnConnect;
        public MyViewHolder(View view) {
            super(view);
            imgProfile=view.findViewById(R.id.profile_image);
            txtName=view.findViewById(R.id.tv_name);
            txtCompName=view.findViewById(R.id.tv_compay_name);
            btnConnect=view.findViewById(R.id.btn_connect);
        }
    }


    public RecentMeetingAdapter(List<SuggUsers.Payload> moviesList,ConnectListener listener) {
        this.moviesList = moviesList;
        this.listener = listener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_recent_meeting, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        final SuggUsers.Payload payload= moviesList.get(position);

        if(!TextUtils.isEmpty(payload.getFirst_name()))
            holder.txtName.setText(payload.getFirst_name()+" "+payload.getLast_name());

         if(!TextUtils.isEmpty(payload.getCompany_name()))
         holder.txtCompName.setText(payload.getCompany_name());

        ImageUtils.displayImageFromUrl(holder.itemView.getContext(),holder.imgProfile,payload.getAvatar());


        if(!payload.isRequested())
            holder.btnConnect.setText("Requested");
        else
            holder.btnConnect.setText("Connnect");

        holder.btnConnect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!payload.isRequested())
                    listener.onConnect(payload);
            }
        });
    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }

    public interface ConnectListener {
        void onConnect(SuggUsers.Payload payload);
    }
}