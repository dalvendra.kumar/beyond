package com.beyond.ve.beyondapp.Adapter;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.beyond.ve.beyondapp.Model.AcceptRequest.AcceptRequest;
import com.beyond.ve.beyondapp.Model.RejectRequest.RejectRequest;
import com.beyond.ve.beyondapp.Model.RequestListDashboard.Payload;
import com.beyond.ve.beyondapp.R;
import com.beyond.ve.beyondapp.Utils.ImageUtils;
import com.beyond.ve.beyondapp.Web.ApiClient;
import com.beyond.ve.beyondapp.Web.WebService;
import com.beyond.ve.beyondapp.data.AppPreferencesHelper;
import com.wajahatkarim3.easyflipview.EasyFlipView;

import org.json.JSONObject;

import java.util.List;

import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RequestListAdapter extends RecyclerView.Adapter<RequestListAdapter.MyViewHolder> {
    private List<Payload> requestList;
    private ItemClickListener mClickListener;
    FragmentActivity dashboardFragment;
    boolean b =true;
    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView tv_full_name, tv_comapny_name,tv_company_details;
        TextView tv_full_name_b,tv_comapny_name_b,tv_position,tv_address,tv_contactnumber,tv_mail,tv_field_b;
        ImageView profile_image;
        EasyFlipView myEasyFlipView;
        ImageView iv_accept,iv_reject;
        public MyViewHolder(View view) {
            super(view);

            myEasyFlipView = itemView.findViewById(R.id.flipView);
            view.setOnClickListener(this);
            tv_full_name = (TextView) view.findViewById(R.id.tv_full_name);
            tv_company_details = (TextView) view.findViewById(R.id.tv_company_details);
            tv_comapny_name = (TextView) view.findViewById(R.id.tv_comapny_name);
            iv_accept = (ImageView) view.findViewById(R.id.iv_accept);
            iv_reject = (ImageView) view.findViewById(R.id.iv_reject);
            profile_image =  view.findViewById(R.id.profile_image_request);

            tv_full_name_b =  view.findViewById(R.id.tv_full_name_b);
            tv_comapny_name_b =  view.findViewById(R.id.tv_comapny_name_b);
            tv_position =  view.findViewById(R.id.tv_position);
            tv_address =  view.findViewById(R.id.tv_address);
            tv_contactnumber = view.findViewById(R.id.tv_contactnumber);
            tv_mail = view.findViewById(R.id.tv_mail);
            tv_field_b = view.findViewById(R.id.tv_field_b);

        }

        @Override
        public void onClick(View view) {
            myEasyFlipView.flipTheView();
            myEasyFlipView.setFlipDuration(1000);
            if(b)
            {
                myEasyFlipView.setFlipTypeFromLeft();
                b=false;

            }
            else
            {

                myEasyFlipView.setFlipTypeFromRight();
                b=true;

            }
            if (mClickListener != null) {
                mClickListener.onItemClick(view, getAdapterPosition());
            }
        }
    }
 
 
    public RequestListAdapter(List<Payload> requestList, FragmentActivity dashboardFragment) {
        this.requestList = requestList;
        this.dashboardFragment=dashboardFragment;
    }
 
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_recyler_view, parent, false);
        return new MyViewHolder(itemView);
    }
 
    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {


        final Payload payload = requestList.get(position);

        if(!TextUtils.isEmpty(payload.getFirstName()))
            holder.tv_full_name.setText(payload.getFirstName()+" "+ payload.getLastName());


        if(!TextUtils.isEmpty(payload.getCompanyName()))
            holder.tv_comapny_name.setText(payload.getCompanyName());

        if(!TextUtils.isEmpty(payload.getCompanyName()))
            holder.tv_comapny_name_b.setText(payload.getCompanyName());

        if(!TextUtils.isEmpty(payload.getFirstName()))
            holder.tv_full_name_b.setText(payload.getFirstName() +" "+ payload.getLastName());

        if(!TextUtils.isEmpty(payload.getPosition()))
            holder.tv_position.setText(payload.getPosition());

        if(!TextUtils.isEmpty(payload.getPosition()))
            holder.tv_address.setText(payload.getState() +" "+payload.getCountry());

        if(!TextUtils.isEmpty(payload.getField()))
            holder.tv_field_b.setText(payload.getField());

        if(!TextUtils.isEmpty(payload.getField()))
            holder.tv_company_details.setText(payload.getField());

        if(!TextUtils.isEmpty(payload.getMobile()))
            holder.tv_contactnumber.setText(payload.getMobile());

        if(!TextUtils.isEmpty(payload.getEmail()))
            holder.tv_mail.setText(payload.getEmail());

        if(!TextUtils.isEmpty(payload.getAvatar()))
            ImageUtils.displayImageFromUrlPicasso(dashboardFragment,holder.profile_image,payload.getAvatar());


        holder.iv_accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AcceptRequest(payload.getUserId());
            }
        });

        holder.iv_reject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               RejectRequest(payload.getUserId());
            }
        });

    }



    private void RejectRequest(Integer userId) {
        WebService service= ApiClient.getClient().create(WebService.class);
        Call<RejectRequest> call= service.rejectRequest("Bearer " + new AppPreferencesHelper(dashboardFragment).getUser_XAUTH() , userId);
        call.enqueue(new Callback<RejectRequest>() {
            @Override
            public void onResponse(Call<RejectRequest> call, Response<RejectRequest> response) {
                if(response.isSuccessful()){
                    Toast.makeText(dashboardFragment, ""+response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }else{
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                    } catch (Exception e) {
                    }
                }
            }
            @Override
            public void onFailure(Call<RejectRequest> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }



    private void AcceptRequest(Integer userId) {
        WebService service= ApiClient.getClient().create(WebService.class);
        Call<AcceptRequest> call= service.acceptRequest("Bearer " + new AppPreferencesHelper(dashboardFragment).getUser_XAUTH() , userId);
        call.enqueue(new Callback<AcceptRequest>() {
            @Override
            public void onResponse(Call<AcceptRequest> call, Response<AcceptRequest> response) {
                if(response.isSuccessful()){

                    Toast.makeText(dashboardFragment, ""+response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }else{
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                    } catch (Exception e) {
                    }
                }
            }
            @Override
            public void onFailure(Call<AcceptRequest> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }


    @Override
    public int getItemCount() {
        return requestList.size();
    }

    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }

    void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;

    }
}