package com.beyond.ve.beyondapp.imagePicker

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.beyond.ve.beyondapp.R
import kotlinx.android.synthetic.main.fragment_dialog_customer_item.view.*

class ImageSelectionAdapter(val mContext: Context, val cityModellist: ArrayList<String>, val clickListener: (String) -> Unit) : RecyclerView.Adapter<ImageSelectionAdapter.ResultItemViewHolder>() {

    var lisData: ArrayList<String> = cityModellist


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ResultItemViewHolder {
        val view = LayoutInflater.from(mContext).inflate(R.layout.fragment_dialog_customer_item, parent, false)
        return ResultItemViewHolder(view, clickListener)
    }

    override fun onBindViewHolder(holder: ResultItemViewHolder, position: Int) {
        holder.bind(mContext, lisData[position])
    }


    override fun getItemCount(): Int {
        return lisData.size
    }


    class ResultItemViewHolder(itemView: View, val clickListener: (String) -> Unit) : RecyclerView.ViewHolder(itemView) {

        fun bind(mContext: Context, offModel: String) {
            itemView.txt_title.text = offModel
            itemView.setOnClickListener { clickListener(offModel) }
        }
    }


}




