package com.beyond.ve.beyondapp.Adapter;

import android.app.Activity;
import android.content.Intent;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.beyond.ve.beyondapp.Model.News.Article;
import com.beyond.ve.beyondapp.R;
import com.beyond.ve.beyondapp.ui.NewsfeedActivity;
import com.squareup.picasso.Picasso;

import java.util.List;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

public class NewsFeedsAdapter extends RecyclerView.Adapter<NewsFeedsAdapter.MyViewHolder> {

    private List<Article> newsList;
    Activity  mActivity;

    public class MyViewHolder extends RecyclerView.ViewHolder  {
        public TextView title;
        public ImageView banner_image;
        ConstraintLayout constraintLayout;
        RelativeLayout rl_header;
        public MyViewHolder(View view) {
            super(view);

            banner_image = view.findViewById(R.id.banner_image);
            title = view.findViewById(R.id.titlenews);
            constraintLayout = view.findViewById(R.id.cl_header);
            rl_header=view.findViewById(R.id.rl_header);
        }


    }


    public NewsFeedsAdapter(List<Article> newsList, Activity  mActivity) {
        this.mActivity  = mActivity;
        this.newsList = newsList;
    }
 
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_news_feed, parent, false);
 
        return new MyViewHolder(itemView);
    }
 
    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

     Article article = newsList.get(position);


     if(!TextUtils.isEmpty(article.getUrlToImage()))
        Picasso.get()
                .load(article.getUrlToImage())
                .into(holder.banner_image);


        if(!TextUtils.isEmpty(article.getTitle()))
            holder.title.setText(article.getTitle());


        holder.rl_header.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent newINtent = new Intent(mActivity, NewsfeedActivity.class);
                newINtent.putExtra("newsUrl", newsList.get(position).getUrl());
                mActivity.startActivity(newINtent);

            }
        });


    }
 
    @Override
    public int getItemCount() {
        return newsList.size();
    }


}