package com.beyond.ve.beyondapp.Model;

import java.util.ArrayList;
import java.util.List;

public class CreateCalendarModel {


//
//    public String getEventName() {
//        return eventName;
//    }
//
//    public void setEventName(String eventName) {
//        this.eventName = eventName;
//    }
//
//    public String getEventLocation() {
//        return eventLocation;
//    }
//
//    public void setEventLocation(String eventLocation) {
//        this.eventLocation = eventLocation;
//    }
//
//    public Double getEventLat() {
//        return eventLat;
//    }
//
//    public void setEventLat(Double eventLat) {
//        this.eventLat = eventLat;
//    }
//
//    public Double getEventLong() {
//        return eventLong;
//    }
//
//    public void setEventLong(Double eventLong) {
//        this.eventLong = eventLong;
//    }
//
//    public String getEventDate() {
//        return eventDate;
//    }
//
//    public void setEventDate(String eventDate) {
//        this.eventDate = eventDate;
//    }
//
//    public String getEventStart() {
//        return eventStart;
//    }
//
//    public void setEventStart(String eventStart) {
//        this.eventStart = eventStart;
//    }
//
//    public String getEventEnd() {
//        return eventEnd;
//    }
//
//    public void setEventEnd(String eventEnd) {
//        this.eventEnd = eventEnd;
//    }
//
//    public Integer getEventAlert() {
//        return eventAlert;
//    }
//
//    public void setEventAlert(Integer eventAlert) {
//        this.eventAlert = eventAlert;
//    }
//
//    public String getEventLabel() {
//        return eventLabel;
//    }
//
//    public void setEventLabel(String eventLabel) {
//        this.eventLabel = eventLabel;
//    }
//
//    public List<Integer> getInvitationList() {
//        return invitations;
//    }
//
//    public void setInvitationList(ArrayList<Integer> invitationList) {
//        this.invitations = invitationList;
//    }
//


    private String event;
    private String location;
    private Double lat;
    private Double lng;
    private String date;
    private String start;
    private String end;
    private Integer alert;
    private String label;
    private String userTimezone;

    private List<Integer> invitations = new ArrayList<>();

    public String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLng() {
        return lng;
    }

    public void setLng(Double lng) {
        this.lng = lng;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public String getEnd() {
        return end;
    }

    public void setEnd(String end) {
        this.end = end;
    }

    public Integer getAlert() {
        return alert;
    }

    public void setAlert(Integer alert) {
        this.alert = alert;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public List<Integer> getInvitations() {
        return invitations;
    }

    public void setInvitations(List<Integer> invitations) {
        this.invitations = invitations;
    }

    public String getUserTimezone() {
        return userTimezone;
    }

    public void setUserTimezone(String userTimezone) {
        this.userTimezone = userTimezone;
    }
}
