package com.beyond.ve.beyondapp.ui.contact


/**
 * Created by Surender
 */

interface DialogAcceptListener {
    fun onMsgclickListener( model:Model)
    fun onContactclickListener(model:Model)
    fun onCallclickListener(model:Model)
}
