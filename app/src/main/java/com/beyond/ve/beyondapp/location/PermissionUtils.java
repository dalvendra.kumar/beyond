package com.beyond.ve.beyondapp.location;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;

import java.util.ArrayList;
import java.util.List;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;


/**
 * Created by Rahul on 5/12/17.
 */

public class PermissionUtils {
    /****************************************************************multiple permission handle block************/

    private static String permissionArrayForLocation[] = {
            Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.ACCESS_COARSE_LOCATION
    };

    public static boolean checkAllPermissionForLocation(Context mContext) {
        if (checkBuildLess23()) {
            return true;
        } else {
            List<String> listPermissionsNeeded = new ArrayList<>();
            for (int i = 0; i < permissionArrayForLocation.length; i++) {
                if (ContextCompat.checkSelfPermission(mContext, permissionArrayForLocation[i]) != PackageManager.PERMISSION_GRANTED) {
                    listPermissionsNeeded.add(permissionArrayForLocation[i]);
                }
            }
            if (!listPermissionsNeeded.isEmpty()) {
                ActivityCompat.requestPermissions((Activity) mContext, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), ConstantsValues.AllPermission);
                return false;
            }
        }
        return true;
    }

    public static boolean checkResultAllPrmission(Context mContext, String permissions[], int[] grantResults) {
        boolean isPermissionGranted = true;
        for (int i = 0; i < permissions.length; i++) {
            if (grantResults[i] != PackageManager.PERMISSION_GRANTED) {
                isPermissionGranted = false;
                break;
            }
        }
        return isPermissionGranted;
    }

    /****************************************************************END************/

    public static boolean checkBuildLess23() {
        if (Build.VERSION.SDK_INT < 23) {
            return true;
        }
        return false;
    }
}
