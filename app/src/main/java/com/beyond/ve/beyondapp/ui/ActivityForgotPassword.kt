package com.beyond.ve.beyondapp.ui

import android.content.Intent
import android.os.Bundle
import android.view.View
import com.beyond.ve.beyondapp.BaseActivity.BaseActivity
import com.beyond.ve.beyondapp.Constant
import com.beyond.ve.beyondapp.Model.ForgotPassword.ModelForgotPassword
import com.beyond.ve.beyondapp.R
import com.beyond.ve.beyondapp.Utils.CommonUtils
import com.beyond.ve.beyondapp.Utils.NetworkUtils
import com.beyond.ve.beyondapp.Web.ApiClient
import com.beyond.ve.beyondapp.Web.WebService
import kotlinx.android.synthetic.main.activity_forgot_password.*
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Response
import java.util.HashMap

class ActivityForgotPassword : BaseActivity(), View.OnClickListener {
    override fun onClick(p0: View?) {
        when (p0!!.id) {
            R.id.btn_sendCode -> {
                validation()
            }
        }
    }

    private fun validation() {
        if (NetworkUtils.isNetworkConnected(this)) {
            if (tie_emailForgotPass.text.toString() != null) {
                if (CommonUtils.isEmailValid(tie_emailForgotPass.text.toString())) {
                    PostForgotPasswordDataToServer(tie_emailForgotPass.text.toString())
                } else {
                    showSnackBar(getString(R.string.msg_enter_valid_email))
                }
            } else {
                showMessage(getString(R.string.msg_enter_valid_email))
            }
        } else {
            showMessage(R.string.msg_nointerconncetion)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportActionBar!!.hide()
        setContentView(R.layout.activity_forgot_password)
        btn_sendCode.setOnClickListener(this)
    }

    private fun PostForgotPasswordDataToServer(email: String) {
        val apiService = ApiClient.getClient().create(WebService::class.java)
        val paramObject = HashMap<String, String>()
        paramObject["email"] = email
        val call = apiService.postForgotPassword(paramObject)
        showLoading()
        call.enqueue(object : retrofit2.Callback<ModelForgotPassword> {
            override fun onResponse(call: Call<ModelForgotPassword>, response: Response<ModelForgotPassword>?) {
                if (response != null) {
                    if (response!!.body() != null) {
                        if (response.isSuccessful && (response.body().statusCode == Constant.RESPONSE_SUCCESSFULLY) || response.body().statusCode == Constant.RESPONSE_SUCCESSFULLLY) {
                            showSnackBar(response.body().message)
                            val intent = Intent(this@ActivityForgotPassword, ActivityVerificationCode::class.java)
                            intent.putExtra("activity", "forgotpassword")
                            intent.putExtra("email", email)
                            intent.putExtra("userId", "" + response.body().payload.id)
                            startActivity(intent)
                            overridePendingTransition(R.anim.entry_from_right, R.anim.exit_to_left);
                            this@ActivityForgotPassword.finish()
                        }
                    } else {
                        try {
                            val jObjError = JSONObject(response.errorBody().string())
                            showSnackBar(jObjError.getString("message"))
                        } catch (e: Exception) {
                            showSnackBar("" + e.message)

                        }
                    }
                }
                hideLoading()
            }

            override fun onFailure(call: Call<ModelForgotPassword>, t: Throwable) {
                // Log error here since request failed
                hideLoading()
                showSnackBar(getString(R.string.failure_please_try_again))
            }
        })
    }

}
