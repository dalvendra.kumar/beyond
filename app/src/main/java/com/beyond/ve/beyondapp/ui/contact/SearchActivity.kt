package com.beyond.ve.beyondapp.ui.contact

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.beyond.ve.beyondapp.BaseActivity.BaseActivity
import com.beyond.ve.beyondapp.Constant
import com.beyond.ve.beyondapp.Model.contact.ModelContact
import com.beyond.ve.beyondapp.Model.contact.Payload
import com.beyond.ve.beyondapp.R
import com.beyond.ve.beyondapp.Web.ApiClient
import com.beyond.ve.beyondapp.Web.WebService
import com.beyond.ve.beyondapp.data.AppPreferencesHelper
import kotlinx.android.synthetic.main.activity_search_dashboard.*
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Response

class SearchActivity : BaseActivity(), View.OnClickListener, TextWatcher {
    override fun afterTextChanged(p0: Editable?) {}

    override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

    override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
        searchContact(p0.toString())
    }


    var searchAdapter: SearchAdapterNew? = null
    var list: ArrayList<Payload> = java.util.ArrayList()
    val TAG: String = SearchActivity::class.java.simpleName


    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.iv_back -> {
                onBackPressed()
            }
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search_dashboard)
        init()
    }

    fun init() {


        searchAdapter = SearchAdapterNew(this@SearchActivity, list) { offerModel ->
            hitConnectRequest(offerModel.id!!)
        }
        recyclerView?.layoutManager = LinearLayoutManager(this@SearchActivity, RecyclerView.VERTICAL, false)
        recyclerView?.adapter = (searchAdapter)

        searchview.addTextChangedListener(this)
        iv_back.setOnClickListener(this)

    }


    private fun searchContact(searchString: String) {
        Log.e("searchContact", searchString)
        val apiService = ApiClient.getClient().create(WebService::class.java)
        val call = apiService.searchContact(searchString,"Bearer " + AppPreferencesHelper(this).getUser_XAUTH())
        showLoading()
        call.enqueue(object : retrofit2.Callback<ModelContact> {
            override fun onResponse(call: Call<ModelContact>, response: Response<ModelContact>?) {
                Log.e("searchContact onRes", response?.body().toString())
                hideLoading()
                if (response != null && response.body() != null) {
                    if ((response.body().statusCode == Constant.RESPONSE_SUCCESSFULLY || response.body().statusCode == Constant.RESPONSE_SUCCESSFULLLY)) {
                        list.clear()
                        list.addAll(response.body().payload)
                        searchAdapter?.notifyDataSetChanged()
                        hideLoading()
                    } else if (response.code() == Constant.RESPONSE_UNAUTHORIZED || response.code() == Constant.RESPONSE_FAILURE) {
                        hideLoading()
                        try {
                            val jObjError = JSONObject(response.errorBody().string())
                            showSnackBar(jObjError.getString("message"))
                        } catch (e: Exception) {
                            showSnackBar("" + e.message)
                        }
                    }
                }
            }

            override fun onFailure(call: Call<ModelContact>, t: Throwable) {
                hideLoading()
                Log.e("searchContact onFai", t.toString())
            }


        })
    }


    private fun hitConnectRequest(searchString: Int) {

        Log.e("hitConnectRequest", searchString.toString())
        val apiService = ApiClient.getClient().create(WebService::class.java)
        val call = apiService.sendRequest(searchString)
        showLoading()
        call.enqueue(object : retrofit2.Callback<ModelContact> {
            override fun onResponse(call: Call<ModelContact>, response: Response<ModelContact>?) {
                Log.e("hitConnectRequest onRes", response?.body().toString())
                hideLoading()
                if (response != null && response.body() != null) {
                    if ((response.body().statusCode == Constant.RESPONSE_SUCCESSFULLY || response.body().statusCode == Constant.RESPONSE_SUCCESSFULLLY)) {

                        hideLoading()
                        searchContact(searchview.text.toString())
                    } else if (response.code() == Constant.RESPONSE_UNAUTHORIZED || response.code() == Constant.RESPONSE_FAILURE) {
                        hideLoading()
                        try {
                            val jObjError = JSONObject(response.errorBody().string())
                            showSnackBar(jObjError.getString("message"))
                        } catch (e: Exception) {
                            showSnackBar("" + e.message)
                        }
                    }
                }
            }

            override fun onFailure(call: Call<ModelContact>, t: Throwable) {
                hideLoading()
                Log.e("hitConnectRequest onFai", t.toString())
            }


        })
    }

}