package com.beyond.ve.beyondapp.ui

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.NonNull
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.beyond.ve.beyondapp.BaseActivity.BaseFragment
import com.beyond.ve.beyondapp.*
import com.beyond.ve.beyondapp.Adapter.RequestListAdapter
import com.beyond.ve.beyondapp.Adapter.NewsFeedsAdapter
import com.beyond.ve.beyondapp.Adapter.UpdatesAdapter
import com.beyond.ve.beyondapp.Model.CalendarUpdatesDashboard.CalendarUpdates
import com.beyond.ve.beyondapp.Model.CalendarUpdatesDashboard.Payload
import com.beyond.ve.beyondapp.Model.News.Article
import com.beyond.ve.beyondapp.Model.RequestListDashboard.RequestList
import com.beyond.ve.beyondapp.Utils.CommonUtils
import com.beyond.ve.beyondapp.Utils.ImageUtils
import com.beyond.ve.beyondapp.Utils.NetworkUtils
import com.beyond.ve.beyondapp.Web.ApiClient
import com.beyond.ve.beyondapp.Web.HttpGetRequest
import com.beyond.ve.beyondapp.Web.WebService
import com.beyond.ve.beyondapp.data.AppPreferencesHelper
import com.beyond.ve.beyondapp.ui.barcode.BarcodeActivity
import com.beyond.ve.beyondapp.ui.contact.SearchActivity
import com.firebase.client.Firebase
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.FirebaseDatabase
import com.google.gson.JsonElement
import kotlinx.android.synthetic.main.fragment_dashboard.*
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Response
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

class DashboardFragment : BaseFragment(), View.OnClickListener, HttpGetRequest.RequestListener, UpdatesAdapter.UpdatesListener, SwipeRefreshLayout.OnRefreshListener {
    override fun onRefresh() {
        if(NetworkUtils.isNetworkConnected(context)){
            refreshedCount=0
            UpdateCalendar()
            RequestList()
            HttpGetRequest(this).execute("https://newsapi.org/v2/top-headlines?country=us&apiKey=b2fbad7eb4d14595b086f0be524969ac")
        }else{
            showSnackBar(getString(R.string.msg_nointerconncetion))
            swipeLayout.isRefreshing=false
        }

    }

    override fun onAccept(payload: Payload?) {
        updateCalUpdate(payload,false)
    }

    override fun onReject(payload: Payload?) {
        updateCalUpdate(payload,true)
    }

    override fun onComplete(response: String?) {

        refreshedCount++
        if(refreshedCount==3)
            swipeLayout.isRefreshing=false

        if(response!=null && response.length>0) {
            var jsonPojo = JSONObject(response)
            var jsonArrray = jsonPojo.getJSONArray("articles")
            for (i in 0..jsonArrray.length() - 1) {

                var articale = Article()
                var jsonInside = jsonArrray.getJSONObject(i)

                if (jsonInside != null) {
                    try {
                        newlist!!.clear()
                        articale.author = jsonInside.getString("author")
                        articale.title = jsonInside.getString("title")
                        articale.description = jsonInside.getString("description")
                        articale.url = jsonInside.getString("url")
                        articale.urlToImage = jsonInside.getString("urlToImage")
                        articale.content = jsonInside.getString("content")
                        newlist.add(articale)
                    } catch (e: java.lang.Exception) {
                        e.printStackTrace()
                    }
                }

            }
            mAdapterNews = NewsFeedsAdapter(newlist, activity)
            val mLayoutManager = LinearLayoutManager(activity)
            rv_news_feeds.setLayoutManager(mLayoutManager)
            mLayoutManager.setReverseLayout(true);
            rv_news_feeds.setItemAnimator(DefaultItemAnimator())
            rv_news_feeds.setLayoutManager(LinearLayoutManager(activity))
            rv_news_feeds.setAdapter(mAdapterNews)
        }
    }

    override fun onClick(p0: View?) {
        when(p0!!.id){

            R.id.profile_image->
            {
                val intent = Intent(activity, ShowProfileActivity::class.java)
                startActivity(intent)
            }

            R.id.tv_updates->
            {

                ManageUpdateView();
            }

            R.id.iv_ -> {
                val intent = Intent(activity, BarcodeActivity::class.java)
                startActivity(intent)
            }

            R.id.tv_search -> {
                val intent = Intent(activity, SearchActivity::class.java)
                startActivity(intent)
            }
            R.id.tv_news_feeds->
            {

                ManageNewsFeedsAction()
            }


            R.id.iv_updownarrow_updates->
            {

                ManageUpdateView();
            }
            R.id.iv_updownnews_updates->
            {

                ManageNewsFeedsAction()
            }
            R.id.notification->
            {
                val points = IntArray(2)
                notification.getLocationInWindow(points)
                val intent = Intent(activity, NotificationActivity::class.java)
                intent.putExtra("pointerYLoc",points[1])
                startActivity(intent)

            }

        }
    }

    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    private var mAdapter: RequestListAdapter? = null
    private var mAdapterUpdates: UpdatesAdapter? = null
    private var mAdapterNews: NewsFeedsAdapter? = null
    var newlist = ArrayList<Article>()
    var calUpdatesList = ArrayList<Payload>()
    var helper: AppPreferencesHelper? = null
    var bolUpdats = true
    var bolNewsFeeds = false
    var refreshedCount=0;
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_dashboard, container, false)
    }

    companion object {

        fun newInstance(title: String): Fragment {
            val fragment = DashboardFragment()
            val args = Bundle()
            args.putString("title", "title")
            fragment.arguments = args
            return fragment
        }}

    private fun RequestList() {

        val apiService = ApiClient.getClient().create(WebService::class.java)
        var pref = AppPreferencesHelper(activity)
        val call = apiService.RequestList( "Bearer " + pref.user_XAUTH)
        showLoading()
        call.enqueue(object : retrofit2.Callback<RequestList> {
            override fun onResponse(call: Call<RequestList>, response: Response<RequestList>?) {
                refreshedCount++
                if(refreshedCount==3)
                    swipeLayout.isRefreshing=false
                if (response != null) {
                    if (response.isSuccessful && (response.body().statusCode == Constant.RESPONSE_SUCCESSFULLY || response.body().statusCode == Constant.RESPONSE_SUCCESSFULLLY)) {
                        hideLoading()
                        if(response.body()!=null && response.body().payload!=null && response.body().payload.size>0) {
                            tv_req.visibility=View.VISIBLE
                            mAdapter = RequestListAdapter(response.body().payload,activity)
                            val mLayoutManager = LinearLayoutManager(activity)
                            rv_request_list.setLayoutManager(mLayoutManager)
                            mLayoutManager.setReverseLayout(true);
                            rv_request_list.setItemAnimator(DefaultItemAnimator())
                            rv_request_list.setLayoutManager(LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, true))
                            mLayoutManager.setStackFromEnd(true)
                            rv_request_list.setAdapter(mAdapter)
                        }
                    } else if (response.code() == Constant.RESPONSE_UNAUTHORIZED || response.code() == Constant.RESPONSE_FAILURE) {
                        hideLoading()
                        try {
                            val jObjError = JSONObject(response.errorBody().string())
                            showSnackBar(jObjError.getString("message"))
                        } catch (e: Exception) {
                            showSnackBar("" + e.message)
                        }
                    }
                }
            }

            override fun onFailure(call: Call<RequestList>, t: Throwable) {
                // Log error here since request failed
                hideLoading()
                showSnackBar(getString(R.string.failure_please_try_again))


            }


        })
    }

    private fun UpdateCalendar() {
        val apiService = ApiClient.getClient().create(WebService::class.java)
        var pref = AppPreferencesHelper(activity)
        val call = apiService.ContactUpdates( "Bearer " + pref.user_XAUTH,TimeZone.getDefault().id)
        showLoading()
        call.enqueue(object : retrofit2.Callback<CalendarUpdates> {
            override fun onResponse(call: Call<CalendarUpdates>, response: Response<CalendarUpdates>?) {

                refreshedCount++
                if(refreshedCount==3)
                    swipeLayout.isRefreshing=false

                if (response != null) {
                    if (response.isSuccessful && (response.body().statusCode == Constant.RESPONSE_SUCCESSFULLY || response.body().statusCode == Constant.RESPONSE_SUCCESSFULLLY)) {
                        hideLoading()
                        if(response.body()!=null && response.body().payload!=null && response.body().payload.size>0) {

                            tv_updates.visibility = View.VISIBLE
                            iv_updownarrow_updates.visibility=View.VISIBLE
                            calUpdatesList.clear()
                            calUpdatesList=response.body().payload
                            mAdapterUpdates = UpdatesAdapter(calUpdatesList,this@DashboardFragment)
                            val mLayoutManagerUpdates = LinearLayoutManager(activity)
                            rv_updates.setLayoutManager(mLayoutManagerUpdates)
                            rv_updates.setItemAnimator(DefaultItemAnimator())
                            if (response.body().payload.size < 10) {
                            } else {
                                rv_updates.layoutParams.height = 800
                            }
                            rv_updates.setAdapter(mAdapterUpdates)
                            refreshedCount++
                            if(refreshedCount==3)
                                swipeLayout.isRefreshing=false
                        }
                    } else if (response.code() == Constant.RESPONSE_UNAUTHORIZED || response.code() == Constant.RESPONSE_FAILURE) {
                        hideLoading()
                        try {
                            val jObjError = JSONObject(response.errorBody().string())
                            showSnackBar(jObjError.getString("message"))
                        } catch (e: Exception) {
                            showSnackBar("" + e.message)
                        }

                    }
                }
            }

            override fun onFailure(call: Call<CalendarUpdates>, t: Throwable) {
                // Log error here since request failed
                hideLoading()
                showSnackBar(getString(R.string.failure_please_try_again))
            }
        })
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        helper = AppPreferencesHelper(activity)
        tv_updates.setOnClickListener(this)
        tv_news_feeds.setOnClickListener(this)
        iv_updownarrow_updates.setOnClickListener(this)
        iv_updownnews_updates.setOnClickListener(this)
        iv_.setOnClickListener(this)
        notification.setOnClickListener (this)

        profile_image.layoutParams.height =CommonUtils.returmWindowWidth(activity)/ 10
        profile_image.layoutParams.width = CommonUtils.returmWindowWidth(activity) / 10

        profile_image.setOnClickListener(this)

        val helper = AppPreferencesHelper(activity!!)
        ImageUtils.displayImageFromUrl(activity, profile_image, helper.user_avatar)

        tv_search.setOnClickListener(this)

        if(NetworkUtils.isNetworkConnected(activity)){
            UpdateCalendar()
            RequestList()
            HttpGetRequest(this).execute("https://newsapi.org/v2/top-headlines?country=us&apiKey=b2fbad7eb4d14595b086f0be524969ac")
        }else{
            showSnackBar(getString(R.string.msg_nointerconncetion))
        }
        swipeLayout.setOnRefreshListener(this)

    }



    override fun onResume() {
        super.onResume()

        if(helper!!.userIdFirebase.length<=0)
            firebaseAuth()
        else
            try {
                var baseUrl = FirebaseDatabase.getInstance().reference.toString()
                var refOnlineStaus = Firebase(baseUrl + "/users/" + helper!!.userIdFirebase + "/credentials")
                refOnlineStaus!!.child("isOnline").setValue(true)
                refOnlineStaus!!.child("lastSeen").setValue(System.currentTimeMillis().toString())
            }catch (e: java.lang.Exception){
                e.message
            }
    }

    private fun ManageNewsFeedsAction() {
        if(bolNewsFeeds) {
            iv_updownnews_updates.animate().rotation(0F).start();
            bolNewsFeeds = false
            rv_news_feeds.setVisibility(View.VISIBLE);
            rv_news_feeds.setAlpha(0.0f);
            rv_news_feeds.animate()
                    .translationY(activity!!.view.getHeight().toFloat())
                    .alpha(1.0f)
                    .setListener(null);
        }else{
            bolNewsFeeds = true
            iv_updownnews_updates.animate().rotation(180F).start();
            rv_news_feeds.animate()
                    .translationY(0F)
                    .alpha(0.0f)
                    .setListener(object : AnimatorListenerAdapter() {
                        override fun onAnimationEnd(animation: Animator) {
                            super.onAnimationEnd(animation)
                            rv_news_feeds.visibility = View.GONE
                        }
                    })
        }

    }

    private fun ManageUpdateView() {
        if(bolUpdats) {
            iv_updownarrow_updates.animate().rotation(180F).start();
            bolUpdats = false
            ll_updates.setVisibility(View.VISIBLE);
            ll_updates.animate()
                    .translationY(activity!!.view.getHeight().toFloat())
                    .alpha(1.0f)
                    .setListener(null);
        }

        else
        {
            iv_updownarrow_updates.animate().rotation(0F).start();
            bolUpdats = true
            ll_updates.animate()
                    .translationY(0F)
                    .alpha(0.0f)
                    .setListener(object : AnimatorListenerAdapter() {
                        override fun onAnimationEnd(animation: Animator) {
                            super.onAnimationEnd(animation)
                            ll_updates.visibility = View.GONE
                        }
                    })}

    }


    private fun firebaseAuth() {
        val email: String
        val password: String
        email = helper!!.user_Id!!.toString() + "@beyond.com"
        password = "12345678"
        val mAuth = FirebaseAuth.getInstance()
        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this!!.activity!!, OnCompleteListener<AuthResult> { task ->
                    if (task.isSuccessful) {
                        val uid = task.result!!.user.uid
                        helper!!.userIdFirebase = uid

                        var baseUrl = FirebaseDatabase.getInstance().reference.toString()
                        var refOnlineStaus = Firebase(baseUrl + "/users/" + uid + "/credentials")
                        try {
                            refOnlineStaus!!.child("isOnline").setValue(true)
                            refOnlineStaus!!.child("lastSeen").setValue(System.currentTimeMillis().toString())
                        }catch (e: java.lang.Exception){
                            e.message
                        }
                    } else {
                        // If sign in fails, display a message to the user.
                        Log.w("firebaseAuth", "signInWithEmail:failure", task.exception)
                        if(task.exception!!.message.equals("There is no user record corresponding to this identifier. The user may have been deleted.",true))
                            addfirebaseUser(email)
                    }
                })
    }


    private fun addfirebaseUser(email:String) {
        Firebase.setAndroidContext(activity)
        val password = "12345678"
        val mAuth = FirebaseAuth.getInstance()
        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this!!.activity!!, object:OnCompleteListener<AuthResult> {
                    override fun onComplete(@NonNull task:Task<AuthResult>) {
                        if (!task.isSuccessful()){
                            Log.e("Signup Error", "onCancelled", task.getException())
                        }else{
                            val user = mAuth.getCurrentUser()
                            val uid = user!!.getUid()
                            Log.e("Fire base User id", uid)
                            var pref=AppPreferencesHelper(activity)
                            pref.userIdFirebase=uid
                            try {
                                var baseUrl = FirebaseDatabase.getInstance().reference.toString()
                                var refOnlineStaus = Firebase(baseUrl + "/users/" + uid + "/credentials")
                                refOnlineStaus!!.child("isOnline").setValue(true)
                                refOnlineStaus!!.child("lastSeen").setValue(System.currentTimeMillis().toString())
                            }catch (e: java.lang.Exception){
                                e.message
                            }
                            registerUser(uid,pref.user_LastName+" "+pref.user_LastName,email,pref.user_avatar)
                        }
                    }
                })
    }

    private fun registerUser(fireBaseId: String, name: String, email: String, urlProfileImg: String) {
        var baseUrl = FirebaseDatabase.getInstance().reference.toString()
        val refUsers = Firebase(baseUrl+"/users/" + fireBaseId + "/credentials/")
        val map = HashMap<String, String>()
        map["email"] = email
        map["isOnline"] = "false"
        map["isTyping"] = "false"
        map["lastSeen"] = "0"
        map["name"] = name
        map["profilePicLink"] = urlProfileImg
        refUsers.setValue(map)
    }

    private fun updateCalUpdate(payload: Payload?,isReject: Boolean ){
        var body =HashMap<String,String>()
        body.put("user_id", payload!!.userId.toString())
        body.put("calender_id",payload.id.toString())
        if(isReject)
            body.put("status","2")
        else
            body.put("status","1")

        val apiService = ApiClient.getClient().create(WebService::class.java)
        var pref = AppPreferencesHelper(activity)
        val call = apiService.updateCalStatus( "Bearer " + pref.user_XAUTH,body)
        showLoading()
        call.enqueue(object : retrofit2.Callback<JsonElement> {
            override fun onResponse(call: Call<JsonElement>, response: Response<JsonElement>?) {
                hideLoading()
                if (response != null) {
                    if (response.isSuccessful) {
                        if(isReject){
                            payload.status_update="2"
                            calUpdatesList.remove(payload)
                        }else {
                            payload.status_update="1"
                        }
                        mAdapterUpdates!!.notifyDataSetChanged()
                    } else if (response.code() == Constant.RESPONSE_UNAUTHORIZED || response.code() == Constant.RESPONSE_FAILURE) {
                        try {
                            val jObjError = JSONObject(response.errorBody().string())
                            showSnackBar(jObjError.getString("message"))
                        } catch (e: Exception) {
                            showSnackBar("" + e.message)
                        }
                    }
                }
            }

            override fun onFailure(call: Call<JsonElement>, t: Throwable) {
                // Log error here since request failed
                hideLoading()
                showSnackBar(getString(R.string.failure_please_try_again))
            }
        })
    }
}
