package com.beyond.ve.beyondapp.ui.contact;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import androidx.recyclerview.widget.RecyclerView;


/**
 * Adapter holding a list of animal names of type String. Note that each item_user_media must be unique.
 */
public abstract class AnimalsAdapter<VH extends RecyclerView.ViewHolder>
    extends RecyclerView.Adapter<VH> {
  private ArrayList<Model> items = new ArrayList<Model>();
  private ArrayList<Model> mainList = new ArrayList<Model>();

  public AnimalsAdapter() {
    setHasStableIds(true);
  }

  public void add(Model object) {
    items.add(object);
    notifyDataSetChanged();
  }

  public void add(int index, Model object) {
    items.add(index, object);
    notifyDataSetChanged();
  }

  public void addAll(Collection<? extends Model> collection) {
    if (collection != null) {
      items.addAll(collection);
//      notifyDataSetChanged();
    }
  }

  public void addAllMain(Collection<? extends Model> collection) {
    if (collection != null) {
      mainList.addAll(collection);
    }
  }

  public void addAll(Model... items) {
    addAll(Arrays.asList(items));
  }

  public void clear() {
    items.clear();
    notifyDataSetChanged();
  }

  public void remove(String object) {
    items.remove(object);
    notifyDataSetChanged();
  }

  public Model getItem(int position) {
    return items.get(position);
  }

  public List<Model> getItems() {
    return items;
  }

  public List<Model> getItemsMain() {
    return mainList;
  }

  @Override
  public long getItemId(int position) {
    return getItem(position).hashCode();
  }

  @Override
  public int getItemCount() {
    return items.size();
  }
}
