package com.beyond.ve.beyondapp.Model;

import java.util.ArrayList;

public class UserProfile {
    private int status,statusCode;
    private String message,description;
    private Payload payload;

    public int getStatus() {
        return status;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public String getMessage() {
        return message;
    }

    public String getDescription() {
        return description;
    }

    public Payload getPayload() {
        return payload;
    }

    public class Payload {
        private int id,is_verified,isActive;
        private double lat,lng;
        private String first_name,last_name,email,avatar,company_name,company_logo,field,position,state,country,linkedIn_id,website,mobile,bio,chat_id,industry,created_at,updated_at;
        private ArrayList<Media> media;

        public int getId() {
            return id;
        }

        public int getIs_verified() {
            return is_verified;
        }

        public int getIsActive() {
            return isActive;
        }

        public void setIsActive(int isActive) {
            this.isActive = isActive;
        }

        public double getLat() {
            return lat;
        }

        public double getLng() {
            return lng;
        }

        public String getFirst_name() {
            return first_name;
        }

        public String getLast_name() {
            return last_name;
        }

        public String getEmail() {
            return email;
        }

        public String getAvatar() {
            return avatar;
        }

        public String getCompany_name() {
            return company_name;
        }

        public String getCompany_logo() {
            return company_logo;
        }

        public String getField() {
            return field;
        }

        public String getPosition() {
            return position;
        }

        public String getState() {
            return state;
        }

        public String getCountry() {
            return country;
        }

        public String getLinkedIn_id() {
            return linkedIn_id;
        }

        public String getWebsite() {
            return website;
        }

        public String getMobile() {
            return mobile;
        }

        public String getBio() {
            return bio;
        }

        public String getChat_id() {
            return chat_id;
        }

        public String getIndustry() {
            return industry;
        }

        public String getCreated_at() {
            return created_at;
        }

        public String getUpdated_at() {
            return updated_at;
        }

        public ArrayList<Media> getMedia() {
            return media;
        }

        @Override
        public String toString() {
            return "Payload{" +
                    "id=" + id +
                    ", is_verified=" + is_verified +
                    ", isActive=" + isActive +
                    ", lat=" + lat +
                    ", lng=" + lng +
                    ", first_name='" + first_name + '\'' +
                    ", last_name='" + last_name + '\'' +
                    ", email='" + email + '\'' +
                    ", avatar='" + avatar + '\'' +
                    ", company_name='" + company_name + '\'' +
                    ", company_logo='" + company_logo + '\'' +
                    ", field='" + field + '\'' +
                    ", position='" + position + '\'' +
                    ", state='" + state + '\'' +
                    ", linkedIn_id='" + linkedIn_id + '\'' +
                    ", website='" + website + '\'' +
                    ", mobile='" + mobile + '\'' +
                    ", bio='" + bio + '\'' +
                    ", chat_id='" + chat_id + '\'' +
                    ", industry='" + industry + '\'' +
                    ", created_at='" + created_at + '\'' +
                    ", updated_at='" + updated_at + '\'' +
                    ", media=" + media +
                    '}';
        }
    }

    public class Media {
        private int id,user_id;
        private String media,created_at,updated_at;

        public int getId() {
            return id;
        }

        public int getUser_id() {
            return user_id;
        }

        public String getMedia() {
            return media;
        }

        public String getCreated_at() {
            return created_at;
        }

        public String getUpdated_at() {
            return updated_at;
        }

        @Override
        public String toString() {
            return "Media{" +
                    "id=" + id +
                    ", user_id=" + user_id +
                    ", media='" + media + '\'' +
                    ", created_at='" + created_at + '\'' +
                    ", updated_at='" + updated_at + '\'' +
                    '}';
        }
    }

    @Override
    public String toString() {
        return "UserProfile{" +
                "status=" + status +
                ", statusCode=" + statusCode +
                ", message='" + message + '\'' +
                ", description='" + description + '\'' +
                ", payload=" + payload +
                '}';
    }
}
