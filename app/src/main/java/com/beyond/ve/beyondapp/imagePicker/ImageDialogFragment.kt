package com.beyond.ve.beyondapp.imagePicker

import android.content.Context
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.*
import android.widget.TextView
import androidx.fragment.app.DialogFragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.beyond.ve.beyondapp.R


class ImageDialogFragment : DialogFragment(), View.OnClickListener {
    var citySelectionAdapter: ImageSelectionAdapter? = null
    private var rv_countryList: RecyclerView? = null
    private var header: TextView? = null


    private var mView: View? = null
    private var mCitySelectionInterface: imageSelectionInterface? = null
    private var fromWhere: Int? = null
    private var headerText: String? = null
    private var dataList = ArrayList<String>()
    private var TAG: String? = ImageDialogFragment::class.java.name


    companion object {

        fun newInstance(heading: String, regionId: Int, dataList: ArrayList<String>): ImageDialogFragment {


            val args = Bundle()
            args.putInt("regionId", regionId)
            args.putString("heading", heading)
            args.putSerializable("datalist", dataList)
            val fragment = ImageDialogFragment()
            fragment.arguments = args
            return fragment
        }
    }

    override fun onClick(v: View?) {
        when (v!!.id) {

        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        fromWhere = arguments?.getInt("regionId")
        headerText = arguments?.getString("heading")
        dataList = (arguments?.getSerializable("datalist") as? ArrayList<String>)!!


    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        mView = inflater.inflate(R.layout.fragment_vehicle_dialog, container, false)
        dialog?.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog?.window!!.setBackgroundDrawable(ColorDrawable(android.graphics.Color.TRANSPARENT))
        dialog?.window!!.setGravity(Gravity.CENTER)
        dialog?.setCancelable(false)

        return mView
    }

    override fun onViewCreated(mView: View, savedInstanceState: Bundle?) {
        super.onViewCreated(mView, savedInstanceState)
        rv_countryList = mView.findViewById(R.id.rv_countryList) as RecyclerView
        header = mView.findViewById(R.id.header) as TextView

        header!!.setText(headerText)

        initializeUIComponent()
    }


    private fun initializeUIComponent() {
        citySelectionAdapter = ImageSelectionAdapter(activity!!, dataList!!) { offerModel ->

            mCitySelectionInterface!!.selectedText(offerModel, this!!.fromWhere)
        }
        val manager = LinearLayoutManager(activity, RecyclerView.VERTICAL, false)
        rv_countryList?.layoutManager = manager
        rv_countryList?.adapter = citySelectionAdapter
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        try {
            mCitySelectionInterface = context as imageSelectionInterface
        } catch (e: ClassCastException) {
            throw ClassCastException("Error in retrieving data. Please try again")
        }
    }

    override fun onDetach() {
        mCitySelectionInterface = null
        super.onDetach()
    }


}


//

interface imageSelectionInterface {
    fun selectedText(bean: String, fromWhere: Int?)
}
