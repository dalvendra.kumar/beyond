package com.beyond.ve.beyondapp.ui

import android.Manifest
import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.database.Cursor
import android.graphics.Bitmap
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.os.Handler
import android.provider.DocumentsContract
import android.provider.MediaStore
import android.text.TextUtils
import android.view.Gravity
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import com.beyond.ve.beyondapp.BaseActivity.BaseActivity
import com.beyond.ve.beyondapp.Constant
import com.beyond.ve.beyondapp.Model.ModelAvtar.ModelUpdateAvtar
import com.beyond.ve.beyondapp.Model.UpdateProfile.ModelUpdateProfile
import com.beyond.ve.beyondapp.R
import com.beyond.ve.beyondapp.Utils.CommonUtils
import com.beyond.ve.beyondapp.Utils.Permissons
import com.beyond.ve.beyondapp.Web.ApiClient
import com.beyond.ve.beyondapp.Web.WebService
import com.beyond.ve.beyondapp.data.AppPreferencesHelper
import com.squareup.picasso.Picasso
import io.reactivex.SingleObserver
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_profile.*
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Response
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*




internal  var photoFile: File?=null
internal  var photoURI: Uri?=null
private val integerFileHashMap = HashMap<Int, File>()
private val integerURIHashMap = HashMap<Int, Uri>()
var isExit=false;

class ActivityProfile : BaseActivity() , View.OnClickListener{
    override fun onClick(p0: View?) {
        when(p0!!.id)
        {
            R.id.btn_profileSave->
            {
                //Check validation for all mandatory fields
                validation();
            }

            R.id.camera->
            {
                if(Permissons.Check_CAMERA(this)) {
                    if(Permissons.Check_STORAGE(this)) {
                        selectImage(1)
                    }else
                    {
                        Permissons.Request_STORAGE(this,6)
                    }
                }
                else {
                    Permissons.Request_CAMERA(this, 5)
                }
            }

        }
    }

    private fun validation() {
        if (!TextUtils.isEmpty(tie_companyName.text.toString())) {
            if (!TextUtils.isEmpty(tie_field.text.toString())) {
                if (!TextUtils.isEmpty(tie_position.text.toString())) {
                    if (!TextUtils.isEmpty(tie_country.text.toString())) {
                        if (!TextUtils.isEmpty(tie_mobile.text.toString())) {
                            PostUserProfileData(tie_companyName.text.toString(),
                                    tie_field.text.toString(),tie_position.text.toString(),
                                    tie_country.text.toString(),
                                    tie_mobile.text.toString())
                        } else {
                            showSnackBar(getString(R.string.msg_enter_mobile_number))
                        }
                    } else {
                        showSnackBar(getString(R.string.msg_enter_country))
                    }
                } else {
                    showSnackBar(getString(R.string.msg_enter_position))
                }
            } else {
                showSnackBar(getString(R.string.msg_enter_field))
            }
        } else {
            showSnackBar(getString(R.string.msg_enter_company_name))
        }
    }



    private fun PostUserProfileData(mCompanyName: String, mField: String, mPosition: String,mCountry: String, mMobile: String) {
        val apiService = ApiClient.getClient().create(WebService::class.java)
        val paramObject = HashMap<String, String>()
        var pref = AppPreferencesHelper(this)
        paramObject["company_name"] = mCompanyName
        paramObject["field"] = mField
        paramObject["position"] = mPosition
        paramObject["chat_id"] = pref.userIdFirebase
        paramObject["country"] = mCountry
        paramObject["mobile"] = mMobile
        paramObject.put("device_token", pref.deviceToken)

        val call = apiService.postUpdateProfile(paramObject,"Bearer " + pref.user_XAUTH)
        showLoading()
        call.enqueue(object : retrofit2.Callback<ModelUpdateProfile> {
            override fun onResponse(call: Call<ModelUpdateProfile>, response: Response<ModelUpdateProfile>?) {

                if (response != null) {
                    if (response.isSuccessful && (response.body().statusCode == Constant.RESPONSE_SUCCESSFULLY || response.body().statusCode == Constant.RESPONSE_SUCCESSFULLLY) ) {
                        hideLoading()
                        showSnackBar(response.body().message)
                        val intent = Intent(this@ActivityProfile, Dashboad::class.java)
                        intent.putExtra("activity","signup")
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent)
                        overridePendingTransition(R.anim.entry_from_right, R.anim.exit_to_left)
                        this@ActivityProfile.finish()

                    } else if (response.code() == Constant.RESPONSE_UNAUTHORIZED || response.code() == Constant.RESPONSE_FAILURE) {
                        hideLoading()

                        try {
                            val jObjError = JSONObject(response.errorBody().string())
                            showSnackBar(jObjError.getString("message"))
                        } catch (e: Exception) {
                            showSnackBar("" + e.message)

                        }


                    }
                }
            }

            override fun onFailure(call: Call<ModelUpdateProfile>, t: Throwable) {
                // Log error here since request failed
                hideLoading()
                showSnackBar(getString(R.string.failure_please_try_again))


            }


        })
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportActionBar!!.hide()
        setContentView(R.layout.activity_profile)


        if(!TextUtils.isEmpty(intent.getStringExtra("email")))
            tie_email.setText(intent.getStringExtra("email"))
        txt_singin_lebel.setText("Hi, "+AppPreferencesHelper(applicationContext).user_Name)
        btn_profileSave.setOnClickListener(this)
        camera.setOnClickListener(this)
    }
    fun selectImage(imagePosition: Int) {
        val items = arrayOf<CharSequence>("Take Photo", "Choose from Library", "Cancel")
        val title = TextView(this)
        title.text = "Add Photo!"
        title.setBackgroundColor(Color.BLACK)
        title.setPadding(10, 15, 15, 10)
        title.gravity = Gravity.CENTER
        title.setTextColor(Color.WHITE)
        title.textSize = 22f


        val builder = AlertDialog.Builder(this
        )


        builder.setCustomTitle(title)

        // builder.setTitle("Add Photo!");
        builder.setItems(items) { dialog, item ->
            if (items[item] == "Take Photo") {
                if (ContextCompat.checkSelfPermission(this!!,Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(this!!,arrayOf(Manifest.permission.CAMERA),0)
                } else {
                    captureProfilePhoto(2)
                }

            } else if (items[item] == "Choose from Library") {
                if (this?.let {
                            ContextCompat.checkSelfPermission(
                                    it,
                                    Manifest.permission.READ_EXTERNAL_STORAGE
                            )
                        } != PackageManager.PERMISSION_GRANTED
                ) {

                    if (ActivityCompat.shouldShowRequestPermissionRationale(
                                    this!!,
                                    Manifest.permission.READ_EXTERNAL_STORAGE
                            )
                    ) {

                    } else {
                        ActivityCompat.requestPermissions(
                                this!!,
                                arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE),
                                0
                        )
                    }

                }
                if (ContextCompat.checkSelfPermission(
                                this!!,
                                android.Manifest.permission.WRITE_EXTERNAL_STORAGE
                        ) != PackageManager.PERMISSION_GRANTED
                ) {

                    if (ActivityCompat.shouldShowRequestPermissionRationale(
                                    this!!,
                                    android.Manifest.permission.WRITE_EXTERNAL_STORAGE
                            )
                    ) {

                    } else {
                        ActivityCompat.requestPermissions(
                                this!!,
                                arrayOf(android.Manifest.permission.WRITE_EXTERNAL_STORAGE),
                                0
                        )
                    }

                } else {
                    browseProfilePhoto(imagePosition)


                }

            } else if (items[item] == "Cancel") {
                dialog.dismiss()
            }
        }
        builder.show()
    }


    private fun browseProfilePhoto(imagePosition: Int) {
        val intent = Intent()
        intent.type = "image/*"
        intent.action = Intent.ACTION_GET_CONTENT
        intent.putExtra("imageNumber", imagePosition)
        startActivityForResult(Intent.createChooser(intent, "Select picture"), imagePosition)
    }



    private fun captureProfilePhoto(imagePosition: Int) {


        val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        photoFile
        try {
            photoFile = createImageFile()
        } catch (ex: IOException) {
            ex.printStackTrace()
        }
        // Continue only if the File was successfully created
        if (photoFile != null) {
            photoURI = FileProvider.getUriForFile(
                    this!!,
                    getApplicationContext().getPackageName() + ".provider",
                    photoFile!!
            )

            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)

            takePictureIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION
                    or Intent.FLAG_GRANT_WRITE_URI_PERMISSION)
            takePictureIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);

            startActivityForResult(takePictureIntent, imagePosition )

        }
    }
    lateinit var  imageFilePath : String ;

    @Throws(IOException::class)
    private fun createImageFile(): File? {
        val timeStamp =
                SimpleDateFormat("yyyyMMdd_HHmmss",
                        Locale.getDefault()).format( Date());
        val imageFileName = "IMG_" + timeStamp + "_";
        val storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        val image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        imageFilePath = image.getAbsolutePath();
        return image;

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when(requestCode)
        {
            1->
            {
                if (resultCode == Activity.RESULT_OK) {
                    if (data != null) {
                        handleGalleryIntent(data,1)
                    }
                    else {
                        showMessage("Fail to upload image ")
                    }
                }
            }
            2->
            {
                if (resultCode == Activity.RESULT_OK) {
                    grabImage(profile_image, 2,data)
                }
            }
        }

    }

    fun grabImage(imageView: ImageView, pos: Int , data: Intent?) {

        contentResolver.notifyChange(photoURI, null)
        val cr = contentResolver
        val bitmap: Bitmap
        try {

            bitmap =   MediaStore.Images.Media.getBitmap(getContentResolver(), FileProvider.getUriForFile(this,getApplicationContext().getPackageName() + ".provider",  File(imageFilePath)));

            profile_image.setImageBitmap(bitmap)

            val uri = getImageUri(applicationContext, bitmap)
            val file = File(getRealPathFromURI(applicationContext, uri))
            integerFileHashMap[1] = file
            imageView.setImageBitmap(bitmap)

            HitServerToUpdateAvtar()

        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    private fun handleGalleryIntent(data: Intent, imagePosition: Int) {
        try {
            val selectedImageUri = data.data
            val file = File(getRealPathForGalley(this, selectedImageUri))
            integerFileHashMap.put(imagePosition, file)
            integerURIHashMap.put(imagePosition, selectedImageUri)
            val imageBitmap = MediaStore.Images.Media.getBitmap(this!!.contentResolver, selectedImageUri)
            if (imagePosition == 1)
                Picasso.get().load(data.data).into(profile_image)

            HitServerToUpdateAvtar()
        } catch (e: IOException) {
            e.printStackTrace()
        }

    }

    companion object {

        fun getRealPathForGalley(activity: Activity?, captureImageUri: Uri): String {
            var wholeID: String? = null
            try {
                wholeID = DocumentsContract.getDocumentId(captureImageUri)
            } catch (e: Exception) {
                return getRealPathFromURI(activity, captureImageUri)
            }
//            var splitChar=""
//            if(wholeID.contains(":"))
//                splitChar=":"
//            else
//                splitChar=";"
            // Split at colon, use second item_user_media in the array
            val id = wholeID!!.split(":".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()[1]

            val column = arrayOf(MediaStore.Images.Media.DATA)

            // where id is equal to
            val sel = MediaStore.Images.Media._ID + "=?"

            val cursor = activity!!.contentResolver.query(
                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                    column, sel, arrayOf(id), null
            )

            var filePath = ""

            val columnIndex = cursor!!.getColumnIndex(column[0])

            if (cursor.moveToFirst()) {
                filePath = cursor.getString(columnIndex)
            }

            cursor.close()
            return CommonUtils.compressImage(filePath, activity)
        }

        fun getRealPathFromURI(context: Context?, contentUri: Uri): String {
            var cursor: Cursor? = null
            try {
                val proj = arrayOf(MediaStore.Images.Media.DATA)
                cursor = context!!.contentResolver.query(contentUri, proj, null, null, null)
                val column_index = cursor!!.getColumnIndexOrThrow(MediaStore.Images.Media.DATA)
                cursor.moveToFirst()
                return cursor.getString(column_index)
            } finally {
                cursor?.close()
            }
        }

        fun getImageUri(inContext: Context?, inImage: Bitmap): Uri {
            val bytes = ByteArrayOutputStream()
            val path = MediaStore.Images.Media.insertImage(inContext!!.contentResolver, inImage, "Title", null)
            return Uri.parse(path)
        }
    }
    private fun HitServerToUpdateAvtar() {
        val a = ArrayList<MultipartBody.Part>()
        var app = AppPreferencesHelper(this)
        showLoading()
        val apiService = ApiClient.getClient().create(WebService::class.java)
        if (integerFileHashMap.size > 0) {

            for (i in 1..integerFileHashMap.size) {

                val requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), integerFileHashMap.get(i))
                val body = MultipartBody.Part.createFormData("avatar",integerFileHashMap.get(i)!!.getName(), requestFile)
                a.add(body)
            }

        }
        apiService.updateCounselorAvtar(a , "Bearer "+app.user_XAUTH).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : SingleObserver<ModelUpdateAvtar> {
                    override fun onSubscribe(d: Disposable) {

                    }
                    override fun onSuccess(updateAvtar: ModelUpdateAvtar) {
                        a.clear()
                        hideLoading()
//                        showMessage(updateAvtar.message)
                        hideLoading()
                    }

                    override fun onError(e: Throwable) {
                        a.clear()
                        hideLoading()
                        handleError(e)
                    }
                })


    }
    private fun handleError(t: Throwable?) {
        if (t != null)
            showMessage(t.message)
    }

    override fun onBackPressed() {
        if(isExit)
            super.onBackPressed()
        else{
            showSnackBar("Press again to exit !")
            isExit=true
            Handler().postDelayed(Runnable { isExit=false},2000)
        }
    }
}
