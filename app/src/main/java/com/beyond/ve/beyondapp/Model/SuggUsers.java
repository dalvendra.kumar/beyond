package com.beyond.ve.beyondapp.Model;

import java.util.ArrayList;

public class SuggUsers {
    private int status,statusCode;
    private String message,description;
    private ArrayList<Payload> payload;

    public class Payload{
        private boolean isRequested=false;
        private int id;
        private String first_name,last_name,avatar,email,company_name,distance;

        public Payload(String first_name, String last_name, String avatar) {
            this.first_name = first_name;
            this.last_name = last_name;
            this.avatar = avatar;
        }

        public int getId() {
            return id;
        }

        public String getFirst_name() {
            return first_name;
        }

        public String getLast_name() {
            return last_name;
        }

        public String getAvatar() {
            return avatar;
        }

        public String getEmail() {
            return email;
        }

        public String getCompany_name() {
            if(company_name==null)
                company_name="";
            return company_name;
        }

        public String getDistance() {
            return distance;
        }

        public boolean isRequested() {
            return isRequested;
        }

        public void setRequested(boolean requested) {
            isRequested = requested;
        }

        @Override
        public String toString() {
            return "Payload{" +
                    "id=" + id +
                    ", first_name='" + first_name + '\'' +
                    ", last_name='" + last_name + '\'' +
                    ", avatar='" + avatar + '\'' +
                    ", email='" + email + '\'' +
                    ", company_name='" + company_name + '\'' +
                    ", distance='" + distance + '\'' +
                    '}';
        }
    }

    public int getStatus() {
        return status;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public String getMessage() {
        return message;
    }

    public String getDescription() {
        return description;
    }

    public ArrayList<Payload> getPayload() {
        return payload;
    }

    @Override
    public String toString() {
        return "SuggUsers{" +
                "status=" + status +
                ", statusCode=" + statusCode +
                ", message='" + message + '\'' +
                ", description='" + description + '\'' +
                ", payload=" + payload +
                '}';
    }
}
