package com.beyond.ve.beyondapp.Adapter;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.beyond.ve.beyondapp.Model.CalendarUpdatesDashboard.Payload;
import com.beyond.ve.beyondapp.R;

import java.util.List;

import androidx.recyclerview.widget.RecyclerView;

public class UpdatesAdapter extends RecyclerView.Adapter<UpdatesAdapter.MyViewHolder> {

    private List<Payload> clendarUpdates;
    private UpdatesListener listener;
    public class MyViewHolder extends RecyclerView.ViewHolder  {
        public TextView tv_event, tv_time;
        private ImageView imgAccept,imgReject;
        public MyViewHolder(View view) {
            super(view);
            tv_event = (TextView) view.findViewById(R.id.tv_event);
            tv_time = (TextView) view.findViewById(R.id.tv_time);
            imgAccept=view.findViewById(R.id.imgAccept);
            imgReject=view.findViewById(R.id.imgReject);
        }
    }


    public UpdatesAdapter(List<Payload> clendarUpdates,UpdatesListener listener) {
        this.clendarUpdates = clendarUpdates;
        this.listener = listener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_updates, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        final Payload payload = clendarUpdates.get(position);
        if(!TextUtils.isEmpty(payload.getEvent())){
            holder.tv_event.setText(payload.getEvent());
        }

        if(!TextUtils.isEmpty(payload.getDate())){
            holder.tv_time.setText(payload.getStart());
        }

        if(payload.getStatus_update().equals("0")){
            holder.tv_time.setVisibility(View.GONE);
            holder.imgAccept.setVisibility(View.VISIBLE);
            holder.imgReject.setVisibility(View.VISIBLE);
        }else if(payload.getStatus_update().equals("1")){
            holder.tv_time.setVisibility(View.VISIBLE);
            holder.imgAccept.setVisibility(View.INVISIBLE);
            holder.imgReject.setVisibility(View.INVISIBLE);
        }

        holder.imgAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {listener.onAccept(payload);}});

        holder.imgReject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {listener.onReject(payload);}});
    }

    @Override
    public int getItemCount() {
        return clendarUpdates.size();
    }

    public interface UpdatesListener{
        void onAccept(Payload payload);
        void onReject(Payload payload);
    }
}