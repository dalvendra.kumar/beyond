package com.beyond.ve.beyondapp.Adapter;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.beyond.ve.beyondapp.R;
import com.beyond.ve.beyondapp.Utils.ImageUtils;
import com.beyond.ve.beyondapp.ui.ScheduleFragmentMonthly;
import com.beyond.ve.beyondapp.ui.ScheduleFragmentWeekly;

import java.util.ArrayList;
import java.util.HashSet;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import de.hdodenhof.circleimageview.CircleImageView;

public class AllContactListAdapter extends RecyclerView.Adapter<AllContactListAdapter.MyViewHolder> {

    private Fragment activity;
    ArrayList<com.beyond.ve.beyondapp.Model.UserSearch.Payload> payloadSearches;
    ArrayList<String> contactList=new ArrayList<>();
    ArrayList<Integer> contactListId=new ArrayList<>();
    HashSet<Integer> stringHashSet=new HashSet<>();
    RecyclerView recyclerView;
    ContactNameAdapter contactNameadapter;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_contact_name;
        LinearLayout ll_item;
        CircleImageView imageView;

        public MyViewHolder(View view) {
            super(view);
            ll_item = view.findViewById(R.id.ll_item);
            tv_contact_name = (TextView) view.findViewById(R.id.tv_contact_name);
            imageView=view.findViewById(R.id.imageView);
        }
    }


    public AllContactListAdapter(Fragment applicationContext, ArrayList<com.beyond.ve.beyondapp.Model.UserSearch.Payload> payloadSearches, RecyclerView recyclerView) {
        activity = applicationContext;
        this.recyclerView=recyclerView;
        this.payloadSearches=payloadSearches;
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_spinner_contact_name, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

   holder.tv_contact_name.setText(payloadSearches.get(position).getFirstName() +" " +payloadSearches.get(position).getLastName());

       if(!TextUtils.isEmpty(payloadSearches.get(position).getAvatar())) {

           ImageUtils.displayImageFromUrlPicasso(activity.getContext(),holder.imageView,payloadSearches.get(position).getAvatar());
       }
        holder.ll_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(contactList.size()==0) {
                    callMethod();

                    if(activity instanceof ScheduleFragmentMonthly)
                    {
                        ( (ScheduleFragmentMonthly) activity).addItemInList(payloadSearches.get(position).getId());
                    }
                    else
                    {
                        ( (ScheduleFragmentWeekly) activity).addItemInList(payloadSearches.get(position).getId());

                    }
                    contactList.add(holder.tv_contact_name.getText().toString());
                    contactListId.add(payloadSearches.get(position).getId());
                    contactNameadapter = new ContactNameAdapter(contactList, activity,contactListId);
                    RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(activity.getContext(),
                            LinearLayoutManager.HORIZONTAL, false);
                    recyclerView.setLayoutManager(mLayoutManager);
                    recyclerView.setItemAnimator(new DefaultItemAnimator());
                    recyclerView.setAdapter(contactNameadapter);
                }
                else
                {
                    callMethod();
                    contactListId.add(payloadSearches.get(position).getId());
                    if(activity instanceof ScheduleFragmentMonthly)
                    {
                        ( (ScheduleFragmentMonthly) activity).addItemInList(payloadSearches.get(position).getId());
                    }
                    else
                    {
                        ( (ScheduleFragmentWeekly) activity).addItemInList(payloadSearches.get(position).getId());
                    }
                    stringHashSet.add(payloadSearches.get(position).getId());
                    contactList.add(holder.tv_contact_name.getText().toString());
                    contactNameadapter.notifyDataSetChanged();
                }
            }
        });


    }

    private void callMethod() {
        if(activity instanceof ScheduleFragmentMonthly)
        {
            ( (ScheduleFragmentMonthly) activity).hideRecylerViewAfterSelect();
        }
        else
        {
            ( (ScheduleFragmentWeekly) activity).hideRecylerViewAfterSelect();
        }
    }

    @Override
    public int getItemCount() {
        return payloadSearches.size();
    }


}