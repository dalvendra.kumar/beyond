
package com.beyond.ve.beyondapp.ui.contact;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Payload {

    @SerializedName("requests")
    @Expose
    private ArrayList<Model> requests;

    @SerializedName("contacts")
    @Expose
    private ArrayList<Model> contacts;

    @SerializedName("local")
    @Expose
    private ArrayList<Model> local;

    public ArrayList<Model> getRequests() {
        return requests;
    }

    public void setRequests(ArrayList<Model> requests) {
        this.requests = requests;
    }

    public ArrayList<Model> getContacts() {
        return contacts;
    }

    public void setContacts(ArrayList<Model> contacts) {
        this.contacts = contacts;
    }

    public ArrayList<Model> getLocal() {
        return local;
    }

    public void setLocal(ArrayList<Model> local) {
        this.local = local;
    }

    @Override
    public String toString() {
        return "Payload{" +
                "requests=" + requests +
                ", contacts=" + contacts +
                ", local=" + local +
                '}';
    }
}
