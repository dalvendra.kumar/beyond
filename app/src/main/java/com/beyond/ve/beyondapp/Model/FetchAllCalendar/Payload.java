
package com.beyond.ve.beyondapp.Model.FetchAllCalendar;

import java.io.Serializable;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Payload implements Serializable {

    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("data")
    @Expose
    private List<Datum> data = null;
    @SerializedName("day")
    @Expose
    private String day;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

}
