package com.beyond.ve.beyondapp.Adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.beyond.ve.beyondapp.Model.Notification;
import com.beyond.ve.beyondapp.R;
import com.beyond.ve.beyondapp.Utils.DateUtils;
import com.beyond.ve.beyondapp.Utils.ImageUtils;
import com.beyond.ve.beyondapp.databinding.ItemNotificationBinding;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.ViewHolder>{

    private ArrayList<Notification> notifications;
    private int deviceWidth;
    public NotificationAdapter(ArrayList<Notification> notifications,int deviceWidth) {
        this.notifications = notifications;
        this.deviceWidth=deviceWidth;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemNotificationBinding binding= ItemNotificationBinding.bind(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_notification,parent,false));
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Notification notification=notifications.get(position);
        holder.binding.txtMsg.setText(notification.getDescription());
        holder.binding.txtTitle.setText(notification.getTitle());
        holder.binding.txtTime.setText(DateUtils.getTimeDiff(notification.getUpdated_at())+" ago");
        ImageUtils.displayImageFromUrl(holder.itemView.getContext(),holder.binding.imgNotification,notification.getAvatar());

    }

    @Override
    public int getItemCount() {
        return notifications.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        ItemNotificationBinding binding;
        public ViewHolder(@NonNull ItemNotificationBinding binding) {
            super(binding.getRoot());
            this.binding=binding;
            ConstraintLayout.LayoutParams params= (ConstraintLayout.LayoutParams) binding.imgNotification.getLayoutParams();
            params.width= (int) (deviceWidth/6.5);
            params.height= (int) (deviceWidth/6.5);
            binding.imgNotification.setLayoutParams(params);
        }
    }
}
