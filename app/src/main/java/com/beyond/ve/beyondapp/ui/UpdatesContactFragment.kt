package com.beyond.ve.beyondapp.ui

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.net.Uri
import android.os.Bundle
import android.os.Looper
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import com.beyond.ve.beyondapp.Adapter.RecentMeetingAdapter
import com.beyond.ve.beyondapp.BaseActivity.BaseFragment
import com.beyond.ve.beyondapp.Model.ModelSingup.ModelSignUp
import com.beyond.ve.beyondapp.Model.SuggUsers

import com.beyond.ve.beyondapp.R
import com.beyond.ve.beyondapp.Utils.ImageUtils
import com.beyond.ve.beyondapp.Web.ApiClient
import com.beyond.ve.beyondapp.Web.WebService
import com.beyond.ve.beyondapp.data.AppPreferencesHelper
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.LocationCallback
import com.google.android.gms.location.LocationListener
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationServices
import com.google.gson.JsonElement
import kotlinx.android.synthetic.main.fragment_updates_contact.*
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.HashMap

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [UpdatesContactFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [UpdatesContactFragment.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class UpdatesContactFragment : BaseFragment(), View.OnClickListener, RecentMeetingAdapter.ConnectListener, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener {


    override fun onClick(p0: View?) {
        when (p0!!.id) {
            R.id.profile_image_first -> {
                val intent = Intent(activity, ShowProfileActivity::class.java)
                startActivity(intent)
            }
        }
    }

    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    var movieList = ArrayList<SuggUsers.Payload>()
    var nearByUsers = ArrayList<SuggUsers.Payload>()
    var nearByImages = ArrayList<de.hdodenhof.circleimageview.CircleImageView>()
    var nearByImagesOver = ArrayList<de.hdodenhof.circleimageview.CircleImageView>()
    var nearByTexts = ArrayList<TextView>()

    var lat: Double = 0.0
    var lng: Double = 0.0

    private var mAdapter: RecentMeetingAdapter? = null

    private var listener: OnFragmentInteractionListener? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }

        mGoogleApiClient = GoogleApiClient.Builder(this.activity!!)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API).build()

        mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval((10 * 1000).toLong())        // 10 seconds, in milliseconds
                .setFastestInterval((1 * 1000).toLong())

    }

    override fun onConnected(p0: Bundle?) {
        try {
            if (ContextCompat.checkSelfPermission(this.activity!!, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this.activity!!)
                mLocationCallback = LocationCallback()
                mFusedLocationClient?.requestLocationUpdates(mLocationRequest, mLocationCallback, Looper.myLooper())
                mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient)
            }

            if (mLastLocation != null) {
                lat = mLastLocation!!.getLatitude()
                lng = mLastLocation!!.getLongitude()

                getNearBy(lat.toString(), lng.toString())
                updateUserLocation()
            }
        } catch (e: java.lang.Exception) {

        }
    }

    override fun onConnectionSuspended(p0: Int) {
    }

    override fun onConnectionFailed(p0: ConnectionResult) {
    }

    override fun onLocationChanged(p0: Location?) {
        if (p0 != null) {
            lat = p0.latitude
            lng = p0.longitude
            updateUserLocation()
            getNearBy(lat.toString(), lng.toString())
        }
    }

    override fun onPause() {
        super.onPause()
        if (mGoogleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
            mGoogleApiClient.disconnect();
        }
    }

    override fun onResume() {
        super.onResume()
        mGoogleApiClient.connect();
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_updates_contact, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        profile_image_first.setOnClickListener(this)

        mAdapter = RecentMeetingAdapter(movieList, this)
        val mLayoutManager = LinearLayoutManager(activity)
        rv_recent_meetings.setLayoutManager(mLayoutManager)
        rv_recent_meetings.setItemAnimator(DefaultItemAnimator())
        rv_recent_meetings.setAdapter(mAdapter)

        initNearByViews(view)

        ImageUtils.displayImageFromUrl(activity, profile_image_center, AppPreferencesHelper(activity).user_avatar)

        getRecentMeetings();

        invisibleNearByViews();

    }

    private fun initNearByViews(view: View) {

        nearByImages.add(view.findViewById(R.id.profile_image_first))
        nearByImages.add(view.findViewById(R.id.profile_image_second))
        nearByImages.add(view.findViewById(R.id.profile_image_third))
        nearByImages.add(view.findViewById(R.id.profile_image_four))
        nearByImages.add(view.findViewById(R.id.profile_image_five))
        nearByImages.add(view.findViewById(R.id.profile_image_six))
        nearByImages.add(view.findViewById(R.id.profile_image_seven))
        nearByImages.add(view.findViewById(R.id.profile_image_eight))

        nearByImagesOver.add(view.findViewById(R.id.profile_image_first_over))
        nearByImagesOver.add(view.findViewById(R.id.profile_image_second_over))
        nearByImagesOver.add(view.findViewById(R.id.profile_image_third_over))
        nearByImagesOver.add(view.findViewById(R.id.profile_image_four_over))
        nearByImagesOver.add(view.findViewById(R.id.profile_image_five_over))
        nearByImagesOver.add(view.findViewById(R.id.profile_image_six_over))
        nearByImagesOver.add(view.findViewById(R.id.profile_image_seven_over))
        nearByImagesOver.add(view.findViewById(R.id.profile_image_eight_over))

        nearByTexts.add(view.findViewById(R.id.first_image_name))
        nearByTexts.add(view.findViewById(R.id.second_image_name))
        nearByTexts.add(view.findViewById(R.id.third_image_name))
        nearByTexts.add(view.findViewById(R.id.four_image_name))
        nearByTexts.add(view.findViewById(R.id.five_image_name))
        nearByTexts.add(view.findViewById(R.id.six_image_name))
        nearByTexts.add(view.findViewById(R.id.seven_image_name))
        nearByTexts.add(view.findViewById(R.id.eight_image_name))
    }

    private fun invisibleNearByViews() {
        for (i in 0..7) {
            nearByImagesOver.get(i).visibility = View.INVISIBLE;
            nearByImages.get(i).visibility = View.INVISIBLE;
            nearByTexts.get(i).visibility = View.INVISIBLE;
        }
    }

    private fun updateUserLocation() {
        val apiService = ApiClient.getClient().create(WebService::class.java)
        val paramObject = HashMap<String, String>()
        paramObject["device_token"] = AppPreferencesHelper(activity).deviceToken
        paramObject["lat"] = ""+ lat
        paramObject["lng"] = ""+ lng
        val call = apiService.updateProfile("Bearer " +AppPreferencesHelper(activity).user_XAUTH,paramObject)
        call.enqueue(object : retrofit2.Callback<JsonElement> {
            override fun onResponse(call: Call<JsonElement>, response: Response<JsonElement>?) {
            }
            override fun onFailure(call: Call<JsonElement>, t: Throwable) {
            }
        })
    }

    private fun getRecentMeetings() {
        val service = ApiClient.getClient().create(WebService::class.java)
        val call = service.getRecentMeetings("Bearer " + AppPreferencesHelper(activity).user_XAUTH)
        call.enqueue(object : Callback<SuggUsers> {
            override fun onResponse(call: Call<SuggUsers>, response: Response<SuggUsers>) {
                if (response.isSuccessful) {
                    movieList.addAll(response.body().payload)
                    mAdapter!!.notifyDataSetChanged()
                } else {
                    try {
                        val jObjError = JSONObject(response.errorBody().string())
                        showSnackBar(jObjError.getString("message"))
                    } catch (e: Exception) {
                        showSnackBar("" + e.message)
                    }
                }
            }

            override fun onFailure(call: Call<SuggUsers>, t: Throwable) {
                t.printStackTrace()
            }
        })
    }

    private fun getNearBy(lat: String, lng: String) {
        val service = ApiClient.getClient().create(WebService::class.java)
        val call = service.getNearBy("Bearer " + AppPreferencesHelper(activity).user_XAUTH, lat, lng)
        call.enqueue(object : Callback<SuggUsers> {
            override fun onResponse(call: Call<SuggUsers>, response: Response<SuggUsers>) {
                if (response.isSuccessful) {
                    if (response.body().payload != null) {
                        nearByUsers.clear()
                        nearByUsers.addAll(response.body().payload)
                    }
                    if (activity != null)
                        toggleNearByUsers()
                } else {
                    try {
                        val jObjError = JSONObject(response.errorBody().string())
                        showSnackBar(jObjError.getString("message"))
                    } catch (e: Exception) {
                        showSnackBar("" + e.message)
                    }
                }
            }

            override fun onFailure(call: Call<SuggUsers>, t: Throwable) {
                t.printStackTrace()
            }
        })
    }


    override fun onConnect(payload: SuggUsers.Payload?) {
        val service = ApiClient.getClient().create(WebService::class.java)
        val call = service.sendConnectReq("Bearer " + AppPreferencesHelper(activity).user_XAUTH, payload!!.id)
        call.enqueue(object : Callback<JsonElement> {
            override fun onResponse(call: Call<JsonElement>, response: Response<JsonElement>) {
                if (response.isSuccessful) {
                    payload.isRequested = true
                    mAdapter!!.notifyDataSetChanged()
                } else {
                    try {
                        val jObjError = JSONObject(response.errorBody().string())
                        showSnackBar(jObjError.getString("message"))
                    } catch (e: Exception) {
                        showSnackBar("" + e.message)
                    }
                }
            }

            override fun onFailure(call: Call<JsonElement>, t: Throwable) {
                t.printStackTrace()
            }
        })
    }

    private fun toggleNearByUsers() {
        var count = nearByUsers.size - 1
        if (count > 7)
            count = 7
        for (i in 0..count) {
            var payload: SuggUsers.Payload = nearByUsers.get(i)
            nearByImages.get(i).visibility = View.VISIBLE
            nearByImagesOver.get(i).visibility = View.VISIBLE
            nearByTexts.get(i).visibility = View.VISIBLE
            nearByTexts.get(i).setText(payload.first_name)
            ImageUtils.displayImageFromUrl(activity, nearByImages.get(i), payload.avatar)
        }
    }

    // TODO: Rename method, update argument and hook method into UI event
    fun onButtonPressed(uri: Uri) {
        listener?.onFragmentInteraction(uri)
    }


    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments]
     * (http://developer.android.com/training/basics/fragments/communicating.html)
     * for more information.
     */
    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onFragmentInteraction(uri: Uri)
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment UpdatesContactFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
                UpdatesContactFragment().apply {
                    arguments = Bundle().apply {
                        putString(ARG_PARAM1, param1)
                        putString(ARG_PARAM2, param2)
                    }
                }
    }
}
