package com.beyond.ve.beyondapp.ui.barcode;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;

import com.beyond.ve.beyondapp.BaseActivity.BaseFragment;
import com.beyond.ve.beyondapp.R;
import com.beyond.ve.beyondapp.Utils.ImageUtils;
import com.beyond.ve.beyondapp.data.AppPreferencesHelper;
import com.google.zxing.WriterException;

import java.io.File;
import java.io.FileOutputStream;

import androidmads.library.qrgenearator.QRGContents;
import androidmads.library.qrgenearator.QRGEncoder;

import static android.content.Context.WINDOW_SERVICE;

public class MycodeFragment extends BaseFragment implements View.OnClickListener {
    private String savePath = Environment.getExternalStorageDirectory().getPath() + "/QRCode/";
    private QRGEncoder qrgEncoder;
    private Bitmap bitmap;
    private ImageView scanImage, profile_image;
    private TextView txt_share, txt_save, name, company;
    private static String IMAGEFROM = "";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_mycode, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        scanImage = (ImageView) view.findViewById(R.id.scanImage);
        profile_image = (ImageView) view.findViewById(R.id.profile_image);

        txt_share = (TextView) view.findViewById(R.id.txt_share);
        txt_save = (TextView) view.findViewById(R.id.txt_save);

        name = (TextView) view.findViewById(R.id.name);
        company = (TextView) view.findViewById(R.id.company);

        txt_save.setOnClickListener(this);
        txt_share.setOnClickListener(this);

        createData();
    }

    public void createData() {
        AppPreferencesHelper helper = new AppPreferencesHelper(getActivity());

        if (!TextUtils.isEmpty(helper.getUser_Name())) {
            name.setText(helper.getUser_Name());
        }

        if (!TextUtils.isEmpty(helper.getUser_Name())) {
            company.setText(helper.getUser_Name());
        }

        if (!TextUtils.isEmpty(helper.getUser_avatar())) {
            ImageUtils.displayImageFromUrl(getActivity(), profile_image, helper.getUser_avatar());
        }

        if (!TextUtils.isEmpty(helper.getUser_Id().toString())) {
            createBarCode(helper.getUser_Id());
        }

    }

    public void createBarCode(Integer inputValue) {
        if (inputValue > 0) {
            WindowManager manager = (WindowManager) getActivity().getSystemService(WINDOW_SERVICE);
            Display display = manager.getDefaultDisplay();
            Point point = new Point();
            display.getSize(point);
            int width = point.x;
            int height = point.y;
            int smallerDimension = width < height ? width : height;
            smallerDimension = smallerDimension * 3 / 4;

            qrgEncoder = new QRGEncoder(
                    inputValue.toString(), null,
                    QRGContents.Type.TEXT,
                    smallerDimension);
            try {
                bitmap = qrgEncoder.encodeAsBitmap();
                scanImage.setImageBitmap(bitmap);
            } catch (WriterException e) {
                Log.v("DATA", e.toString());
            }
        } else {
            Log.e("DATA", "Required");
        }

    }

    private void saveImage(Bitmap finalBitmap) {

        String root = Environment.getExternalStorageDirectory().toString();
        File myDir = new File(root + "/saved_images");
        myDir.mkdir();

        String fname = "Image1" + ".png";
        File file = new File(myDir, fname);
        if (file.exists()) file.delete();
        try {
            FileOutputStream out = new FileOutputStream(file);
            finalBitmap.compress(Bitmap.CompressFormat.PNG, 90, out);
            out.flush();
            out.close();
            if (IMAGEFROM.equals("save"))
                showSnackBar(getResources().getString(R.string.saved));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txt_share: {
                IMAGEFROM = "share";
                checkPermissionsForShare(2, getActivity());

            }
            case R.id.txt_save: {
                IMAGEFROM = "save";

//                saveImage(bitmap);
                checkPermissionsForImage(1, getActivity());
            }
        }
    }

    private void shareImage() {

        String bitmapPath = MediaStore.Images.Media.insertImage(getActivity().getContentResolver(), bitmap, "title", null);
        Uri bitmapUri = Uri.parse(bitmapPath);

        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("image/png");
        intent.putExtra(Intent.EXTRA_STREAM, bitmapUri);
        startActivity(Intent.createChooser(intent, "Share"));

    }


    @TargetApi(Build.VERSION_CODES.M)
    private void checkPermissionsForImage(int permissionCode, Context context) {

        String[] PERMISSIONS = {Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE};
        if (!hasPermissions(context, PERMISSIONS)) {
            ActivityCompat.requestPermissions((Activity) context, PERMISSIONS, permissionCode);
        } else {
            saveImage(bitmap);
        }
    }


    @TargetApi(Build.VERSION_CODES.M)
    private void checkPermissionsForShare(int permissionCode, Context context) {

        String[] PERMISSIONS = {Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE};
        if (!hasPermissions(context, PERMISSIONS)) {
            ActivityCompat.requestPermissions((Activity) context, PERMISSIONS, permissionCode);
        } else {
            shareImage();
        }
    }

    private boolean hasPermissions(Context context, String... permissions) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    public void onRequestPermission(int requestCode, String[] permissions, int[] grantResults) {
        if(grantResults.length>0)
            switch (requestCode) {
                case 1:
                    if (grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                        saveImage(bitmap);
                    }
                    break;

                case 2:
                    if (grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                        shareImage();
                    }
                    break;


            }
    }


}
