package com.beyond.ve.beyondapp.ui

import android.content.Intent
import android.os.Bundle
import android.text.InputType
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import com.beyond.ve.beyondapp.BaseActivity.BaseFragment
import com.beyond.ve.beyondapp.Constant
import com.beyond.ve.beyondapp.Model.ModelSignin
import com.beyond.ve.beyondapp.R
import com.beyond.ve.beyondapp.Web.ApiClient
import com.beyond.ve.beyondapp.Web.WebService
import kotlinx.android.synthetic.main.fragment_sign_in.*
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Response
import java.util.*
import java.util.regex.Matcher
import java.util.regex.Pattern
import com.beyond.ve.beyondapp.Utils.NetworkUtils
import com.beyond.ve.beyondapp.data.AppPreferencesHelper
import kotlin.Exception as Exception1


private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

class Fragment_Signin : BaseFragment() , View.OnClickListener{

    var  isEyeOn : Boolean = false


    override fun onClick(p0: View?) {
        when(p0!!.id)
        {
            R.id.txt_forgot_password->
            {
                val intent = Intent(context, ActivityForgotPassword::class.java)

                startActivity(intent)
                activity!!.overridePendingTransition(R.anim.entry_from_right, R.anim.exit_to_left);
            }

            R.id.btn_singin->
            {

                if(NetworkUtils.isNetworkConnected(context))
                {
                    validation()

                }

                else
                {
                    showSnackBar(getString(R.string.msg_nointerconncetion))
                }
            }
        }
    }
    private fun validation() {
        if(tie_email.text.toString()!=null)
        {
            if(emailValidator(tie_email.text.toString())) {

                if (!TextUtils.isEmpty(tie_password.text.toString())) {

                    PostSingInDataToServer(tie_email.text.toString(),tie_password.text.toString())
                } else {
                    showSnackBar(getString(R.string.msg_enter_password))
                }
            }
            else
            {
                showSnackBar(getString(R.string.msg_enter_valid_email))
            }
        }
        else
        {
            showMessage(getString(R.string.msg_enter_valid_email))
        }
    }
    private fun PostSingInDataToServer(email : String , pass : String ) {
        val apiService = ApiClient.getClient().create(WebService::class.java)
        val paramObject = HashMap<String, String>()
        paramObject["email"] = email
        paramObject["password"] = pass
        val call = apiService.postSignIn(paramObject )
        showLoading()
        call.enqueue(object : retrofit2.Callback<ModelSignin> {
            override fun onResponse(call: Call<ModelSignin>, response: Response<ModelSignin>?) {

                if (response != null) {
                    if (response.isSuccessful && response.body().statusCode == Constant.RESPONSE_SUCCESSFULLY)
                    {
                        hideLoading()

                        var appPreferencesHelper = AppPreferencesHelper(activity)

                        appPreferencesHelper.user_Name =  response.body().payload.firstName
                        appPreferencesHelper.user_LastName =  response.body().payload.lastName
                        appPreferencesHelper.user_Id =  response.body().payload.id
                        appPreferencesHelper.user_Email =  response.body().payload.email
                        appPreferencesHelper.user_avatar =  response.body().payload.avatar
                        appPreferencesHelper.user_XAUTH = response.headers().get("X-Auth")
                        appPreferencesHelper.otpVerify=true
                        showSnackBar(response.body().message)
                        val intent = Intent(activity, Dashboad::class.java)
                        startActivity(intent)
                        activity!!.overridePendingTransition(R.anim.entry_from_right, R.anim.exit_to_left)
                        activity!!.finish()

                    }else  if (response.code() == Constant.RESPONSE_UNAUTHORIZED){
                        hideLoading()

                        try {
                            val jObjError = JSONObject(response.errorBody().string())
                            showSnackBar(jObjError.getString("message"))
                        } catch (e: Exception) {
                            showSnackBar(""+e.message)

                        }
                    }
                }
            }
            override fun onFailure(call: Call<ModelSignin>, t: Throwable) {
                // Log error here since request failed
                hideLoading()
                showSnackBar(getString(R.string.failure_please_try_again))
            }
        })
    }


    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    fun emailValidator(email: String): Boolean {
        val pattern: Pattern
        val matcher: Matcher
        val EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$"
        pattern = Pattern.compile(EMAIL_PATTERN)
        matcher = pattern.matcher(email)
        return matcher.matches()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_sign_in, container, false)
    }
    override fun onResume() {
        super.onResume()
        hideLoading()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        txt_forgot_password.setOnClickListener(this)
        btn_singin.setOnClickListener(this)

        tie_password.setOnTouchListener(View.OnTouchListener { v, event ->
            val DRAWABLE_RIGHT = 2
            if (event.action == MotionEvent.ACTION_UP) {
                if (event.rawX >= tie_password.getRight() - tie_password.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width()) {
                    if (isEyeOn) {
                        isEyeOn = false
                        tie_password.setInputType(InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD)
                        tie_password.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_eye_off, 0)
                    } else {
                        isEyeOn = true
                        tie_password.setInputType(InputType.TYPE_CLASS_TEXT)
                        tie_password.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_eye_on, 0)
                    }
                    return@OnTouchListener false
                }
            }
            false
        })


    }




}
