/*
 * Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://mindorks.com/license/apache-v2
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package com.beyond.ve.beyondapp.data;


/**
 * Created by Satendra on 27/01/17.
 */

public interface PreferencesHelper {

    void setUser_Name(String userName) ;

    String getUser_Name();

    void setUser_LastName(String userLastName) ;

    public String getUser_LastName();

    void setUser_Id(Integer userId) ;

    public Integer getUser_Id() ;

    void setUser_Email(String userEmail);

    public String getUser_Email() ;

    void setUser_avatar(String userAvatar);

    public String getUser_avatar() ;

    void setUser_XAUTH(String xAuth);

    public String getUser_XAUTH() ;

    void setUserIdFirebase(String id);

    public String getUserIdFirebase() ;


    void setOTPVerify(Boolean isOTPVerify);

    Boolean getOTPVerify() ;
    void setDeviceToken(String deviceToken);
    void setNotificationEnable(Boolean isEnable);
    Boolean getNotificationEnable();
    String getDeviceToken();
    boolean clearPrefe();


}
