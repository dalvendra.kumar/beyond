package com.beyond.ve.beyondapp.Adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.beyond.ve.beyondapp.R;
import com.beyond.ve.beyondapp.ui.ScheduleFragmentMonthly;
import com.beyond.ve.beyondapp.ui.ScheduleFragmentWeekly;

import java.util.ArrayList;
import java.util.List;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

public class ContactNameAdapter extends RecyclerView.Adapter<ContactNameAdapter.MyViewHolder> {

    private List<String> moviesList;
    ArrayList<Integer> contactListId;
    private Fragment mActivity;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView contactName, year;
        public ImageView cancel_contactName;
        public MyViewHolder(View view) {
            super(view);
            contactName = view.findViewById(R.id.contactName);
            cancel_contactName = view.findViewById(R.id.cancel_contactName);
        }

    }

    public ContactNameAdapter(List<String> moviesList, Fragment mActivity, ArrayList<Integer> contactListId) {
        this.moviesList = moviesList;
        this.mActivity=mActivity;
        this.contactListId=contactListId;
    }
 
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_selected_contact_name, parent, false);
        return new MyViewHolder(itemView);
    }
 
    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final String movie = moviesList.get(position).toString();
        holder.contactName.setText(movie);
        holder.cancel_contactName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                moviesList.remove(position);

                if(mActivity instanceof ScheduleFragmentMonthly)
                {
                    ( (ScheduleFragmentMonthly) mActivity).removeItemInList(contactListId.get(position).intValue());
                }
                else
                {
                    ( (ScheduleFragmentWeekly) mActivity).removeItemInList(contactListId.get(position).intValue());
                }
                notifyDataSetChanged();
            }
        });
    }
 
    @Override
    public int getItemCount() {
        return moviesList.size();
    }


}