package com.beyond.ve.beyondapp.ui

import android.app.DatePickerDialog
import android.app.Dialog
import android.app.TimePickerDialog
import android.os.Bundle
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.CalendarView
import android.widget.DatePicker
import android.widget.TimePicker
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import com.beyond.ve.beyondapp.BaseActivity.BaseFragment
import com.beyond.ve.beyondapp.Adapter.AllContactListAdapter
import com.beyond.ve.beyondapp.Adapter.MeetingScheduleAdapter
import com.beyond.ve.beyondapp.Adapter.RecyclerSectionItemDecoration
import com.beyond.ve.beyondapp.Constant
import com.beyond.ve.beyondapp.Model.CreateCalendarModel
import com.beyond.ve.beyondapp.Model.FetchAllCalendar.Payload
import com.beyond.ve.beyondapp.Model.ModelCreateCalendar
import com.beyond.ve.beyondapp.Model.UserSearch.ModelUserSearch
import com.beyond.ve.beyondapp.R
import com.beyond.ve.beyondapp.Utils.CommonUtils
import com.beyond.ve.beyondapp.Utils.NetworkUtils
import com.beyond.ve.beyondapp.Web.ApiClient
import com.beyond.ve.beyondapp.Web.WebService
import com.beyond.ve.beyondapp.data.AppPreferencesHelper
import com.beyond.ve.beyondapp.reciver.AlarmReceiver
import com.prolificinteractive.materialcalendarview.*
import kotlinx.android.synthetic.main.dialog_new_event.*
import kotlinx.android.synthetic.main.dialog_new_event_alert.*
import kotlinx.android.synthetic.main.fragment_schedule_weekly.*
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Response
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"
private var mAdapter: MeetingScheduleAdapter? = null
private var allContactList: AllContactListAdapter? = null


class ScheduleFragmentWeekly : BaseFragment(), View.OnClickListener, RecyclerSectionItemDecoration.SectionCallback {

    override fun isSection(position: Int): Boolean {
        if (position % 2 == 0)
            return true;
        else
            return false

    }

    override fun getSectionHeader(position: Int): String {
        return "Friday"

    }

    override fun onClick(p0: View?) {
        when (p0!!.id) {

            R.id.show_month -> {
                var fragment: ScheduleFragmentMonthly =
                        ScheduleFragmentMonthly();

                val ft = activity!!.supportFragmentManager.beginTransaction()
                ft.setCustomAnimations(R.anim.entry_from_right, R.anim.exit_to_left);
                ft.replace(R.id.flContent, fragment).addToBackStack(null)
                ft.commit()
            }
            R.id.iv_alert_cancel -> {
                dialogNewEventAlert!!.cancel()
            }

            R.id.tv_none -> {
                currentAlertPosition = 0;
                dialogNewEventAlert!!.cancel()
                SetMeetingTime("None")

            }

            R.id.tv_at_the_time_of_event -> {
                currentAlertPosition = 1;
                dialogNewEventAlert!!.cancel()
                SetMeetingTime("At time of event")
            }

            R.id.tv_two_day_before -> {
                currentAlertPosition = 8;
                dialogNewEventAlert!!.cancel()

                SetMeetingTime("2 day before")

            }

            R.id.tv_one_day_before -> {
                currentAlertPosition = 7;
                dialogNewEventAlert!!.cancel()
                SetMeetingTime("1 day before")

            }

            R.id.tv_two_hours_before -> {

                currentAlertPosition = 6;
                dialogNewEventAlert!!.cancel()

                SetMeetingTime("2 hour before")

            }

            R.id.tv_one_hour_before -> {
                currentAlertPosition = 5;
                dialogNewEventAlert!!.cancel()
                SetMeetingTime("1 hour before")

            }

            R.id.tv_threezero_min_before -> {
                currentAlertPosition = 4;
                dialogNewEventAlert!!.cancel()
                SetMeetingTime("30 min before")

            }

            R.id.tv_onefive_min_before -> {
                currentAlertPosition = 3;
                dialogNewEventAlert!!.cancel()
                SetMeetingTime("15 min before")
            }

            R.id.tv_five_min_before -> {
                currentAlertPosition = 2;
                dialogNewEventAlert!!.cancel()
                SetMeetingTime("5 min before")

            }

            R.id.tv_date -> {

                var datePickerDialog = DatePickerDialog(activity, date, myCalendar.get(Calendar.YEAR),
                        myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH))

                val c = Calendar.getInstance()
                c.add(Calendar.DATE, 0)
                val newDate = c.getTime()
                datePickerDialog.datePicker.minDate = newDate.time
                datePickerDialog.show()


            }

            R.id.btn_cancel -> {
                dialogNewEvent!!.cancel()
                allContactList = null
            }

            R.id.tv_start_time -> {
                openTimePicker("start")
            }

            R.id.tv_endtime -> {
                openTimePicker("end")
            }

            R.id.semigray -> {
                dialogNewEvent!!.semigray.setBackgroundResource(R.mipmap.ic_correct);
                dialogNewEvent!!.bgcolor.setBackgroundResource(R.drawable.circle_bgcolor)
            }

            R.id.bgcolor -> {
                dialogNewEvent!!.bgcolor.setBackgroundResource(R.mipmap.ic_correct_bgcolor);
                dialogNewEvent!!.semigray.setBackgroundResource(R.drawable.circle_semigray)
            }


            R.id.btn_save_event -> {


                if (NetworkUtils.isNetworkConnected(activity)) {

                    if (!TextUtils.isEmpty(dialogNewEvent!!.tie_event.text.toString())) {


                        if (!TextUtils.isEmpty(dialogNewEvent!!.tie_location.text.toString())) {

                            if (!TextUtils.isEmpty(dialogNewEvent!!.tv_date.text.toString())) {

                                if (!TextUtils.isEmpty(dialogNewEvent!!.tv_start_time.text.toString())) {

                                    if (!TextUtils.isEmpty(dialogNewEvent!!.tv_endtime.text.toString())) {


                                        if (Objects.equals(dialogNewEvent!!.semigray!!.getBackground().getConstantState(), activity!!.getResources().getDrawable(R.mipmap.ic_correct).getConstantState())) {
//
                                            CreateEventAPI(dialogNewEvent!!.tie_event.text.toString(),

                                                    dialogNewEvent!!.tie_location.text.toString()
                                                    , dialogNewEvent!!.tv_date.text.toString(),
                                                    dialogNewEvent!!.tv_start_time.text.toString(),
                                                    dialogNewEvent!!.tv_endtime.text.toString(),
                                                    "gray")

                                        } else if (Objects.equals(dialogNewEvent!!.bgcolor!!.getBackground().getConstantState(), activity!!.getResources().getDrawable(R.mipmap.ic_correct_bgcolor).getConstantState())) {

                                            CreateEventAPI(dialogNewEvent!!.tie_event.text.toString(),

                                                    dialogNewEvent!!.tie_location.text.toString()
                                                    , dialogNewEvent!!.tv_date.text.toString(),
                                                    dialogNewEvent!!.tv_start_time.text.toString(),
                                                    dialogNewEvent!!.tv_endtime.text.toString(),
                                                    "blue")


                                        } else {
                                            showMessage("Please select label")
                                        }


                                    } else {
                                        showMessage("Please select end time")
                                    }


                                } else {
                                    showMessage("Please select start time")
                                }


                            } else {
                                showMessage("Please select valid date")
                            }

                        } else {
                            showMessage("Please enter event location")
                        }


                    } else {
                        showMessage("Please enter event name")
                    }
                } else {
                    showSnackBar(getString(R.string.msg_nointerconncetion))
                }
            }


            R.id.ll_alert -> {
                dialogNewEventAlert = Dialog(activity)
                dialogNewEventAlert!!.setContentView(R.layout.dialog_new_event_alert)
                val lp = WindowManager.LayoutParams()
                lp.copyFrom(dialogNewEventAlert!!.getWindow().getAttributes())
                lp.width = (CommonUtils.returmWindowWidth(activity) / 1.1).toInt()
                dialogNewEventAlert!!.getWindow().setAttributes(lp)
                dialogNewEventAlert!!.iv_alert_cancel.setOnClickListener(this)
                dialogNewEventAlert!!.tv_two_day_before.setOnClickListener(this)
                dialogNewEventAlert!!.tv_one_day_before.setOnClickListener(this)
                dialogNewEventAlert!!.tv_two_hours_before.setOnClickListener(this)
                dialogNewEventAlert!!.tv_one_hour_before.setOnClickListener(this)
                dialogNewEventAlert!!.tv_threezero_min_before.setOnClickListener(this)
                dialogNewEventAlert!!.tv_onefive_min_before.setOnClickListener(this)
                dialogNewEventAlert!!.tv_five_min_before.setOnClickListener(this)
                dialogNewEventAlert!!.tv_at_the_time_of_event.setOnClickListener(this)
                dialogNewEventAlert!!.tv_none.setOnClickListener(this)
                dialogNewEventAlert!!.show()
            }
            R.id.newevent -> {
                dialogNewEvent = Dialog(activity)
                dialogNewEvent!!.setContentView(R.layout.dialog_new_event)
                val lp = WindowManager.LayoutParams()
                lp.copyFrom(dialogNewEvent!!.getWindow().getAttributes())
                lp.width = (CommonUtils.returmWindowWidth(activity) / 1).toInt()
                dialogNewEvent!!.getWindow().setAttributes(lp)
                dialogNewEvent!!.ll_alert.setOnClickListener(this)
                dialogNewEvent!!.btn_cancel.setOnClickListener(this)
                dialogNewEvent!!.tv_start_time.setOnClickListener(this)
                dialogNewEvent!!.tv_endtime.setOnClickListener(this)
                dialogNewEvent!!.rv_contact_Alllist.layoutParams.height = CommonUtils.returmWindowHeight(activity) / 5.toInt()
                dialogNewEvent!!.semigray.setOnClickListener(this)
                dialogNewEvent!!.bgcolor.setOnClickListener(this)


                var c = Calendar.getInstance().getTime();
                var df = SimpleDateFormat("dd MMM yyyy")
                var formattedDate = df.format(c)

                dialogNewEvent!!.et_contact_name.addTextChangedListener(object : TextWatcher {
                    override fun afterTextChanged(mEdit: Editable) {

                        if (!TextUtils.isEmpty(mEdit.toString()))
                            ApiForGetContact(mEdit.toString())
                        else
                            dialogNewEvent!!.rv_contact_Alllist.visibility = View.GONE

                    }

                    override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
                    override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
                })

                dialogNewEvent!!.tv_date.setOnClickListener(this)
                dialogNewEvent!!.btn_save_event.setOnClickListener(this)
                dialogNewEvent!!.show()
            }//            R.id.show_week->

        }
    }

    var date: DatePickerDialog.OnDateSetListener = object : DatePickerDialog.OnDateSetListener {

        override fun onDateSet(view: DatePicker, year: Int, monthOfYear: Int,
                               dayOfMonth: Int) {
            // TODO Auto-generated method stub
            myCalendar.set(Calendar.YEAR, year)
            myCalendar.set(Calendar.MONTH, monthOfYear)
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)

            var c = Calendar.getInstance().getTime();
            var df = SimpleDateFormat("d/M/yyyy")

            var formattedDate = df.format(c)

            var datepickerdate = "" + dayOfMonth + "/" + monthOfYear + "/" + year

            dialogNewEvent!!.tv_date.setText("" + dayOfMonth + "-" +( monthOfYear +1)+ "-" + year)
        }

    }


    private fun openTimePicker(s: String) {
        val mcurrentTime = Calendar.getInstance()
        val hour = mcurrentTime.get(Calendar.HOUR_OF_DAY)
        val minute = mcurrentTime.get(Calendar.MINUTE)
//        val am_pm = mcurrentTime.get(Calendar.AM_PM)
        val mTimePicker: TimePickerDialog


        mTimePicker = TimePickerDialog(activity, object : TimePickerDialog.OnTimeSetListener {

            override fun onTimeSet(timePicker: TimePicker, selectedHour: Int, selectedMinute: Int) {
                val mcurrentTimeOld = Calendar.getInstance()
                val hour = mcurrentTimeOld.set(Calendar.HOUR_OF_DAY, timePicker.hour)
                val minute = mcurrentTimeOld.set(Calendar.MINUTE, selectedMinute)
                if (s.equals("start")) {


                    if (mcurrentTimeOld.timeInMillis > mcurrentTime.timeInMillis) {
                        dialogNewEvent!!.tv_start_time.setText("" + timePicker.hour + ":" + selectedMinute)
                    } else {
                        showSnackBar("Invalid Time")
                    }
                } else

                    if ((mcurrentTimeOld.timeInMillis) > mcurrentTime.timeInMillis + 300000) {
                        dialogNewEvent!!.tv_endtime.setText("" + timePicker.hour + ":" + selectedMinute)

                    } else {
                        showSnackBar("Invalid Time")
                    }
            }
        }, hour, minute, true)//Yes 24 hour time
        mTimePicker.setTitle("Select Time")
        mTimePicker.show()
    }


    private fun CreateEventAPI(event: String, locatio: String, date: String, starttime: String, endtime: String, labelcolor: String) {


        var inputPattern = "dd-MM-yyyy";
        var inputFormat = SimpleDateFormat(inputPattern);


        var dateInput = inputFormat.parse(date);


        var format = SimpleDateFormat("yyyy-MM-dd");

        var date = format.format(Date.parse(dateInput.toString()))


        val apiService = ApiClient.getClient().create(WebService::class.java)
        var abc: CreateCalendarModel = CreateCalendarModel()
        abc.event = event
        abc.location = locatio
        abc.lat = lat
        abc.lng = lng
        abc.date = date
        abc.start = starttime
        abc.end = endtime
        abc.alert = currentAlertPosition
        abc.label = labelcolor

        if (contactListId.size != null && contactListId.size >= 0) {
            for (i in 0..contactListId.size - 1) {
                abc.invitations.add(contactListId.get(i))
            }

        }

        var pref = AppPreferencesHelper(activity)

        val call = apiService.postCreateCalendar(abc, "Bearer " + pref.user_XAUTH)
        showLoading()
        call.enqueue(object : retrofit2.Callback<ModelCreateCalendar> {
            override fun onResponse(call: Call<ModelCreateCalendar>, response: Response<ModelCreateCalendar>?) {

                if (response != null) {
                    if (response.isSuccessful && (response.body().statusCode == Constant.RESPONSE_SUCCESSFULLY || response.body().statusCode == Constant.RESPONSE_SUCCESSFULLLY)) {
                        hideLoading()
                        setNotificationAlarm(abc)
                        showSnackBar(response.body().message)
                        dialogNewEvent!!.dismiss()

                    } else if (response.code() == Constant.RESPONSE_UNAUTHORIZED || response.code() == Constant.RESPONSE_FAILURE) {
                        hideLoading()
                        try {
                            val jObjError = JSONObject(response.errorBody().string())
                            showSnackBar(jObjError.getString("message"))
                        } catch (e: Exception) {
                            showSnackBar("" + e.message)
                        }
                    }
                }
            }

            override fun onFailure(call: Call<ModelCreateCalendar>, t: Throwable) {
                // Log error here since request failed
                hideLoading()
                showSnackBar(getString(R.string.failure_please_try_again))
            }


        })
    }
    private fun setNotificationAlarm(calendarEvent: CreateCalendarModel) {
        if(currentAlertPosition ==0)
            return
        var format = SimpleDateFormat("yyyy-MM-dd hh:mm")
        var date =format.parse(calendarEvent.date+" "+calendarEvent.start)
        var eventMsg="This is a reminder you have an Event on "+calendarEvent.date+" "+calendarEvent.start
        AlarmReceiver.setReminder(activity,date,getAlertTimeMili(),eventMsg)
    }

    private fun getAlertTimeMili() : Long{
        when(currentAlertPosition) {
            8 -> {
                return 1000 * 60 * 60 * 48
            }
            7 -> {
                return 1000*60*60*24
            }

            6 -> {
                return 1000*60*60*2
            }

            5 -> {
                return 1000*60*60*1
            }

            4 -> {
                return 1000*60*30
            }

            3 -> {
                return 1000*60*15
            }

            2 -> {
                return 1000*60*5

            }
            1 -> {
                return 0
            }
        }
        return 0
    }
    private fun SetMeetingTime(s: String) {
        dialogNewEvent!!.tv_meeting_time.setText(s)
    }

    private fun ApiForGetContact(value: String) {
        val apiService = ApiClient.getClient().create(WebService::class.java)
        var app = AppPreferencesHelper(activity)
        showLoading()
        val call = apiService.getSearchCalendar(value, "Bearer " + app.user_XAUTH)
        call.enqueue(object : retrofit2.Callback<ModelUserSearch> {
            override fun onResponse(call: Call<ModelUserSearch>, response: Response<ModelUserSearch>?) {
                hideLoading()
                if (response != null) {
                    if (response.isSuccessful && (response.body().statusCode == Constant.RESPONSE_SUCCESSFULLY || response.body().statusCode == Constant.RESPONSE_SUCCESSFULLLY)) {

                        arrayListPayloadSearch.clear()


                        if (response.body().payload != null && response.body().payload.size > 0) {

                            for (i in 0..response.body().payload.size - 1) {

                                var payloadSearch = com.beyond.ve.beyondapp.Model.UserSearch.Payload()
                                payloadSearch.firstName = response.body().payload.get(i).firstName
                                payloadSearch.lastName = response.body().payload.get(i).lastName
                                payloadSearch.avatar = response.body().payload.get(i).avatar
                                payloadSearch.id = response.body().payload.get(i).id
                                arrayListPayloadSearch.add(payloadSearch)
                            }
                        }
                        dialogNewEvent!!.rv_contact_Alllist.visibility = View.VISIBLE

                        if (allContactList == null) {
                            allContactList = AllContactListAdapter(mActivity, arrayListPayloadSearch, dialogNewEvent!!.rv_contact_name)

                            val mLayoutManager = LinearLayoutManager(activity)
                            dialogNewEvent!!.rv_contact_Alllist.setLayoutManager(mLayoutManager)
                            dialogNewEvent!!.rv_contact_Alllist.setItemAnimator(DefaultItemAnimator())
                            dialogNewEvent!!.rv_contact_Alllist.setAdapter(allContactList)
                        } else {
//                            allContactList = AllContactListAdapter(mActivity, arrayListPayloadSearch, dialogNewEvent!!.rv_contact_name)

                            allContactList!!.notifyDataSetChanged()

                        }

                    } else if (response.code() == Constant.RESPONSE_UNAUTHORIZED || response.code() == Constant.RESPONSE_FAILURE) {
                        hideLoading()
                        try {
                            val jObjError = JSONObject(response.errorBody().string())
                            showSnackBar(jObjError.getString("message"))
                        } catch (e: Exception) {
                            showSnackBar("" + e.message)

                        }

                    }
                }
            }

            override fun onFailure(call: Call<ModelUserSearch>, t: Throwable) {
                // Log error here since request failed
                hideLoading()
                showSnackBar(getString(R.string.failure_please_try_again))


            }


        })
    }

    public fun setWeightProgramtically() {
        dialogNewEvent!!.rv_contact_Alllist.visibility = View.GONE
        dialogNewEvent!!.rv_contact_name.visibility = View.VISIBLE
    }

    fun hideRecylerViewAfterSelect() {
        setWeightProgramtically()
    }

    fun addItemInList(id: Int) {
        contactListId.add(id)
        dialogNewEvent!!.et_contact_name.setText("")
        dialogNewEvent!!.et_contact_name.setHint("Contact name")
    }

    fun removeItemInList(id: Int) {
        contactListId.remove(id)

    }

    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    val mActivity = this;
    private var currentAlertPosition = 0;

    val myCalendar = Calendar.getInstance()


    var dialogNewEventAlert: Dialog? = null
    var dialogNewEvent: Dialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment



            return inflater.inflate(R.layout.fragment_schedule_weekly, container, false)


    }

    private fun getSectionCallback(people: List<Payload>): com.beyond.ve.beyondapp.Dumnp.RecyclerSectionItemDecoration.SectionCallback {

        return object : com.beyond.ve.beyondapp.Dumnp.RecyclerSectionItemDecoration.SectionCallback {
            override fun isSection(position: Int): Boolean {
                return true
            }

            override fun getSectionHeader(position: Int): CharSequence {
                return people.get(position).day + " " + people.get(position).date
            }
        }
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        var recentMettingList: ArrayList<Payload> = getArguments()!!.getSerializable("recentMettingList") as ArrayList<Payload>

        newevent.setOnClickListener(this)

        mAdapter = MeetingScheduleAdapter(recentMeetingList, activity, "week")
        val mLayoutManager = LinearLayoutManager(activity)
        rv_meeting_schedule_weekly.setLayoutManager(mLayoutManager)
        rv_meeting_schedule_weekly.setItemAnimator(DefaultItemAnimator())

        val sectionItemDecoration =
                com.beyond.ve.beyondapp.Dumnp.RecyclerSectionItemDecoration(resources.getDimensionPixelSize(R.dimen.sizeNotificationBg),
                        true, getSectionCallback(recentMettingList), "week")

        rv_meeting_schedule_weekly.addItemDecoration(sectionItemDecoration)
//
        rv_meeting_schedule_weekly.setAdapter(mAdapter)
        show_month.setOnClickListener(this)
        calendarViewWeekly.topbarVisible = false
        calendarViewWeekly.state().edit()
                .setFirstDayOfWeek(Calendar.SUNDAY)
                .setMinimumDate(CalendarDay.from(2016, 4, 3))
                .setMaximumDate(CalendarDay.from(2095, 5, 12))
                .setCalendarDisplayMode(CalendarMode.WEEKS)
                .commit();
        show_month.setOnClickListener(this)
        var calendar = Calendar.getInstance();
        month_name.setText("" + calendar.getDisplayName(Calendar.MONTH, Calendar.SHORT, Locale.getDefault()))
        calendarViewWeekly.setDateSelected(calendar.getTime(), true);
        calendarViewWeekly.setOnMonthChangedListener(object : OnMonthChangedListener {
            @Override
            override fun onMonthChanged(widget: MaterialCalendarView, date: CalendarDay) {

                month_name.setText("" + CommonUtils.returnMonthNameShort(widget.currentDate.month))
            }
        })

        calendarViewWeekly.setOnDateChangedListener(object : CalendarView.OnDateChangeListener, OnDateSelectedListener {
            override fun onSelectedDayChange(p0: CalendarView, p1: Int, p2: Int, p3: Int) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun onDateSelected(widget: MaterialCalendarView, date: CalendarDay, selected: Boolean) {
//                tv_currentDate.setText("" + date.date.date)
                var a = widget.currentDate.month + 1;

                filter("" + date.date.date + "-" + String.format("%02d", a) + "-" + widget.currentDate.year)

            }
        })


    }

    fun filter(text: String) {

        showMessage(text)
        val temp = ArrayList<Payload>()
        for (i in 0..movieListFilter.size - 1) {
            if (movieListFilter[i].date.contains(text)) {
                temp.add(movieListFilter[i])
            }
        }
        updateList(temp)
    }

    fun updateList(list: ArrayList<Payload>) {
        recentMeetingList.clear()
        recentMeetingList.addAll(list)
        mAdapter!!.notifyDataSetChanged()
    }

}
