package com.beyond.ve.beyondapp.ui

import android.Manifest
import android.app.DatePickerDialog
import android.app.Dialog
import android.app.TimePickerDialog
import android.content.IntentSender
import android.content.pm.PackageManager
import android.location.Location
import android.os.Bundle
import android.os.Looper
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.util.Log
import android.view.*
import android.view.animation.AnimationUtils
import android.widget.CalendarView
import android.widget.DatePicker
import android.widget.TimePicker
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import com.beyond.ve.beyondapp.BaseActivity.BaseFragment
import com.beyond.ve.beyondapp.Adapter.AllContactListAdapter
import com.beyond.ve.beyondapp.Adapter.MeetingScheduleAdapter
import com.beyond.ve.beyondapp.Constant
import com.beyond.ve.beyondapp.Model.CreateCalendarModel
import com.beyond.ve.beyondapp.Model.FetchAllCalendar.Datum
import com.beyond.ve.beyondapp.Model.FetchAllCalendar.ModelAllCalendar
import com.beyond.ve.beyondapp.Model.FetchAllCalendar.Payload
import com.beyond.ve.beyondapp.Model.ModelCreateCalendar
import com.beyond.ve.beyondapp.Model.UserSearch.ModelUserSearch
import com.beyond.ve.beyondapp.R
import com.beyond.ve.beyondapp.Utils.CommonUtils
import com.beyond.ve.beyondapp.Utils.NetworkUtils
import com.beyond.ve.beyondapp.Web.ApiClient
import com.beyond.ve.beyondapp.Web.WebService
import com.beyond.ve.beyondapp.data.AppPreferencesHelper
import com.beyond.ve.beyondapp.reciver.AlarmReceiver
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.common.api.ResultCallback
import com.google.android.gms.location.*
import com.prolificinteractive.materialcalendarview.CalendarDay
import com.prolificinteractive.materialcalendarview.MaterialCalendarView
import com.prolificinteractive.materialcalendarview.OnDateSelectedListener
import com.prolificinteractive.materialcalendarview.OnMonthChangedListener
import kotlinx.android.synthetic.main.dialog_new_event.*
import kotlinx.android.synthetic.main.dialog_new_event_alert.*
import kotlinx.android.synthetic.main.fragment_schedule.*
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Response
import java.sql.Time
import java.text.Format
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"
private var START_TIME: String? = ""
private var END_TIME: String? = ""
private var b = true
private var currentAlertPosition = 0;
private var mAdapter: MeetingScheduleAdapter? = null
private var allContactList: AllContactListAdapter? = null
var recentMeetingList = ArrayList<Payload>()
var movieListFilter = ArrayList<Payload>()

var contactListId = ArrayList<Int>()
internal val REQUEST_LOCATION = 199
var arrayListPayloadSearch = ArrayList<com.beyond.ve.beyondapp.Model.UserSearch.Payload>()
val starttime = Calendar.getInstance()

class ScheduleFragmentMonthly : BaseFragment(), View.OnClickListener, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener {
    override fun onConnectionFailed(p0: ConnectionResult) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onLocationChanged(p0: Location?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onConnectionSuspended(p0: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onConnected(p0: Bundle?) {

        if (ContextCompat.checkSelfPermission(
                        activity!!,
                        Manifest.permission.ACCESS_COARSE_LOCATION
                ) == PackageManager.PERMISSION_GRANTED
        ) {
            mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this.activity!!)

            mLocationCallback = LocationCallback()

            mFusedLocationClient?.requestLocationUpdates(mLocationRequest, mLocationCallback, Looper.myLooper())


            mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                    mGoogleApiClient
            );

            if (mLastLocation != null) {
                lat = mLastLocation!!.getLatitude();
                lng = mLastLocation!!.getLongitude();
            } else {
                mLocationRequest = LocationRequest.create()
                mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                mLocationRequest.setInterval(30 * 1000)
                mLocationRequest.setFastestInterval(5 * 1000)
                val builder = LocationSettingsRequest.Builder()
                        .addLocationRequest(mLocationRequest)
                builder.setAlwaysShow(true)
                var result = LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build())
                result.setResultCallback(object : ResultCallback<LocationSettingsResult> {
                    override fun onResult(result: LocationSettingsResult) {
                        val status = result.getStatus()
                        //final LocationSettingsStates state = result.getLocationSettingsStates();
                        when (status.getStatusCode()) {
                            LocationSettingsStatusCodes.SUCCESS -> {
                            }
                            LocationSettingsStatusCodes.RESOLUTION_REQUIRED ->
                                try {
                                    status.startResolutionForResult(
                                            getActivity(),
                                            REQUEST_LOCATION)
                                } catch (e: IntentSender.SendIntentException) {
                                }
                            LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE -> {
                            }
                        }
                    }
                })
            }
        }
    }

    override fun onStart() {
        super.onStart()
        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }
    }

    override fun onResume() {
        super.onResume()
        try{
            mGoogleApiClient.connect()
        }catch (e :java.lang.Exception){
        }
    }

    override fun onPause() {
        super.onPause()

        try{
            if (mGoogleApiClient.isConnected()) {
                LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
                mGoogleApiClient.disconnect();
            }
        }catch (e :java.lang.Exception){
        }

    }

    override fun onClick(p0: View?) {
        when (p0!!.id) {
            R.id.show_week -> {
                if (recentMeetingList != null && recentMeetingList.size > 0) {
                    var fragment: ScheduleFragmentWeekly =
                            ScheduleFragmentWeekly();
                    var b = Bundle()
                    b.putSerializable("recentMettingList", recentMeetingList)
                    fragment.arguments = b
                    val ft = activity!!.supportFragmentManager.beginTransaction()
                    ft.setCustomAnimations(R.anim.entry_from_right, R.anim.exit_to_left);
                    ft.replace(R.id.flContent, fragment).addToBackStack(null)
                    ft.commit()
                } else {
                    showSnackBar("There is no event.")
                }
            }

            R.id.add_new_event -> {
                dialogNewEvent = Dialog(activity)
                dialogNewEvent!!.setContentView(R.layout.dialog_new_event)
                val lp = WindowManager.LayoutParams()
                lp.copyFrom(dialogNewEvent!!.getWindow().getAttributes())
                lp.width = (CommonUtils.returmWindowWidth(activity) / 1)
                dialogNewEvent!!.getWindow().setAttributes(lp)
                dialogNewEvent!!.ll_alert.setOnClickListener(this)
                dialogNewEvent!!.btn_cancel.setOnClickListener(this)
                dialogNewEvent!!.tv_start_time.setOnClickListener(this)
                dialogNewEvent!!.tv_endtime.setOnClickListener(this)
                dialogNewEvent!!.rv_contact_Alllist.layoutParams.height = CommonUtils.returmWindowHeight(activity) / 5.toInt()
                dialogNewEvent!!.semigray.setOnClickListener(this)
                dialogNewEvent!!.bgcolor.setOnClickListener(this)


                var c = Calendar.getInstance().getTime();
                var df = SimpleDateFormat("dd MMM yyyy")
                var formattedDate = df.format(c)

                dialogNewEvent!!.et_contact_name.addTextChangedListener(object : TextWatcher {
                    override fun afterTextChanged(mEdit: Editable) {

                        if (!TextUtils.isEmpty(mEdit.toString()))
                            ApiForGetContact(mEdit.toString())
                        else
                            dialogNewEvent!!.rv_contact_Alllist.visibility = View.GONE
                    }
                    override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
                    override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
                })

                dialogNewEvent!!.tv_date.setOnClickListener(this)
                dialogNewEvent!!.btn_save_event.setOnClickListener(this)
                dialogNewEvent!!.show()
            }

            R.id.iv_alert_cancel -> {
                dialogNewEventAlert!!.cancel()
            }

            R.id.tv_none -> {
                currentAlertPosition = 0;
                dialogNewEventAlert!!.cancel()
                SetMeetingTime("None")

            }

            R.id.tv_at_the_time_of_event -> {
                currentAlertPosition = 1;
                dialogNewEventAlert!!.cancel()
                SetMeetingTime("At time of event")
            }

            R.id.tv_two_day_before -> {
                currentAlertPosition = 8;
                dialogNewEventAlert!!.cancel()

                SetMeetingTime("2 day before")

            }

            R.id.tv_one_day_before -> {
                currentAlertPosition = 7;
                dialogNewEventAlert!!.cancel()
                SetMeetingTime("1 day before")

            }

            R.id.tv_two_hours_before -> {

                currentAlertPosition = 6;
                dialogNewEventAlert!!.cancel()

                SetMeetingTime("2 hour before")

            }

            R.id.tv_one_hour_before -> {
                currentAlertPosition = 5;
                dialogNewEventAlert!!.cancel()
                SetMeetingTime("1 hour before")

            }

            R.id.tv_threezero_min_before -> {
                currentAlertPosition = 4;
                dialogNewEventAlert!!.cancel()
                SetMeetingTime("30 min before")

            }

            R.id.tv_onefive_min_before -> {
                currentAlertPosition = 3;
                dialogNewEventAlert!!.cancel()
                SetMeetingTime("15 min before")
            }

            R.id.tv_five_min_before -> {
                currentAlertPosition = 2;
                dialogNewEventAlert!!.cancel()
                SetMeetingTime("5 min before")

            }

            R.id.tv_date -> {
                var datePickerDialog = DatePickerDialog(activity, date, myCalendar.get(Calendar.YEAR),
                        myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH))

                val c = Calendar.getInstance()
                c.add(Calendar.DATE, 0)
                val newDate = c.getTime()
                datePickerDialog.datePicker.minDate = newDate.time
                datePickerDialog.show()


            }

            R.id.btn_cancel -> {
                dialogNewEvent!!.cancel()
                allContactList = null
            }

            R.id.tv_start_time -> {
                openTimePicker("start")
            }

            R.id.tv_endtime -> {
                openTimePicker("end")
            }

            R.id.semigray -> {
                dialogNewEvent!!.semigray.setBackgroundResource(R.mipmap.ic_correct);
                dialogNewEvent!!.bgcolor.setBackgroundResource(R.drawable.circle_bgcolor)
            }

            R.id.bgcolor -> {
                dialogNewEvent!!.bgcolor.setBackgroundResource(R.mipmap.ic_correct_bgcolor);
                dialogNewEvent!!.semigray.setBackgroundResource(R.drawable.circle_semigray)
            }

            R.id.btn_save_event -> {


                if (NetworkUtils.isNetworkConnected(activity)) {

                    if (!TextUtils.isEmpty(dialogNewEvent!!.tie_event.text.toString())) {


                        if (!TextUtils.isEmpty(dialogNewEvent!!.tie_location.text.toString())) {

                            if (!TextUtils.isEmpty(dialogNewEvent!!.tv_date.text.toString())) {

                                if (!TextUtils.isEmpty(START_TIME)) {

                                    if (!TextUtils.isEmpty(END_TIME)) {

                                        if (Objects.equals(dialogNewEvent!!.semigray!!.getBackground().getConstantState(), activity!!.getResources().getDrawable(R.mipmap.ic_correct).getConstantState())) {
//
                                            CreateEventAPI(dialogNewEvent!!.tie_event.text.toString(),
                                                    dialogNewEvent!!.tie_location.text.toString()
                                                    , dialogNewEvent!!.tv_date.text.toString(),
                                                    START_TIME!!,
                                                    END_TIME!!,
                                                    "gray")

                                        } else if (Objects.equals(dialogNewEvent!!.bgcolor!!.getBackground().getConstantState(), activity!!.getResources().getDrawable(R.mipmap.ic_correct_bgcolor).getConstantState())) {

                                            CreateEventAPI(dialogNewEvent!!.tie_event.text.toString(),
                                                    dialogNewEvent!!.tie_location.text.toString()
                                                    , dialogNewEvent!!.tv_date.text.toString(),
                                                    START_TIME!!,
                                                    END_TIME!!,
                                                    "blue")


                                        } else {
                                            showSnackBar("Please select label")
                                        }


                                    } else {
                                        showSnackBar("Please select end time")
                                    }


                                } else {
                                    showSnackBar("Please select start time")
                                }


                            } else {
                                showSnackBar("Please select valid date")
                            }

                        } else {
                            showSnackBar("Please enter event location")
                        }


                    } else {
                        showSnackBar("Please enter event name")
                    }
                } else {
                    showSnackBar(getString(R.string.msg_nointerconncetion))
                }
            }

            R.id.ll_alert -> {
                dialogNewEventAlert = Dialog(activity)
                dialogNewEventAlert!!.setContentView(R.layout.dialog_new_event_alert)
                val lp = WindowManager.LayoutParams()
                lp.copyFrom(dialogNewEventAlert!!.getWindow().getAttributes())
                lp.width = (CommonUtils.returmWindowWidth(activity) / 1.1).toInt()
                dialogNewEventAlert!!.getWindow().setAttributes(lp)
                dialogNewEventAlert!!.iv_alert_cancel.setOnClickListener(this)
                dialogNewEventAlert!!.tv_two_day_before.setOnClickListener(this)
                dialogNewEventAlert!!.tv_one_day_before.setOnClickListener(this)
                dialogNewEventAlert!!.tv_two_hours_before.setOnClickListener(this)
                dialogNewEventAlert!!.tv_one_hour_before.setOnClickListener(this)
                dialogNewEventAlert!!.tv_threezero_min_before.setOnClickListener(this)
                dialogNewEventAlert!!.tv_onefive_min_before.setOnClickListener(this)
                dialogNewEventAlert!!.tv_five_min_before.setOnClickListener(this)
                dialogNewEventAlert!!.tv_at_the_time_of_event.setOnClickListener(this)
                dialogNewEventAlert!!.tv_none.setOnClickListener(this)
                dialogNewEventAlert!!.show()
            }
        }
    }

    private fun ApiForGetContact(value: String) {
        val apiService = ApiClient.getClient().create(WebService::class.java)
        var app = AppPreferencesHelper(activity)
        showLoading()
        val call = apiService.getSearchCalendar(value, "Bearer " + app.user_XAUTH)
        call.enqueue(object : retrofit2.Callback<ModelUserSearch> {
            override fun onResponse(call: Call<ModelUserSearch>, response: Response<ModelUserSearch>?) {
                hideLoading()
                if (response != null) {
                    if (response.isSuccessful && (response.body().statusCode == Constant.RESPONSE_SUCCESSFULLY || response.body().statusCode == Constant.RESPONSE_SUCCESSFULLLY)) {

                        arrayListPayloadSearch.clear()

                        if (response.body().payload != null && response.body().payload.size > 0) {

                            for (i in 0..response.body().payload.size - 1) {

                                var payloadSearch = com.beyond.ve.beyondapp.Model.UserSearch.Payload()
                                payloadSearch.firstName = response.body().payload.get(i).firstName
                                payloadSearch.lastName = response.body().payload.get(i).lastName
                                payloadSearch.avatar = response.body().payload.get(i).avatar
                                payloadSearch.id = response.body().payload.get(i).id
                                arrayListPayloadSearch.add(payloadSearch)
                            }
                        }
                        dialogNewEvent!!.rv_contact_Alllist.visibility = View.VISIBLE

                        if (allContactList == null) {
                            allContactList = AllContactListAdapter(mActivity, arrayListPayloadSearch, dialogNewEvent!!.rv_contact_name)
                            val mLayoutManager = LinearLayoutManager(activity)
                            dialogNewEvent!!.rv_contact_Alllist.setLayoutManager(mLayoutManager)
                            dialogNewEvent!!.rv_contact_Alllist.setItemAnimator(DefaultItemAnimator())
                            dialogNewEvent!!.rv_contact_Alllist.setAdapter(allContactList)
                        } else {
//                            allContactList = AllContactListAdapter(mActivity, arrayListPayloadSearch, dialogNewEvent!!.rv_contact_name)
                            allContactList!!.notifyDataSetChanged()
                        }

                    } else if (response.code() == Constant.RESPONSE_UNAUTHORIZED || response.code() == Constant.RESPONSE_FAILURE) {
                        hideLoading()
                        try {
                            val jObjError = JSONObject(response.errorBody().string())
                            showSnackBar(jObjError.getString("message"))
                        } catch (e: Exception) {
                            showSnackBar("" + e.message)

                        }

                    }
                }
            }

            override fun onFailure(call: Call<ModelUserSearch>, t: Throwable) {
                // Log error here since request failed
                hideLoading()
                showSnackBar(getString(R.string.failure_please_try_again))


            }


        })
    }

    private fun SetMeetingTime(s: String) {
        dialogNewEvent!!.tv_meeting_time.setText(s)
    }


    fun hideRecylerViewAfterSelect() {
        setWeightProgramtically()
    }

    fun addItemInList(id: Int) {
        contactListId.add(id)
        dialogNewEvent!!.et_contact_name.setText("")
        dialogNewEvent!!.et_contact_name.setHint("Contact name")

    }

    fun removeItemInList(id: Int) {
        contactListId.remove(id)
        if (contactListId.size == 0) {
            dialogNewEvent!!.rv_contact_name.visibility = View.GONE
            val slideDown = AnimationUtils.loadAnimation(activity, R.anim.right_to_left)
            dialogNewEvent!!.et_contact_name.startAnimation(slideDown)
        }

    }

    public fun setWeightProgramtically() {
        dialogNewEvent!!.rv_contact_Alllist.visibility = View.GONE
        dialogNewEvent!!.rv_contact_name.visibility = View.VISIBLE

        if (contactListId.size < 1) {
            val slideDown = AnimationUtils.loadAnimation(activity, R.anim.left_to_right)
            dialogNewEvent!!.et_contact_name.startAnimation(slideDown)
        }

    }

    private fun CreateEventAPI(event: String, locatio: String, date: String, starttime: String, endtime: String, labelcolor: String) {


        var inputPattern = "dd-MM-yyyy"
        var inputFormat = SimpleDateFormat(inputPattern)
        var dateInput = inputFormat.parse(date)
        var format = SimpleDateFormat("yyyy-MM-dd")
        var date = format.format(Date.parse(dateInput.toString()))

        val apiService = ApiClient.getClient().create(WebService::class.java)
        var abc: CreateCalendarModel = CreateCalendarModel()
        abc.event = event
        abc.location = locatio
        abc.lat = lat
        abc.lng = lng
        abc.date = date
        abc.start = starttime
        abc.end = endtime
        abc.alert = currentAlertPosition
        abc.label = labelcolor
        abc.userTimezone=TimeZone.getDefault().id

        if (contactListId.size != null && contactListId.size >= 0) {
            for (i in 0..contactListId.size - 1) {
                abc.invitations.add(contactListId.get(i))
            }

        }

        var pref = AppPreferencesHelper(activity)

        val call = apiService.postCreateCalendar(abc, "Bearer " + pref.user_XAUTH)
        showLoading()
        call.enqueue(object : retrofit2.Callback<ModelCreateCalendar> {
            override fun onResponse(call: Call<ModelCreateCalendar>, response: Response<ModelCreateCalendar>?) {

                if (response != null) {
                    if (response.isSuccessful && (response.body().statusCode == Constant.RESPONSE_SUCCESSFULLY || response.body().statusCode == Constant.RESPONSE_SUCCESSFULLLY)) {
                        hideLoading()
                        setNotificationAlarm(abc)
                        showSnackBar(response.body().message)
                        dialogNewEvent!!.dismiss()
                        START_TIME =""
                        END_TIME=""

                        if (NetworkUtils.isNetworkConnected(context)) {
                            GetAllcaledendar()
                        } else {
                            showSnackBar("" + R.string.msg_nointerconncetion)
                        }

                    } else if (response.code() == Constant.RESPONSE_UNAUTHORIZED || response.code() == Constant.RESPONSE_FAILURE) {
                        hideLoading()
                        try {
                            val jObjError = JSONObject(response.errorBody().string())
                            showSnackBar(jObjError.getString("message"))
                        } catch (e: Exception) {
                            showSnackBar("" + e.message)
                        }

                    }
                }
            }

            override fun onFailure(call: Call<ModelCreateCalendar>, t: Throwable) {
                // Log error here since request failed
                hideLoading()
                showSnackBar(getString(R.string.failure_please_try_again))


            }


        })
    }

    private fun setNotificationAlarm(calendarEvent: CreateCalendarModel) {
        if(currentAlertPosition==0)
            return
        var format = SimpleDateFormat("yyyy-MM-dd hh:mm")
        var date =format.parse(calendarEvent.date+" "+calendarEvent.start)
        var eventMsg="This is a reminder you have an Event on "+calendarEvent.date+" "+calendarEvent.start
        AlarmReceiver.setReminder(activity,date,getAlertTimeMili(),eventMsg)
    }

    private fun getAlertTimeMili() : Long{
        when(currentAlertPosition) {
            8 -> {
                return 1000 * 60 * 60 * 48
            }
            7 -> {
                return 1000*60*60*24
            }

            6 -> {
                return 1000*60*60*2
            }

            5 -> {
                return 1000*60*60*1
            }

            4 -> {
                return 1000*60*30
            }

            3 -> {
                return 1000*60*15
            }

            2 -> {
                return 1000*60*5

            }
            1 -> {
                return 0
            }
        }
        return 0
    }

    private fun openTimePicker(s: String) {
        val mcurrentTime = Calendar.getInstance()
        val hour = mcurrentTime.get(Calendar.HOUR_OF_DAY)
        val minute = mcurrentTime.get(Calendar.MINUTE)
//        val am_pm = mcurrentTime.get(Calendar.AM_PM)
        val mTimePicker: TimePickerDialog


        mTimePicker = TimePickerDialog(activity, object : TimePickerDialog.OnTimeSetListener {

            override fun onTimeSet(timePicker: TimePicker, selectedHour: Int, selectedMinute: Int) {
                val mcurrentTimeOld = Calendar.getInstance()
                val hour = mcurrentTimeOld.set(Calendar.HOUR_OF_DAY, timePicker.hour)
                val minute = mcurrentTimeOld.set(Calendar.MINUTE, selectedMinute)
                if (s.equals("start")) {
                    starttime.set(Calendar.HOUR_OF_DAY, timePicker.hour)
                    starttime.set(Calendar.MINUTE, selectedMinute)

                    Log.e("check-----", ""+myCalendar.time + "-------" + Calendar.getInstance().time)
                    if (myCalendar.time > Calendar.getInstance().time) {
                        START_TIME = "" + timePicker.hour + ":" + selectedMinute
                        dialogNewEvent!!.tv_start_time.setText(convert12(timePicker.hour, selectedMinute))
                    } else if (mcurrentTimeOld.timeInMillis > mcurrentTime.timeInMillis) {
                        START_TIME = "" + timePicker.hour + ":" + selectedMinute
                        dialogNewEvent!!.tv_start_time.setText(convert12(timePicker.hour, selectedMinute))
                    } else {
                        showSnackBar("Invalid Time")
                    }
                } else

                    if ((mcurrentTimeOld.timeInMillis) > starttime.timeInMillis + 300000) {

                        END_TIME = "" + timePicker.hour + ":" + selectedMinute
                        dialogNewEvent!!.tv_endtime.setText(convert12(timePicker.hour, selectedMinute))

                    } else {
                        showSnackBar("Invalid Time")
                    }
            }
        }, hour, minute, true)//Yes 24 hour time
        mTimePicker.setTitle("Select Time")
        mTimePicker.show()
    }

    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    var dialogNewEventAlert: Dialog? = null
    var dialogNewEvent: Dialog? = null
    val myCalendar = Calendar.getInstance()

    val mActivity = this;


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }

        mGoogleApiClient = GoogleApiClient.Builder(this.activity!!)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API).build()

        mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval((10 * 1000).toLong())        // 10 seconds, in milliseconds
                .setFastestInterval((1 * 1000).toLong())
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        val contextThemeWrapper = ContextThemeWrapper(activity, R.style.AppThemeDatePicker)

        val localInflater = inflater.cloneInContext(contextThemeWrapper)
//        val spin = findViewById(R.id.sp_contact_list) as Spinner


        return localInflater.inflate(R.layout.fragment_schedule, container, false)
    }

    var date: DatePickerDialog.OnDateSetListener = object : DatePickerDialog.OnDateSetListener {

        override fun onDateSet(view: DatePicker, year: Int, monthOfYear: Int,
                               dayOfMonth: Int) {
            // TODO Auto-generated method stub
            myCalendar.set(Calendar.YEAR, year)
            myCalendar.set(Calendar.MONTH, monthOfYear)
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)

            var c = Calendar.getInstance().getTime();
            var df = SimpleDateFormat("d/M/yyyy")

            var formattedDate = df.format(c)

            var datepickerdate = "" + dayOfMonth + "/" + monthOfYear + "/" + year

            dialogNewEvent!!.tv_date.setText("" + dayOfMonth + "-" + (monthOfYear+1) + "-" + year)
        }

    }


    private fun getTime(hr: Int, min: Int): String {
        val tme = Time(hr, min, 0)//seconds by default set to zero
        val formatter: Format
        formatter = SimpleDateFormat("hh:mm")
        return formatter.format(tme)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        show_week.setOnClickListener(this)

        calendarView.layoutParams.height = (CommonUtils.returmWindowHeight(activity) / 2.5).toInt()

        calendarView.topbarVisible = false


        calendarView.setOnMonthChangedListener(object : OnMonthChangedListener {
            @Override
            override fun onMonthChanged(widget: MaterialCalendarView, date: CalendarDay) {

                tv_currentMonth.setText("" + CommonUtils.returnMonthName(widget.currentDate.month) + " " + widget.currentDate.year)
            }
        });



        show_week.setOnClickListener(this)

        add_new_event.setOnClickListener(this)


        var calendar = Calendar.getInstance();

        tv_currentMonth.setText("" + calendar.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault()) + " " + calendar.get(Calendar.YEAR))

        tv_currentDate.setText("" + calendar.get(Calendar.DATE))


        calendarView.setDateSelected(calendar.getTime(), true);

        calendarView.setOnDateChangedListener(object : CalendarView.OnDateChangeListener, OnDateSelectedListener {
            override fun onSelectedDayChange(p0: CalendarView, p1: Int, p2: Int, p3: Int) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun onDateSelected(widget: MaterialCalendarView, date: CalendarDay, selected: Boolean) {
                tv_currentDate.setText("" + date.date.date)
                var a = widget.currentDate.month + 1;

                filter("" + date.date.date + "-" + String.format("%02d", a) + "-" + widget.currentDate.year)

            }
        })

        if (NetworkUtils.isNetworkConnected(context)) {
            GetAllcaledendar()
        } else {
            showSnackBar("" + R.string.msg_nointerconncetion)
        }


    }

    private fun GetAllcaledendar() {
        val apiService = ApiClient.getClient().create(WebService::class.java)
        val paramObject = HashMap<String, String>()
        var app = AppPreferencesHelper(activity)
        val call = apiService.getCalendar("Bearer " + app.user_XAUTH,TimeZone.getDefault().id)
        showLoading()
        call.enqueue(object : retrofit2.Callback<ModelAllCalendar> {
            override fun onResponse(call: Call<ModelAllCalendar>, response: Response<ModelAllCalendar>?) {

                if (response != null) {
                    if (response.isSuccessful && response.body().statusCode == Constant.RESPONSE_SUCCESSFULLLY && response.body().payload != null) {
                        hideLoading()

                        recentMeetingList.clear()

                        for (i in 0..response.body().payload.size - 1) {
                            var payload = Payload()
                            var datum = ArrayList<Datum>()
                            payload.date = response.body().payload[i].date
                            payload.day = response.body().payload[i].day

                            for (j in 0..response.body().payload[i].data.size - 1) {
                                var datam = Datum()
                                datam.alert = response.body().payload[i].data[j].alert
                                datam.id = response.body().payload[i].data[j].id
                                datam.userId = response.body().payload[i].data[j].userId
                                datam.createdAt = response.body().payload[i].data[j].createdAt
                                datam.date = response.body().payload[i].data[j].date
                                datam.description = response.body().payload[i].data[j].description
                                datam.end = response.body().payload[i].data[j].end
                                datam.event = response.body().payload[i].data[j].event
                                datam.start = response.body().payload[i].data[j].start
                                datam.label = response.body().payload[i].data[j].label
                                datum.add(datam)
                            }
                            payload.data = datum
                            recentMeetingList.add(payload)
                            movieListFilter.add(payload)

                            mAdapter = MeetingScheduleAdapter(recentMeetingList, activity, "month")
                            val mLayoutManager = LinearLayoutManager(activity)
                            if(rv_meeting_schedule==null)
                                return
                            rv_meeting_schedule.setLayoutManager(mLayoutManager)
                            rv_meeting_schedule.setItemAnimator(DefaultItemAnimator())
                            val sectionItemDecoration =
                                    com.beyond.ve.beyondapp.Dumnp.RecyclerSectionItemDecoration(resources.getDimensionPixelSize(R.dimen.sizeOnlineIndi7),true,
                                            getSectionCallback(recentMeetingList), "month")

                            rv_meeting_schedule.addItemDecoration(sectionItemDecoration)
//
                            rv_meeting_schedule.setAdapter(mAdapter)
                        }

                    } else if (response.code() == Constant.RESPONSE_UNAUTHORIZED) {
                        hideLoading()
                        try {
                            val jObjError = JSONObject(response.errorBody().string())
                            showSnackBar(jObjError.getString("message"))
                        } catch (e: Exception) {
                            showSnackBar("" + e.message)

                        }
                    } else {
                        if (response.body().payload == null) {
                            hideLoading()
                        }
                    }
                }
            }

            override fun onFailure(call: Call<ModelAllCalendar>, t: Throwable) {
                // Log error here since request failed
                hideLoading()
                showSnackBar(getString(R.string.failure_please_try_again))
            }
        })
    }

    fun convert12(strHour: Int, strMin: Int): String {
        if (strHour < 12)
            return strHour.toString() + ":" + strMin + " AM"
        else if (strHour === 12)
            return strHour.toString() + ":" + strMin + " PM"
        else
            return (strHour - 12).toString() + ":" + strMin + " PM"
    }

    fun filter(text: String) {

        val temp = ArrayList<Payload>()
        for (i in 0..movieListFilter.size - 1) {
            if (movieListFilter[i].date.contains(text)) {
                temp.add(movieListFilter[i])
            }
        }
        updateList(temp)
    }

    fun updateList(list: ArrayList<Payload>) {
        recentMeetingList.clear()
        recentMeetingList.addAll(list)
        if (mAdapter != null)
            mAdapter!!.notifyDataSetChanged()
    }

    private fun getSectionCallback(people: List<Payload>): com.beyond.ve.beyondapp.Dumnp.RecyclerSectionItemDecoration.SectionCallback {

        return object : com.beyond.ve.beyondapp.Dumnp.RecyclerSectionItemDecoration.SectionCallback {
            override fun isSection(position: Int): Boolean {
                return true
            }

            override fun getSectionHeader(position: Int): CharSequence {
                return people.get(position).day + " " + people.get(position).date
            }
        }
    }


}
