package com.beyond.ve.beyondapp.ui;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.beyond.ve.beyondapp.R;
import com.beyond.ve.beyondapp.Web.ApiClient;
import com.beyond.ve.beyondapp.Web.WebService;
import com.google.gson.JsonElement;

import org.json.JSONObject;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
public class ShowContentActivity extends AppCompatActivity {

    private TextView txtContent,txtTitle;
    private String valueAPI;
    public static final int KEY_ABOUT=1,KEY_TERMS=2,KEY_PRIVACY=3;
    public static String KEY_DATA="value";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_show_content);

        if (Build.VERSION.SDK_INT >= 21) {
            getWindow().setStatusBarColor(ContextCompat.getColor(this,R.color.bgColor)); //status bar or the time bar at the top
        }

        txtTitle= findViewById(R.id.txtTitle);
        txtContent = findViewById(R.id.txtContent);

        switch (getIntent().getExtras().getInt(KEY_DATA)){
            case KEY_ABOUT:
                txtTitle.setText("About");
                valueAPI="about";
                break;
            case KEY_TERMS:
                txtTitle.setText("Term & Condition");
                valueAPI="tc";
                break;
            case KEY_PRIVACY:
                txtTitle.setText("Privacy Policy");
                valueAPI="policy";
                break;
        }
        getContent();
        findViewById(R.id.imgBack).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private void getContent() {
        findViewById(R.id.lytBar).setVisibility(View.VISIBLE);
        WebService service= ApiClient.getClient().create(WebService.class);
        Call<JsonElement> call= service.getContent(valueAPI);
        call.enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                findViewById(R.id.lytBar).setVisibility(View.GONE);
                if(response.isSuccessful()){
                    String content=response.body().getAsJsonObject().get("payload").getAsJsonObject().get("content").getAsString();
                    txtContent.setText(content);
                }else{
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        Toast.makeText(ShowContentActivity.this,jObjError.getString("message"),Toast.LENGTH_SHORT).show();
                    } catch (Exception e) {
                    }
                }
            }
            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {
                t.printStackTrace();
                findViewById(R.id.lytBar).setVisibility(View.GONE);
            }
        });
    }

}
