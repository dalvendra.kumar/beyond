package com.beyond.ve.beyondapp.ui
import android.content.Intent
import android.os.Bundle
import android.text.InputType
import android.view.MotionEvent
import android.view.View
import android.widget.CompoundButton
import com.beyond.ve.beyondapp.BaseActivity.BaseActivity
import com.beyond.ve.beyondapp.R
import com.beyond.ve.beyondapp.Web.ApiClient
import com.beyond.ve.beyondapp.Web.WebService
import com.beyond.ve.beyondapp.data.AppPreferencesHelper
import com.google.firebase.messaging.FirebaseMessaging
import com.google.gson.JsonElement
import kotlinx.android.synthetic.main.activity_user_setting.*
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.HashMap

class UserSettingActivity : BaseActivity()  , View.OnClickListener, CompoundButton.OnCheckedChangeListener {
    override fun onCheckedChanged(buttonView: CompoundButton?, isChecked: Boolean) {
        if(isChecked) {
            FirebaseMessaging.getInstance().subscribeToTopic("beyondNotification")
        }else {
            FirebaseMessaging.getInstance().unsubscribeFromTopic("beyondNotification")
        }
        AppPreferencesHelper(this).notificationEnable=isChecked
    }

    private var PASSWORD_LENGTH=5
    private var isOldPassVis=false
    private var isNewPassVis=false
    private var isConfirmPassVis=false
    override fun onClick(p0: View?) {
        when(p0!!.id)
        {
            R.id.back_userSetting->
            {
                finish()
            }
            R.id.btnLogout->{
                AppPreferencesHelper(this).user_XAUTH=""
                val intent = Intent(applicationContext, ActivitySignupSignin::class.java)
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_CLEAR_TASK)
                intent.putExtra("isPrevSignIn",true)
                startActivity(intent)
                finishAffinity()
            }
            R.id.btnSave->{
                if(isValidPass())
                    updatePassword();
            }
            R.id.tv_about->{
                var about=Intent(this,ShowContentActivity::class.java)
                about.putExtra(ShowContentActivity.KEY_DATA,ShowContentActivity.KEY_ABOUT)
                startActivity(about)
            }
            R.id.tv_privacypolicy->{
                var about=Intent(this,ShowContentActivity::class.java)
                about.putExtra(ShowContentActivity.KEY_DATA,ShowContentActivity.KEY_PRIVACY)
                startActivity(about)
            }
            R.id.tv_termcondition->{
                var about=Intent(this,ShowContentActivity::class.java)
                about.putExtra(ShowContentActivity.KEY_DATA,ShowContentActivity.KEY_TERMS)
                startActivity(about)
            }

            R.id.tv_changePassword->
            {

                if(bolNewsFeeds) {

                    bolNewsFeeds = false

                    til_old_password.visibility = View.VISIBLE
                    til_new_password.visibility = View.VISIBLE
                    til_confirm_password.visibility = View.VISIBLE
                    btnSave.visibility = View.VISIBLE

                    ManageUpdateView()
                }
                else
                {
                    ManageUpdateView()

                    bolNewsFeeds = true
                    til_old_password.visibility = View.GONE
                    til_new_password.visibility = View.GONE
                    til_confirm_password.visibility = View.GONE
                    btnSave.visibility = View.GONE
                }
            }
        }
    }

    private fun isValidPass(): Boolean {
        if(tie_old_password.text!!.length<PASSWORD_LENGTH) {
            tie_old_password.setError("Invalid Password")
            return false
        }else  if(tie_new_password.text!!.length<PASSWORD_LENGTH) {
            tie_new_password.setError("Invalid Password")
            return false
        }else  if(tie_confirm_password.text!!.length<PASSWORD_LENGTH) {
            tie_confirm_password.setError("Invalid Password")
            return false
        }else  if(!tie_confirm_password.text.toString().equals(tie_new_password.text.toString())) {
            tie_confirm_password.setError("Both new & confirm password must be same.")
            return false
        }
        return true
    }

    private fun updatePassword() {
        val map = HashMap<String, String>()
        map["oldPassword"] = tie_old_password.text.toString()
        map["password"] = tie_new_password.text.toString()

        val service = ApiClient.getClient().create(WebService::class.java)
        val call = service.updatePassword("Bearer " + AppPreferencesHelper(this).user_XAUTH,map)
        call.enqueue(object : Callback<JsonElement> {
            override fun onResponse(call: Call<JsonElement>, response: Response<JsonElement>) {
                if (response.isSuccessful) {
                    showSnackBar("Password has successfully updated.")
                } else {
                    try {
                        val jObjError = JSONObject(response.errorBody().string())
                        showSnackBar(jObjError.getString("message"))
                    } catch (e: Exception) {
                        showSnackBar("" + e.message)
                    }

                }
            }

            override fun onFailure(call: Call<JsonElement>, t: Throwable) {
                t.printStackTrace()
            }
        })
    }

    var bolNewsFeeds = true

    var bolUpdats = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportActionBar!!.hide()
        setContentView(R.layout.activity_user_setting)

        back_userSetting.setOnClickListener(this)

        tv_changePassword.setOnClickListener(this)
        btnSave.setOnClickListener(this)
        btnLogout.setOnClickListener(this)
        tv_about.setOnClickListener(this)
        tv_termcondition.setOnClickListener(this)
        tv_privacypolicy.setOnClickListener(this)
        toggleButton.setOnCheckedChangeListener(this)
        managePassVisibility()
        if(AppPreferencesHelper(this).notificationEnable) {
            toggleButton.isChecked = true
            FirebaseMessaging.getInstance().subscribeToTopic("beyondNotification")
        }else {
            toggleButton.isChecked = false
            FirebaseMessaging.getInstance().unsubscribeFromTopic("beyondNotification")
        }
    }

    private fun managePassVisibility() {
        tie_old_password.setOnTouchListener(View.OnTouchListener { v, event ->
            val DRAWABLE_RIGHT = 2
            if (event.action == MotionEvent.ACTION_UP) {
                if (event.rawX >= tie_old_password.getRight() - tie_old_password.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width()) {
                    if (isOldPassVis) {
                        isOldPassVis = false
                        tie_old_password.setInputType(InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD)
                        tie_old_password.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_eye_off, 0)
                    } else {
                        isOldPassVis = true
                        tie_old_password.setInputType(InputType.TYPE_CLASS_TEXT)
                        tie_old_password.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_eye_on, 0)
                    }
                    return@OnTouchListener false
                }
            }
            false
        })
        tie_new_password.setOnTouchListener(View.OnTouchListener { v, event ->
            val DRAWABLE_RIGHT = 2
            if (event.action == MotionEvent.ACTION_UP) {
                if (event.rawX >= tie_new_password.getRight() - tie_new_password.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width()) {
                    if (isNewPassVis) {
                        isNewPassVis = false
                        tie_new_password.setInputType(InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD)
                        tie_new_password.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_eye_off, 0)
                    } else {
                        isNewPassVis = true
                        tie_new_password.setInputType(InputType.TYPE_CLASS_TEXT)
                        tie_new_password.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_eye_on, 0)
                    }
                    return@OnTouchListener false
                }
            }
            false
        })
        tie_confirm_password.setOnTouchListener(View.OnTouchListener { v, event ->
            val DRAWABLE_RIGHT = 2
            if (event.action == MotionEvent.ACTION_UP) {
                if (event.rawX >= tie_confirm_password.getRight() - tie_confirm_password.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width()) {
                    if (isConfirmPassVis) {
                        isConfirmPassVis = false
                        tie_confirm_password.setInputType(InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD)
                        tie_confirm_password.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_eye_off, 0)
                    } else {
                        isConfirmPassVis = true
                        tie_confirm_password.setInputType(InputType.TYPE_CLASS_TEXT)
                        tie_confirm_password.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_eye_on, 0)
                    }
                    return@OnTouchListener false
                }
            }
            false
        })
    }

    private fun ManageUpdateView() {
        if(bolUpdats) {
            iv_changepassword.animate().rotation(180F).start();
            bolUpdats = false
            til_old_password.setVisibility(View.VISIBLE);
            til_new_password.setVisibility(View.VISIBLE);
            til_confirm_password.setVisibility(View.VISIBLE);
        }

        else
        {
            bolUpdats = true
            iv_changepassword.animate().rotation(0F).start();

        }}

}
