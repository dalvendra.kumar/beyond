package com.beyond.ve.beyondapp.BaseActivity

import android.app.ProgressDialog
import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.os.PersistableBundle
import android.view.inputmethod.InputMethodManager
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.beyond.ve.beyondapp.R
import com.beyond.ve.beyondapp.Utils.CommonDialog
import com.beyond.ve.beyondapp.Utils.NetworkUtils
import com.androidadvance.topsnackbar.TSnackbar


open class BaseActivity : AppCompatActivity(), MvpView {
    override fun onCreate(savedInstanceState: Bundle?, persistentState: PersistableBundle?) {
        super.onCreate(savedInstanceState, persistentState)
        getSupportActionBar()!!.hide()
    }

    private var mProgressDialog: ProgressDialog? = null

    override fun showLoading() {
        hideLoading()
        mProgressDialog = CommonDialog.showLoadingDialog(this)
    }

    override fun hideLoading() {
        try {
            if (mProgressDialog != null && mProgressDialog!!.isShowing()) {
                mProgressDialog!!.cancel()
            }

        }catch (e : Exception)
        {}
    }

    fun showSnackBar(message: String) {


        val snackbar = TSnackbar.make(findViewById(android.R.id.content), message, TSnackbar.LENGTH_LONG)
        snackbar.setIconPadding(8)
        snackbar.setMaxWidth(3000) //if you want fullsize on tablets
        val snackbarView = snackbar.view
        snackbarView.setBackgroundColor(Color.parseColor("#F5F5F5"))
        val textView = snackbarView.findViewById(com.androidadvance.topsnackbar.R.id.snackbar_text) as TextView
        textView.setTextColor(resources.getColor(R.color.bgColor))
        snackbar.show()


//        val snackbar = Snackbar.make(findViewById<View>(android.R.id.content),
//            message, Snackbar.LENGTH_LONG
//        )
//        val sbView = snackbar.getView()
//        val textView = sbView
//            .findViewById(R.id.snackbar_text) as TextView
//        textView.setTextColor(ContextCompat.getColor(this, R.color.white))
//        snackbar.show()
    }
    override fun openActivityOnTokenExpire() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onError(resId: Int) {
        onError(getString(resId))
    }

    override fun onError(message: String?) {
        if (message != null) {
            showSnackBar(message)
        } else {
            showSnackBar(getString(R.string.error_msg))
        }
    }

    override fun showMessage(message: String?) {
        if (message != null) {
            Toast.makeText(this, message, Toast.LENGTH_LONG).show()
        } else {
            Toast.makeText(this, getString(R.string.error_msg), Toast.LENGTH_LONG).show()
        }
    }

    override fun showMessage(resId: Int) {
        showMessage(getString(resId))
    }

    override fun isNetworkConnected(): Boolean {
        return NetworkUtils.isNetworkConnected(applicationContext)
    }

    override fun hideKeyboard() {
        val view = this.currentFocus
        if (view != null) {
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_base)

    }
}
