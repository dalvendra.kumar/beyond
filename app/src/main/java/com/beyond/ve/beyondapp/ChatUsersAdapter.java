package com.beyond.ve.beyondapp;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.beyond.ve.beyondapp.Model.Contact;
import com.beyond.ve.beyondapp.Utils.ImageUtils;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import de.hdodenhof.circleimageview.CircleImageView;

public class ChatUsersAdapter extends RecyclerView.Adapter<ChatUsersAdapter.ViewHolder>{
    private ArrayList<Contact> users;
    private UserObserver observer;
    private int deviceWidth;

    public ChatUsersAdapter(ArrayList<Contact> users, int deviceWidth, UserObserver observer) {
        this.users = users;
        this.observer = observer;
        this.deviceWidth=deviceWidth;
    }

    public interface UserObserver{
        void onSelectUser(Contact user);
    }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.item_chat_users,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final Contact user=users.get(position);
        holder.txtName.setText(user.getFirst_name());
        holder.txtProfession.setText(user.getPosition());
        ImageUtils.displayImageFromUrl(holder.itemView.getContext(),holder.imgProfile,user.getAvatar());

        holder.imgProfile.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                if(user.isSelected())
                    user.setSelected(false);
                else
                    user.setSelected(true);
                observer.onSelectUser(user);
                notifyDataSetChanged();
                return false;
            }
        });

        if(user.isSelected())
            holder.imgSelectedIndi.setVisibility(View.VISIBLE);
        else
            holder.imgSelectedIndi.setVisibility(View.GONE);
    }

    @Override
    public int getItemCount() {
        return users.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView txtName,txtProfession;
        private CircleImageView imgProfile;
        private ImageView imgSelectedIndi;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            txtName=itemView.findViewById(R.id.txtName);
            txtProfession=itemView.findViewById(R.id.txtProfession);
            imgProfile=itemView.findViewById(R.id.imgProfile);
            imgSelectedIndi=itemView.findViewById(R.id.imgSelectedIndi);

            ConstraintLayout.LayoutParams params= (ConstraintLayout.LayoutParams) imgProfile.getLayoutParams();
            int size=deviceWidth/6;
            params.height=size;
            params.width=size;
            imgProfile.setLayoutParams(params);

            ConstraintLayout.LayoutParams paramsIndi= (ConstraintLayout.LayoutParams) imgSelectedIndi.getLayoutParams();
            paramsIndi.circleRadius=size/2;
            imgSelectedIndi.setLayoutParams(paramsIndi);

        }
    }
}
