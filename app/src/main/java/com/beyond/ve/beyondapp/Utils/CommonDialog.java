package com.beyond.ve.beyondapp.Utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.beyond.ve.beyondapp.Model.UserProfile;
import com.beyond.ve.beyondapp.R;


/**
 * Created by satendra on 29/11/17.
 */

public class CommonDialog {


    public static ProgressDialog showLoadingDialog(Context context) {
        ProgressDialog progressDialog = new ProgressDialog(context);
        if (!progressDialog.isShowing()) {
            progressDialog.show();
            if (progressDialog.getWindow() != null) {
                progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            }
            progressDialog.setContentView(R.layout.progress_dialog);
            progressDialog.setIndeterminate(true);
            progressDialog.setCancelable(true);
            progressDialog.setCanceledOnTouchOutside(false);
        }
        return progressDialog;
    }


    public static AlertDialog.Builder showProgressDialog(Context context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("NFC is disabled");
        builder.setMessage("Do you want to enable?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
        return builder;
    }

//    public static void showSnackBar(String message, Activity activity) {
//        Snackbar snackbar = Snackbar.make(findViewById<View>(android.R.id.content),message, Snackbar.LENGTH_LONG);
//        View sbView = snackbar.getView();
//        TextView textView = sbView.findViewById(R.id.snackbar_text);
//        textView.setTextColor(ContextCompat.getColor(activity, R.color.white));
//        snackbar.show();
//    }

    public static void showAlertDialogWithSingleButton(Context mActivity, String title , String message)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
        builder.setTitle(title);
        builder.setMessage(message)
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });

        AlertDialog alert = builder.create();
        alert.show();
    }
    public static void showAreYouSureAlert(Context mActivity, String message, final Runnable runnable)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
        builder.setTitle(mActivity.getString(R.string.app_name));
        builder.setMessage(message)
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                        runnable.run();
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

        AlertDialog alert = builder.create();
        alert.show();
    }


    public static void showAvailiabilityOpt(Activity activity, final UserProfile.Payload payload, final Runnable runnable) {
        //before inflating the custom alert dialog layout, we will get the current activity viewgroup
        ViewGroup viewGroup = activity.findViewById(android.R.id.content);

        //then we will inflate the custom alert dialog xml that we created
        View dialogView = LayoutInflater.from(activity).inflate(R.layout.dialog_availiability_opt, viewGroup, false);

        //Now we need an AlertDialog.Builder object
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);

        //setting the view of the builder to our custom view that we already inflated
        builder.setView(dialogView);

        //finally creating the alert dialog and displaying it
        final AlertDialog alertDialog = builder.create();

        TextView txtAvailiable= dialogView.findViewById(R.id.txtAvailiable);
        TextView txtBusy= dialogView.findViewById(R.id.txtBusy);
        TextView txtInvisible= dialogView.findViewById(R.id.txtInvisible);
        txtAvailiable.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                payload.setIsActive(1);
                runnable.run();
                alertDialog.dismiss();
            }
        });
        txtBusy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                payload.setIsActive(2);
                runnable.run();
                alertDialog.dismiss();
            }
        });
        txtInvisible.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                payload.setIsActive(3);
                runnable.run();
                alertDialog.dismiss();
            }
        });

        alertDialog.show();
    }
}
