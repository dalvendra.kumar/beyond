package com.beyond.ve.beyondapp;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.beyond.ve.beyondapp.Model.chat.ChatUser;
import com.beyond.ve.beyondapp.Utils.ImageUtils;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;
import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import de.hdodenhof.circleimageview.CircleImageView;

public class ChatsAdapter  extends RecyclerView.Adapter<ChatsAdapter.ViewHolder>{
    private ArrayList<ChatUser> users;
    private UserObserver observer;
    private int deviceWidth;
    private StorageReference storageRef;

    public ChatsAdapter(ArrayList<ChatUser> users,int deviceWidth, UserObserver observer) {
        this.users = users;
        this.observer = observer;
        this.deviceWidth=deviceWidth;
        this.storageRef= FirebaseStorage.getInstance().getReference();
    }

    public interface UserObserver{
        void onUser(ChatUser user);
        void onDelete(ChatUser user);
        void onArchive(ChatUser user);
    }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.item_chats,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        final ChatUser user=users.get(position);
        holder.txtName.setText(user.getName());
        holder.txtStatus.setText(user.getLastMsg());
        holder.txtMsgTime.setText(user.getLastUpdateTime());
        holder.txtMsgCount.setText(user.getNotificationCount());

        if(user.getOnline())
            holder.imgOnlineIndi.setBackgroundResource(R.drawable.online_indi);
        else
            holder.imgOnlineIndi.setBackgroundResource(R.drawable.offline_indi);

        if(user.getNotificationCount().equalsIgnoreCase("0"))
            holder.txtMsgCount.setVisibility(View.GONE);
        else
            holder.txtMsgCount.setVisibility(View.VISIBLE);

        if(user.getMsgReaded())
            holder.imgReadTick.setImageResource(R.mipmap.ic_tick_green);
        else
            holder.imgReadTick.setImageResource(R.mipmap.ic_chat_read);

        if(user.getId().startsWith("-"))
            ImageUtils.displayImageFromUrl(holder.itemView.getContext(), holder.imgProfile, user.getProfileUrl(),R.drawable.ic_group);
        else
            ImageUtils.displayImageFromUrl(holder.itemView.getContext(), holder.imgProfile, user.getProfileUrl(),R.drawable.ic_user_placeholdr);

        holder.lytUserDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                observer.onUser(user);
            }
        });
        holder.lytDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                observer.onDelete(user);
            }
        });
        holder.lytArchive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                observer.onArchive(user);
            }
        });
    }
    public void getDownloadableUrl(StorageReference storageRef, String imgPath){

    }
    @Override
    public int getItemCount() {
        return users.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView txtName,txtStatus,txtMsgTime,txtMsgCount;
        private CircleImageView imgProfile;
        private ImageView imgReadTick,imgOnlineIndi;
        private ConstraintLayout lytUserDetails;
        private LinearLayout lytDelete,lytArchive;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            txtName=itemView.findViewById(R.id.txtName);
            txtStatus=itemView.findViewById(R.id.txtStatus);
            txtMsgTime=itemView.findViewById(R.id.txtMsgTime);
            txtMsgCount=itemView.findViewById(R.id.txtMsgCount);
            imgProfile=itemView.findViewById(R.id.imgProfile);
            lytUserDetails=itemView.findViewById(R.id.lytUserDetails);
            lytDelete=itemView.findViewById(R.id.lytDelete);
            lytArchive=itemView.findViewById(R.id.lytArchive);
            imgReadTick=itemView.findViewById(R.id.imgReadTick);
            imgOnlineIndi=itemView.findViewById(R.id.imgOnlineIndi);

            ConstraintLayout.LayoutParams params= (ConstraintLayout.LayoutParams) imgProfile.getLayoutParams();
            int size=deviceWidth/7;
            params.height=size;
            params.width=size;
            imgProfile.setLayoutParams(params);

            ConstraintLayout.LayoutParams paramsIndi= (ConstraintLayout.LayoutParams) imgOnlineIndi.getLayoutParams();
            paramsIndi.circleRadius=size/2;
            imgOnlineIndi.setLayoutParams(paramsIndi);


        }
    }
}
