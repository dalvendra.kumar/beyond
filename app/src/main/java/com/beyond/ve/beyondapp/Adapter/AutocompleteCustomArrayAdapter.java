package com.beyond.ve.beyondapp.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.beyond.ve.beyondapp.R;

public class AutocompleteCustomArrayAdapter extends ArrayAdapter<String> {
 
    final String TAG = "AutocompleteCustomArrayAdapter.java";
         
    Context mContext;
    int layoutResourceId;
    String data[] = null;
 
    public AutocompleteCustomArrayAdapter(Context mContext, int layoutResourceId, String[] data) {
 
        super(mContext, layoutResourceId, data);
         
        this.layoutResourceId = layoutResourceId;
        this.mContext = mContext;
        this.data = data;
    }
 
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
         
        try{
             
            /*
             * The convertView argument is essentially a "ScrapView" as described is Lucas post
             * http://lucasr.org/2012/04/05/performance-tips-for-androids-listview/
             * It will have a non-null value when ListView is asking you recycle the row layout.
             * So, when convertView is not null, you should simply update its contents instead of inflating a new row layout.
             */
            if(convertView==null){
                // inflate the layout
                convertView =  LayoutInflater.from(getContext()).inflate(R.layout.item_spinner_contact_name, null);
            }
             
//            // object item_user_media based on the position
//            MyObject objectItem = data[position];
//
//            // get the TextView and then set the text (item_user_media name) and tag (item_user_media ID) values
//            TextView textViewItem = (TextView) convertView.findViewById(R.id.textViewItem);
//            textViewItem.setText(objectItem.objectName);
             
            // in case you want to add some style, you can do something like:
//            textViewItem.setBackgroundColor(Color.CYAN);
             
        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
         
        return convertView;
         
    }
}