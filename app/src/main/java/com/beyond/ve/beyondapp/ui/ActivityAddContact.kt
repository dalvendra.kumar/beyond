package com.beyond.ve.beyondapp.ui

import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.fragment.app.FragmentManager
import com.beyond.ve.beyondapp.BaseActivity.BaseActivity
import com.beyond.ve.beyondapp.Constant
import com.beyond.ve.beyondapp.Model.contact.ModelContact
import com.beyond.ve.beyondapp.R
import com.beyond.ve.beyondapp.Utils.CommonUtils
import com.beyond.ve.beyondapp.Web.ApiClient
import com.beyond.ve.beyondapp.Web.WebService
import com.beyond.ve.beyondapp.data.AppPreferencesHelper
import com.beyond.ve.beyondapp.imagePicker.CaptureImageUtils
import com.beyond.ve.beyondapp.imagePicker.ImageDialogFragment
import com.beyond.ve.beyondapp.imagePicker.imageSelectionInterface
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.android.gms.vision.Frame
import com.google.android.gms.vision.text.TextRecognizer
import kotlinx.android.synthetic.main.activity_add_contact.*
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Response
import java.io.File


class ActivityAddContact : BaseActivity(), View.OnClickListener, imageSelectionInterface, CaptureImageUtils.ImageSelectionListener {
    override fun capturedImage(uri: Uri?, imageFile: File?) {
        Log.e("check", uri.toString() + "----" + imageFile.toString())

        if (apiCall.equals("profile")) {
            if (imageFile != null) {
                this.imageFile = imageFile

                val options = RequestOptions()
                options.placeholder(R.drawable.ic_placeholder)
//            options.override(120, 120)
                options.error(R.drawable.ic_placeholder)
                options.circleCrop()
                Glide.with(this).load(imageFile.absolutePath).apply(options).into(profile_image)
            }
        } else if (apiCall.equals("back")) {
            if (imageFile != null) {
//                Runnable {
                datalist = readImageContents(BitmapFactory.decodeFile(imageFile.absolutePath), detector!!)
//                }
            }
        }
    }

    override fun selectedText(bean: String, fromWhere: Int?) {
        imageDialogFragment?.dismiss()
        handleDialogResponse(bean, fromWhere)

    }

    private var isCompanyClicked: Boolean = true
    private var isEdit: Boolean = false
    var imageDialogFragment: ImageDialogFragment? = null
    var manager: FragmentManager? = null
    var datalist = ArrayList<String>()
    private var captureImageUtils: CaptureImageUtils? = null
    var imageFile: File? = null
    var apiCall: String? = null
    var detector: TextRecognizer? = null
    val FIRST_NAME = 1
    val LAST_NAME = 2
    val COMAPNY_NAME = 3
    val FIELD_NAME = 4
    val POSITION_NAME = 5
    val COUNTRY_NAME = 6
    val MOBILE_NAME = 7
    val EMAIL_NAME = 8


    override fun onClick(p0: View?) {
        when (p0!!.id) {
            R.id.btn_profileSave -> validation()

            R.id.back -> onBackPressed()

            R.id.camera -> {
                captureImageUtils?.openSelectImageFromDialog("")
                apiCall = "profile"
            }
            R.id.tie_firstname -> {
                if (!isEdit)
                    callDataAdapter(FIRST_NAME, "Select FirstName")
                else {
                    tie_firstname.requestFocusFromTouch()
                    tie_firstname.requestFocus()
                    showKeyboard(tie_firstname)

                }
            }
            R.id.tie_lastname -> {
                if (!isEdit)
                    callDataAdapter(LAST_NAME, "Select LastName")
                else {
                    tie_lastname.requestFocusFromTouch()
                    tie_lastname.requestFocus()
                    showKeyboard(tie_lastname)

                }
            }
            R.id.tie_companyName -> {
                if (!isEdit) {
                    isCompanyClicked = true
                    callDataAdapter(COMAPNY_NAME, "Select Company")
                } else {
                    tie_companyName.requestFocusFromTouch()
                    tie_companyName.requestFocus()
                    showKeyboard(tie_companyName)

                }


            }
            R.id.tie_field -> {
                if (!isEdit)
                    callDataAdapter(FIELD_NAME, "Select Field")
                else {
                    tie_field.requestFocusFromTouch()
                    tie_field.requestFocus()
                    showKeyboard(tie_field)

                }
            }
            R.id.tie_position -> {

                if (!isEdit)
                    callDataAdapter(POSITION_NAME, "Select Position")
                else {
                    tie_position.requestFocusFromTouch()
                    tie_position.requestFocus()
                    showKeyboard(tie_position)

                }
            }
            R.id.tie_country -> {
                if (!isEdit)
                    callDataAdapter(COUNTRY_NAME, "Select Country")
                else {
                    tie_country.requestFocusFromTouch()
                    tie_country.requestFocus()
                    showKeyboard(tie_country)

                }
            }
            R.id.tie_mobile -> {
                if (!isEdit)
                    callDataAdapter(MOBILE_NAME, "Select Mobile")
                else {
                    tie_mobile.requestFocusFromTouch()
                    tie_mobile.requestFocus()
                    showKeyboard(tie_mobile)
                }
            }
            R.id.tie_email -> {
                if (!isEdit)
                    callDataAdapter(EMAIL_NAME, "Select Email")
                else {
                    tie_email.requestFocusFromTouch()
                    tie_email.requestFocus()
                    showKeyboard(tie_email)

                }
            }
            R.id.edit -> {
                handleEdit()
            }
        }
    }

    private fun validation() {

        if (!TextUtils.isEmpty(tie_firstname.text.toString())) {
            if (!TextUtils.isEmpty(tie_lastname.text.toString())) {
                if (!TextUtils.isEmpty(tie_companyName.text.toString())) {
                    if (!TextUtils.isEmpty(tie_field.text.toString())) {

                    }
                    if (!TextUtils.isEmpty(tie_field.text.toString())) {
                        if (!TextUtils.isEmpty(tie_position.text.toString())) {
                            if (!TextUtils.isEmpty(tie_country.text.toString())) {
                                if (!TextUtils.isEmpty(tie_email.text.toString())) {
                                    if (CommonUtils.isEmailValid(tie_email.text.toString())) {
                                        if (!TextUtils.isEmpty(tie_mobile.text.toString())) {
//
                                            if (imageFile != null)
                                                hitApi() else {
                                                showSnackBar(getString(R.string.please_choose_image))
                                            }

                                        } else {
                                            showSnackBar(getString(R.string.msg_enter_mobile_number))
                                        }
                                    } else {
                                        showSnackBar(getString(R.string.msg_enter_valid_email))
                                    }
                                } else {
                                    showSnackBar(getString(R.string.msg_enter_valid_email))
                                }
                            } else {
                                showSnackBar(getString(R.string.msg_enter_country))
                            }
                        } else {
                            showSnackBar(getString(R.string.msg_enter_position))
                        }
                    } else {
                        showSnackBar(getString(R.string.msg_enter_field))
                    }
                } else {
                    showSnackBar(getString(R.string.msg_enter_company_name))
                }
            } else {
                showSnackBar(getString(R.string.msg_enter_last_name))
            }
        } else {
            showSnackBar(getString(R.string.msg_enter_first_name))
        }

    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportActionBar!!.hide()
        setContentView(R.layout.activity_add_contact)
        var myFile = intent.getSerializableExtra("image") as? File
        detector = TextRecognizer.Builder(this).build();
        if (myFile != null) {

            Runnable {
                datalist = readImageContents(BitmapFactory.decodeFile(myFile.absolutePath), detector!!)
                Log.e("check123", datalist.toString())
            }

        }

        if (!TextUtils.isEmpty(intent.getStringExtra("email")))
            tie_email.setText(intent.getStringExtra("email"))

        initView()
        if (isEdit) {
            tie_firstname.setFocusable(true)
            tie_lastname.setFocusable(true)
            tie_companyName.setFocusable(true)
            tie_field.setFocusable(true)
            tie_position.setFocusable(true)
            tie_country.setFocusable(true)
            tie_mobile.setFocusable(true)
            tie_email.setFocusable(true)
        } else {
            tie_firstname.setFocusable(false)
            tie_lastname.setFocusable(false)
            tie_companyName.setFocusable(false)
            tie_field.setFocusable(false)
            tie_position.setFocusable(false)
            tie_country.setFocusable(false)
            tie_mobile.setFocusable(false)
            tie_email.setFocusable(false)

        }


    }

    fun initView() {
        captureImageUtils = CaptureImageUtils(this, this)

        tie_firstname.setOnClickListener(this)
        tie_lastname.setOnClickListener(this)
        tie_companyName.setOnClickListener(this)
        tie_field.setOnClickListener(this)
        tie_position.setOnClickListener(this)
        tie_country.setOnClickListener(this)
        tie_mobile.setOnClickListener(this)
        tie_email.setOnClickListener(this)

        btn_profileSave.setOnClickListener(this)
        camera.setOnClickListener(this)
        edit.setOnClickListener(this)
        back.setOnClickListener(this)

        captureImageUtils?.openSelectImageFromDialog("")
        apiCall = "back"
    }


    fun callDataAdapter(fromWhere: Int, headertext: String) {
        if (datalist.size > 0) {
            imageDialogFragment = ImageDialogFragment.newInstance(headertext, fromWhere, datalist)
            manager = supportFragmentManager
            imageDialogFragment!!.show(manager!!, "My Dialog")
        }
    }

    fun handleDialogResponse(bean: String, fromWhere: Int?) {
        when (fromWhere) {
            FIRST_NAME -> tie_firstname.setText(bean)
            LAST_NAME -> tie_lastname.setText(bean)
            COMAPNY_NAME -> tie_companyName.setText(bean)
            FIELD_NAME -> tie_field.setText(bean)
            POSITION_NAME -> tie_position.setText(bean)
            COUNTRY_NAME -> tie_country.setText(bean)
            MOBILE_NAME -> tie_mobile.setText(bean)
            EMAIL_NAME -> tie_email.setText(bean)
        }

    }

    fun handleEdit() {

        isEdit = !isEdit;
        if (isEdit) {
            tie_firstname.requestFocus()
            tie_firstname.setFocusable(true)
            tie_firstname.isEnabled = true
            tie_firstname.setFocusable(true)
            tie_lastname.setFocusable(true)
            tie_companyName.setFocusable(true)
            tie_field.setFocusable(true)
            tie_position.setFocusable(true)
            tie_country.setFocusable(true)
            tie_mobile.setFocusable(true)
            tie_email.setFocusable(true)
        } else {
            tie_firstname.setFocusable(false)
            tie_lastname.setFocusable(false)
            tie_companyName.setFocusable(false)
            tie_field.setFocusable(false)
            tie_position.setFocusable(false)
            tie_country.setFocusable(false)
            tie_mobile.setFocusable(false)
            tie_email.setFocusable(false)

        }


/*      tie_firstname.setFocusable(true)
      tie_lastname.setFocusable(true)
      tie_companyName.setFocusable(true)
      tie_field.setFocusable(true)
      tie_position.setFocusable(true)
      tie_country.setFocusable(true)
      tie_mobile.setFocusable(true)
      tie_email.setFocusable(true)


//        tie_firstname.setFocusable(true)


      tie_firstname.setEnabled(true)
      tie_companyName.setEnabled(true)

     tie_firstsetClickable(false)
      tie_lastname.setClickable(false)
      tie_companyName.setClickable(false)
      tie_field.setClickable(false)
      tie_position.setClickable(false)
      tie_country.setClickable(false)
      tie_mobile.setClickable(false)
      tie_email.setClickable(false)*/
    }


    fun hitApi() {
        showLoading()

        var img: MultipartBody.Part? = null
//        if (imageFile != null) {
        val requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), imageFile)
        img = MultipartBody.Part.createFormData("avatar", imageFile?.name, requestFile)
//        }

        var map = HashMap<String, RequestBody>()


        var first_name = RequestBody.create(MediaType.parse("text/plain"), tie_firstname.text.toString())
        map.put("first_name", first_name)

        var last_name = RequestBody.create(MediaType.parse("text/plain"), "" + tie_lastname.text.toString())
        map.put("last_name", last_name)

        var company_name = RequestBody.create(MediaType.parse("text/plain"), "" + tie_lastname.text.toString())
        map.put("company_name", company_name)

        var field = RequestBody.create(MediaType.parse("text/plain"), "" + tie_field.text.toString())
        map.put("field", field)

        var position = RequestBody.create(MediaType.parse("text/plain"), "" + tie_position.text.toString())
        map.put("position", position)

        var country = RequestBody.create(MediaType.parse("text/plain"), "" + tie_country.text.toString())
        map.put("country", country)

        var mobile = RequestBody.create(MediaType.parse("text/plain"), "" + tie_mobile.text.toString())
        map.put("mobile", mobile)

        var email = RequestBody.create(MediaType.parse("text/plain"), "" + tie_email.text.toString())
        map.put("email", email)

        val apiService = ApiClient.getClient().create(WebService::class.java)

        apiService.addContactLocal("Bearer " + AppPreferencesHelper(this).user_XAUTH, map, img).enqueue(object : retrofit2.Callback<ModelContact> {
            override fun onResponse(call: Call<ModelContact>, response: Response<ModelContact>?) {
                Log.e("hitConnectRequest onRes", response?.body().toString())
                hideLoading()
                if (response != null && response.body() != null) {
                    if ((response.body().statusCode == Constant.RESPONSE_SUCCESSFULLY || response.body().statusCode == Constant.RESPONSE_SUCCESSFULLLY)) {
                        hideLoading()
                        showSnackBar(response.body().message)
                        finish()
                    } else if (response.code() == Constant.RESPONSE_UNAUTHORIZED || response.code() == Constant.RESPONSE_FAILURE) {
                        hideLoading()
                        try {
                            val jObjError = JSONObject(response.errorBody().string())
                            showSnackBar(jObjError.getString("message"))
                        } catch (e: Exception) {
                            showSnackBar("" + e.message)
                        }
                    }
                }
            }

            override fun onFailure(call: Call<ModelContact>, t: Throwable) {
                hideLoading()
                Log.e("hitConnectRequest onFai", t.toString())
            }


        })
    }


    private fun handleError(t: Throwable?) {
        if (t != null)
            showMessage(t.message)
    }

    fun readImageContents(bitmap: Bitmap?, detector: TextRecognizer): ArrayList<String> {


        try {
            if (detector.isOperational() && bitmap != null) {
//                Runnable {
                val frame = Frame.Builder().setBitmap(bitmap).build()
                val textBlocks = detector.detect(frame)
                var blocks = ArrayList<String>()

                for (index in 0 until textBlocks.size()) {
                    //extract scanned text blocks here
                    val tBlock = textBlocks.valueAt(index)
                    blocks.add(tBlock.getValue())

                }
                if (textBlocks.size() === 0) {
                    Log.d("SCAN", "Scan Failed: Found nothing to scan")
                }
//                }
                return blocks
            } else {
                Log.d("SCAN", "Could not set up the detector!")
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }




        return ArrayList<String>()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            captureImageUtils?.onRequestPermission(requestCode, permissions, grantResults)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        captureImageUtils?.onActivityResult(requestCode, resultCode, data)
    }


    fun showKeyboard(view: View) {
        var imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT);
    }


}
