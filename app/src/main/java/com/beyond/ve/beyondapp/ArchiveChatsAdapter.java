package com.beyond.ve.beyondapp;

import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.beyond.ve.beyondapp.Model.chat.ChatUser;
import com.beyond.ve.beyondapp.Utils.ImageUtils;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import de.hdodenhof.circleimageview.CircleImageView;

public class ArchiveChatsAdapter extends RecyclerView.Adapter<ArchiveChatsAdapter.ViewHolder>{
    private ArrayList<ChatUser> users;
    private UserObserver observer;
    private StorageReference storageRef;
    private int deviceWidth;

    public ArchiveChatsAdapter(ArrayList<ChatUser> users, int deviceWidth, UserObserver observer) {
        this.users = users;
        this.deviceWidth = deviceWidth;
        this.observer = observer;
        storageRef= FirebaseStorage.getInstance().getReference();
    }

    public interface UserObserver{
        void onArchive(ChatUser user);
    }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.item_archive_chats,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        final ChatUser user=users.get(position);
        holder.txtName.setText(user.getName());
        holder.txtStatus.setText(user.getLastMsg());
        holder.txtMsgTime.setText(user.getLastUpdateTime());
        holder.txtMsgCount.setText(user.getNotificationCount());
        try {
            storageRef.child(user.getProfileUrl()).getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                @Override
                public void onSuccess(Uri uri) {
                    ImageUtils.displayImageFromUrl(holder.itemView.getContext(), holder.imgProfile, uri.toString());
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {
                    exception.printStackTrace();
                    // Handle any errors
                }
            });

        }catch (Exception e){
            e.printStackTrace();
        }

        holder.lytArchive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                observer.onArchive(user);
            }
        });
    }

    @Override
    public int getItemCount() {
        return users.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView txtName,txtStatus,txtMsgTime,txtMsgCount;
        private CircleImageView imgProfile;
        private ConstraintLayout lytParent;
        private LinearLayout lytArchive;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            txtName=itemView.findViewById(R.id.txtName);
            txtStatus=itemView.findViewById(R.id.txtStatus);
            txtMsgTime=itemView.findViewById(R.id.txtMsgTime);
            txtMsgCount=itemView.findViewById(R.id.txtMsgCount);
            imgProfile=itemView.findViewById(R.id.imgProfile);
            lytParent=itemView.findViewById(R.id.lytParent);
            lytArchive=itemView.findViewById(R.id.lytArchive);

            ConstraintLayout.LayoutParams params= (ConstraintLayout.LayoutParams) imgProfile.getLayoutParams();
            int size=deviceWidth/7;
            params.height=size;
            params.width=size;
            imgProfile.setLayoutParams(params);

        }
    }
}
