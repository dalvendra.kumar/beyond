package com.beyond.ve.beyondapp;

import android.app.Application;
import com.google.firebase.FirebaseApp;
import com.omralcorut.linkedinsignin.Linkedin;

import java.util.Arrays;

public class BeyondApp extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        FirebaseApp.initializeApp(this);
//        if (!FirebaseApp.getApps(this).isEmpty()) {
//            FirebaseRemoteConfig mFirebaseRemoteConfig = FirebaseRemoteConfig.getInstance();
//
//        }

        Linkedin.Companion.initialize(getApplicationContext(),
                getApplicationContext().getResources().getString(R.string.client_id),
                getApplicationContext().getResources().getString(R.string.client_secret),
                getApplicationContext().getResources().getString(R.string.redirect_url),
                "RANDOM_STRING",
                Arrays.asList("r_liteprofile", "r_emailaddress"));
    }
}
