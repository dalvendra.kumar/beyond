package com.beyond.ve.beyondapp.ui.contact;

import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatSpinner;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.beyond.ve.beyondapp.BaseActivity.BaseFragment;
import com.beyond.ve.beyondapp.R;
import com.beyond.ve.beyondapp.Utils.ImageUtils;
import com.beyond.ve.beyondapp.Web.ApiClient;
import com.beyond.ve.beyondapp.Web.WebService;
import com.beyond.ve.beyondapp.data.AppPreferencesHelper;
import com.beyond.ve.beyondapp.data.PreferencesHelper;
import com.beyond.ve.beyondapp.ui.ChatActivity;
import com.beyond.ve.beyondapp.ui.ShowProfileActivity;
import com.beyond.ve.beyondapp.ui.ShowProfileContactActivity;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.google.firebase.database.FirebaseDatabase;
import com.timehop.stickyheadersrecyclerview.StickyRecyclerHeadersAdapter;
import com.timehop.stickyheadersrecyclerview.StickyRecyclerHeadersDecoration;
import com.viethoa.RecyclerViewFastScroller;
import com.viethoa.models.AlphabetItem;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ContactFragment extends BaseFragment implements ObservableClick, View.OnClickListener {

    ArrayList<Model> requestList = new ArrayList<>();
    ArrayList<Model> contactList = new ArrayList<>();
    AnimalsHeadersAdapter adapter;
    ReqAdapter reqAdapter;
    RecyclerViewFastScroller fastScroller;
    AppCompatSpinner spinnner;
    RecyclerView recyclerView, rv_request;
    TextView request, contact;
    LinearLayout ll_contact;
    String[] country = {"ALL", "Requests", "Contacts"};
    static final String ALL = "ALL";
    static final String REQUESTS = "Requests";
    static final String CONTACTS = "Contacts";
    private ImageView search;
    private AppCompatEditText ed_search;

    private ImageView userimage;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_contact, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        ll_contact = (LinearLayout) view.findViewById(R.id.ll_contact);

        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerview);
        rv_request = (RecyclerView) view.findViewById(R.id.rv_request);

        request = (TextView) view.findViewById(R.id.request);
        contact = (TextView) view.findViewById(R.id.contact);

        fastScroller = (RecyclerViewFastScroller) view.findViewById(R.id.fast_scroller);
        spinnner = (AppCompatSpinner) view.findViewById(R.id.spinnner);

        search = (ImageView) view.findViewById(R.id.search);
        ed_search = (AppCompatEditText) view.findViewById(R.id.ed_search);
        userimage = (ImageView) view.findViewById(R.id.userimage);

        userimage.setOnClickListener(this);
        search.setOnClickListener(this);
        ed_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                adapter.getFilter().filter(editable.toString());
            }
        });

        PostSingInDataToServer();

        PreferencesHelper helper = new AppPreferencesHelper(getActivity());
        ImageUtils.displayImageFromUrl(getActivity(), userimage, helper.getUser_avatar());

        // Set adapter populated with example dummy data
        adapter = new AnimalsHeadersAdapter();

        recyclerView.setAdapter(adapter);

        reqAdapter = new ReqAdapter(getActivity(), requestList, this);
        rv_request.setLayoutManager(new LinearLayoutManager(getActivity(), RecyclerView.VERTICAL, false));
        rv_request.setAdapter(reqAdapter);

        final LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), RecyclerView.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);

        // Add the sticky headers decoration
        final StickyRecyclerHeadersDecoration headersDecor = new StickyRecyclerHeadersDecoration(adapter);
        recyclerView.addItemDecoration(headersDecor);

        // Add decoration for dividers between list items
        recyclerView.addItemDecoration(new DividerDecoration(getActivity()));

        recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, final int position) {

                showAcceptRidePopUp(contactList.get(position), new DialogAcceptListener() {

                    @Override
                    public void onCallclickListener(Model model) {
                        Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", model.getMobile(), null));
                        startActivity(intent);
                    }

                    @Override
                    public void onContactclickListener(Model model) {
                        Intent intent = new Intent(getActivity(), ShowProfileContactActivity.class);
                        intent.putExtra("model", model);
                        startActivity(intent);
                    }

                    @Override
                    public void onMsgclickListener(Model model) {
                        setUserToUserChat(model);
                    }
                });

            }
        }));
        adapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
            @Override
            public void onChanged() {
            }
        });


        adapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
            @Override
            public void onChanged() {
//                headersDecor.invalidateHeaders();
            }
        });


        ArrayAdapter aa = new ArrayAdapter(getActivity(), android.R.layout.simple_spinner_item, country);
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //Setting the ArrayAdapter data on the Spinner
        spinnner.setAdapter(aa);


        spinnner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int positon, long l) {
                if (adapterView.getChildAt(0) instanceof TextView) {
                    ((TextView) adapterView.getChildAt(0)).setTextColor(getResources().getColor(R.color.alphabet_text_color));
                    handleSelection(country[positon]);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    private void handleSelection(String selVaue) {

        if (selVaue.equals(ALL)) {
            Log.e("TAG", ALL);

//            if (requestList.size() > 0) {
            request.setVisibility(View.VISIBLE);
            rv_request.setVisibility(View.VISIBLE);
//            }

//            if (contactList.size() > 0) {
            contact.setVisibility(View.VISIBLE);
            ll_contact.setVisibility(View.VISIBLE);
//            }
        } else if (selVaue.equals(REQUESTS)) {
            Log.e("TAG", REQUESTS);
//            if (requestList.size() > 0) {
            request.setVisibility(View.VISIBLE);
            rv_request.setVisibility(View.VISIBLE);
//            }

            contact.setVisibility(View.GONE);
            ll_contact.setVisibility(View.GONE);
        } else if (selVaue.equals(CONTACTS)) {
            Log.e("TAG", CONTACTS);
            request.setVisibility(View.GONE);
            rv_request.setVisibility(View.GONE);

            if (contactList.size() > 0) {
                contact.setVisibility(View.VISIBLE);
                ll_contact.setVisibility(View.VISIBLE);
            }
        }

    }

    private void getDummyDataSet(ArrayList<Model> contactList) {

        fastScroller.setRecyclerView(recyclerView);

        ArrayList<AlphabetItem> mAlphabetItems = new ArrayList<>();
        List<String> strAlphabets = new ArrayList<>();
        for (int i = 0; i < contactList.size(); i++) {
            String name = contactList.get(i).getFirst_name();
            if (name == null || name.trim().isEmpty())
                continue;

            String word = name.substring(0, 1);
            if (!strAlphabets.contains(word)) {
                strAlphabets.add(word);
                mAlphabetItems.add(new AlphabetItem(i, word, false));
            }
        }

        fastScroller.setUpAlphabet(mAlphabetItems);
    }


    private void PostSingInDataToServer() {

        AppPreferencesHelper abc = new AppPreferencesHelper(getActivity());
        WebService apiService = ApiClient.getClient().create(WebService.class);


        apiService.postGetContact("Bearer " + abc.getUser_XAUTH()).enqueue(new Callback<ModelContacts>() {
            @Override
            public void onResponse(Call<ModelContacts> call, Response<ModelContacts> response) {
                Log.e("onDataResponse==", response.body().toString());

                if (response.body().getPayload().getContacts().size() > 0) {
                    contact.setVisibility(View.VISIBLE);
                    ll_contact.setVisibility(View.VISIBLE);

                    adapter.clear();
                    contactList.clear();

                    contactList.addAll(response.body().getPayload().getContacts());
                    adapter.addAll(response.body().getPayload().getContacts());
                    adapter.addAllMain(response.body().getPayload().getContacts());
                    if (response.body().getPayload().getLocal().size() > 0) {
                        contactList.addAll(response.body().getPayload().getLocal());
                        adapter.addAll(response.body().getPayload().getLocal());
                        adapter.addAllMain(response.body().getPayload().getLocal());
                    }
                    adapter.notifyDataSetChanged();
                } else {
                    contact.setVisibility(View.GONE);
                    ll_contact.setVisibility(View.GONE);
                }

                if (response.body().getPayload().getRequests().size() > 0) {
                    request.setVisibility(View.VISIBLE);
                    rv_request.setVisibility(View.VISIBLE);

                    requestList.clear();
                    requestList.addAll(response.body().getPayload().getRequests());
                    if (response.body().getPayload().getLocal().size() > 0) {
                        requestList.addAll(response.body().getPayload().getLocal());
                    }
                    reqAdapter.notifyDataSetChanged();
                } else {
                    request.setVisibility(View.GONE);
                    rv_request.setVisibility(View.GONE);
                }

                getDummyDataSet(contactList);

            }

            @Override
            public void onFailure(Call<ModelContacts> call, Throwable t) {
                Log.e("onDataFailure==", t.toString());
            }
        });


    }

    @Override
    public void observeCancel(Model model) {
        Toast.makeText(getActivity(), "observeCancel" + model.toString(), Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onserveSelect(Model model) {
        Toast.makeText(getActivity(), "onserveSelect" + model.toString(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.search: {
                handleSearch();
                break;
            }
            case R.id.userimage: {
                startActivity(new Intent(getActivity(), ShowProfileActivity.class));
                break;
            }
        }
    }

    private void handleSearch() {
        if (ed_search.getVisibility() == View.VISIBLE) {
            ed_search.setVisibility(View.GONE);
            spinnner.setVisibility(View.VISIBLE);

        } else if (spinnner.getVisibility() == View.VISIBLE) {
            ed_search.setAnimation(new AnimationUtils().loadAnimation(getActivity(), R.anim.right_to_left));
            ed_search.setVisibility(View.VISIBLE);
            spinnner.setVisibility(View.GONE);
        }


    }


    class AnimalsHeadersAdapter extends AnimalsAdapter<RecyclerView.ViewHolder>
            implements StickyRecyclerHeadersAdapter<RecyclerView.ViewHolder>, RecyclerViewFastScroller.BubbleTextGetter, Filterable {
        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.view_item_contact, parent, false);
            return new ViewHoldee(view) {
            };
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            ViewHoldee viewHoldee = (ViewHoldee) holder;

            Log.e("position====", String.valueOf(position) + "-----" + getItem(position));
//            if (position != 0) {

            if (!TextUtils.isEmpty(getItem(position).getFirst_name()))
                viewHoldee.mTextView.setText(getItem(position).getFirst_name());

            if (!TextUtils.isEmpty(getItem(position).getCompany_name()))
                viewHoldee.companyname.setText(getItem(position).getCompany_name());

            if (!TextUtils.isEmpty(getItem(position).getAvatar())) {
                ImageUtils.displayImageFromUrl(getActivity(), viewHoldee.image, getItem(position).getAvatar());
            }

            viewHoldee.ll_main.setVisibility(View.VISIBLE);
//
        }

        @Override
        public long getHeaderId(int position) {
//            if (position == 0) {
//                return -1;
//            } else {
            return getItem(position).getFirst_name().charAt(0);
//            }
        }

        @Override
        public RecyclerView.ViewHolder onCreateHeaderViewHolder(ViewGroup parent) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.view_header_contact, parent, false);
            return new RecyclerView.ViewHolder(view) {
            };
        }

        @Override
        public void onBindHeaderViewHolder(RecyclerView.ViewHolder holder, int position) {
            Log.e("position====Header", String.valueOf(position) + "---" + String.valueOf(getItem(position).getFirst_name().charAt(0)));
            TextView textView = (TextView) holder.itemView;
            textView.setText(String.valueOf(getItem(position).getFirst_name().charAt(0)));
//      holder.itemView.setBackgroundColor(getRandomColor());
        }


        @Override
        public String getTextToShowInBubble(int pos) {
            if (pos < 0 || pos >= getItems().size())
                return null;

            String name = getItems().get(pos).getFirst_name();
            if (name == null || name.length() < 1)
                return null;

            return getItems().get(pos).getFirst_name().substring(0, 1);
        }

        @Override
        public Filter getFilter() {
            return new Filter() {
                @Override
                protected FilterResults performFiltering(CharSequence charSequence) {
                    List<Model> contactListFiltered = new ArrayList<>();
                    String charString = charSequence.toString();
                    if (charString.isEmpty()) {
                        contactListFiltered = getItemsMain();
                    } else {
                        List<Model> filteredList = new ArrayList<>();
                        for (Model row : getItemsMain()) {

                            // name match condition. this might differ depending on your requirement
                            // here we are looking for name or phone number match
                            if (row.getFirst_name().toLowerCase().contains(charString.toLowerCase()) || row.getLast_name().contains(charSequence)) {
                                filteredList.add(row);
                            }
                        }

                        contactListFiltered = filteredList;
                    }

                    FilterResults filterResults = new FilterResults();
                    filterResults.values = contactListFiltered;
//                    getItems()
                    return filterResults;
                }

                @Override
                protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                    clear();
                    addAll((ArrayList<Model>) filterResults.values);
                    notifyDataSetChanged();

                }
            };
        }


        class ViewHoldee extends RecyclerView.ViewHolder {
            TextView mTextView, companyname;
            LinearLayout ll_main;
            CircleImageView image;

            public ViewHoldee(View itemView) {
                super(itemView);
                mTextView = (TextView) itemView.findViewById(R.id.name);
                companyname = (TextView) itemView.findViewById(R.id.companyname);
                ll_main = (LinearLayout) itemView.findViewById(R.id.ll_main);
                image = (de.hdodenhof.circleimageview.CircleImageView) itemView.findViewById(R.id.image);
            }
        }
    }

    private void setUserToUserChat(final Model frndChat) {

        Firebase.setAndroidContext(getContext());
        String baseUrl = FirebaseDatabase.getInstance().getReference().toString();
        final Firebase refCon = new Firebase(baseUrl + "/conversations/");
        final Firebase refUser = new Firebase(baseUrl + "/users/");
        refUser.child(new AppPreferencesHelper(getContext()).getUserIdFirebase()).child("conversations").child(frndChat.getChat_id());
        Firebase refFrndConv = new Firebase(baseUrl + "/users/" + new AppPreferencesHelper(getContext()).getUserIdFirebase() + "/conversations/" + frndChat.getChat_id());

        refFrndConv.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                if (dataSnapshot.getValue() != null) {
                    Intent intent = new Intent(getContext(), ChatActivity.class);
                    intent.putExtra("friendId", frndChat.getChat_id());
                    intent.putExtra("friendName", frndChat.getFirst_name() + " " + frndChat.getLast_name());
                    intent.putExtra("friendLocation", ((Map<String, String>) dataSnapshot.getValue()).get("location"));
                    intent.putExtra("friendProfilePic", frndChat.getAvatar());
                    intent.putExtra("friendLastSeen", "");
                    startActivity(intent);
                } else {
                    String keyConversation = refCon.push().getKey();
                    Map<String, String> mapUser = new HashMap<>();
                    mapUser.put("isArchive", "false");
                    mapUser.put("location", keyConversation);
                    mapUser.put("unreadCount", "0");

                    Map<String, String> mapMsg = new HashMap<>();
                    mapMsg.put("timestamp", String.valueOf(System.currentTimeMillis()));
                    mapMsg.put("isRead", String.valueOf(false));
                    mapMsg.put("content", "Welcome in Beyond chat");
                    mapMsg.put("toID", " ");
                    mapMsg.put("fromID", " ");
                    mapMsg.put("type", "text");

                    refUser.child(new AppPreferencesHelper(getContext()).getUserIdFirebase()).child("conversations").child(frndChat.getChat_id()).setValue(mapUser);
                    refUser.child(frndChat.getChat_id()).child("conversations").child(new AppPreferencesHelper(getContext()).getUserIdFirebase()).setValue(mapUser);
                    refCon.child(keyConversation).push().setValue(mapMsg);

                    Intent intent = new Intent(getContext(), ChatActivity.class);
                    intent.putExtra("friendId", frndChat.getChat_id());
                    intent.putExtra("friendName", frndChat.getFirst_name() + " " + frndChat.getLast_name());
                    intent.putExtra("friendLocation", keyConversation);
                    intent.putExtra("friendProfilePic", frndChat.getAvatar());
                    intent.putExtra("friendLastSeen", "");
                    startActivity(intent);
                }
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });
    }

    private void showAcceptRidePopUp(final Model model, final DialogAcceptListener listener
    ) {
        final Dialog delayDialog = new Dialog(getActivity());
        delayDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        delayDialog.getWindow().setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT));
        delayDialog.setContentView(R.layout.contact_dialog_layout);
        delayDialog.setCancelable(true);
        delayDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);


        TextView tv_full_name = (TextView) delayDialog.findViewById(R.id.tv_full_name);
        TextView tv_comapny_name = (TextView) delayDialog.findViewById(R.id.tv_comapny_name);
        ImageView qrcode = (ImageView) delayDialog.findViewById(R.id.qrcode);

        ImageView iv_contact = (ImageView) delayDialog.findViewById(R.id.iv_contact);
        ImageView iv_msg = (ImageView) delayDialog.findViewById(R.id.iv_msg);
        ImageView iv_call = (ImageView) delayDialog.findViewById(R.id.iv_call);
        ImageView profile_image = (ImageView) delayDialog.findViewById(R.id.profile_image);


        if (!TextUtils.isEmpty(model.getFirst_name()) && !TextUtils.isEmpty(model.getLast_name()))
            tv_full_name.setText(model.getFirst_name() + " " + model.getLast_name());

        if (!TextUtils.isEmpty(model.getCompany_name()))
            tv_comapny_name.setText(model.getCompany_name());

        if (!TextUtils.isEmpty(model.getAvatar()))
            ImageUtils.displayImageFromUrl(getActivity(), profile_image, model.getAvatar());

        if (!TextUtils.isEmpty(model.getChat_id()) && !model.getChat_id().equals("null")) {
            iv_contact.setVisibility(View.VISIBLE);
            iv_msg.setVisibility(View.VISIBLE);
        } else {
            iv_contact.setVisibility(View.GONE);
            iv_msg.setVisibility(View.GONE);
        }


        iv_contact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                delayDialog.dismiss();
                listener.onContactclickListener(model);
            }
        });

        iv_msg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                delayDialog.dismiss();
                listener.onMsgclickListener(model);
            }
        });

        iv_call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                delayDialog.dismiss();
                listener.onCallclickListener(model);
            }
        });
        delayDialog.show();
    }


}
