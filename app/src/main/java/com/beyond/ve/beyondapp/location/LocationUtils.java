package com.beyond.ve.beyondapp.location;

import android.annotation.SuppressLint;
import android.content.*;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.*;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;

/**
 * Created by Rahul on 13/12/17.
 */

public class LocationUtils implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
    private LocationListenerActivity activity;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    private LocationDetector detector;
    public static final int LocationTag = 10001;
    private final String Wifi_Action = "android.net.conn.CONNECTIVITY_CHANGE";

    public LocationUtils(LocationListenerActivity activity, LocationDetector detector) {
        this.activity = activity;
        this.detector = detector;
        buildGoogleApiClient();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        mLocationRequest = new LocationRequest().create();
        mLocationRequest.setInterval(0);
        mLocationRequest.setFastestInterval(0);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, listener);
        checkResolutionAndProceed();
    }

    @Override
    public void onConnectionSuspended(int i) {
        detector.onErrors(ConstantsValues.GOOGLE_PLAY_CONNECTION_SUSPEND);
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        detector.onErrors(ConstantsValues.GOOGLE_PLAY_CONNECTION_ERROR);
    }


    /*
    * Request for Connecting Google Client
    * */
    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(activity)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();

    }


    /*
    * Location will update here
    * */
    private LocationListener listener = new LocationListener() {
        @Override
        public void onLocationChanged(Location location) {
           activity.stopProgress();
            detector.OnLocationChange(location);
        }
    };


    /*
    * To check gps is Enable or not
    * */
    private void checkResolutionAndProceed() {
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(mLocationRequest);
        builder.setAlwaysShow(true);
        PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                Status status = result.getStatus();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        startProgressNow();
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        try {
                            status.startResolutionForResult(activity, LocationTag);
                        } catch (IntentSender.SendIntentException e) {
                            e.printStackTrace();
                        }
                        break;
                }
            }
        });
    }

    /*
    * start location listener
    * */
    private void startProgressNow() {
        if (ActivityCompat.checkSelfPermission(activity, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(activity, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        Location mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        activity.stopProgress();
        if (mLastLocation != null) {
            detector.OnLastLocationFound(mLastLocation);
        } else {
            detector.onErrors(ConstantsValues.NO_LAST_LOCATION_FOUND);
        }
        registerWifiListener();
    }

    /*
    * Stop all Service as location and networks
    *
    * */
    public void onStop() {
        unRigisterWifiListener();
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, listener);
            mGoogleApiClient.disconnect();
        }
    }


    /*Network enable disable event tracker*/
    private BroadcastReceiver recieverWifi = new BroadcastReceiver() {
        @SuppressLint("MissingPermission")
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(Wifi_Action)) {
                ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo netInfo = cm.getActiveNetworkInfo();
                if (netInfo != null && netInfo.isConnectedOrConnecting()) {
                    activity.listenLocation(detector);
                    LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, listener);
                } else {
                    LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, listener);
                    detector.onErrors(ConstantsValues.NO_NETWORK);
                }
            }
        }
    };


    /*
    * Rigister Network listener
    * */
    private void registerWifiListener() {
        IntentFilter filter = new IntentFilter();
        filter.addAction(Wifi_Action);
//        activity.registerReceiver(recieverWifi, filter);
    }

    /*
    *
    * Unregister Network Listener
    *
    * */

    private void unRigisterWifiListener() {
        try {
//            activity.unregisterReceiver(recieverWifi);
        } catch (Exception e) {
        }
    }


}