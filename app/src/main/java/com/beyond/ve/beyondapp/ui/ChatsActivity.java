package com.beyond.ve.beyondapp.ui;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.beyond.ve.beyondapp.BaseActivity.BaseActivity;
import com.beyond.ve.beyondapp.ChatsAdapter;
import com.beyond.ve.beyondapp.Constant;
import com.beyond.ve.beyondapp.Model.chat.ChatUser;
import com.beyond.ve.beyondapp.R;
import com.beyond.ve.beyondapp.Utils.CommonUtils;
import com.beyond.ve.beyondapp.Utils.ImageUtils;
import com.beyond.ve.beyondapp.data.AppPreferencesHelper;
import com.beyond.ve.beyondapp.data.PreferencesHelper;
import com.firebase.client.Firebase;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.FirebaseApp;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.SearchView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import de.hdodenhof.circleimageview.CircleImageView;

public class ChatsActivity extends BaseActivity implements ChatsAdapter.UserObserver,ChatUser.ChangeObserver, SearchView.OnQueryTextListener, SearchView.OnCloseListener, View.OnClickListener {
    private ChatsAdapter adapter;
    private ArrayList<ChatUser> users = new ArrayList<>();
    private ArrayList<ChatUser> queryUsers = new ArrayList<>();
    private String userId;
    private int archviedUserCount=0;
    private TextView txtArchiveUserCount;
    private ConstraintLayout lytArchive,lytGroup;
    private CircleImageView imgProfile;
    private ImageView imgNotification;
    private int deviceWidth;
    private RecyclerView recyclerView;
    private ConstraintLayout cl_onaddbutton;
    private ImageView imgCamera,imgUpload,imgCancel,imgFab,imgChat,imgHome;
    private static final int REQUEST_CODE_ARCHIVE=101,REQUEST_CODE_CREATE_GROUP=102;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_chats);


        try {
            Firebase.setAndroidContext(getApplication());

            FirebaseApp.initializeApp(getApplicationContext());

            String baseUrl = FirebaseDatabase.getInstance().getReference().toString();


            if (Build.VERSION.SDK_INT >= 21) {
                getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.bgColor)); //status bar or the time bar at the top
            }

            init();

            getUsers();

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(users.size()>0)
            refreshMsgNdTime();
    }

    @Override
    protected void onPause() {
        super.onPause();
        overridePendingTransition(0,0);
    }

    public void getUsers(){
        archviedUserCount=0;
        DatabaseReference rootRef = FirebaseDatabase.getInstance().getReference();
        DatabaseReference usersdRef = rootRef.child("users").child(userId);
        ValueEventListener eventListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                String baseUrl=FirebaseDatabase.getInstance().getReference().toString();
                Map mapData=(Map) dataSnapshot.getValue();
                Map mapConversion= (Map) mapData.get("conversations");
                if(mapConversion==null){
                    Snackbar.make(recyclerView,"No Chat till now",Snackbar.LENGTH_SHORT).show();
                    return;
                }

                users.clear();

                for (Object id: mapConversion.keySet()) {
                    try {
                        String idStr=id.toString();
                        Map jsonConversion =(Map) mapConversion.get(idStr);

                        String location=jsonConversion.get("location").toString();
                        try {
                            Boolean isArchive = Boolean.parseBoolean(String.valueOf(jsonConversion.get("isArchive")));
                            if(isArchive) {
                                archviedUserCount++;
                                continue;
                            }
                        }catch (Exception e){
                            e.printStackTrace();
                        }

                        String unreadCount=jsonConversion.get("unreadCount")==null?"":jsonConversion.get("unreadCount").toString();

                        if(idStr.startsWith("-")){
                            // Add Group Chat
                            String groupImage=jsonConversion.get("groupImage").toString();
                            String groupName=jsonConversion.get("groupName").toString();
                            String groupUserIds=jsonConversion.get("groupUserIds").toString();
                            ChatUser user=new ChatUser(idStr,groupName,"",groupImage,"",unreadCount+"");
                            user.setGroupUserIds(groupUserIds);
                            user.setConversationLocation(location);
                            Firebase firebase= new Firebase(baseUrl+"/users/"+userId+"/conversations/"+idStr+"/unreadCount");
                            user.unReadCountListener(firebase,ChatsActivity.this);
                            users.add(user);
                        }else  {
                            ChatUser user=new ChatUser();
                            user.setId(idStr);
                            user.setNotificationCount(unreadCount);
                            user.setConversationLocation(location);
                            Firebase firebaseCount= new Firebase(baseUrl+"/users/"+userId+"/conversations/"+idStr+"/unreadCount");
                            user.unReadCountListener(firebaseCount,ChatsActivity.this);
                            Firebase firebase= new Firebase(baseUrl+"/users/"+user.getId()+"/credentials");
                            user.setRefCredentials(firebase);
                            users.add(user);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                adapter.notifyDataSetChanged();
                toggleArchiveLyt();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };
        usersdRef.addListenerForSingleValueEvent(eventListener);
    }


    private void refreshMsgNdTime(){
        new Thread(new Runnable() {
            @Override
            public void run() {
                for (ChatUser user: users){
                    getLastMsg(user);
                }
            }
        }).start();
    }

    private void getLastMsg(final ChatUser user){
        FirebaseApp.initializeApp(getApplicationContext());
        DatabaseReference databaseReference= FirebaseDatabase.getInstance().getReference();
        Query query=databaseReference.child("conversations").child(user.getConversationLocation()).limitToLast(1);
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(com.google.firebase.database.DataSnapshot dataSnapshot) {
                Map map= (Map) dataSnapshot.getValue();
                if(map!=null)
                    for (Object key: map.keySet()) {
                        try {
                            Map mapMsg = (Map) map.get(key);
                            String message = mapMsg.get("content").toString();
                            String createdOn = mapMsg.get("timestamp").toString();
                            String isMsgReaded = mapMsg.get("isRead").toString();
                            String type = mapMsg.get("type").toString();
                            if(type.equalsIgnoreCase("photo"))
                                message="Image";
                            user.setLastMsg(message);
                            user.setMsgReaded(Boolean.valueOf(isMsgReaded));
                            user.setLastUpdateTime(createdOn);
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                else
                    user.setLastMsg("");
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void init() {
        PreferencesHelper helper=new AppPreferencesHelper(getBaseContext());
        userId=helper.getUserIdFirebase();
        deviceWidth=CommonUtils.returmWindowWidth(this);
        recyclerView = findViewById(R.id.recyclerChats);
        txtArchiveUserCount = findViewById(R.id.txtArchiveUserCount);
        lytArchive = findViewById(R.id.lytArchive);
        lytGroup = findViewById(R.id.lytGroup);
        imgNotification=findViewById(R.id.imgNotification);
        SearchView searchView=findViewById(R.id.searchView);
        ImageView imgGroup=findViewById(R.id.imgGroup);
        imgProfile=findViewById(R.id.profile_image);

        ConstraintLayout.LayoutParams params= (ConstraintLayout.LayoutParams) imgGroup.getLayoutParams();
        params.width=deviceWidth/7;
        params.height=deviceWidth/7;
        imgGroup.setLayoutParams(params);

        ConstraintLayout.LayoutParams paramsProfile= (ConstraintLayout.LayoutParams) imgProfile.getLayoutParams();
        paramsProfile.width=deviceWidth/9;
        paramsProfile.height=deviceWidth/9;
        imgProfile.setLayoutParams(paramsProfile);

        adapter = new ChatsAdapter(users,deviceWidth ,this);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);

        cl_onaddbutton=findViewById(R.id.cl_onaddbutton);
        imgUpload=findViewById(R.id.iv_upload);
        imgCamera=findViewById(R.id.iv_camera);
        imgCancel=findViewById(R.id.iv_cancel);
        imgChat=findViewById(R.id.imgChat);
        imgHome=findViewById(R.id.imgHome);
        imgFab=findViewById(R.id.imgFab);


        ImageUtils.displayImageFromUrl(this,imgProfile,helper.getUser_avatar());

        searchView.setOnQueryTextListener(this);
        searchView.setOnCloseListener(this);
        searchView.setOnSearchClickListener(this);

        lytArchive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(ChatsActivity.this,ArchiveChatsActivity.class),REQUEST_CODE_ARCHIVE);
            }});

        lytGroup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(ChatsActivity.this,CreateGroupActivity.class),REQUEST_CODE_CREATE_GROUP);
            }});

        findViewById(R.id.imgCalendar).setOnClickListener(this);
        findViewById(R.id.imgContact).setOnClickListener(this);
        findViewById(R.id.imgHome).setOnClickListener(this);
        findViewById(R.id.iv_camera).setOnClickListener(this);
        findViewById(R.id.iv_cancel).setOnClickListener(this);
        findViewById(R.id.iv_upload).setOnClickListener(this);

        imgProfile.setOnClickListener(this);
        imgNotification.setOnClickListener(this);
        imgFab.setOnClickListener(this);
        imgChat.setImageResource(R.drawable.ic_messages_on_home);
        imgHome.setImageResource(R.drawable.ic_home_trans_24dp);
    }

    @Override
    public void onUser(ChatUser user) {
        Intent intent=new Intent(getBaseContext(),ChatActivity.class);
        intent.putExtra("friendId", user.getId());
        intent.putExtra("friendAppId", user.getAppId());
        intent.putExtra("friendName", user.getName());
        intent.putExtra("friendLocation", user.getConversationLocation());
        intent.putExtra("friendProfilePic", user.getProfileUrl());
        intent.putExtra("friendLastSeen", user.getLastSeen()==null?"0":user.getLastSeen());
        intent.putExtra("groupUserIds", user.getGroupUserIds());
        startActivity(intent);
    }

    @Override
    public void onDelete(ChatUser user) {
        deleteChat(user);
    }

    private void deleteChat(ChatUser user) {

        String baseUrl=FirebaseDatabase.getInstance().getReference().toString();

        // Deleting the users from conversation of each other.
        new Firebase(baseUrl+"/users/"+userId+"/conversations/"+user.getId()).removeValue();
        try {
            new Firebase(baseUrl + "/users/" + user.getId() + "/conversations/" + userId).removeValue();
        }catch (Exception e){
            e.printStackTrace();
        }
        if(!user.getId().contains("-")) // Delete chat of the users
            new Firebase(baseUrl+"/conversations/"+user.getConversationLocation()).removeValue();

        users.remove(user);
        adapter.notifyDataSetChanged();

    }

    @Override
    public void onArchive(ChatUser user) {
        makeArchiveUser(user);
    }

    private void makeArchiveUser(ChatUser user){
        String baseUrl=FirebaseDatabase.getInstance().getReference().toString();
        Firebase userRef = new Firebase(baseUrl+"/users/"+userId+"/conversations/"+user.getId());
        userRef.child("isArchive").setValue("true");
        users.remove(user);
        adapter.notifyDataSetChanged();
        archviedUserCount++;
        txtArchiveUserCount.setText(archviedUserCount+"");
        if(lytArchive.getVisibility()==View.GONE)
            lytArchive.setVisibility(View.VISIBLE);
    }

    private void toggleArchiveLyt() {
        txtArchiveUserCount.setText(archviedUserCount+"");
        if(archviedUserCount>0)
            lytArchive.setVisibility(View.VISIBLE);
        else
            lytArchive.setVisibility(View.GONE);
    }

    @Override
    public void onUserChange() {
        adapter.notifyDataSetChanged();
    }


    @Override
    public boolean onQueryTextSubmit(String query) {
        showSearchResult(query);
        return false;
    }



    @Override
    public boolean onQueryTextChange(String newText) {
        if(newText.length()>1)
            showSearchResult(newText);
        return false;
    }

    private void showSearchResult(String query) {
        queryUsers.clear();
        for (ChatUser user:users){
            if(user.getName().contains(query))
                queryUsers.add(user);
        }
        if(queryUsers.size()>0)
            adapter.notifyDataSetChanged();
    }

    @Override
     public boolean onClose() {
        imgNotification.setVisibility(View.VISIBLE);
        lytGroup.setVisibility(View.GONE);

        adapter = new ChatsAdapter(users,deviceWidth ,this);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);
        return false;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if(requestCode==REQUEST_CODE_ARCHIVE && resultCode==ArchiveChatsActivity.RESULT_CODE_UPDATE){
            getUsers();
        }else if(requestCode==REQUEST_CODE_CREATE_GROUP && resultCode==CreateGroupActivity.RESULT_CODE_GROUP_CREATED){
            HashMap<String, String> map= (HashMap<String, String>) data.getSerializableExtra("groupDetails");
            // Add Group Chat
            String groupImage=map.get("groupImage");
            String groupName=map.get("groupName");
            String groupUserIds=map.get("groupUserIds");
            String idStr=map.get("keyGroup");
            String location=map.get("location");
            String unreadCount=map.get("unreadCount");
            ChatUser user=new ChatUser(idStr,groupName,"",groupImage,"",unreadCount);
            user.setGroupUserIds(groupUserIds);
            user.setConversationLocation(location);
            Firebase firebase= new Firebase(FirebaseDatabase.getInstance().getReference().toString()+"/users/"+userId+"/conversations/"+idStr+"/unreadCount");
            user.unReadCountListener(firebase,ChatsActivity.this);
            users.add(user);
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.searchView:
                queryUsers.clear();
                imgNotification.setVisibility(View.GONE);
                Animation slideDown=new  AnimationUtils().loadAnimation(getApplicationContext(), R.anim.slide_down_group);
                lytGroup.setAnimation(slideDown);
                lytGroup.setVisibility(View.VISIBLE);
                adapter = new ChatsAdapter(queryUsers,deviceWidth ,this);
                recyclerView.setLayoutManager(new LinearLayoutManager(this));
                recyclerView.setAdapter(adapter);
                break;
            case R.id.imgCalendar:
                Intent intentCalendar=new Intent();
                intentCalendar.putExtra(Constant.Chat.KEY_ACTION,Constant.Chat.ACTION_CALANDER);
                setResult(Constant.Chat.RESULT_CODE,intentCalendar);
                ChatsActivity.this.finish();
                break;
            case R.id.imgContact:
                Intent intentContact=new Intent();
                intentContact.putExtra(Constant.Chat.KEY_ACTION,Constant.Chat.ACTION_CONTACT);
                setResult(Constant.Chat.RESULT_CODE,intentContact);
                ChatsActivity.this.finish();
                break;
            case R.id.imgHome:
                Intent intentHome=new Intent();
                intentHome.putExtra(Constant.Chat.KEY_ACTION,Constant.Chat.ACTION_HOME);
                setResult(Constant.Chat.RESULT_CODE,intentHome);
                ChatsActivity.this.finish();
                break;
            case R.id.iv_camera:
                Intent intentCamera=new Intent();
                intentCamera.putExtra(Constant.Chat.KEY_ACTION,Constant.Chat.ACTION_CAMERA);
                setResult(Constant.Chat.RESULT_CODE,intentCamera);
                hideFabOptions(true);
                break;
            case R.id.iv_cancel:
                hideFabOptions(false);
                break;
            case R.id.iv_upload:
                Intent intentUpload=new Intent();
                intentUpload.putExtra(Constant.Chat.KEY_ACTION,Constant.Chat.ACTION_UPLOAD);
                setResult(Constant.Chat.RESULT_CODE,intentUpload);
                hideFabOptions(true);
                break;
            case R.id.imgFab:
                showFabOptions();
                break;
            case R.id.imgNotification:
                int[] points = new int[2];
                imgNotification.getLocationInWindow(points);
                Intent intentNotifi =new  Intent(this, NotificationActivity.class);
                intentNotifi.putExtra("pointerYLoc",points[1]);
                startActivity(intentNotifi);
                break;
            case R.id.profile_image:
                Intent intentProfie=new  Intent(this, ShowProfileActivity.class);
                startActivity(intentProfie);
                break;
        }
    }

    private void showFabOptions(){
        Animation shake = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.shake);

        Animation slideUp =new  AnimationUtils().loadAnimation(getApplicationContext(), R.anim.slide_up);

        if (cl_onaddbutton.getVisibility() == View.INVISIBLE) {
            cl_onaddbutton.startAnimation(slideUp);
            cl_onaddbutton.setVisibility(View.VISIBLE);
            cl_onaddbutton.setAlpha(1.0F);

            imgCamera.startAnimation(shake);
            imgUpload.startAnimation(shake);
            imgFab.animate().rotation(0F).start();
            imgFab.animate()
                    .translationY(0F)
                    .alpha(0.0f).setDuration(500)
                    .setListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            super.onAnimationEnd(animation);
                            imgFab.setVisibility(View.INVISIBLE);
                        }
                    });

        }
    }

    private void hideFabOptions(boolean isClose){
        Animation slideDown=new  AnimationUtils().loadAnimation(getApplicationContext(), R.anim.slide_down);
        if (imgFab.getVisibility() == View.INVISIBLE) {
            imgFab.animate()
                    .translationY(10F)
                    .alpha(1.0f).setDuration(200)
                    .setListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            super.onAnimationEnd(animation);
                            imgFab.setVisibility(View.VISIBLE);
                        }
                    });
            cl_onaddbutton.startAnimation(slideDown);
            cl_onaddbutton.animate().rotation(0F).start();
            cl_onaddbutton.animate()
                    .translationY(0F)
                    .alpha(0.0f)
                    .setListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            super.onAnimationEnd(animation);
                            cl_onaddbutton.setVisibility(View.INVISIBLE);
                        }
                    });
        }
        if (isClose)
            ChatsActivity.this.finish();
    }

}
