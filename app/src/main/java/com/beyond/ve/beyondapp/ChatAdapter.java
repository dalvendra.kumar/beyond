package com.beyond.ve.beyondapp;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.RelativeLayout;

import com.beyond.ve.beyondapp.Model.chat.ChatMsg;
import com.beyond.ve.beyondapp.Utils.DateUtils;
import com.beyond.ve.beyondapp.Utils.ImageUtils;
import com.beyond.ve.beyondapp.databinding.ItemChatBinding;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class ChatAdapter extends RecyclerView.Adapter<ChatAdapter.ViewHolder>{
    private final StorageReference storageRef;
    private ArrayList<ChatMsg> msgs;
    private String userName;
    private int deviceWidth;
    private boolean isShowMsgAnim=true;

    public void setShowMsgAnim(boolean showMsgAnim) {
        isShowMsgAnim = showMsgAnim;
    }

    public ChatAdapter(ArrayList<ChatMsg> msgs, String userName, int deviceWidth) {
        this.msgs=msgs;
        this.userName=userName;
        this.deviceWidth=deviceWidth;
        this.storageRef= FirebaseStorage.getInstance().getReference();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemChatBinding binding= ItemChatBinding.bind(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_chat,parent,false));
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        final ChatMsg msg=msgs.get(position);
        if(msg.getIsRead())
            holder.binding.imgFiendReadTick.setImageResource(R.mipmap.ic_tick_green);
        else
            holder.binding.imgFiendReadTick.setImageResource(R.mipmap.ic_tick_white);

        if(msg.getFromID().equalsIgnoreCase(userName)){
            holder.binding.lytMsgFriend.setVisibility(View.GONE);
            holder.binding.imgFrnd.setVisibility(View.GONE);
            holder.binding.imgUser.setVisibility(View.GONE);
            holder.binding.txtUserMsg.setText(msg.getContent());
            holder.binding.txtUserMsgTime.setText(DateUtils.getTime(msg.getTimestamp()));
            if(msg.getType().equalsIgnoreCase("text")) {
                holder.binding.lytMsgUser.setVisibility(View.VISIBLE);
                if (isShowMsgAnim) {
                    Animation slideDown = new AnimationUtils().loadAnimation(holder.itemView.getContext(), R.anim.left_to_right);
                    holder.binding.lytMsgUser.setAnimation(slideDown);
                }
                holder.binding.imgUser.setVisibility(View.GONE);
            }else {
                holder.binding.lytMsgUser.setVisibility(View.GONE);
                holder.binding.imgUser.setVisibility(View.VISIBLE);
                if(isShowMsgAnim) {
                    Animation slideDown = new AnimationUtils().loadAnimation(holder.itemView.getContext(), R.anim.left_to_right);
                    holder.binding.imgUser.setAnimation(slideDown);
                }
                ImageUtils.displayImageFromUrl(holder.itemView.getContext(),holder.binding.imgUser,msg.getContent());
            }
        }else {
            holder.binding.lytMsgUser.setVisibility(View.GONE);
            holder.binding.imgFrnd.setVisibility(View.GONE);
            holder.binding.imgUser.setVisibility(View.GONE);
            holder.binding.txtFriendMsg.setText(msg.getContent());
            holder.binding.txtFriendMsgTime.setText(DateUtils.getTime(msg.getTimestamp()));
            if(msg.getType().equalsIgnoreCase("text")) {
                holder.binding.lytMsgFriend.setVisibility(View.VISIBLE);
                if(isShowMsgAnim) {
                    Animation slideDown = new AnimationUtils().loadAnimation(holder.itemView.getContext(), R.anim.right_to_left);
                    holder.binding.lytMsgFriend.setAnimation(slideDown);
                }
                holder.binding.imgFrnd.setVisibility(View.GONE);
            }else {
                holder.binding.lytMsgFriend.setVisibility(View.GONE);
                holder.binding.imgFrnd.setVisibility(View.VISIBLE);
                if(isShowMsgAnim) {
                    Animation slideDown = new AnimationUtils().loadAnimation(holder.itemView.getContext(), R.anim.right_to_left);
                    holder.binding.imgFrnd.setAnimation(slideDown);
                }
                ImageUtils.displayImageFromUrl(holder.itemView.getContext(),holder.binding.imgFrnd,msg.getContent());
            }
        }
    }


    @Override
    public int getItemCount() {
        return msgs.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private ItemChatBinding binding;
        public ViewHolder(@NonNull ItemChatBinding binding) {
            super(binding.getRoot());
            this.binding=binding;
            int imageSize=deviceWidth-deviceWidth/5;
            RelativeLayout.LayoutParams paramUser= (RelativeLayout.LayoutParams) binding.imgUser.getLayoutParams();
            RelativeLayout.LayoutParams paramFrnd= (RelativeLayout.LayoutParams) binding.imgFrnd.getLayoutParams();
            paramUser.height=imageSize;
            paramUser.width=imageSize;
            paramFrnd.height=imageSize;
            paramFrnd.width=imageSize;
            binding.imgFrnd.setLayoutParams(paramFrnd);
            binding.imgUser.setLayoutParams(paramUser);
        }
    }
}
