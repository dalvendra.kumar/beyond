package com.beyond.ve.beyondapp.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LinkedinResponse {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("statusCode")
    @Expose
    private Integer statusCode;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("description")
    @Expose
    private String localizedLastName;
    LastName lastName;
    FirstName firstName;
    ProfilePicture profilePicture;
    private String id;
    private String localizedFirstName;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    // Getter Methods

    public String getLocalizedLastName() {
        return localizedLastName;
    }

    public LastName getLastName() {
        return lastName;
    }

    public FirstName getFirstName() {
        return firstName;
    }

    public ProfilePicture getProfilePicture() {
        return profilePicture;
    }

    public String getId() {
        return id;
    }

    public String getLocalizedFirstName() {
        return localizedFirstName;
    }

    // Setter Methods

    public void setLocalizedLastName(String localizedLastName) {
        this.localizedLastName = localizedLastName;
    }

    public void setLastName(LastName lastNameObject) {
        this.lastName = lastNameObject;
    }

    public void setFirstName(FirstName firstNameObject) {
        this.firstName = firstNameObject;
    }

    public void setProfilePicture(ProfilePicture profilePictureObject) {
        this.profilePicture = profilePictureObject;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setLocalizedFirstName(String localizedFirstName) {
        this.localizedFirstName = localizedFirstName;
    }

    @Override
    public String toString() {
        return "LinkedinResponse{" +
                "localizedLastName='" + localizedLastName + '\'' +
                ", LastNameObject=" + lastName +
                ", FirstNameObject=" + firstName +
                ", ProfilePictureObject=" + profilePicture +
                ", id='" + id + '\'' +
                ", localizedFirstName='" + localizedFirstName + '\'' +
                '}';
    }
}

class ProfilePicture {
    private String displayImage;


    // Getter Methods

    public String getDisplayImage() {
        return displayImage;
    }

    // Setter Methods

    public void setDisplayImage(String displayImage) {
        this.displayImage = displayImage;
    }

    @Override
    public String toString() {
        return "ProfilePicture{" +
                "displayImage='" + displayImage + '\'' +
                '}';
    }
}

class FirstName {
    Localized localized;
    PreferredLocale preferredLocale;


    // Getter Methods

    public Localized getLocalized() {
        return localized;
    }

    public PreferredLocale getPreferredLocale() {
        return preferredLocale;
    }

    // Setter Methods

    public void setLocalized(Localized localizedObject) {
        this.localized = localizedObject;
    }

    public void setPreferredLocale(PreferredLocale preferredLocaleObject) {
        this.preferredLocale = preferredLocaleObject;
    }

    @Override
    public String toString() {
        return "FirstName{" +
                "LocalizedObject=" + localized +
                ", PreferredLocaleObject=" + preferredLocale +
                '}';
    }
}

class PreferredLocale {
   private String country;
   private String language;


   // Getter Methods

   public String getCountry() {
       return country;
   }

   public String getLanguage() {
       return language;
   }

   // Setter Methods

   public void setCountry(String country) {
       this.country = country;
   }

   public void setLanguage(String language) {
       this.language = language;
   }

    @Override
    public String toString() {
        return "PreferredLocale{" +
                "country='" + country + '\'' +
                ", language='" + language + '\'' +
                '}';
    }
}
class Localized {
    private String en_US;


    // Getter Methods

    public String getEn_US() {
        return en_US;
    }

    // Setter Methods

    public void setEn_US(String en_US) {
        this.en_US = en_US;
    }

    @Override
    public String toString() {
        return "Localized{" +
                "en_US='" + en_US + '\'' +
                '}';
    }
}
class LastName {
    Localized localized;
    PreferredLocale preferredLocale;


    // Getter Methods

    public Localized getLocalized() {
        return localized;
    }

    public PreferredLocale getPreferredLocale() {
        return preferredLocale;
    }

    // Setter Methods

    public void setLocalized(Localized localizedObject) {
        this.localized = localizedObject;
    }

    public void setPreferredLocale(PreferredLocale preferredLocaleObject) {
        this.preferredLocale = preferredLocaleObject;
    }

    @Override
    public String toString() {
        return "LastName{" +
                "LocalizedObject=" + localized +
                ", PreferredLocaleObject=" + preferredLocale +
                '}';
    }
}
