package com.beyond.ve.beyondapp.location;

import android.location.Location;



public abstract class LocationDetector {

    public abstract void OnLocationChange(Location location);

    public abstract void OnLastLocationFound(Location location);

    public void onErrors(int errorCode) {
    }
}
