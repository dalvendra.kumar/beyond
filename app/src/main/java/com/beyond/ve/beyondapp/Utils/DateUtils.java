package com.beyond.ve.beyondapp.Utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateUtils {

    public static String getTime(String milliSeconds){
        // Create a DateFormatter object for displaying date in specified format.
        SimpleDateFormat formatter = new SimpleDateFormat("hh:mm a");
        // Create a calendar object that will convert the date and time value in milliseconds to date.
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(Long.parseLong(milliSeconds));
        return formatter.format(calendar.getTime());
    }

    public static String getTimeDate(String milliSeconds){
        if(milliSeconds.length()<=0)
            return "";
        String time=getTime(milliSeconds);
        // Create a DateFormatter object for displaying date in specified format.
        SimpleDateFormat formatter = new SimpleDateFormat("MMM d");
        // Create a calendar object that will convert the date and time value in milliseconds to date.
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(Long.parseLong(milliSeconds));
        String dateStr="";
        Date date=calendar.getTime();
        Date dateCurent=new Date();
//        if(date.compareTo(dateCurent)>0)
//            dateStr="Yesterday";
//        else
        if(date.compareTo(dateCurent)==0)
            dateStr="Today";
        else
            dateStr=formatter.format(date);

        return  time+", " +dateStr;
    }


    public static String getTimeDiff(String updatedAt) {
//        2019-08-06 17:51:03
        int SECOND = 1000;
        int MINUTE = 60 * SECOND;
        int HOUR = 60 * MINUTE;
        int DAY = 24 * HOUR;

        SimpleDateFormat formatter = new SimpleDateFormat("yy-mm-dd hh:mm:ss");
        try {
            Date dateUpdatedAt=formatter.parse(updatedAt);
            Long diffMillis=System.currentTimeMillis()-dateUpdatedAt.getTime();

            StringBuffer text = new StringBuffer("");
            if (diffMillis > DAY) {
                text.append(diffMillis / DAY).append(" days ");
//                diffMillis %= DAY;
            }else {
                if (diffMillis > HOUR) {
                    text.append(diffMillis / HOUR).append(" hours ");
                    diffMillis %= HOUR;
                }
                if (diffMillis > MINUTE) {
                    text.append(diffMillis / MINUTE).append(" min ");
                    diffMillis %= MINUTE;
                }

                if (diffMillis > SECOND)
                    text.append(diffMillis / SECOND).append(" sec ");
            }
            return text.toString();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return null;
    }
}
