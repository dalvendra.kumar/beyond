package com.beyond.ve.beyondapp.service;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.util.Log;

import com.beyond.ve.beyondapp.R;
import com.beyond.ve.beyondapp.Web.ApiClient;
import com.beyond.ve.beyondapp.Web.WebService;
import com.beyond.ve.beyondapp.data.AppPreferencesHelper;
import com.beyond.ve.beyondapp.ui.ActivitySplash;
import com.beyond.ve.beyondapp.ui.ChatsActivity;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.JsonElement;

import java.util.HashMap;
import java.util.Random;

import androidx.core.app.NotificationCompat;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyFirebaseMessagingService extends FirebaseMessagingService {
    private static final String TAG = MyFirebaseMessagingService.class.getSimpleName();
    public static Boolean isChatShowNotification=true;
    private static final String NOTIFICATION_CHANNEL_ID = "notificaton_channel_beyond";
    private static final String NOTIFICATION_CHANNEL_NAME = "Notification_channel_beyond_chat";
    private static final int NOTIFICATION_ID = 123456;
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        String title="",message="";
        if(remoteMessage.getNotification()!=null){
            title=remoteMessage.getNotification().getTitle();
            message=remoteMessage.getNotification().getBody();
        }
        Log.d(TAG,"Title : "+title+",Msg : "+message);
        if(isChatShowNotification){
            genrateNotification(this,title,message);
        }

    }

    @Override
    public void onNewToken(String deviceToken) {
        super.onNewToken(deviceToken);
        Log.d(TAG,"onNewToken : "+deviceToken);
        new AppPreferencesHelper(this).setDeviceToken(deviceToken);
        if(new AppPreferencesHelper(this).getUser_XAUTH().length()>5)
            pushToServer(deviceToken);
    }

    private void pushToServer(String deviceToken) {
        HashMap<String,String> body=new HashMap<>();
        body.put("device_token",deviceToken);
        WebService service= ApiClient.getClient().create(WebService.class);
        Call<JsonElement> call= service.updateProfile("Bearer " + new AppPreferencesHelper(this).getUser_XAUTH(),body);
        call.enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                if(response.isSuccessful())
                    Log.d("FirebaseMsgService","device token updated");
                else
                    Log.d("FirebaseMsgService","device token updation Failure");

            }
            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    public static boolean genrateNotification(Context context,String title,String message){
        try{
            Intent notificationIntent = new Intent(context, ChatsActivity.class);
            PendingIntent pendingIntent=PendingIntent.getActivity(context,0,notificationIntent,0);
            NotificationCompat.Builder builder = new NotificationCompat.Builder(context,NOTIFICATION_CHANNEL_ID)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setContentTitle(title)
                    .setContentText(message)
                    .setTicker("TICKER")
                    .setContentIntent(pendingIntent);
            Notification notification=builder.build();

            NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.O) {
                NotificationChannel channel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, NOTIFICATION_CHANNEL_NAME, NotificationManager.IMPORTANCE_DEFAULT);
                channel.setDescription(message);
                notificationManager.createNotificationChannel(channel);
            }
            notificationManager.notify(new Random().nextInt(),notification);

        }catch (Exception e){
            e.printStackTrace();
            return false;
        }
        return true;
    }

}
