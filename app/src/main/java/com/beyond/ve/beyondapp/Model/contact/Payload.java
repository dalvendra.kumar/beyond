
package com.beyond.ve.beyondapp.Model.contact;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Payload {

    @SerializedName("first_name")
    @Expose
    private String first_name;

    @SerializedName("last_name")
    @Expose
    private String last_name;

    @SerializedName("email")
    @Expose
    private String email;

    @SerializedName("avatar")
    @Expose
    private String avatar;

    @SerializedName("is_verified")
    @Expose
    private Integer is_verified;

    @SerializedName("isActive")
    @Expose
    private Integer isActive;

    @SerializedName("id")
    @Expose
    private Integer id;

    @SerializedName("company_name")
    @Expose
    private String company_name;

    @SerializedName("company_logo")
    @Expose
    private String company_logo;

    @SerializedName("chat_id")
    @Expose
    private String chat_id;


    @SerializedName("isConnected")
    @Expose
    private String isConnected;

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public Integer getIs_verified() {
        return is_verified;
    }

    public void setIs_verified(Integer is_verified) {
        this.is_verified = is_verified;
    }

    public Integer getIsActive() {
        return isActive;
    }

    public void setIsActive(Integer isActive) {
        this.isActive = isActive;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCompany_name() {
        return company_name;
    }

    public void setCompany_name(String company_name) {
        this.company_name = company_name;
    }

    public String getCompany_logo() {
        return company_logo;
    }

    public void setCompany_logo(String company_logo) {
        this.company_logo = company_logo;
    }

    public String getChat_id() {
        return chat_id;
    }

    public void setChat_id(String chat_id) {
        this.chat_id = chat_id;
    }

    public String getIsConnected() {
        return isConnected;
    }

    public void setIsConnected(String isConnected) {
        this.isConnected = isConnected;
    }

    @Override
    public String toString() {
        return "Payload{" +
                "first_name='" + first_name + '\'' +
                ", last_name='" + last_name + '\'' +
                ", email='" + email + '\'' +
                ", avatar='" + avatar + '\'' +
                ", is_verified=" + is_verified +
                ", isActive=" + isActive +
                ", id=" + id +
                ", company_name='" + company_name + '\'' +
                ", company_logo='" + company_logo + '\'' +
                ", chat_id='" + chat_id + '\'' +
                ", isConnected='" + isConnected + '\'' +
                '}';
    }
}
