package com.beyond.ve.beyondapp.Web;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class ApiClient {

    //Testing server
    public static final String BASE_URL = "http://52.56.215.230/beyond-app/api/v1/";


    private static Retrofit retrofit = null;

    public static final String BASE_URL_LINKEDIN = "https://api.linkedin.com/v2/";


    private static Retrofit retrofitLinkedin = null;
    static Gson gson = new GsonBuilder()
            .setLenient()
            .create();


    final static   OkHttpClient okHttpClient = new OkHttpClient().newBuilder()


            .connectTimeout(30, TimeUnit.SECONDS)
            .readTimeout(30, TimeUnit.SECONDS)
            .writeTimeout(30, TimeUnit.SECONDS).addInterceptor(new Interceptor() {
                @Override
                public Response intercept(Chain chain) throws IOException {
                    Request.Builder requestBuilder = chain.request().newBuilder();
                    requestBuilder.header("Content-Type", "application/json");
                    return chain.proceed(requestBuilder.build());
                }
            })
            .build();



    public static Retrofit getClient() {

//        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
//        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        if (retrofit==null) {
//            okHttpClient.interceptors().add(interceptor);
            okHttpClient.dispatcher().setMaxRequestsPerHost(20);
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL).client(okHttpClient)
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build();
        }
        return retrofit;
    }

    public static Retrofit getClientLinkedin() {

//        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
//        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        if (retrofitLinkedin==null) {
//            okHttpClient.interceptors().add(interceptor);
            okHttpClient.dispatcher().setMaxRequestsPerHost(20);
            retrofitLinkedin = new Retrofit.Builder()
                    .baseUrl(BASE_URL_LINKEDIN).client(okHttpClient)
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build();
        }
        return retrofitLinkedin;
    }



}